﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;

namespace TravelBot.Common.Repository.Contracts
{
    public interface IHotelPriceSearchQueueRepository
    {
        HotelPriceSearchQueue Add(HotelPriceSearchQueue queueReq);
        HotelPriceSearchQueue Find(Expression<Func<HotelPriceSearchQueue, bool>> func);
        bool IsRemainingResponsesExists(long responseId, long dataSourceId);
        HotelPriceSearchQueue Update(HotelPriceSearchQueue queueReq);
    }
}
