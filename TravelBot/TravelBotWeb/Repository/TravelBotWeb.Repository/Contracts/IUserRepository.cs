﻿using System;
using System.Collections.Generic;
using TravelBotWeb.Common;
using static TravelBot.Common.Enums;

namespace TravelBotWeb.Repository.Contracts
{
    public interface IUserRepository
    {
        UserSessionModel FindRequestFromId(string UserId);
        void AddUserRequest(UserSessionModel UserSession);
        UserSessionModel FindRequestFromStatus(RequestStatus Status);
        void Update(UserSessionModel UserSession);
        UserSessionModel FindRequestConsideringEmailSending(EmailStatus EmailStatus);
        List<UserSessionModel> FindAllrequestsfromStatus(EmailStatus emailStatus, int maxRepetitions, int errorCheckinghours);
        List<UserSessionModel> FindAllrequestsUsingCriteria(EmailStatus emailStatus, DateTime date, bool _stopEmails);
        UserSessionModel FindRequestFromSearchingObjectId(string SearchingObjectId);
        List<UserSessionModel> FindNonClosedRequests(RequestStatus status);
        List<UserSessionModel> FindExpiredRequests();
    }
}