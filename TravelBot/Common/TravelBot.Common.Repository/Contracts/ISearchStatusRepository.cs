﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;

namespace TravelBot.Common.Repository.Contracts
{
    public interface ISearchStatusRepository
    {
        List<SearchStatus> AddAll(List<SearchStatus> p);
        SearchStatus Update(SearchStatus p);
        SearchStatus GetSearchStatus(long searchResId, long sourceId);
        bool IsSearchCompleted(long searchResponseId);
    }
}
