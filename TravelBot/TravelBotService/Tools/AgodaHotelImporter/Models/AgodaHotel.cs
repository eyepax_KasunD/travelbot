﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgodaHotelImporter.Models
{
    public class AgodaHotel
    {
        public string hotel_id  { get; set; }

        public string hotel_name  { get; set; }

        public string addressline1 { get; set; }

        public double star_rating { get; set; }

        public decimal longitude { get; set; }
        public decimal latitude { get; set; }
        public List<string> Images { get; set; }
        public int number_of_reviews { get; set; }
        public double rating_average { get; set; }

    }
}
