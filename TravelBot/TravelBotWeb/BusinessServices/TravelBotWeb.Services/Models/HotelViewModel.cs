﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelBotWeb.Services.Models
{
    public class CityHotelData
    {
        public string City { get; set; }
        public int NoOfStays { get; set; }
        public string CurrencyPrefix { get; set; }
        public List<HotelViewModel> Hotels { get; set; }
    }
    public class HotelViewModel
    {
        public string BookingUrl { get; set; }
        public string HotelImgUrl { get; set; }
        public string HotelName { get; set; }
        public string Address { get; set; }
        public decimal Price { get; set; }
        public Review TravelbotReview { get; set; }
        public Review SourceReview { get; set; }
        public string HotelRatingImgUrl { get; set; }
    }
}