﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Models;

namespace TravelbotService.BusinessServices.Contracts
{
    public interface IHotelPriceRetrieveService
    {
        HotelPriceSearchRsp GetPrices(HotelPriceSearchReq request);
    }
}
