﻿using TravelBot.Common.Models;
using Scraper.WebAPI.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Unity.Attributes;
using Newtonsoft.Json;
using System.Messaging;
using System;
using Scraper.Services.Contracts;
using Scraper.Common.Models.Responses;
using Scraper.Common.Models.Requests;
using Scraper.Common.Models.Queue;

namespace Scraper.WebAPI.Controllers
{
    public class ScraperController : BaseApiController
    {
        private readonly IScraperService _scraperService;

        public ScraperController([Dependency("QueueService")]IScraperService scraperService) //give ScraperQueueService object
        {
            _scraperService = scraperService;    
        }
        [HttpPost]
        public ApiResponseModel<HotelPriceSearchRsp> Start([FromBody]ScraperHotelPriceSearchReq requestModel)
        {
            logger.Info("Scraping request recieved");
            var responses = _scraperService.SearchHotelPrices(requestModel);
            return GetApiResponseModel(responses);
        }

        public ApiResponseModel<List<HotelPriceResponse>> FindPricesWithFlexibility([FromBody]HotelPriceRequest requestModel) // when user enters how many days he stay, checkin, checkout days, this will return 
        {
            logger.Info("Price query with flexible dates, request recieved");
            var validationMessages = Validator.ValidateRequest(requestModel);
            if (validationMessages.Any())
            {
                return GetApiResponseModel<List<HotelPriceResponse>>(null, validationMessages);
            }
            else
            {
                var responses = _scraperService.GetPriceWithFlexibility(requestModel);
                return GetApiResponseModel<List<HotelPriceResponse>>(responses);
            }

        }

        [HttpPost]
        public ApiResponseModel<ScraperHotelDetailRsp> HolteFeatures([FromBody]ScraperHotelDetailReq requestModel)
        {
            logger.Info("Hotel feature request recieved");
            var validationMessages = Validator.ValidateFeatureRequest(requestModel);
            if (validationMessages.Any())
            {
                return GetApiResponseModel<ScraperHotelDetailRsp>(null, validationMessages);
            }
            else
            {
                var responses = _scraperService.ScrapeHotelInfo(requestModel);
                return GetApiResponseModel<ScraperHotelDetailRsp>(responses);

            }
        }

    }
}
