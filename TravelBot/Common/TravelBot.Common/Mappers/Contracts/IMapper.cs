﻿namespace TravelBot.Common.Mappers.Contracts
{
    public interface IMapper<TSource, TDestination>
    {
        TDestination Map(TSource source_object);
    }
}
