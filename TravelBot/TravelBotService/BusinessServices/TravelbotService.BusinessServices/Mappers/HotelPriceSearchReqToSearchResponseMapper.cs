﻿using System;
using System.Collections.Generic;
using System.Linq;
using TravelBot.Common.Domain;
using TravelBot.Common.Mappers.Contracts;
using TravelBot.Common.Models;
using TravelBot.Common.Repository.Contracts;
using TravelBotService.Common;
using static TravelBot.Common.Enums;

namespace TravelbotService.BusinessServices.Mappers
{
    public class HotelPriceSearchReqToSearchResponseMapper : IMapper<HotelPriceSearchReq, SearchResponse>
    {
        private readonly ICurrencyRepository _currencyRepository;
        public HotelPriceSearchReqToSearchResponseMapper(ICurrencyRepository currencyRepository)
        {
            _currencyRepository = currencyRepository;
        }
        public SearchResponse Map(HotelPriceSearchReq request)
        {
            var currency = _currencyRepository.GetCurrencyByCode(request.Currency);
            return new SearchResponse
            {
                RequestId = request.RequestId,
                UserSessionId = request.UserSessionId,
                ResponseStatus = ResponseStatus.Pending.ToString(),
                CurrencyId = currency?.Id,
                SearchCriteria = GetSearchCriteria(request)
            };
        }

        private List<SearchCriteria> GetSearchCriteria(HotelPriceSearchReq request)
        {
            return request.SearchCriteria.Select(s => 
            new SearchCriteria
            {
                CheckIn = s.CheckinDate,
                CheckOut = s.CheckoutDate,
                CityId = s.CityId,
                CountryId = s.CountryId,
                PriceFrom = s.PriceFrom,
                PriceTo = s.PriceTo,
                searchCriteriaCombinations = GetSearchCriteriaCombinations(s, request.FlexDatesPlus, request.FlexDatesMinus)
            }).ToList();
        }

        private List<SearchCriteriaCombination> GetSearchCriteriaCombinations(SearchReqCriterion criteria, int flexDatesPlus, int flexDatesMinus)
        {

            List<DateTime> checkingdates = new List<DateTime>();
            for (var i = -Math.Abs(flexDatesMinus); i <= flexDatesPlus; i++)
            {
                checkingdates.Add(criteria.CheckinDate.AddDays(i));
            }
            return checkingdates.Select(d =>
            new SearchCriteriaCombination
            {
                CheckIn = d,
                CheckOut = d.AddDays(criteria.NoOfStays),
            }).ToList();
        }
    }
}
