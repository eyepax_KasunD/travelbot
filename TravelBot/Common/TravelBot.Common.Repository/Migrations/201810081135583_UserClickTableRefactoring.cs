namespace TravelBot.Common.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserClickTableRefactoring : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Trn_UserClick", new[] { "UserSession_Id" });
            DropColumn("dbo.Trn_UserClick", "UserSessionId");
            RenameColumn(table: "dbo.Trn_UserClick", name: "UserSession_Id", newName: "UserSessionId");
            AlterColumn("dbo.Trn_UserClick", "UserSessionId", c => c.String(maxLength: 40));
            CreateIndex("dbo.Trn_UserClick", "UserSessionId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Trn_UserClick", new[] { "UserSessionId" });
            AlterColumn("dbo.Trn_UserClick", "UserSessionId", c => c.Long());
            RenameColumn(table: "dbo.Trn_UserClick", name: "UserSessionId", newName: "UserSession_Id");
            AddColumn("dbo.Trn_UserClick", "UserSessionId", c => c.Long());
            CreateIndex("dbo.Trn_UserClick", "UserSession_Id");
        }
    }
}
