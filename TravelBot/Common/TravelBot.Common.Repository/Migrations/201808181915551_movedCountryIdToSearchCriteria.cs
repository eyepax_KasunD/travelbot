namespace TravelBot.Common.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class movedCountryIdToSearchCriteria : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Trn_UserSession", "CountryId", "dbo.Sys_Country");
            DropIndex("dbo.Trn_UserSession", new[] { "CountryId" });
            AddColumn("dbo.Trn_SearchCriteria", "CountryId", c => c.Long());
            CreateIndex("dbo.Trn_SearchCriteria", "CountryId");
            AddForeignKey("dbo.Trn_SearchCriteria", "CountryId", "dbo.Sys_Country", "Id");
            DropColumn("dbo.Trn_UserSession", "CountryId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Trn_UserSession", "CountryId", c => c.Long());
            DropForeignKey("dbo.Trn_SearchCriteria", "CountryId", "dbo.Sys_Country");
            DropIndex("dbo.Trn_SearchCriteria", new[] { "CountryId" });
            DropColumn("dbo.Trn_SearchCriteria", "CountryId");
            CreateIndex("dbo.Trn_UserSession", "CountryId");
            AddForeignKey("dbo.Trn_UserSession", "CountryId", "dbo.Sys_Country", "Id");
        }
    }
}
