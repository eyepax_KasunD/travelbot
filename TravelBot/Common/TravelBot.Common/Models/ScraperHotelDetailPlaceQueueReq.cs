﻿using TravelBot.Common.Helpers;
using static TravelBot.Common.Enums;

namespace TravelBot.Common.Models
{
    public class ScraperHotelDetailPlaceQueueReq
    {
        public long QueueRequestId { get; set; }
        public ScraperHotelDetailReq ScraperHotelDetailReq { get; set; }
    }
}