﻿using Scraper.WebAPI.Scheduling;
using System.Threading;
using System.Threading.Tasks;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(Scraper.WebAPI.App_Start.StartQueue), nameof(Scraper.WebAPI.App_Start.StartQueue.Start))]

namespace Scraper.WebAPI.App_Start
{
    public static class StartQueue
    {
        public static void Start()
        {
            //Task.Run(() => ProcessSearchRequestsFromQueue.Start());

            new Thread(ProcessSearchRequestsFromQueue.Start) { IsBackground = true }.Start();
            new Thread(ProcessFeatureRequestsFromQueue.Start) { IsBackground = true }.Start();
        }
    }
}