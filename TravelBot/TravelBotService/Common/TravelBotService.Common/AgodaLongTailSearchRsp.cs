﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelBotService.Common
{
    public class AgodaLongTailSearchRsp
    {
        public List<Results> results{ get; set;}
        public AgodaLongTailSearchRsp()
        {
            results = new List<Results>();
        }
    }
    public class Results
    {
        public int hotelId { get; set; }
        public string hotelName { get; set; }
        public decimal starRating { get; set; }
        public decimal reviewScore { get; set; }
        public int reviewCount { get; set; }
        public string currency { get; set; }
        public decimal dailyRate { get; set; }
        public decimal crossedOutRate { get; set; }
        public decimal discountPercentage { get; set; }
        public string imageURL { get; set; }
        public string landingURL { get; set; }
        public bool includeBreakfast { get; set; }
        public bool freeWifi { get; set; }
    }
}
