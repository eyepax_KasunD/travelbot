﻿using AgodaHotelImporter.Models;
using CsvHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgodaHotelImporter
{
    public class AgodaHotelImporter
    {
        public List<AgodaHotel> Import(string filePath)
        {
            var result = new List<AgodaHotel>();

            using (TextReader fileReader = File.OpenText(filePath))
            {
                var csv = new CsvReader(fileReader);
                csv.Configuration.HasHeaderRecord = true;
                csv.Configuration.MissingFieldFound = null;
                csv.Configuration.HeaderValidated = null;
                csv.Configuration.RegisterClassMap<AgodaHotelImportMapper>();
                while (csv.Read())
                {
                    result.Add(csv.GetRecord<AgodaHotel>());
                }
            }
            return result;
        }
    }
}
