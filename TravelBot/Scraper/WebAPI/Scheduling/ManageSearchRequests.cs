﻿using NLog;
using Quartz;
using Scraper.Repository.Contracts;
using System;
using System.Threading.Tasks;
using TravelBot.Common;

namespace Scraper.WebAPI.Scheduling
{
    public class ManageSearchRequests : IJob
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private readonly IQueueRepository _queueRepository;

        public ManageSearchRequests(IQueueRepository queueRepository)
        {
            _queueRepository = queueRepository;
        }
        public async Task Execute(IJobExecutionContext context)
        {
            //logger.Info("error handling of proccessing search requests started");
            //var SearchFeatureRequests = _queueRepository.FindAllSearchRequestsfromStatus(Enums.ResponseStatus.Error, Constants.ErrorHandlingVariables.MaxRepetitions, Constants.ErrorHandlingVariables.ErrorCheckinghours);

            //try
            //{
            //    foreach (var SearchFeatureRequest in SearchFeatureRequests)
            //    {
            //        SearchFeatureRequest.SearchResponse.Status = Enums.ResponseStatus.Pending;
            //        SearchFeatureRequest.ErrorRepetitionCount = SearchFeatureRequest.ErrorRepetitionCount + 1;
            //        _queueRepository.Update(SearchFeatureRequest);
            //        logger.Info("Search Request is redirected to the pending state");

            //        if (SearchFeatureRequest.ErrorRepetitionCount > Constants.ErrorHandlingVariables.MaxRepetitions)
            //        {
            //            //TODO : handle error notification
            //            logger.Info("Repetition count is more than max repetitions");
            //        }
            //    }
            //}
            //catch (Exception e)
            //{
            //    logger.Info("Error occured when handling search request errors :  " + e);
            //}
        }
}
}