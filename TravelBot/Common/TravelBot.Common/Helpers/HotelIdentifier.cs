﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
namespace TravelBot.Common.Helpers
{
    public static class HotelIdentifier
    {

        private static readonly Dictionary<string,Tuple<string, int, int>> patterns = new Dictionary<string, Tuple<string, int, int>>()
        {
            { "expedia", new Tuple<string, int, int>(@"\/(\w|-|\.)+\?",1,1) },
            { "booking", new Tuple<string, int, int>(@"\/(\w|-|\.)+\.html\?",1,6) }
        };

        public static string GetIdentifierFromUrl(string url)
        {
            foreach(var item in patterns)
            {
                if (url.Contains(item.Key))
                {
                    MatchCollection matchList = Regex.Matches(url, item.Value.Item1);
                    var uniqeText = matchList.Cast<Match>().First().Value.Substring(item.Value.Item2);
                    uniqeText = uniqeText.Substring(0, uniqeText.Length - item.Value.Item3);
                    return uniqeText;
                }
            }
            return null;
        }
    }
}