﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelBot.Common
{
    public static class Constants
    {
        public static class ScrapingSources
        {
            public static string booking = "bookingcom";
            public static string expedia = "expedia";
        }
        public static class BookingHotelGuestReviews
        {
            public static string clean = "hotel_clean";
            public static string staff = "hotel_staff";
            public static string comfort = "hotel_comfort";
            public static string service = "hotel_services";
        }
        public static class ExpediaHotelGuestReviews
        {
            public static string clean = "Room cleanliness";
            public static string staff = "Service & staff";
            public static string comfort = "Room comfort";
            public static string service = "Hotel condition";
        }
        public static class ValidationStatements
        {           
            public static string DateNotOk = "Date is Invalid";          
            public static string DaysNotOk = "No of Days Not Valid";          
            public static string flexNotok = "Invalid Flex No";
            public static string urlNotok = "Invalid Url";
        }
        public static class ErrorHandlingVariables
        {
            public static int MaxRepetitions = 3;
            public static int ErrorCheckinghours = 0;
        }
        public static class Queues
        {
            public static string ScraperHotelPriceSearchReqQueue = ".\\private$\\ScraperHotelPriceSearchReqQueue";
            public static string ScraperHotelDetailReqQueue = ".\\private$\\ScraperHotelDetailReqQueue"; 
            public static string ScraperHotelPriceSearchRspQueue = ".\\private$\\ScraperHotelPriceSearchRspQueue"; 
            public static string ScraperHotelDetailRspQueue = ".\\private$\\ScraperHotelDetailRspQueue"; 
            public static string UserPreferencesQueue = ".\\private$\\UserPreferencesQueue";
            public static string HotelPriceSearchReqQueue = ".\\private$\\HotelPriceSearchReqQueue";
            public static string HotelPriceSearchRspQueue = ".\\private$\\HotelPriceSearchRspQueue";
        }
    }
}