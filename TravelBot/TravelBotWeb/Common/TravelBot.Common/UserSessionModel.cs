﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TravelBot.Common.Models;
using static TravelBot.Common.Enums;

namespace TravelBotWeb.Common
{
    public class UserSessionModel
    {
        public UserSessionModel()
        {
            SearchObjects = new List<ContinuousSearch>();           
        }
        public string UserId { get; set; }
        public List<SearchReqCriterion> SearchSegmentList { get; set; }
        public DateTime CheckInDate { get; set; }
        public int AdultCount { get; set; }
        public int ChildCount { get; set; }
        public decimal PriceFrom { get; set; }
        public decimal PriceTo { get; set; }
        public string Email { get; set; }
        public DateTime RequestTime { get; set; }
     // public RequestStatus RequestStatus { get; set; }
        public EmailStatus EmailStatus { get; set; }
        public int FlexDatesPlus { get; set; }
        public int FlexDatesMinus { get; set; }
        public DateTime LoggedErrorTime { get; set; }
        public int RepetitionCount { get; set; }
        public DateTime EmailScheduleTime { get; set; }
        public bool StopEmails { get; set; }
        public string ReasonforStoppingEmails { get; set; }
        public List<ContinuousSearch> SearchObjects { get; set; }
        public string DeviceId { get; set; }

    }
    public class ContinuousSearch
    {
        public DateTime RequestDate { get; set; }
        public string RequestId { get; set; }
        public HotelPriceSearchRsp HotelSearchResponse { get; set; }
        public RequestStatus RequestStatus { get; set; }
    }
}