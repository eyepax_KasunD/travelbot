﻿using System.Collections.Generic;
using System.Linq;
using TravelBot.Common.Domain;
using TravelBot.Common.Helpers.Contracts;
using TravelBot.Common.Repository.Contracts;

namespace TravelBot.Common.Repository
{
    public class CachedCountryRepository : CountryRepository, ICountryRepository
    {
        private readonly ICaching _cache;
        const string prefix = "TravelBot.Common.Repository.AllCountries";
        const int ttl = 60 * 60 * 24;
        public CachedCountryRepository(ITravelbotDbContext context, ICaching cache) : base(context)
        {
            _cache = cache;
        }
       
        public List<Country> GetAllEnabledCountries()
        {
            List<Country> countryList = _cache.Get<List<Country>>(prefix);
            if(countryList == null)
            {
                countryList = base.GetAllEnabledCountries();
                _cache.Set(prefix, countryList, ttl);
            }

            return countryList;
        }

        public Country Add(Country p)
        {
            return base.Add(p);
        }
    }
}
