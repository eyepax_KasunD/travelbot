namespace TravelBot.Common.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class movedCurrencyToSearchResponse : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Trn_UserSession", "CurrencyId", "dbo.Sys_Currency");
            DropIndex("dbo.Trn_UserSession", new[] { "CurrencyId" });
            AddColumn("dbo.Trn_SearchResponse", "CurrencyId", c => c.Long());
            CreateIndex("dbo.Trn_SearchResponse", "CurrencyId");
            AddForeignKey("dbo.Trn_SearchResponse", "CurrencyId", "dbo.Sys_Currency", "Id");
            DropColumn("dbo.Trn_UserSession", "CurrencyId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Trn_UserSession", "CurrencyId", c => c.Long());
            DropForeignKey("dbo.Trn_SearchResponse", "CurrencyId", "dbo.Sys_Currency");
            DropIndex("dbo.Trn_SearchResponse", new[] { "CurrencyId" });
            DropColumn("dbo.Trn_SearchResponse", "CurrencyId");
            CreateIndex("dbo.Trn_UserSession", "CurrencyId");
            AddForeignKey("dbo.Trn_UserSession", "CurrencyId", "dbo.Sys_Currency", "Id");
        }
    }
}
