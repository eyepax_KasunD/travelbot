﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static TravelBot.Common.Enums;

namespace TravelBot.Common.Models
{
    public class DataSource
    {
        public DataSourceCode Code { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }
    }
}
