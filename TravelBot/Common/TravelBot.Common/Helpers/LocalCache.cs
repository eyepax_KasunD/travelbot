﻿using TravelBot.Common.Helpers.Contracts;
using System;

namespace TravelBot.Common.Helpers
{
    public class LocalCache : ICaching
    {
        public object Get(string key)
        {
            return System.Web.HttpRuntime.Cache.Get(key);
        }

        public T Get<T>(string key) where T : class
        {
            return GetFromLocal<T>(key);
        }

        private void Remove(string key)
        {
            System.Web.HttpRuntime.Cache.Remove(key);
        }


        public void Set(string key, object value, int ttl = 900, bool useCamelCaseResolver = true)
        {
            SetInLocal(key, value, ttl);
        }

        private void SetInLocal(string key, object value, int localCacheTTL = 900)
        {
            System.Web.HttpRuntime.Cache.Insert(key, value, null, DateTime.Now.AddSeconds(localCacheTTL), System.Web.Caching.Cache.NoSlidingExpiration);
        }

        private T GetFromLocal<T>(string key) where T : class
        {
            return (T)System.Web.HttpRuntime.Cache.Get(key);
        }

        public void Invalidate(string key)
        {
            System.Web.HttpRuntime.Cache.Remove(key);
        }
    }
}
