﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;
using TravelBot.Common.Repository.Contracts;
using static TravelBot.Common.Enums;

namespace TravelBot.Common.Repository
{
    public class SearchStatusRepository : Repository<SearchStatus>, ISearchStatusRepository
    {
        private readonly ITravelbotDbContext _context;
        public SearchStatusRepository(ITravelbotDbContext context) : base(context)
        {
            _context = context;
        }

        public bool IsSearchCompleted(long searchResponseId)
        {
            return base.Find(s => s.SearchResponseId == searchResponseId && s.Status == (int)ResponseStatus.Pending).Count() == 0 ;
        }

        public SearchStatus GetSearchStatus(long searchResId, long sourceId)
        {
            return base.Find(s => s.SearchResponseId == searchResId && s.DataSourceId == sourceId).FirstOrDefault();
        }
    }
}
