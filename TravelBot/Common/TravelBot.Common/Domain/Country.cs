﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TravelBot.Common.Domain
{

    [Table("Sys_Country")]
    public class Country: BaseEntity
    {
        public int SortOrder { get; set; }
        public virtual ICollection<CountryImage> CountryImages { get; set; }
        public virtual ICollection<City> Cities { get; set; }
        public virtual ICollection<CountryContent> CountryContents { get; set; }
        public virtual ICollection<SourceCountryMap> SourceMaps { get; set; }
        public bool IsEnabled { get; set; }
        public virtual Image Thumbnail { get; set; } //thumbnail
        public long? ThumbnailId { get; set; }
    }

    [Table("Sys_CountryContent")]
    public class CountryContent : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual Language Language { get; set; }
        public virtual Country Country { get; set; }
    }

    [Table("Sys_CountryImage")]
    public class CountryImage : BaseEntity
    {
        public virtual Country Country { get; set; }
        public virtual Image Image { get; set; }
    }


    [Table("Set_SourceCountryMap")]
    public class SourceCountryMap
    {
        public virtual Country Country { get; set; }
        [Key, Column(Order = 0)]
        public long? CountryId { get; set; }
        public virtual DataSource Source { get; set; }
        [Key, Column(Order = 1)]
        public long? DataSourceId { get; set; }
        public string Value { get; set; }
    }
}
