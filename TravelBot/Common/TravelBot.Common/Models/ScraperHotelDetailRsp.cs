﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Models;

namespace TravelBot.Common.Models
{
    public class ScraperHotelDetailRsp
    {
        public Hotel Hotel { get; set; }
        public bool CachedResponse { get; set; }
        public List<string> Errors { get; set; }
        public List<string> Warnings { get; set; }

        public ScraperHotelDetailRsp()
        {
            Errors = new List<string>();
            Warnings = new List<string>();
        }
    }
}
