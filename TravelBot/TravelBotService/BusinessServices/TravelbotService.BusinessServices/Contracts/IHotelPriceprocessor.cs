﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Models;
using TravelBot.Common.Models.Queue;

namespace TravelbotService.BusinessServices.Contracts
{
    public interface IHotelPriceProcessor
    {
        void ProcessHotelPrices(long requestId);
    }
}
