﻿using NLog;
using Scraper.Common.Models.Queue;
using Scraper.Repository.Contracts;
using Scraper.Services.Contracts;
using System;
using System.Linq;
using System.Messaging;
using System.Threading;
using System.Threading.Tasks;
using TravelBot.Common;
using TravelBot.Common.Helpers;
using TravelBot.Common.Models.Queue;
using enums = TravelBot.Common.Enums;

namespace Scraper.WebAPI.Scheduling
{
    public class ProcessSearchRequestsFromQueue
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static readonly IQueueRepository _queueRepository = UnityConfig.Resolve<IQueueRepository>();
        private static readonly IScraperService _scraperService = UnityConfig.Resolve<IScraperService>();

        public static void Start()
        {
            var _queue = new QueueProvider();
            logger.Info("STARTING SUBSCRIBER FOR SEARCH REQUEST");
            using (var Queue = new MessageQueue(Constants.Queues.ScraperHotelPriceSearchReqQueue))
            {
                Queue.Formatter = new XmlMessageFormatter(new Type[] { typeof(SearchRequestQueueElement) });
                while (true)
                {
                    logger.Info("INSIDE WHILE LOOP SEARCH REQUEST");
                    var message = Queue.Receive();
                    if (message == null)
                        continue;
                    logger.Info("SEARCH REQUEST FOUND !!!");
                    SearchRequestQueueElement element = new SearchRequestQueueElement();
                    element = (SearchRequestQueueElement)message.Body;
                    var response = new HotelPriceSearchRspQueueElement { QueueRequest = element.SearchRequest, QueueRequestId = element.SearchRequest.RequestId };

                    try
                    {
                        var res = _scraperService.SearchHotelPrices(element.SearchRequest.ScraperHotelPriceSearchReq);

                        if (res.Errors.Count == 0)
                        {
                            response.ResponseStatus = enums.ResponseStatus.Completed;
                            response.SearchResponse = res;
                        }
                        else
                        {
                            throw new Exception(string.Join(Environment.NewLine, res.Errors));
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error("Ex : " + ex);
                        response.ResponseStatus = enums.ResponseStatus.Error;
                    }

                    _queue.PublishInQueue(response, Constants.Queues.ScraperHotelPriceSearchRspQueue); //save search response to queue
                }
            }
        }
    }
}