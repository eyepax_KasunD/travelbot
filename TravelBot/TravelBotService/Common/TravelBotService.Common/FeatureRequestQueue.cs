﻿using System;
using TravelBot.Common.Models;
using static TravelBot.Common.Enums;

namespace TravelBotService.Common
{
    public class FeatureRequestQueue
    {
        //[BsonId]
        //[BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public ScraperHotelDetailReq Request { get; set; }
        public RequestStatus Status { get; set; }
        public string HotelId { get; set; }
        public DateTime LastUpdatedDateTime { get; set; }
        public string RequestId { get; set; }
        public int RepetitionCount { get; set; }
    }
}
