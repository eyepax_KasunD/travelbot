﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;
using TravelBot.Common.Helpers.Contracts;
using TravelBot.Common.Repository.Contracts;

namespace TravelBot.Common.Repository
{
    public class CachedDataSourceRepository : DataSourceRepository, IDataSourceRepository
    {
        private readonly ICaching _cache;
        const string prefix = "TravelBot.Common.Repository.HotelSourceMap";
        const int ttl = 60 * 60 * 24;
        public CachedDataSourceRepository(ITravelbotDbContext context, ICaching cache) : base(context)
        {
            _cache = cache;
        }
        //public HotelSourceMap Find(string id)
        //{
        //    HotelSourceMap hotelSourceMap = _cache.Get<HotelSourceMap>(prefix);
        //    if (hotelSourceMap == null)
        //    {
        //        hotelSourceMap = base.Find(id);
        //        _cache.Set(prefix, hotelSourceMap, ttl);
        //    }
        //    return hotelSourceMap;
        //}
    }
}
