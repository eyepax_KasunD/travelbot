﻿using NLog;
using Quartz;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TravelBotWeb.Common;
using static TravelBot.Common.Enums;
using TravelBotWeb.Services.Contracts;
using TravelBotWeb.Repository.Contracts;
using TravelBotWeb.Common.CommonWeb;

namespace TravelBotWeb.Scheduling
{
    public class ManageUserEmails : IJob
    {
        private readonly IUserRepository _userRepository;
        private readonly IEmailService _emailService;
        private readonly IAppSettings _appsettings;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public ManageUserEmails(IUserRepository userRepository, IAppSettings appsettings)
        {
            _userRepository = userRepository;
            _emailService = UnityConfig.Resolve<IEmailService>();
            _appsettings = appsettings;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            logger.Info("error handling of email sending execution started");
            var UserSessions = _userRepository.FindAllrequestsfromStatus(EmailStatus.Error,Constants.MaxRepetitions, Constants.ErrorCheckinghours);

            try
            {
                foreach (var UserSession in UserSessions)
                {
                    UserSession.EmailStatus = EmailStatus.Pending;
                    UserSession.RepetitionCount = UserSession.RepetitionCount + 1;
                    _userRepository.Update(UserSession);
                    logger.Info("Session is redirected to the pending state");

                    if (UserSession.RepetitionCount > Constants.MaxRepetitions)
                    {
                        //TODO : handle error notification
                        logger.Info("Repetition count is more than max repetitions");
                        try
                        {
                            _emailService.SendEmailDirectly(GetEmailRequest("Error in session ID : "+ UserSession.UserId));
                        }catch(Exception ex)
                        {
                            logger.Info("Repetition count is more than max repetitions, error : "+ex);
                        }

                    }
                }
            }
            catch (Exception e)
            {
                logger.Info("Error occured when handling email errors :  " + e);
            }
        }

        private EmailRequestModel GetEmailRequest(string message)
        {
            return new EmailRequestModel
            {
                To = _appsettings.Admin_Emails,
                Cc = new List<string>(),
                Bcc = new List<string>(),
                Body = message,
                Subject = "Travelbot : Error in user session!"
            };
        }
    }
}