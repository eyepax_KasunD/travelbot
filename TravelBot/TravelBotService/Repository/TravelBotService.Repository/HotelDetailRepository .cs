﻿using Newtonsoft.Json;
using RestSharp;
using System.Web.Script.Serialization;
using TravelBot.Common.Models;
using TravelBotService.Common;
using TravelBotService.Repository.Contracts;

namespace TravelBotService.Repository
{
    public class HotelDetailRepository: IHotelDetailRepository
    {
        private readonly IAppSettings _appSettings;
        public HotelDetailRepository(IAppSettings appSettings)
        {
            _appSettings = appSettings;
        }
        public bool GetHotelDetailFromScraper(ScraperHotelDetailPlaceQueueReq req)
        {
            var client = new RestClient(_appSettings.Scraper_Domain);
            var request = new RestRequest("api/Queue/PlaceHotelDetailReq", Method.POST);
            var json = new JavaScriptSerializer().Serialize(req);
            request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            var responseContent = JsonConvert.DeserializeObject<ApiResponseModel<bool>>(content);
            return responseContent.Data;
        }
    }
}
