﻿using TravelBot.Common.Domain;
using TravelBot.Common.Repository.Contracts;

namespace TravelBot.Common.Repository
{
    public class HotelPriceRepository: Repository<HotelPrice>, IHotelPriceRepository
    {
        public HotelPriceRepository(ITravelbotDbContext context) : base(context)
        {
        }
        public HotelPrice Add(HotelPrice p)
        {
            return base.Add(p);
        }
    }
}
