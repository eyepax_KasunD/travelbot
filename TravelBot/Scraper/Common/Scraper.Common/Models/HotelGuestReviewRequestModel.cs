﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scraper.Models
{
    public class HotelGuestReviewRequestModel
    {
        public string hotelurl { get; set; }
        public string website { get; set; }
    }
}