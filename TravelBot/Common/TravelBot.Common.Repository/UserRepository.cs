﻿using System;
using System.Linq;
using TravelBot.Common.Domain;
using TravelBot.Common.Repository.Contracts;

namespace TravelBot.Common.Repository
{
    public class UserRepository : Repository<UserSession>,IUserRepository
    {
        public UserRepository(ITravelbotDbContext context) : base(context)
        {
        }
        public UserSession AddSession(UserSession p)
        {
            return Add(p);
        }
        public bool GetUser(string id)
        {
            return Find(s => true).Any();    
        }
        public UserSession FindSession(string usersessionId)
        {
            return Find(p => true).FirstOrDefault();
        }
        public UserSession UpdateSession(UserSession userSession)
        {
            return Update(userSession);
        }   
      
    }
}
