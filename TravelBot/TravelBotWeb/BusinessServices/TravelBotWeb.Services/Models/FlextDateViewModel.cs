﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelBotWeb.Services.Models
{
    public class FlextDateViewModel
    {
        public string DateText { get; set; }
        public int Flex { get; set; }
    }
}
