﻿using TravelBot.Common.Models;
using TravelBotWeb.Common;
using static TravelBot.Common.Enums;

namespace TravelBotWeb.Services.Helpers
{
    public class Helper
    {
        public static string GetReviewText(double score)
        {
            string reviewtext;
            if (score <= 5)
            {
                reviewtext = "Moderate";
            }
            else if (score <= 8 && 5 < score)
            {
                reviewtext = "Good";
            }
            else if (score <= 8.5 && 8 < score)
            {
                reviewtext = "Very Good";
            }
            else if (score <= 9 && 8.5 < score)
            {
                reviewtext = "Excellent";
            }
            else
            {
                reviewtext = "Wonderful";
            }
            return reviewtext;
        }
        public static string GetEncodedHotelLink(string hotelPriceId, string userSessionId, string requestId, string appDomain)
        {
            return appDomain + "/redirect?id=" + Crypto.Base64Encode($"uId={userSessionId}&rId={requestId}&prId={hotelPriceId}");
        }
        public static string GetUserFreindlySourceName(DataSourceCode source)
        {
            string friendlyName;
            switch (source)
            {
                case DataSourceCode.Booking:
                    friendlyName = "www.booking.com";
                    break;
                case DataSourceCode.Expedia:
                    friendlyName = "www.expedia.com";
                    break;
                case DataSourceCode.Agoda:
                    friendlyName = "www.agoda.com";
                    break;
                default:
                    friendlyName = "travelbot";
                    break;
            }
            return friendlyName;
        }
    }
}