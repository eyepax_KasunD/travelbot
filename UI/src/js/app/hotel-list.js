var hotelDetailsBG;
app.controller("hotelListCtrl", function($scope){
    $("main").css({'background-image': 'url(' + durationBG + ')', 'background-repeat':'no-repeat', 'background-attachment' : 'fixed','background-size':'cover', 'margin':'0'});
    var noOfCities;
    var hotelsForDates;
	var currencyPrefix = '';
    datesSlots();
	hotelsForDates = hotellistAPI + (hotellistAPI.includes('?')?'&' : '?') + "flex=0";
    hotelsForDatesAPI();

    // dynamic date slots 
    function datesSlots() {
        for(d=0; d<datesLength; d++) {
            if(datesLength > 1) {
                timeLine = 
                '<a id='+flexDates[d].Flex+'>\n\
                    <div class="inner-border">\n\
                        <p></p>\n\
                        <label class="date-range"></label>\n\
                    </div>\n\
                </a>';
                $("#dates").append(timeLine);
                $("#dates a#"+flexDates[d].Flex).find(".date-range").text(flexDates[d].DateText);  
                $(".main-content").removeClass("content-up");  
                $scope.withFlexDates = true;
                $scope.withoutFlexDates = false;
            }
            else {
                $("#dates").remove();
                $(".main-content").addClass("content-up");
                $scope.withoutFlexDates = true;
                $scope.withFlexDates = false;
                $scope.dateRange = flexDates[d].DateText;
            }       
        }
        $("#dates a p").text(fromText);
        $("#dates a#0").addClass("departure-date checking");
    }

	//set the selected hotel
    $("#hotels").on("click", ".hotels-card", function(){
		var hotel = $(this).closest('.hotels-card');  
		var hotelId =hotel.first().attr('id');
		var cityAndHotelIds = hotel.first().attr('id').replace('city','').replace('hotels',',').split(',').map(Number);
		
		if(cityAndHotelIds[1] != 0){
			hotel.parent().children().first().nextAll('section:not(#'+hotelId+')').hide();
			hotel.parent().children().first().nextAll('#'+hotelId).slideUp("fast").promise().done(function(){
				[datalist[cityAndHotelIds[0]].Hotels[cityAndHotelIds[1]], datalist[cityAndHotelIds[0]].Hotels[0]] = 
				[datalist[cityAndHotelIds[0]].Hotels[0], datalist[cityAndHotelIds[0]].Hotels[cityAndHotelIds[1]]];
				hotelList();
 		   });	   
		}		   
 	   });

    // call relevant API call to display hotels relavant to seleted date slot	
	$("#dates").on("click", "a", function(){
        $("#dates a.checking").not(this).removeClass("checking");
        $(this).toggleClass("checking");
        selectedDateId = $(this).attr("id");
        hotelsForDates = hotellistAPI + (hotellistAPI.includes('?')?'&' : '?') + "flex="+selectedDateId;
        hotelsForDatesAPI();  
    });

    //API call to get the hotel details
    function hotelsForDatesAPI(){
        $("#loading").modal('show');
        $.ajax({
            type: "GET",
            url: hotelsForDates,
            success: function (res) {
                $("#loading").modal('hide');               
                JSON.stringify(res);
                if ((res.Errors == null) && (res.Data !== null && res.Data !== "" && typeof res.Data !== "undefined")) {
                    noOfCities = res.Data.length;
                    datalist = res.Data;
					currencyPrefix = noOfCities > 0 ? datalist[0].CurrencyPrefix : '';
                    hotelList(); 
                }
            }
        });
        return false;
    }

    //dynamic hotel lists
    function hotelList(){
        $(".city-card").remove(); // remove  previous hotel list
        total=0;
        for(i=0; i<noOfCities; i++){
            noOfHotels = datalist[i].Hotels.length;
            cityId = "city"+i;
            
            cityMainSection = '<div class="city-card" id='+cityId+'></div>';
            $("#hotels").append(cityMainSection); // parent div as city wise

            for(x=0; x<noOfHotels; x++){ 
                hotelDetails = datalist[i].Hotels[x];
                sectionId = "city"+i+"hotels"+ x;

                hotelSections = 
                '<section class="hotels-card" id='+sectionId+'>\n\
                    <p class="normal-text"></p>\n\
                    <h2 class="city"></h2>\n\
                    <a class="row">\n\
                        <div class="col-12">\n\
                            <div class="hotel-image">\n\
                                <img src="" class="main-image">\n\
                            </div>\n\
                            <div class="hotel-description">\n\
                                <b class="hotel-name"></b>\n\
                                <span class="hotel-address"></span>\n\
                                <img src="" class="star-ratings"/>\n\
                                <p class="nights"></p>\n\
                            </div>\n\
                            <div class="rating">\n\
                                <div class="reviews" id="sourceReview"><label></label></div>\n\
                                <div class="review-comments" id="trvlbtReview"><label></label></div>\n\
                                <br/><br/><br/>\n\
                                <img src="" border="0" class="partner-logo float-right" width="100px">\n\
                                <br/>\n\
                                <label class="price"></label>\n\
                            </div>\n\
                        </div>\n\
                    </a>\n\
                    <div class="row actions-btns">\n\
                        <div class="col-12">\n\
                            <a target="_blank" class="btn btn-primary">Book now</a>\n\
                            <button class="btn btn-link" type="button"></button>\n\
                        </div>\n\
                    </div>\n\
                </section>';

                $(".city-card#"+cityId).append(hotelSections); // print all hotels inside its parent city div
                                
                $('.hotels-card#'+sectionId).find(".normal-text").text(hotelListMainSectionText);
                $('.hotels-card#'+sectionId).find(".city").text(datalist[i].City);
                $('.hotels-card#'+sectionId).find(".main-image").attr("src", hotelDetails.HotelImgUrl);
                $('.hotels-card#'+sectionId).find(".hotel-name").text(hotelDetails.HotelName);
                $('.hotels-card#'+sectionId).find(".hotel-address").text(hotelDetails.Address);
                $('.hotels-card#'+sectionId).find(".star-ratings").attr("src", hotelDetails.HotelRatingImgUrl);
                $('.hotels-card#'+sectionId).find(".nights").text(datalist[i].NoOfStays+ ' ' +night);
               
                $('.hotels-card#'+sectionId).find(".partner-logo").attr("src", hotelDetails.SourceReview.IconUrl); 
                $('.hotels-card#'+sectionId).find(".price").text(datalist[i].CurrencyPrefix+hotelDetails.Price);
                $('.hotels-card#'+sectionId).find(".btn-primary").attr("href", hotelDetails.BookingUrl);
                $(".total .currency").text(datalist[i].CurrencyPrefix);
                $(".city-card section:first-child").addClass("selected");


                if(hotelDetails.SourceReview.Score > 0) {
                    $('.hotels-card#'+sectionId).find("#sourceReview label").text(hotelDetails.SourceReview.Score);
                }
                else {
                    $('.hotels-card#'+sectionId).find("#sourceReview, .partner-logo").hide();
                }

                if(hotelDetails.TravelbotReview.Score > 0) {
                    $('.hotels-card#'+sectionId).find("#trvlbtReview label").text(hotelDetails.TravelbotReview.Score);
                }
                else {
                    $('.hotels-card#'+sectionId).find("#trvlbtReview").hide();
                }

                if(hotelDetails.SourceReview.Score < 0 && hotelDetails.TravelbotReview.Score < 0 ) {
                    $('.hotels-card#'+sectionId).find(".price").css("top", "-96px");
                }
                
            }
        }
        // only for first hotel section
        $(".city-card section:not(:first-child) .normal-text").hide();
        $(".city-card section:not(:first-child) .city").hide();
        $(".city-card section:not(:first-child) .actions-btns").hide();
        $(".city-card section:not(:first-child)").hide();
        $(".actions-btns .btn-link").text(more);

        // print total of selected hotel prices
        $('.city-card section').each(function() {
            if($(this).hasClass("selected")){
                sumValue = parseFloat($(this).find(".price").text().replace(currencyPrefix,''));
                total += sumValue||0;
            }
        });
        $(".total .sum").text(total); 
        
        //toggle sections when click "more hotels"
        $(".actions-btns").on("click", ".btn-link", function(e) {
            $(this).parent().parent().parent().nextAll().slideToggle("slow");
            $(this).text(function(i, v){
                return v === more  ? less : more;
            })
        });
    }
});




