﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;
using TravelBot.Common.Helpers.Contracts;
using TravelBot.Common.Repository.Contracts;

namespace TravelBot.Common.Repository
{
    public class CachedCurrencyRepository : CurrencyRepository, ICurrencyRepository
    {
        private readonly ICaching _cache;
        const string prefix = "TravelBot.Common.Repository.CurrencyCode";
        const int ttl = 60 * 60 * 24;
        public CachedCurrencyRepository(ITravelbotDbContext context, ICaching cache) : base(context)
        {
            _cache = cache;
        }
        public List<Currency> GetCurrency(Enums.CurrencyCode code)
        {
            List<Currency> currencyList = _cache.Get<List<Currency>>(prefix);
            if (currencyList == null)
            {
                currencyList = base.GetCurrency(code);
                _cache.Set(prefix, currencyList, ttl);
            }
            return currencyList;
        }
    }
}
