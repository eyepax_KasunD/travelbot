﻿using System.Globalization;
using System.Threading;
using System.Web.Mvc;
using System;
using TravelBotWeb.Common;
using TravelBotWeb.Services.Contracts;

namespace TravelBotWeb.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(IAppSettings appSettings) : base(appSettings)
        {
        }

        [OutputCache(Duration = int.MaxValue, VaryByParam = "lang")]
        public ActionResult Index(string lang)
        {
            logger.Info("Web page loading with language: " + lang);
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(lang);
            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(lang);   
            ViewBag.Lang = lang;
            return View();      
        }
    }
    
}

