
//---------------------------------------------------------------------------------
//-------------------------------------- Destination ------------------------------
//---------------------------------------------------------------------------------
app.controller("tripPlanCtrl", function($scope){
    displayLocalStorageValues();
    // display selected/default country on dropdown
    if(lsLabelDestinationCode !== null && typeof lsLabelDestinationCode !== "undefined" && lsLabelDestinationCode !== "") {
        countryCode = lsLabelDestinationCode;
        /*********** uncomment when country dropdown enable************** */
        //$scope.country = $("#country .dropdown-menu li").find('[data-value='+lsLabelDestinationCode+']').text();
        $scope.country = $(".country-box").text();
        $scope.countryCode = countryCode;
    }

    if(window.location.href.indexOf("#") > -1) {
        cleanLocalStorage();
        cleanLocalStorageAtIndexPage();
        cleanLocalStorageAtEmailPage();
        window.location.href =  "#dates";
        localStorage.setItem("pageSection","#dates");
        currentPage();
        // currentPage();
        // sortableCitiesList();
        // carouselCitiesImages(0);
        // cityAndDurationStoryLine();
        // InitBudjectSlider();
        // pkgDetailsChoice();
    }
    // page load after the index page. At that time no # tag has appended to url
    else {
        window.location.href =  "#dates";
        localStorage.setItem("pageSection","#dates");
        currentPage();
    }

    // country selection
    $scope.countrySelection = function(item){
        $scope.country =  event.target.innerHTML;
        $scope.countryCode =  item.currentTarget.getAttribute("data-value");
        countryCode = $scope.countryCode;
        backgroundImageAsCountry();
    }

	var slideshow;
    // 'sneak peek' link  - unskip cities section
    $scope.unskipCities = function(){
        localStorage.setItem("destination", $(".country-box").text());
        localStorage.setItem("destinationCode", $scope.countryCode);
        localStorage.setItem("pageSection", "#cities");
        localStorage.setItem("citiesSkipped", false);
        window.location.href =  "#cities";
        displayLocalStorageValues();
        currentPage();
        citiesList();
        carouselCitiesImages(0);
        cityAndDurationStoryLine();
		slideshow = $(".slider").slick({
			centerMode: true,
			centerPadding: '0',
			slidesToShow: 3,
			responsive: [
				{
					breakpoint: 768,
					settings: {
						arrows: false,
						centerMode: true,
						centerPadding: '40px',
						slidesToShow: 3
					}
				},
				{
					breakpoint: 480,
					settings: {
						arrows: false,
						centerMode: true,
						centerPadding: '40px',
						slidesToShow: 1
					}
				}
			]
		});
			
		 $('.cities').on('click','.slick-active:not(.slick-current)>div>label',function(event) {
				var id = $(this).parent().parent().attr('data-slick-index');
				var idOfFirst = $('.slick-active:not(.slick-current)').first().attr('data-slick-index');
				
				if(id === idOfFirst)
					slideshow.slick('slickPrev');
				else
					slideshow.slick('slickNext');
				event.preventDefault();
		 });
		 
		 $('.cities').on('change','.slick-current .checkbox',function(event) {
			 var noOfCities = CityInfo[0].Cities.length;
			 var isChecked = this.checked;
			 var offset = 100;
			 var parnt = $(this).closest('.slick-current');
			var parentId = parseInt(parnt.attr('data-slick-index')) + offset;
			$('.slick-slide:not(.slick-current) .checkbox').each(function() {
					var pId = parseInt($(this).closest('.slick-slide').attr('data-slick-index')) + offset;
					if((parentId%noOfCities) === (pId%noOfCities)){
						$(this).removeClass('selected');
						$(this).attr('checked', isChecked);
						if(isChecked)
							$(this).addClass('selected');
					}
				});			

		 });
		 
		  	
        //$(".slick-current img").after('<input type="checkbox" class="checkbox city" id="3" /><span class="checkmark"></span>');
    }



    // next button click - jump to duration by skipping cities selection
    $scope.skipCities = function(){
        localStorage.setItem("destination", $(".country-box").text());
        localStorage.setItem("destinationCode", $scope.countryCode);
        localStorage.setItem("pageSection", "#duration");
        localStorage.setItem("citiesSkipped", true);
        $scope.countryThumbnail = countryThumbnail;
        window.location.href =  "#duration";
        displayLocalStorageValues();
        currentPage();
        cityAndDurationStoryLine();
        //citiesList();
    }

// display city and country
function displayDestination() {
    /*********** uncomment when country dropdown enable************** */
   // country = $("#country .dropdown-menu li").find('[data-value=' + lsLabelDestinationCode + ']').text(); 
    country = $(".country-box").text();
    $(".destination").text(country);
}

$scope.gotoDestination = function(){
    window.location.href =  "#destination";
    localStorage.setItem("pageSection","#destination");
    currentPage();
}

//---------------------------------------------------------------------------------
//---------------------------------------- dates ----------------------------------
//---------------------------------------------------------------------------------
isFlexEnable = "false";

// highlight selected flexible dates
$('.flex').click(function(){
    $('.flex').not(this).removeClass('flex-active'); // remove buttonactive from the others
    $(this).toggleClass('flex-active'); // toggle current clicked element
});

// highlight already selected flexible dates when page refresh
if(lsLabelFlexibleDateRange !== null && typeof lsLabelFlexibleDateRange !== "undefined" && lsLabelFlexibleDateRange !== ""){
    $(".flex[value=" + lsLabelFlexibleDateRange + "]").addClass("flex-active");
}

$scope.saveDates = function() {
    destinationDate = $scope.destinationDate || $scope.fixedDate;
    $scope.fixedDate = destinationDate;

    selectedFlexRange = $(".flex-active").attr("value");
    if(selectedFlexRange !== null && typeof selectedFlexRange !== "undefined" && selectedFlexRange !== ""){
        isFlexEnable = "true";
        localStorage.setItem("flexEnable", isFlexEnable);
        localStorage.setItem("flexDatesRange", selectedFlexRange);
    }
    else {
        isFlexEnable = "false";
        localStorage.setItem("flexEnable", isFlexEnable);
    }

    if(destinationDate !== null &&  destinationDate !== undefined && destinationDate !== ""){
        $scope.dateError = false;
        window.location.href =  "#destination";
        sectionName  = window.location.href.split('#')[1];
        localStorage.setItem("pageSection", "#destination");
        localStorage.setItem("departureDate",destinationDate);
        //$scope.fixedDate = destinationDate;
        displayLocalStorageValues();
        currentPage();
    }
    else {
        $scope.dateError = true;
    }
}


//config datepicker
$('.date').datepicker({
    format: 'dd/mm/yyyy',
    startDate: '+4d',
	todayHighlight: true,
    autoclose: true,
    orientation: 'bottom'
});


$(".dates").mousedown(function(){
    $("#form-dates #error-dates").hide();
});


if (lsLabelDepartureDate !== null && typeof lsLabelDepartureDate !== undefined && lsLabelDepartureDate !==""){
    $scope.dateError = false;
    $scope.fixedDate = lsLabelDepartureDate;
}
// else {
//     $scope.dateError = true;
// }

if ((lsLabelDepartureDate !== null && typeof lsLabelDepartureDate !== "undefined" && lsLabelDepartureDate !== "") && (lsLabelTravellingDatesCount !== null && typeof lsLabelTravellingDatesCount !== "undefined" && lsLabelTravellingDatesCount !== "") && (isDaysCountWrong == 'false') ){
    window.location.href =  "#budget";
    localStorage.setItem("pageSection", "#destination");
    displayTravellingDatesCount();
    $("#form-destination, #form-dates").hide();
    $("#form-budget").show();
    displayLocalStorageValues();
}

$scope.gotoDates = function(){
    window.location.href =  "#dates";
    localStorage.setItem("pageSection","#dates");
    currentPage();
    // sortableCitiesList();
}

//----------------------------------------------------------------------------------
//---------------------------------------- cities ----------------------------------
//----------------------------------------------------------------------------------

function citiesList() {
    if(CityInfo !== null && typeof CityInfo !== "undefined" && CityInfo !== "") {
        citiesCount = CityInfo[0].Cities.length; //total cities count

        for(x=0; x < citiesCount; x++) {
            $('.center.slider').append('<label class="carousel-item">'+
			'<label class="clickable-city-thumb"><img src=' + CityInfo[0].Cities[x].CityThumbnailUrl + 
			' class="carousel-image img-fluid"> <input type="checkbox" class="checkbox city city-id-'+x+'"/><span class="checkmark-map"></span></label><p class="city-name">' + CityInfo[0].Cities[x].CityName 
			+ '</p><p class="province">' + CityInfo[0].Cities[x].Province + '</p></label>');
        }
        
        //selectedCityCarousel();
     }
}


// function citiesList() {
//     if($("#cities .carousel-item").hasClass("active")){
//         if (cityRotated == "true"){
//             $scope.selectedCity = $('#cities .carousel-item.active').find("img").attr('src');
//             selectedCityCarousel();
//             prevAndNextCarouselImages();
//         }
//     }
//     else {
//         if(CityInfo !== null && typeof CityInfo !== "undefined" && CityInfo !== "") {
//             citiesCount = CityInfo[0].Cities.length; //total cities count

//             for(x=0; x < citiesCount; x++) {
//                 $('#cities .carousel-inner').append('<label class="carousel-item"><img src=' + CityInfo[0].Cities[x].CityThumbnailUrl + ' class="carousel-image img-fluid"> <input type="checkbox" class="checkbox city" id="'+x+'"/><span class="checkmark"></span><p class="city-name">' + CityInfo[0].Cities[x].CityName + '</p><span class="province">' + CityInfo[0].Cities[x].Province + '</span></label>');
//             }
//             $("#cities .carousel-item:first-child").addClass("active");

//             selectedCityCarousel();
//             prevAndNextCarouselImages();
//         }
//     }
// }

$scope.saveCities = function(){
    if($('input.city:checked').length > 0) {
        $scope.errorCities = false;
        localStorage.setItem("pageSection", "#duration");
        window.location.href =  "#duration";
        selectedCityWithImage();
        displayLocalStorageValues();
        sortableCitiesList()
        currentPage();
    }
    else {
        $scope.errorCities = true;
    }
}

// save seleced city ids with its images as array in localhost
function selectedCityWithImage(){
    localStorage.removeItem("selectedCitiesAndImages");
    var selectedCitiesAndImages = new Array();
	var selectedElements = $(".center.slider .carousel-item .selected");
	var uniqueClassNames = [];
	var uniqueElements = $([]);
	selectedElements.each(function () {
		if (this.className != "" && uniqueClassNames.indexOf(this.className) == -1) 
		{
			uniqueClassNames.push(this.className);  
			uniqueElements.push(this);  
		}			
	});
    uniqueElements.each(function(){
			var selectedCityID =  this.className.split(/\s+/).filter(a => a.includes("city-id-"))[0].replace('city-id-','');
			var selectedCityImage  = $(this).prev().attr("src");
			var cityName = $(this).closest('.carousel-item').find(".city-name").text();

			selectedCitiesAndImages.push({cityName: cityName, cityId: selectedCityID, cityImage: selectedCityImage});
			localStorage.setItem("selectedCitiesAndImages",  JSON.stringify(selectedCitiesAndImages));
    });
    totalCitiesSelected = selectedCitiesAndImages.length;
    localStorage.setItem("selectedCityCount", totalCitiesSelected);
}


// add class to selected cities
$(".center.slider").on("change", ":checkbox", function() {
    $(this).toggleClass("selected");
});

// Fires after slide change
$('.center.slider').on('afterChange', function(event, slick, currentSlide){
    //citiesList();
    
    //$(".slick-current img").after('<input type="checkbox" class="checkbox city"/><span class="checkmark"></span>');

   //activeCity = $("#cities .carousel-item.active .city").attr("id"); // checkbox id
   var slideNumber = currentSlide % CityInfo[0].Cities.length;
    carouselCitiesImages(slideNumber);
});

// Fires before slide change
$('.center.slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
   // $(".city, .city + .checkmark").remove(); //remove checkbox from prev and next cities
});

// cityRotated = "false";
// $('#cities').on('slid.bs.carousel', function () {
//     cityRotated = "true";
//     citiesList();
//     activeCity = $("#cities .carousel-item.active .city").attr("id");
//     carouselCitiesImages(activeCity);
// });

//upper carousel - more images about active city 
function carouselCitiesImages(currentSlide){   
    if(CityInfo  !== null && typeof CityInfo  !== "undefined" && CityInfo  !== "") {
        carouselCount = CityInfo[0].Cities[currentSlide].CityImages.length; //total city images for carousel
        City = CityInfo[0].Cities[currentSlide];
        carouselSection = 
        '<div class="carousel-item" id="">\n\
            <img src="" />\n\
        </div>';

        y = "";
        for(x=0; x < carouselCount; x++) {
            var id = carouselSection.replace('id=""', 'id="'+x+'"');
            var el = x ==0 ? id.replace("carousel-item", "carousel-item active") : id;
            y += el; 
        }
        
        $(".selected-city-description .carousel-inner .carousel-item").remove();
        $(".selected-city-description .carousel-inner").append(y);

        for(x=0; x < carouselCount; x++) {
            $(".selected-city-description .carousel-item#" +x+ " img").attr("src", City.CityImages[x].URL);
        }

        $("#form-cities .carousel-caption .city-heading").text(City.CityName);
        $("#form-cities .carousel-caption .city-description").text(City.Description);
        
    } 
}

// make checked on checkboxes wich have already checked as selected cities
//function selectedCityCarousel(){
//    if(lsLabelsortedCitiesAndImages !== null && typeof lsLabelsortedCitiesAndImages !== "undefined" && lsLabelsortedCitiesAndImages !==""){
 //       for(i=0; i<lsLabelsortedCitiesAndImages.length;i++){
//            $("#cities .carousel-item").find("input#"+lsLabelsortedCitiesAndImages[i].cityId).addClass("selected").prop("checked", true);
//        }
//    }
//}

// previous and next cities - lower carousel
// function prevAndNextCarouselImages(){
//     $(".prevItem img, .prevItem  p, .nextItem img, .nextItem p").remove();
//     prevItemImg = $('#cities .carousel-item.active').prev().find("img").attr('src');
//     prevItemName = $('#cities .carousel-item.active').prev().find("p.city-name").text();
//     nextItemImg = $('#cities .carousel-item.active').next().find("img").attr('src');
//     nextItemName = $('#cities .carousel-item.active').next().find("p.city-name").text();
//     $(".prevItem").append("<img src="+ prevItemImg +" ng-if='noPrev'><p class='city-name'>"+prevItemName+"</p>");
//     $(".nextItem").append("<img src="+ nextItemImg +" ng-if='noNext'><p class='city-name'>"+nextItemName+"</p>");

//     if(prevItemImg == null || typeof prevItemImg == "undefined" || prevItemImg == ""){
//         $(".prevItem img").remove();
//     }

//     if(nextItemImg == null || typeof nextItemImg == "undefined" || nextItemImg == ""){
//         $(".nextItem img").remove()
//     }
// }

//----------------------------------------------------------------------------------
//---------------------------------------- Duration --------------------------------
//----------------------------------------------------------------------------------

//selected cities will display in sortable mode
function sortableCitiesList(){
    $(".city-duration .city-section").remove();
    getValuesFromLocalStorage();
    
    //first, selected cities in cities section are display in dates section - unsorted list
    for(i=0;i<lsLabelselectedCityCount;i++) {
        id = lsLabelsortedCitiesAndImages[i].cityId;
        url = lsLabelsortedCitiesAndImages[i].cityImage;		
        name = lsLabelsortedCitiesAndImages[i].cityName;;
        // <div class="drag-icons">\n\
        // <a class="far fa-sort-up"></a>\n\
        // <a class="far fa-sort-down"></a>\n\
        // </div>\n\

        cityList =
        '<div class="city-section" value='+id+'>\n\
            <div class="city-image">\n\
                <img src="'+url+'"/>\n\
            </div>\n\
            <div class="duration-days">\n\
                <h1 class="middle-heading city city-name text-ellipsis text-capitalize" value='+id+'></h1>\n\
                <h3 class="middle-heading city duration-subText"></h3>\n\
                <input type="number" class="form-control days"/>\n\
                <label></label>\n\
            </div>\n\
        </div>';

        $(cityList).insertBefore("#error-duration");
        $('.city-section[value='+id+'] .city-name').text(name);
        $('.city-section[value='+id+'] .duration-days label').text(days);
    }

    $('.city-section .duration-subText').text(forText);
    $('.city-section input').attr("placeholder",MaxDaysHelpText);

    alreadyEnteredDuration();

    //secondly, compare sorted cities with selected ids and rearrange the order
    // if (lsLabelsortedCitiesAndDates !== "" && typeof lsLabelsortedCitiesAndDates !== "undefined" && lsLabelsortedCitiesAndDates !== null) {
    //     z=0;
    //     idWithDaysCount = lsLabelsortedCitiesAndDates.length;
    //     for(y=0;y<idWithDaysCount;y++){
    //         idWithDays = lsLabelsortedCitiesAndDates[y].cityId;
    //         daysStaying = lsLabelsortedCitiesAndDates[y].days;

    //         for(x=0;x<lsLabelselectedCityCount;x++) {
    //             // if sorted city was also in the selected city list, rearrange the city order according to the sorted order.
    //             if(idWithDays == id[x]){
    //                 selectedCityName = $('.selected-cities button[value=' +idWithDays+ ']').text();
    //                 $(".city-section:eq("+z+") .city").text(selectedCityName);
    //                 $(".city-section:eq("+z+") .days").val(daysStaying);
    //                 z++;
    //             }
    //         }
    //     }
    // }
}

function alreadyEnteredDuration(){
    // if days have already entered, print days
    if (lsLabelsortedCitiesAndDates !== "" && typeof lsLabelsortedCitiesAndDates !== "undefined" && lsLabelsortedCitiesAndDates !== null) {
        daysCount = lsLabelsortedCitiesAndImages.length;

        for(y=0;y<daysCount;y++){
            latestCity  = lsLabelsortedCitiesAndImages[y].cityId;

            for (i=0;i<lsLabelsortedCitiesAndDates.length;i++){
                if(lsLabelsortedCitiesAndDates[i].cityId == latestCity){
                    $(".city-section[value="+lsLabelsortedCitiesAndImages[y].cityId+"] .duration-days .days").val(lsLabelsortedCitiesAndDates[i].days);
                }
            }
        }
    }
}

//cities and dates
$scope.saveDuration = function(){
    displayLocalStorageValues();
    sum = 0;
    // save sorted cities in order to localhost
    var sortedCitiesAndDates = new Array();
    $(".city-duration .city-section").each(function(){
        sortedCitiesAndDates.push({cityId:$(this).find(".city-name").attr("value"),cityName:$(this).find(".city-name").text(), days:$(this).find(".days").val()});
        localStorage.setItem("sortedCitiesAndDates",  JSON.stringify(sortedCitiesAndDates));
        sum += Number($(this).find(".days").val());
    });

    var isValid = true;
    $('input[type="number"].days').each(function() {
        if ($.trim($(this).val()) == '') {
            isValid = false;
        }
        else {
            //isValid = true;
        }
    });
    if (!isValid) {
        $scope.errorDurationUnskipped = true;
    }
    else {
        $scope.errorDurationUnskipped = false;
        localStorage.setItem("totalDaysStay", sum);
        localStorage.setItem("citiesSkipped", false);
        localStorage.setItem("pageSection", "#count");
        window.location.href =  "#count";
        currentPage();
        cityAndDurationStoryLine();
    }
}

function displayCities(){
    getValuesFromLocalStorage();
    //citiesList();
    $(".city-list-with-days").empty();
    if(lsLabelcitiesSkipped == "false") {
        if(lsLabelsortedCitiesAndDates !== "" && typeof lsLabelsortedCitiesAndDates !== "undefined" && lsLabelsortedCitiesAndDates !== null){
            cityWithDays = "";

            for(i=0;i<lsLabelsortedCitiesAndDates.length;i++){
                city = lsLabelsortedCitiesAndDates[i].cityName;

                // first letter capilised
                city = city.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                    return letter.toUpperCase();
                });

                duration = lsLabelsortedCitiesAndDates[i].days > 1 ? days : day;
                cityWithDays +=   city + " " + forText + " " + lsLabelsortedCitiesAndDates[i].days + " " + duration + "" + "," + " ";
            }

            //remove comma from last duration
            var line = $.trim(cityWithDays);
            var lastChar = line.slice(-1);
            var appendList = lastChar == ',' ? line = line.slice(0, -1): "";

            $(".city-list-with-days").empty();
            $(".city-list-with-days").append(appendList);        
        } 
    }
    else {
        if(typeof days !== "undefined" && typeof day !== "undefined"){
            if(lsLabeltotalDaysStay > 1) {
                $(".duration").text(lsLabeltotalDaysStay+' ' +days);
            }
            else {
                $(".duration").text(lsLabeltotalDaysStay+' ' +day);
            }
        }
    }
}

// contry and dates
$scope.saveDurationSkippedMode = function(){
    if(!$("input.total-days").val() || $("input.total-days").val() == 0){
        $scope.errorDurationSkipped = true;
        return false;
    }
    else {
        countryDuration = $scope.getCountryDuration || $scope.setCountryDuration;
        $scope.setCountryDuration = countryDuration;

        $scope.errorDurationSkipped = false;
        localStorage.setItem("totalDaysStay", countryDuration);
        localStorage.setItem("citiesSkipped", true);
        localStorage.setItem("pageSection", "#count");
        window.location.href =  "#count";

        // save dates as array when no cities have selected
        var totalDaysStayInArray = new Array();
        totalDaysStayInArray.push({cityId:"", days:countryDuration});
        localStorage.setItem("totalDaysStayInArray",  JSON.stringify(totalDaysStayInArray));
        currentPage();
    }
}

if( lsLabeltotalDaysStay !== "" && typeof lsLabeltotalDaysStay !== "undefined" && lsLabeltotalDaysStay !== null) {
    $scope.setCountryDuration = lsLabeltotalDaysStay;

}

$scope.gotoDuration = function(){
    window.location.href =  "#duration";
    localStorage.setItem("pageSection","#duration");
    currentPage();
}

//----------------------------------------------------------------------------------
//---------------------------------------- Traveler Count ---------------------------
//----------------------------------------------------------------------------------
$scope.saveCount = function(){
    var adultsCount = $scope.getAdult || $scope.setAdult; // if getAdult is undefined(when reading already entered value), get setAdult value
    $scope.setAdult = adultsCount;

    var childrenCount = $scope.getChild || 0;
    $scope.setChild = childrenCount;

    if(adultsCount !== "" && typeof adultsCount !== "undefined" && adultsCount !== null) {
        $scope.errorCount = false;
        localStorage.setItem("adultsCount", adultsCount);
        localStorage.setItem("childrenCount", childrenCount);
        localStorage.setItem("pageSection", "#budget");
        window.location.href =  "#budget";
        displayLocalStorageValues();
        currentPage();
        cityAndDurationStoryLine();
		InitBudjectSlider();
    }
    else {
        $scope.errorCount = true;
    }
}

if( lsLabelAdultCount !== "" && typeof lsLabelAdultCount !== "undefined" && lsLabelAdultCount !== null) {
    $scope.setAdult = lsLabelAdultCount;
}

if(lsLabelChildCount !== "" && typeof lsLabelChildCount !== "undefined" && lsLabelChildCount !== null) {
    $scope.setChild = lsLabelChildCount;
}

$scope.gotoCount = function(){
    localStorage.setItem("pageSection", "#count");
    window.location.href =  "#count";
    displayLocalStorageValues();
    currentPage();
}

$scope.gotoCities = function(){
    window.location.href =  "#cities";
    localStorage.setItem("pageSection","#cities");
    currentPage();
}

function cityAndDurationStoryLine(){
    if(lsLabelcitiesSkipped == "true"){
        $scope.skippedCitiesLine = true;
        $scope.unSkippedCitiesLine = false;
    }
    else {
        $scope.skippedCitiesLine = false;
        $scope.unSkippedCitiesLine = true;
    }
}

//----------------------------------------------------------------------------------
//---------------------------------------- budget ----------------------------------
//----------------------------------------------------------------------------------
$scope.saveBudget = function(){
    localStorage.setItem("minBudget", $("#minBudget1").text());
    localStorage.setItem("maxBudget", $("#maxBudget1").text());
    localStorage.setItem("pageSection", "#email");
    window.location.href =  "#email";
    displayLocalStorageValues();
    currentPage();
    cityAndDurationStoryLine();
    $(".package-details input[value='1']").prop("checked", true);
}

$scope.gotoBudget = function(){
    localStorage.setItem("pageSection", "#budget");
    window.location.href =  "#budget";
    displayLocalStorageValues();
    currentPage();
}

//nouislider
function InitBudjectSlider() {
    var paddingSlider = document.getElementById('slider-custom');

	var minBudjet = perDayMin * lsLabeltotalDaysStay;
	var maxBudjet = perDayMax * lsLabeltotalDaysStay;
	var budgetRange = maxBudjet - minBudjet;
	var defaultStart = 0;
    var defaultEnd = minBudjet + budgetRange/2;
    
    $("#minBudget").text("$"+ " " +minBudjet);
    $("#maxBudget").text("$"+ " " +maxBudjet);
	
	noUiSlider.create(paddingSlider, {
		start: [ defaultStart, defaultEnd ],
		padding: [ 0, 0 ],
		range: {
			'min': minBudjet,
			'max': maxBudjet
		},
		format: wNumb({
			decimals: 0,
			prefix: '$',
		}),
		connect: true,
		tooltips:[false, false],
		step: 1 
	});

	var paddingMin = document.getElementById('minBudget1'),
		paddingMax = document.getElementById('maxBudget1');

	paddingSlider.noUiSlider.on('update', function ( values, handle ) {
		if ( handle ) {
			paddingMax.innerHTML = values[handle];
		} else {
			paddingMin.innerHTML = values[handle];
		}
	});
}
//---------------------------------------------------------------------------------
//---------------------------------------- email ----------------------------------
//---------------------------------------------------------------------------------

function ValidateEmail(email) {
    var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return expr.test(email);
};

$scope.saveEmail = function(){
    emailAddress = $('input.email').val();
    choice = $('.package-details input:checked').val();

    if(choice == 1){
        if((emailAddress !== null && typeof emailAddress !== "undefined" && emailAddress !=="") && (isEmailValid == "true")){
            localStorage.setItem("email", emailAddress);
            localStorage.setItem("packageDetailsChoice", choice);
            $("#form-email #error-email").hide();
            bookingConfirm();    
        }
        else {
            $("#form-email #error-email").show();
            $(".email").focus();
        }
    }
    else {
        localStorage.setItem("email", "");
        localStorage.setItem("packageDetailsChoice", choice);
        bookingConfirm();
    }
    getValuesFromLocalStorage();
    cityAndDurationStoryLine();
}

function pkgDetailsChoice(){
    // if package details has already selected
    if(lsLabelPackageDetailsChoice !== null && typeof lsLabelPackageDetailsChoice !== "undefined" && lsLabelPackageDetailsChoice !== "") {
        $(".package-details input[value="+lsLabelPackageDetailsChoice+"]").prop("checked", true);
    }
    // set default 'send email' for the first time page load
    else {
        $(".package-details input[value='1']").prop("checked", true);
        localStorage.setItem("packageDetailsChoice", "1");
    }
}

$(".email").keyup(function() {
    if (ValidateEmail($(".email").val())) {
        $("#form-email #error-email").hide();
        isEmailValid = "true";
    }
    else {
        isEmailValid = "false";
    }
});

// package details option radio buttons
$(".package-details input").on("change", function() {
    packageDetailsChoice = $(".package-details input:checked").val();
    emailAddress = $('input.email').val();

    //email
    if(packageDetailsChoice == "1") {
        $(".email").prop("readonly", false);
        $(".email").val('');
        
        if (emailAddress !== null && typeof emailAddress !== "undefined" && emailAddress !=="") {
            $(".email").show();
            if(isEmailValid == "false") {
                $("#form-email #error-email").show();
            }
            else {
                $("#form-email #error-email").hide();
            }
        }
        else {
            $(".email").show();
        }
    }
    // view on web
    else {
        $(".email").prop("readonly", true).val("");
        $("#form-email #error-email").hide();
        $("#saveEmail").css("visibility", "hidden");

        //cookie
        if ($("#cookie").is(":hidden")) {
            $("#cookie").slideDown('slow');
            $(".package-details input[type='radio']").attr("disabled", true);
        }
    }
});

$("#cookie .accept-btn").click(function(){
    $("#cookie").slideUp('slow');
});

$scope.cancelCookie = function(){
    $("#cookie").slideUp('slow');
    $(".package-details input[type='radio']").removeAttr("disabled");
    $(".email").removeAttr("readonly");
    $(".package-details").prop("readonly", false);
    $(".package-details input[value='1']").prop("checked", true);
    $(".package-details input[value='2']").prop("checked", false);
    $("#saveEmail").css("visibility", "visible");
}

function bookingConfirm(){
    $("#loading").modal('show');
    getValuesFromLocalStorage();

    var booking = {
		cultureCode: lsCultureCode,
        CountryId: lsLabelDestinationCode,
        Destinations: lsLabelcitiesSkipped == "true" ? lsLabeltotalDaysStayAsArray : lsLabelsortedCitiesAndDates,
		CheckInDate: lsLabelDepartureDate,
		FlexDates: isFlexEnable == "true" ? lsLabelFlexibleDateRange : 0,
		PriceFrom:parseInt(lsLabelMinBudget.replace('$','')),
        PriceTo:parseInt(lsLabelMaxBudget.replace('$','')),
		Email: lsLabelEmail,
		AdultCount: lsLabelAdultCount,
		ChildCount: lsLabelChildCount,
		DeviceId: lsLabelBookingDeviceId
	};
    $.ajax({
        type: "POST",
        url: bookingAPI,
        data: JSON.stringify(booking),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (res) {
           JSON.stringify(res);
           $("#loading").modal('hide');
            if ((res.Errors == null) && (res.Data !== null && res.Data !== "" && typeof res.Data !== "undefined")) {
                var minPrice = lsLabelMinBudget;
                var maxPrice = lsLabelMaxBudget;
                var checkingDate = $('.date').datepicker('getDate');
                var noOfStays = parseInt(lsLabeltotalDaysStay);
                var newCheckingDate = $('.date').datepicker('getDate');
                var checkoutdate = new Date();
                checkoutdate = newCheckingDate.setDate(newCheckingDate.getDate() + noOfStays);
                checkoutdate = new Date(checkoutdate);
                var today =  new Date();
                currentDate = today;
                var timeDiff = Math.abs(checkingDate.getTime() - today.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

                if(lsLabelDestinationCode == 1) {
                    var country = "Sri Lanka"
                }
                var ResponseObj = new ResponseObject(country,minPrice,maxPrice,checkingDate,checkoutdate.toLocaleDateString('en-US'),diffDays,currentDate,res.Data.Link);
                ArrayItem = localStorage.getItem("ResponseArray");
                if(ArrayItem !== null) {
                    ResponseArray= JSON.parse(ArrayItem);
                }
                ResponseArray.push(ResponseObj);
                localStorage.setItem("ResponseArray",  JSON.stringify(ResponseArray));
                localStorage.setItem("hotelDetailsResponseUrl",  res.Data.Link);
                localStorage.setItem("bookingDeviceId", res.Data.DeviceId);
                window.location.href = "#booking-complete";
                localStorage.setItem("pageSection", "#booking-complete");
                $("#form-email, #form-error").hide();
                $("#form-confirm").show();
                $(".navbar-brand").attr("style","pointer-events:none");

                if(lsLabelEmail !== null && typeof lsLabelEmail !== "undefined" && lsLabelEmail !== ""){
                    cleanLocalStorage();
                    cleanLocalStorageAtIndexPage();
                    cleanLocalStorageAtEmailPage();
                }
            }
            else {
                $("#form-error").show();
                $("#form-email").hide();
            }
        }
    });
    return false;
}


//-----------------------------------------------------------------------------------
//---------------------------------------- confirm ----------------------------------
//-----------------------------------------------------------------------------------

$("#confirm, #error").on("click", function () {
    $(".navbar-brand").removeAttr("style");
    indexPageUrl = $('a.navbar-brand').attr("href");
    window.location.href = indexPageUrl;
});

//-----------------------------------------------------------------------------------
//---------------------------------------- common ----------------------------------
//-----------------------------------------------------------------------------------

// change body background image according to selected destination
function backgroundImageAsCountry(){
    if(backgroundImages  !== null && typeof backgroundImages !=="undefined" && backgroundImages !== "") {
        for (key in backgroundImages) {
            if(countryCode == backgroundImages[key].Id) {
                $("main").css('background', 'url(' + backgroundImages[key].URL + ') center center / cover');
                break;
            }
        }
    }
}

function preloadResForDestination(){
    $('preload').remove();
    $('body').append('<div class="preload" style=" background: '+
    ' url('+destinationBg+') no-repeat -9999px -9999px; "></div>');
}
function preloadResForCities(){
    $('preload').remove();
    $('body').append('<div class="preload" style=" background: '+
    ' url('+destinationBg+') no-repeat -9999px -9999px; "></div>');
}
function preloadResForDuration(){
    $('preload').remove();
    $('body').append('<div class="preload" style=" background: '+
    ' url('+durationBG+') no-repeat -9999px -9999px; "></div>');
}
function preloadResForCount(){
    $('preload').remove();
    $('body').append('<div class="preload" style=" background: '+
    ' url('+countBG+') no-repeat -9999px -9999px; "></div>');
}
function preloadResForBudget(){
    $('preload').remove();
    $('body').append('<div class="preload" style=" background: '+
    ' url('+budgetBG+') no-repeat -9999px -9999px; "></div>');
}
function preloadResForEmail(){
    $('preload').remove();
    $('body').append('<div class="preload" style=" background: '+
    ' url('+emailBG+') no-repeat -9999px -9999px; "></div>');
}
// form sections handle
function currentPage() {
    displayLocalStorageValues();
    sectionName  = window.location.href.split('#')[1];
    if(lsLabelDestinationCode !== null && typeof lsLabelDestinationCode !== "undefined" && lsLabelDestinationCode !== "") {
        if (sectionName == "dates"){
            $scope.formDates = true;
            $scope.formDestination = false;
            $scope.formCities = false;
            $scope.formDuration = false;
            $scope.formCount = false;
            $scope.formBudget = false;
            $scope.formEmail = false;
            $("#form-confirm, #form-error").hide();
            if (lsLabelDestinationCode == 1){
                // $scope.backgroundImage = {'background' : 'url(' + backgroundImages[key].URL + ') center center / cover'}
                //$("main").css('background', 'url(' + datesBG + ') center center / cover');
            }
            preloadResForDestination();
        }
        else if (sectionName == "destination"){
            displayDestination();
            $scope.formDestination = true;
            $scope.formDates = false;
            $scope.formCities = false;
            $scope.formDuration = false;
            $scope.formCount = false;
            $scope.formBudget = false;
            $scope.formEmail = false;
            $("#form-confirm, #form-error").hide();
            backgroundImageAsCountry();
			preloadResForCities();
			preloadResForDuration();
        }
        else if(sectionName == "cities") {
            $scope.formCities = true;
            $scope.formDates = false;
            $scope.formDestination = false;
            $scope.formDuration = false;
            $scope.formCount = false;
            $scope.formBudget = false;
            $scope.formEmail = false;
            $("#form-confirm, #form-error").hide();
            backgroundImageAsCountry();
            citiesList();
			preloadResForDuration();
        }
        else if(sectionName == "duration") {
            displayDestination();
            $scope.formDuration = true;
            if (lsLabelcitiesSkipped !== null && typeof lsLabelcitiesSkipped !== "undefined" && lsLabelcitiesSkipped!== ""){
                if(lsLabelcitiesSkipped == "true"){
                    $scope.skippedCities = true;
                    $scope.unskippedCities = false;
                }
                else {
                    $scope.unskippedCities = true;
                    $scope.skippedCities = false;
                }
            } 
            $scope.formDates = false;
            $scope.formDestination = false;
            $scope.formCities = false;
            $scope.formCount = false;
            $scope.formBudget = false;
            $scope.formEmail = false;
            $("#form-confirm, #form-error").hide();
            if (lsLabelDestinationCode == 1){
                $("main").css('background', 'url(' + durationBG + ') center center / cover');
            }
			preloadResForCount();
        }
        else if(sectionName == "count") {
            displayDestination();
            displayCities();
            $scope.formCount = true;
            $scope.formDates = false;
            $scope.formDestination = false;
            $scope.formCities = false;
            $scope.formDuration = false;
            $scope.formBudget = false;
            $scope.formEmail = false;
            $("#form-confirm, #form-error").hide();
            if (lsLabelDestinationCode == 1){
                $("main").css('background', 'url(' + countBG + ') center center / cover');
            }
			preloadResForBudget();
        }
        else if(sectionName == "budget") {
            displayDestination();
            displayCities();
            $scope.formBudget = true;
            $scope.formDates = false;
            $scope.formDestination = false;
            $scope.formCities = false;
            $scope.formDuration = false;
            $scope.formCount = false;
            $scope.formEmail = false;
            $(" #form-confirm, #form-error").hide();
            if (lsLabelDestinationCode == 1){
                $("main").css('background', 'url(' + budgetBG + ') center center / cover');
            }
			preloadResForEmail();
        }
        else if(sectionName == "email") {
            displayDestination();
            displayCities();
            $scope.formEmail = true;
            $scope.formDates = false;
            $scope.formDestination = false;
            $scope.formCities = false;
            $scope.formDuration = false;
            $scope.formCount = false;
            $scope.formBudget = false;

            $("#form-confirm, #form-error").hide();
            if (lsLabelDestinationCode == 1){
                $("main").css('background', 'url(' + emailBG + ') center center / cover');
            }
            
        }
        else if(sectionName == "booking-complete") {
            $scope.formDates = false;
            $scope.formDestination = false;
            $scope.formCities = false;
            $scope.formDuration = false;
            $scope.formCount = false;
            $scope.formBudget = false;
            $scope.formEmail = true;
            $("#form-confirm, #form-error").hide();
            if (lsLabelDestinationCode == 1){
            }
        }
    }
}

$('.tb-hidden').css({"visibility" :"visible", "opacity" : 1});
});

