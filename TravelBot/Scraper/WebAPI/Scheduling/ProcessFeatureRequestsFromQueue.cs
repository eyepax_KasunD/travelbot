﻿using NLog;
using Scraper.Common.Models.Queue;
using Scraper.Services.Contracts;
using System;
using System.Messaging;
using TravelBot.Common;
using TravelBot.Common.Models.Queue;
using enums = TravelBot.Common.Enums;

namespace Scraper.WebAPI.Scheduling
{
    public class ProcessFeatureRequestsFromQueue
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static readonly IScraperService _scraperService = UnityConfig.Resolve<IScraperService>();
        private static readonly string _featureResponseQueueName = Constants.Queues.ScraperHotelDetailRspQueue; 

        public static void Start()
        {
            var _queue = new QueueProvider();
            logger.Info("STARTING SUBSCRIBER FOR FEATURE REQUEST");
            using (var Queue = new MessageQueue(Constants.Queues.ScraperHotelDetailReqQueue))
            {
                Queue.Formatter = new XmlMessageFormatter(new Type[] { typeof(FeatureRequestQueue) });
                while (true)
                {
                    logger.Info("INSIDE WHILE LOOP FEATURE REQUEST");
                    var message = Queue.Receive();
                    if (message == null)
                        continue;
                    logger.Info("FEATURE REQUEST FOUND !!!");
                    FeatureRequestQueue element = new FeatureRequestQueue();
                    element = (FeatureRequestQueue)message.Body;
                    var response = new HotelDetailRspQueueElement() { QueueRequestId = element.QueueRequestId };
                    try
                    {
                        logger.Info($"Scrape hotel, request id  :{ element.QueueRequestId},  hotel id : { element.Request.HotelId}");
                        var res = _scraperService.ScrapeHotelInfo(element.Request);
                        if (res.Errors.Count == 0)
                        {
                            response.SearchResponse = res;
                            response.ResponseStatus = enums.ResponseStatus.Completed;
                        }
                        else
                        {
                            throw new Exception(string.Join(Environment.NewLine, res.Errors));
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error("Url : " + element.Request.Hotelurl);
                        logger.Error("Ex : " + ex);
                        response.ResponseStatus = enums.ResponseStatus.Error;
                    }

                    _queue.PublishInQueue(response, _featureResponseQueueName); //publish to feature response queue
                }
            }
        }
    }
}