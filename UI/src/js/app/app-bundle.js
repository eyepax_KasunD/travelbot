var cultureCode, culture, destination, destinationCode, dates, travellingDatesCount, isFlexEnable, budget, variation,
    isVariationExceed, email, isEmailValid, lsLabelDepartureDate, lsLabelDestination, lsLabelTravellingDatesCount, defaultDestination,
    defaultFlexType, backgroundImages, CityInfo, countryIndex, cityIds, country, city, indexPageUrl, sectionName,
    lsCultureCode, lsCulture, lsLabelDestination, lsLabelDestinationCode, lsLabelCityIds, lsLabelDepartureDate, lsLabelTravellingDatesCount,
    isDaysCountWrong, lsLabelFlexibleDateRange, isFlexEnable, lsLabelFlexibleDateFullRange, lsLabelBudget, lsLabelBudgetWrong, lsLabelBudgetVariation, lsLabelVariationExceed,
    lsLabelEmail, lsLabelhotelDetailsResUrl, lsLabelBookingDeviceId, lsLabelReasonToStop, lsLabelReasonToStopComment, lsLabelPageSection,
    lsLabelselectedCityCount,lsLabelsortedCitiesAndDates, minPrice, maxPrice, minPriceRoundOff, maxPriceRoundOff, budgetRange,
    splitBudgetRange,ids,selectedCityCount, imageSet, countryCode, destinationDate, lsLabelAdultCount , lsLabelChildCount, ResponseArray= new Array(), lsLabelResponseArray,
    country, minPrice, maxPrice, checkingdate, checkoutdate, numOfDays, totalCitiesSelected, slider2, slider1, lsLabelcitiesSkipped, days, day,
    and, adult, adults, child, children, to, cityId0, cityId1, forText, countryThumbnail, ofText, more, less;


var app = angular.module('travelbotApp', []);

// main controller
app.controller('appCtrl', function($scope) {

    displayLocalStorageValues();

    //clean localstorage at index.cshtml
    if($('section#index').length)  {
        cleanLocalStorage();
    }
    // set section name to url in plan.cshtml
    else if($('section#trip-plan').length){
        if (lsLabelPageSection !== null && typeof lsLabelPageSection !== "undefined" && lsLabelPageSection !== ""){
            window.location.href =  lsLabelPageSection;
        }
    }

    // if (window.history && window.history.pushState) {
    //     window.history.pushState('forward', null);
    //     $(window).on('popstate', function() {
    //     //    currentPage();
    //     });

    // }
});

app.controller('headerCtrl', function($scope) {
    $scope.language = [
        {language: 'English', id: 'en', flag: flag0, langUrl: en},
        {language: 'Deutsche', id: 'de', flag: flag1, langUrl: de}
    ];

    // click function
    $scope.languageChange = function(language, id){
        $scope.languageText = language;
        $scope.languageId = id;

        localStorage.setItem("culture", language);
        localStorage.setItem("cultureCode", id);
    }

    // enable selected culture even after the page reloads
    if(lsCulture !== "" && typeof lsCulture !== "undefined" && lsCulture !== null) {
        $scope.languageText = lsCulture;
        $scope.languageId = lsCultureCode;
        //localStorage.setItem("destinationCode", "0"); // set default destination as Sri Lanka
    }
    else {
        $scope.languageText = $scope.language[0].language;
        $scope.languageId = $scope.language[0].id;
        localStorage.setItem("culture", $scope.language[0].language);
        localStorage.setItem("cultureCode", $scope.language[0].id);
    }

    // set default destination as Sri Lanka
    if(lsLabelDestinationCode == null || typeof lsLabelDestinationCode == "undefined" || lsLabelDestinationCode == "") {
        localStorage.setItem("destinationCode", 1);
    }
});

//------------------------------------------ common to trip plan form -------------------------------------------

//allow integers only
$(".days, .budget, .variation").on("keypress keyup blur",function (event) {
    $(this).val($(this).val().replace(/[^\d].+/, ""));
     if ((event.which < 48 || event.which > 57)) {
         event.preventDefault();
     }
 });

 // retrieve values from localstorage
function getValuesFromLocalStorage(){
    lsCultureCode = localStorage.getItem("cultureCode");
    lsCulture = localStorage.getItem("culture");
    lsLabelDestination = localStorage.getItem('destination');
    lsLabelDestinationCode = localStorage.getItem('destinationCode');
    //lsLabelCityIds = localStorage.getItem("CityIds");
    lsLabelselectedCityCount = localStorage.getItem("selectedCityCount");
    lsLabelsortedCitiesAndImages = JSON.parse(localStorage.getItem("selectedCitiesAndImages"));
    lsLabelsortedCitiesAndDates = JSON.parse(localStorage.getItem("sortedCitiesAndDates"));
    lsLabelDepartureDate = localStorage.getItem('departureDate');
    lsLabelTravellingDatesCount = localStorage.getItem('travellingDatesCount');
    isDaysCountWrong = localStorage.getItem('isDaysCountWrong');
    lsLabelFlexibleDateRange = localStorage.getItem('flexDatesRange');
    isFlexEnable = localStorage.getItem("flexEnable");
    lsLabelFlexibleDateFullRange = localStorage.getItem("flexDatesFullRange");
    lsLabelMinBudget = localStorage.getItem("minBudget");
    lsLabelMaxBudget = localStorage.getItem("maxBudget");
    lsLabelEmail = localStorage.getItem('email');
    lsLabelPackageDetailsChoice = localStorage.getItem("packageDetailsChoice");
    lsLabelhotelDetailsResUrl = localStorage.getItem("hotelDetailsResponseUrl");
    lsLabelBookingDeviceId = localStorage.getItem("bookingDeviceId");
    lsLabelReasonToStop = localStorage.getItem("reasonToStop");
    lsLabelReasonToStopComment = localStorage.getItem("reasonToStopComment");
    lsLabelPageSection = localStorage.getItem("pageSection");
    lsLabelAdultCount = localStorage.getItem("adultsCount");
    lsLabelChildCount = localStorage.getItem("childrenCount");
    lsLabeltotalDaysStay = localStorage.getItem("totalDaysStay");
    lsLabeltotalDaysStayAsArray = JSON.parse(localStorage.getItem("totalDaysStayInArray"));
    lsLabelcitiesSkipped = localStorage.getItem("citiesSkipped");
    lsLabelResponseArray = JSON.parse(localStorage.getItem("ResponseArray"));

}

// // need for plan.cshtml
function displayLocalStorageValues(){
    getValuesFromLocalStorage();

    // date format for story line
    concatOf = ofText;
    dateFormat = moment(lsLabelDepartureDate, "DD-MM-YYYY").format("YYYY-MM-DD");
    $(".departureDate").text(moment(dateFormat).format('Do ' +concatOf+ ' MMMM YYYY'));
    
    $(".destination").text(lsLabelDestination);
    //$(".budget").text('$'+ lsLabelBudget);
    $(".dates").val(lsLabelDepartureDate);

    // no.of adults and no.of children handle in story line
    if((typeof adult !== "undefined" && typeof adults !== "undefined") && (typeof child !== "undefined" && typeof children !== "undefined")){
        if(lsLabelAdultCount > 1) {
            adultText = adults;
        }
        else {
            adultText = adult;
        }
        
        if(lsLabelChildCount > 1) {
            childText = children;
            $(".count").text(lsLabelAdultCount+' '+adultText+ ' ' +and+ ' ' +lsLabelChildCount+' ' + childText);
        }
        else if(lsLabelChildCount == 1) {
            childText = child;
            $(".count").text(lsLabelAdultCount+' '+adultText+ ' ' +and+ ' ' +lsLabelChildCount+' ' + childText);
        }
        else {
            $(".count").text(lsLabelAdultCount+' '+adultText);
        }
    }

    // print budget values in story line
    if((lsLabelMinBudget !== null && typeof lsLabelMinBudget !== "undefined" && lsLabelMinBudget !== "") && (lsLabelMaxBudget !== null && typeof lsLabelMaxBudget !== "undefined" && lsLabelMaxBudget !== "")){
        $(".budget").text(lsLabelMinBudget+ ' ' +to+ ' ' +lsLabelMaxBudget);
    }
  
}

//remove from localstorage
function cleanLocalStorage(){
    localStorage.removeItem("destination");
    localStorage.removeItem("CityIds");
    localStorage.removeItem("selectedCityCount");
    localStorage.removeItem("selectedCitiesAndImages");
    localStorage.removeItem("sortedCitiesAndDates");
    localStorage.removeItem("totalDaysStayInArray");
    localStorage.removeItem("flexDatesRange");
    localStorage.removeItem("flexEnable");
    localStorage.removeItem("flexDatesFullRange");
    localStorage.removeItem("email");
    localStorage.removeItem("packageDetailsChoice");
    localStorage.removeItem("reasonToStop");
    localStorage.removeItem("reasonToStopComment");
    localStorage.removeItem("pageSection");
    localStorage.removeItem("adultsCount");
    localStorage.removeItem("childrenCount");
    localStorage.removeItem("citiesSkipped");
}
function cleanLocalStorageAtIndexPage(){
    localStorage.removeItem("departureDate");
    localStorage.removeItem("minBudget");
    localStorage.removeItem('maxBudget');
    localStorage.removeItem('totalDaysStay');
}

function cleanLocalStorageAtEmailPage(){
    localStorage.removeItem("ResponseArray");
    localStorage.removeItem("hotelDetailsResponseUrl");
    localStorage.removeItem("bookingDeviceId");
}

//add background color to header section when scroll up
$(function() {
    $(window).on("scroll", function() {
        if($(window).scrollTop() > 10) {
            $("header").addClass("header-wrapper");
            $("header .navbar-brand").css("border-bottom", "none");
        } else {
           $("header").removeClass("header-wrapper");
           $("header .navbar-brand").removeAttr("style");
        }
    });
});

function ResponseObject(country, minPrice, maxPrice, checkingdate,checkoutdate, numOfDays, currentDate, link) {
    this.country = country;
    this.minPrice = minPrice;
    this.maxPrice = maxPrice;
    this.checkingdate = checkingdate;
    this.checkoutdate = checkoutdate;
    this.numOfDays = numOfDays;
    this.currentDate = currentDate;
    this.link = link;
  }
