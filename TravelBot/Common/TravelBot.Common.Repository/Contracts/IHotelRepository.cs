﻿

using TravelBot.Common.Domain;

namespace TravelBot.Common.Repository.Contracts
{
    public interface IHotelRepository
    {
        Hotel AddHotel(Hotel p);
        Hotel GetHotelFromId(long id);
    }
}
