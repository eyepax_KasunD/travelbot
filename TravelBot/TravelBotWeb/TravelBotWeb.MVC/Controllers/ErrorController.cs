﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TravelBotWeb.Common;

namespace TravelBotWeb.Controllers
{
    public class ErrorController : BaseController
    {
        public ErrorController(IAppSettings appSettings) : base(appSettings)
        {
        }
        public ViewResult Index()
        {
            return View("Error");
        }
        public ViewResult NotFound()
        {
            Response.StatusCode = 404; 
            ViewBag.ErrorCode = 404;
            return View("Error");
        }
        public ViewResult InternalError()
        {
            Response.StatusCode = 500; 
            ViewBag.ErrorCode = Response.StatusCode;
            return View("Error");
        }
    }
}