﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TravelBot.Common;

namespace TravelBotWeb.Common
{
    public class StopSearchingModel
    {
        public Enums.StopSearchingReasons ReasonNo { get; set; }
        public string ReasonComment { get; set; }

    }
}