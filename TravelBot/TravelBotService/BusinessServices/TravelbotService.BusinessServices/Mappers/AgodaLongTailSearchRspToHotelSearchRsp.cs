﻿using System;
using System.Collections.Generic;
using System.Linq;
using TravelBot.Common.Mappers.Contracts;
using TravelBot.Common.Models;
using TravelBotService.Common;
using static TravelBot.Common.Enums;

namespace TravelbotService.BusinessServices.Mappers
{
    public class AgodaLongTailSearchRspToHotelSearchRsp : IMapper<Tuple<AgodaLongTailSearchRsp, HotelPriceSearchReq, SearchRspCriteriaCombination>, HotelPriceSearchRsp>
    {
        
        public HotelPriceSearchRsp Map(Tuple<AgodaLongTailSearchRsp, HotelPriceSearchReq, SearchRspCriteriaCombination> sourceObject)
        {
            var searchReq = sourceObject.Item2;
            var searchCombination = sourceObject.Item3;
            var noOfStays = (int)(searchCombination.CheckOut - searchCombination.CheckIn).TotalDays;
            var priceWrappers = new List<SearchRspCriteriaCombination>();

            foreach (var result in sourceObject.Item1.results)
            {
                var guestReview = new TravelBot.Common.Models.GuestReview
                {
                    ReviewCount = result.reviewCount,
                    ReviewScore = (Double)result.reviewScore

                };
                var hotelPrice = new HotelPrice
                {
                    Price = result.dailyRate * noOfStays,
                    HotelRef = result.hotelId.ToString(),
                    Source = DataSourceCode.Agoda,
                    URL = result.landingURL
                };
                var hotel = new Hotel
                {
                    Name = result.hotelName,
                    GuestReview = guestReview,
                    HotelRating = (Double)result.starRating,
                    ImgUrl = new List<string> { result.imageURL }
                };
                var hotelPriceWrapper = new SearchRspCriteriaCombination
                {
                    //Hotel = hotel,
                    HotelPrices = new List<HotelPrice> { hotelPrice }
                };
                priceWrappers.Add(hotelPriceWrapper);
            };

            var status = ResponseStatus.Completed;
            var hotelSearchRsp = new HotelPriceSearchRsp
            {
                SearchCriteria = new List<SearchRspCriterion>
                {
                    new SearchRspCriterion
                    {
                        CityId = searchReq.SearchCriteria.First().CityId,
                        SearchRspCriteriaCombinations = priceWrappers
                    }
                },
                Status =status                
            };
            return hotelSearchRsp;
        }
    }
}