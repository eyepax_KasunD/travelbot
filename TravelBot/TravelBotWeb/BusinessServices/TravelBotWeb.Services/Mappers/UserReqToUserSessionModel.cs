﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Mappers.Contracts;
using TravelBot.Common.Models;
using TravelBotWeb.Common;
using static TravelBot.Common.Enums;

namespace TravelBotWeb.Services.Mappers
{
    public class UserReqToUserSessionModel : IMapper<UserRequestModel, UserSessionModel>
    {
        public UserSessionModel Map(UserRequestModel sourceObject)
        {
            return new UserSessionModel()
            {
                SearchObjects = ProduceSearchingObjects(sourceObject),
                AdultCount = sourceObject.AdultCount,
                CheckInDate = sourceObject.CheckInDateTime,
                ChildCount = sourceObject.ChildCount,
                Email = sourceObject.Email,
                PriceFrom = sourceObject.PriceFrom,
                PriceTo = sourceObject.PriceTo,
                SearchSegmentList = GetSearchReqCriteria(sourceObject.Destinations, sourceObject.CountryId, sourceObject.CheckInDateTime),
                UserId = Guid.NewGuid().ToString(),
                RequestTime = DateTime.Now,
                FlexDatesPlus = Math.Abs(sourceObject.FlexDates),
                FlexDatesMinus = Math.Abs(sourceObject.FlexDates),
                EmailStatus = EmailStatus.Pending,
                EmailScheduleTime = DateTime.Now.AddHours(Common.CommonWeb.Constants.FirstEmailScheduleTimePeriod),
                DeviceId = sourceObject.DeviceId == null ? Guid.NewGuid().ToString() : sourceObject.DeviceId
            };
        }
        private List<SearchReqCriterion> GetSearchReqCriteria(List<CityDetails> destinations, int countryId, DateTime CheckInDate)
        {
            List<SearchReqCriterion> SearchSegmentList = new List<SearchReqCriterion>();
            DateTime Checkin = CheckInDate;
            foreach (var x in destinations)
            {
                SearchSegmentList.Add(new SearchReqCriterion() { CountryId = countryId, CityId = x.CityId.HasValue? x.CityId + 1 : null, CheckinDate = Checkin, CheckoutDate = Checkin.AddDays(x.Days) });
                Checkin = Checkin.AddDays(x.Days);
            }
            return SearchSegmentList;
        }
        private List<ContinuousSearch> ProduceSearchingObjects(UserRequestModel UserRequest)
        {
            var SearchObjects = new List<ContinuousSearch>();
            for (DateTime d = DateTime.Now; d <= UserRequest.CheckInDateTime; d = d.AddDays(1))
            {
                var SearchingObjectModel = new ContinuousSearch()
                {
                    RequestStatus = TravelBot.Common.Enums.RequestStatus.Pending,
                    RequestDate = d,
                    RequestId = Guid.NewGuid().ToString()
                };
                SearchObjects.Add(SearchingObjectModel);
            }
            return SearchObjects;
        }
    }
}
