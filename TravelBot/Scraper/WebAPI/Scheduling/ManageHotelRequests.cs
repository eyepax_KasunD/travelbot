﻿using NLog;
using Quartz;
using Scraper.Repository.Contracts;
using System;
using System.Linq;
using System.Threading.Tasks;
using TravelBot.Common;
using TravelBot.Common.Helpers;

namespace Scraper.WebAPI.Scheduling
{
    public class ManageHotelRequests : IJob
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private readonly IQueueRepository _queueRepository;

        public ManageHotelRequests(IQueueRepository queueRepository)
        {
            _queueRepository = queueRepository;
        }
        public async Task Execute(IJobExecutionContext context)
        {
            //logger.Info("error handling of proccessing hotel requests started");
            //var HotelFeatureRequests = _queueRepository.FindAllHotelRequestsfromStatus(TravelBot.Common.Enums.RequestStatus.Error, Constants.ErrorHandlingVariables.MaxRepetitions, Constants.ErrorHandlingVariables.ErrorCheckinghours);

            //try
            //{
            //    foreach (var HotelFeatureRequest in HotelFeatureRequests)
            //    {
            //        HotelFeatureRequest.RepetitionCount = HotelFeatureRequest.RepetitionCount + 1;

            //        if (HotelFeatureRequest.RepetitionCount > Constants.ErrorHandlingVariables.MaxRepetitions)
            //        {
            //            //TODO : handle error notification
            //            logger.Info($"Repetition count is more than max repetitions, request id : {HotelFeatureRequest.RequestId}, hotel id : {HotelFeatureRequest.HotelId}");
            //            var relatedSearch = _queueRepository.GetSearchElementFromQueue(x => x.SearchRequest.RequestId.Equals(HotelFeatureRequest.RequestId));
            //            var hotelPrices = relatedSearch.SearchResponse.SearchCriteria.First().SearchRspCriteriaCombinations;
            //            foreach(var price in hotelPrices)
            //            {
            //                price.HotelPrices = price.HotelPrices.Where(d => !HotelIdentifier.GetIdentifierFromUrl(d.URL).Equals(HotelFeatureRequest.Request.HotelId)).ToList();
            //            }
            //            relatedSearch.SearchResponse.SearchCriteria.First().SearchRspCriteriaCombinations = hotelPrices.Where(p => p.HotelPrices.Count > 0).ToList();
            //            _queueRepository.Update(relatedSearch);
            //            _queueRepository.DeleteFeatureRequest(HotelFeatureRequest);
            //        }
            //        else
            //        {
            //            logger.Info($"Hotel Request is redirected to the pending state, request id : {HotelFeatureRequest.RequestId}, hotel id : {HotelFeatureRequest.HotelId}");
            //            HotelFeatureRequest.Status = TravelBot.Common.Enums.RequestStatus.Pending;
            //            _queueRepository.Update(HotelFeatureRequest);
            //        }
            //    }
            //}
            //catch (Exception e)
            //{
            //    logger.Info("Error occured when handling hotel request errors :  " + e);
            //}
        }
    }
}