﻿using NLog;
using System.Collections.Generic;
using System.Web.Http;
using TravelBotService.Common;

namespace TravelBotService.MVC.Controllers.API
{
    public class BaseApiController: ApiController
    {
        protected static Logger logger = LogManager.GetCurrentClassLogger();

        public ApiResponseModel<T> GetApiResponseModel<T>(T data)
        {
            return new ApiResponseModel<T>
            {
                Data = data
            };
        }
        public ApiResponseModel<T> GetApiResponseModel<T>(T data, List<string> errors)
        {
            return new ApiResponseModel<T>
            {
                Data = data,
                Errors = errors
            };
        }
    }
}