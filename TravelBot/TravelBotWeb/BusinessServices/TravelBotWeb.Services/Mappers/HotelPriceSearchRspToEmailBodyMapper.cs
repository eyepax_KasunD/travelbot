﻿using System;
using System.Linq;
using TravelBot.Common.Mappers.Contracts;
using TravelBot.Common.Models;
using TravelBot.Common.Repository.Contracts;
using TravelBotWeb.Common;
using TravelBotWeb.Services.Contracts;
using TravelBotWeb.Services.Helpers;
using TravelBotWeb.Services.Models;

namespace TravelBotWeb.Services.Mappers
{
    public class HotelPriceSearchRspToEmailBodyMapper : IMapper<Tuple<HotelPriceSearchRsp, string, string>, EmailBodyContentModel>
    {
        private readonly IAppSettings _appSettings;
        private readonly ICityContentRepository _cityContentRepository; 
        private readonly ICountryContentRepository _countryContentRepository;
        private readonly IImageRepository _imageRepository;
        private readonly ICityRepository _cityRepository;
        private readonly ICountryRepository _countryRepository;
        public HotelPriceSearchRspToEmailBodyMapper(IAppSettings appSettings,
            ICityContentRepository cityContentRepository,
            ICountryRepository countryRepository,
            ICountryContentRepository countryContentRepository,
            IImageRepository imageRepository, 
            ICityRepository cityRepository)
        {
            _appSettings = appSettings;
            _cityContentRepository = cityContentRepository;
            _countryContentRepository = countryContentRepository;
            _imageRepository = imageRepository;
            _cityRepository = cityRepository;
            _countryRepository = countryRepository;
        }

        public EmailBodyContentModel Map(Tuple<HotelPriceSearchRsp, string, string> input)
        {
            var response = input.Item1;

            return new EmailBodyContentModel()
            {
                Icon = _appSettings.CDN_Domain + "/assets/images/travel-bot.png",
                App_Domain = _appSettings.APP_Domain,
                HotelCities = response.SearchCriteria.Select(s => MapCriteriaCmbToEmailHotelCities(s.SearchRspCriteriaCombinations.FirstOrDefault(cm => cm.CheckIn.Equals(s.CheckinDate)), s, input)).ToList()
            };
        }

        private EmailHotelCities MapCriteriaCmbToEmailHotelCities(SearchRspCriteriaCombination searchRspCriteriaCombination, SearchRspCriterion searchCriteria, Tuple<HotelPriceSearchRsp, string, string> input)
        {
            var rsp = input.Item1;
            var userSessionId = input.Item2;
            var requestId = input.Item3;

            return new EmailHotelCities()
            {
                CheckingDate = searchRspCriteriaCombination.CheckIn,
                CheckoutDate = searchRspCriteriaCombination.CheckOut,
                WeatherIcon = _appSettings.CDN_Domain + "/assets/images/weather-icon-1.png",
                HotelList = searchRspCriteriaCombination.HotelPrices.Where(h => !string.IsNullOrEmpty(h.HotelRef)).Take(10).Select(h => MapToHotelEmailViewModel(h, rsp, userSessionId, requestId)).ToList(),
                CityName = searchCriteria.CityId.HasValue? _cityContentRepository.FindCityContent((long)searchCriteria.CityId, 1).Name
                            : _countryContentRepository.FindCountryContentByLanguage((long)searchCriteria.CountryId, 1).Name, //1 for english TODO: must include in rsp
                ImgUrl =  _appSettings.CDN_Domain + (searchCriteria.CityId.HasValue ? _cityRepository.Find((int)searchCriteria.CityId).Thumbnail.URL
                            : _countryRepository.FindbyId((long)searchCriteria.CountryId).Thumbnail.URL),
                NoOfStays = (searchRspCriteriaCombination.CheckOut - searchRspCriteriaCombination.CheckIn).Days
            };
        }

        private HotelEmailViewModel MapToHotelEmailViewModel(HotelPrice hotelPrice, HotelPriceSearchRsp rsp, string userSessionId, string requestID)
        {
            var hotel = rsp.HotelList.Where(h => h.Id.Equals(hotelPrice.HotelRef)).FirstOrDefault();
            var datasource = rsp.DataSources.Where(h => h.Code == hotelPrice.Source).FirstOrDefault();
            return new HotelEmailViewModel
            {
                HotelLocation = hotel.Location.LocationString,
                HotelName = hotel.Name,
                BookingUrl = Helper.GetEncodedHotelLink(hotelPrice.HotelPriceId.ToString(), userSessionId, requestID, _appSettings.APP_Domain),
                HotelPrice = hotelPrice.Price,
                HotelRatingImgUrl = _appSettings.CDN_Domain + "/assets/images/" + hotel.HotelRating + "-star.png",
                HotelImage = hotel.ImgUrl.FirstOrDefault(),
                Review = MapReviews(hotel.GuestReview, datasource),
                //SourceIcon = datasource.Icon,
                TravelbotReview = MapReviews(hotel.GuestReview, datasource)
            };
        }

        private Review MapReviews(GuestReview guestReview, DataSource dataSource)
        {
            return new Review()
            {
                IconUrl = dataSource.Icon,
                NoofReviews = guestReview.ReviewCount,
                // ReviewText=guestReview.,
                Score = guestReview.ReviewScore
            };
        }
    }
}
