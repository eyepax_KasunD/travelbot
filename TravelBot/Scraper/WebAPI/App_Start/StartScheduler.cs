﻿
using Scraper.WebAPI.Scheduling;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(Scraper.WebAPI.App_Start.StartScheduler), nameof(Scraper.WebAPI.App_Start.StartScheduler.Start))]

namespace Scraper.WebAPI.App_Start
{

    public static class StartScheduler
    {
        /// <summary>
        /// Stars the scheduler.
        /// </summary>
        public static void Start()
        {
           // JobScheduler.Start();
        }
    }
}