﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelBotWeb.Common.CommonWeb
{
    public static class Constants
    {
        public static int MaxRepetitions = 3;
        public static int EmailScheduleTimePeriod =24 ; //hour
        public static int FirstEmailScheduleTimePeriod =0; //hour
        public static int ErrorCheckinghours = 0;
        public static int Offset = 1;
    }
}