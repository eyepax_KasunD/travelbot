﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TravelBotWeb.Common;

namespace TravelBotWeb.Controllers
{
    public class AdminController : BaseController
    {
        public AdminController(IAppSettings appsettings) : base(appsettings)
        {
        }

        // GET: Admin
        public ActionResult Index()
        {
            ViewBag.Module = "Admin";
            return View();
        }
    }
}