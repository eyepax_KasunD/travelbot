﻿using System;
using System.Linq;
using Quartz;
using System.Threading.Tasks;
using NLog;
using static TravelBot.Common.Enums;
using TravelBotWeb.Repository.Contracts;
using TravelBotWeb.Services.Contracts;
using TravelBotWeb.Common.CommonWeb;

namespace TravelBotWeb.Scheduling
{
    public class ProcessUserRequests : IJob
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private readonly IUserRepository _userRepository;
        private readonly IUserService uuserService;

        public ProcessUserRequests(IUserRepository userRepository, IUserService userService)
        {
            _userRepository = userRepository;
           
            uuserService = userService;
        }

        public async Task Execute(IJobExecutionContext context)
        {           
            IUserService _userservice = UnityConfig.Resolve<IUserService>();
            logger.Info("Reading pending user requests");
            var userSessions = _userRepository.FindNonClosedRequests(RequestStatus.Closed);
            foreach (var userSession in userSessions)
            {
                if (userSession != null)
                {
                    var searchingObject = userSession.SearchObjects.Where(s => s.RequestDate < DateTime.Now.AddHours(Constants.Offset)).OrderByDescending(s => s.RequestDate).FirstOrDefault();
                    if (searchingObject != null && searchingObject.RequestStatus == RequestStatus.Pending)
                    {
                        uuserService.ScrappingCallerForSearchingObjects(userSession, searchingObject.RequestId);
                    }
                }
            }
        }
       
    }
}