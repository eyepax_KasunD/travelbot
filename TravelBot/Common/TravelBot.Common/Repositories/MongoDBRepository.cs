﻿using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace TravelBot.Common.Repositories
{
    public class MongoDBRepository<T>
    {
        private static IMongoDatabase _database;
        private static IMongoCollection<T> _collection;
        private static string _databaseName;
        private static string _collectionName;
        private static string _connectionString;
        private static bool _initialized = false;
        public MongoDBRepository(string databseName, string collectioName)
        {
            if (!_initialized)
            {
                _databaseName = databseName;
                _collectionName = collectioName;
                _connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["mongodb_travelbot"].ConnectionString;
                if(!BsonClassMap.IsClassMapRegistered(typeof(T)))
                     BsonClassMap.RegisterClassMap<T>(map =>
                        {
                            map.AutoMap();
                            map.SetIgnoreExtraElements(true);
                        });
                _initialized = true;
            }
        }

        private static IMongoDatabase Database
        {
            get
            {
                return _database ?? (_database = GetOrCreateDatabase());
            }
        }

        private static IMongoCollection<T> Collection
        {
            get
            {
                return _collection ?? (_collection = GetOrCreateCollection());
            }
        }

        public void Insert(T item)
        {
            Collection.InsertOne(item);
        }

        public void Insert(List<T> items)
        {
            Collection.InsertMany(items);
        }

        public async void InsertAsync(T item)
        {
            await Collection.InsertOneAsync(item);
        }

        public async void InsertAsync(List<T> items)
        {
            await Collection.InsertManyAsync(items);
        }

        public List<T> Find(Expression<Func<T, bool>> predicate)
        {
            return Collection.Find(predicate).ToEnumerable().ToList();
        }

        public void Update(Expression<Func<T, bool>> predicate, T item)
        {
            Collection.ReplaceOne<T>(predicate, item);
        }

        public async void UpdateAsync(Expression<Func<T, bool>> predicate, T item)
        {
            await Collection.ReplaceOneAsync<T>(predicate, item);
        }

        public void Delete(Expression<Func<T, bool>> predicate)
        {
            Collection.DeleteOne<T>(predicate);
        }

        public async void DeleteAsync(Expression<Func<T, bool>> predicate)
        {
            await Collection.DeleteOneAsync<T>(predicate);
        }
        public void DeleteMany(Expression<Func<T, bool>> predicate)
        {
            Collection.DeleteMany<T>(predicate);
        }
        public void DeleteManyAsync(Expression<Func<T, bool>> predicate)
        {
            Collection.DeleteManyAsync<T>(predicate);
        }

        private static IMongoCollection<T> GetOrCreateCollection()
        {
            return Database.GetCollection<T>(_collectionName);
        }

        private static IMongoDatabase GetOrCreateDatabase()
        {
            var client = new MongoClient(_connectionString);
            return client.GetDatabase(_databaseName);  //Create DB if not exists
        }
    }
}
