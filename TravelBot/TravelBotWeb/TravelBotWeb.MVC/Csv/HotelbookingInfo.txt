Bathroom
Private bathroom
Toilet
Hairdryer
 
Bedroom
Wardrobe/Closet
 
View
City view
Sea view
 
Outdoors
Terrace
 
Pets
Pets are not allowed.

 
Media & Technology
Flat-screen TV
Satellite Channels
 
Food & Drink
Restaurant (� la carte)
Bar
Very good coffee!
 
Internet
Free! WiFi is available in all areas and is free of charge. 
 
Parking
No parking available.

 
Services
Shared lounge/TV area
Luggage storage
Laundry
24-hour front desk
Room service
 
General
Designated smoking area
Tile/Marble floor
Soundproofing
Safety deposit box
Lift
Fan
Ironing Facilities
Non-smoking rooms
Iron
Air conditioning