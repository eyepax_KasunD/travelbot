﻿using System;

namespace Scraper.Common.Models.Responses
{
    public class HotelPriceResponse
    {
        public string Source { get; set; }

        public DateTime Checkindate { get; set; }

        public DateTime Checkoutdate { get; set; }

        public double MinPricePernightPerRoom { get; set; }

        public double MinTotalPrice { get; set; }
    }
}