﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;
using TravelBot.Common.Models;

namespace TravelBotService.Common
{
    public class HotelPriceSearchReqWrapper
    {
        public HotelPriceSearchReq HotelPriceSearchReq { get; set; }
        public SearchResponse SearchResponse { get; set; }
    }
}
