﻿

using TravelBot.Common.Domain;

namespace TravelBot.Common.Repository.Contracts
{
    public interface IUserRepository
    {
        bool GetUser(string id);
        UserSession AddSession(UserSession p);
        UserSession FindSession(string usersessionId);
        UserSession UpdateSession(UserSession userSession);
    }
}
