﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelBot.Common.Models
{
    public class CityNameInfo
    {
        public string CityName { get; set; }
    }
}
