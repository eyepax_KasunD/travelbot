﻿namespace TravelbotService.BusinessServices.Contracts
{
    public interface IQueueProcessor<T>
    {
        void Process(T req);
    }
}
