﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TravelBot.Common.Models;

namespace TravelBot.Common.Domain
{
    [Table("Set_Hotel")]
    public class Hotel : ModifiableBaseEntity
    {
        public string Name { get; set; }
        public virtual ICollection<HotelSourceMap> HotelIds { get; set; }//class for HotelIds such as : public string Source, public string Id Ex:- "Agoda":"SomeID" ,"BookingCom":"SomeID"
        public double? HotelRating { get; set; }
        public virtual ICollection<GuestReview> GuestReviews { get; set; }
        public virtual ICollection<HotelImage> Images { get; set; }
        public bool IsApproved { get; set; }
        public bool IsEnabled { get; set; }
        public string Address { get; set; }

        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }
    }

    [Table("Trn_GuestReview")]
    public class GuestReview : ModifiableBaseEntity
    {
        public int ReviewCount { get; set; }
        public double ReviewScore { get; set; }
        public double RoomCleanliness { get; set; }
        public double ServiceStaff { get; set; }
        public double RoomComfort { get; set; }
        public double HotelCondition { get; set; }
        public long? DataSourceId { get; set; }
        public virtual DataSource DataSource { get; set; }
        public virtual Hotel Hotel { get; set; }

    }

    [Table("Set_HotelImage")]
    public class HotelImage : BaseEntity
    {
        public string URL { get; set; }
    }

    [Table("Set_HotelSourceMap")]
    public class HotelSourceMap
    {
        public string HotelIdFromSource { get; set; } //unique to source //coming from source
        [Key, Column(Order = 1)]
        public long? DataSourceId { get; set; }
        public virtual DataSource DataSource { get; set; }
        public virtual Hotel Hotel { get; set; }
        [Key, Column(Order = 0)]
        public long? HotelId { get; set; }
    }
}
