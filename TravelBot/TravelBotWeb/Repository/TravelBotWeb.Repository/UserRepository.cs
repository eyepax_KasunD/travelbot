﻿using TravelBot.Common.Repositories;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using TravelBotWeb.Repository.Contracts;
using TravelBotWeb.Common;

namespace TravelBotWeb.Repository
{
    public class UserRepository : IUserRepository
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private readonly MongoDBRepository<UserSessionModel> _userRepository;
        public UserRepository()
        {
            _userRepository = new MongoDBRepository<UserSessionModel>("User", "UserSessions");
        }

        public void AddUserRequest(UserSessionModel UserSession)
        {
            _userRepository.Insert(UserSession);
        }

        public UserSessionModel FindRequestFromId(string UserId)
        {
            return _userRepository.Find(x => x.UserId.Equals(UserId)).FirstOrDefault();
        }

        public UserSessionModel FindRequestFromStatus(TravelBot.Common.Enums.RequestStatus Status)
        {
            return _userRepository.Find(x =>x.SearchObjects.First().RequestStatus.Equals(Status)).FirstOrDefault();
        }

        public void Update(UserSessionModel UserSession)
        {
            _userRepository.Update(x => x.UserId.Equals(UserSession.UserId), UserSession);
        }

        public UserSessionModel FindRequestConsideringEmailSending(TravelBot.Common.Enums.EmailStatus emailStatus)
        {
            return _userRepository.Find(x => x.EmailStatus.Equals(emailStatus)).FirstOrDefault();
        }

        public List<UserSessionModel> FindAllrequestsfromStatus(TravelBot.Common.Enums.EmailStatus emailStatus, int maxRepetitions, int errorCheckinghours)
        {
            return _userRepository.Find(x => x.EmailStatus.Equals(emailStatus) && DateTime.Now.AddHours(-errorCheckinghours) > x.LoggedErrorTime  && x.RepetitionCount <= maxRepetitions).ToList();
        }
        public List<UserSessionModel> FindAllrequestsUsingCriteria(TravelBot.Common.Enums.EmailStatus emailStatus,DateTime date,bool _stopEmails)
        {
            return _userRepository.Find(x => x.EmailStatus.Equals(emailStatus) && x.EmailScheduleTime < date && x.StopEmails.Equals(_stopEmails)).ToList();
        }

        public UserSessionModel FindRequestFromSearchingObjectId(string SearchingObjectId)
        {
            return _userRepository.Find(x => x.SearchObjects.Any(y => y.RequestId.Contains(SearchingObjectId))).FirstOrDefault();
        }

        public List<UserSessionModel> FindNonClosedRequests(TravelBot.Common.Enums.RequestStatus status)
        {
            return _userRepository.Find(x => x.SearchObjects.Any(y => !y.RequestStatus.Equals(status)));
        }

        public List<UserSessionModel> FindExpiredRequests()
        {
            return _userRepository.Find(x => x.CheckInDate<DateTime.Now).ToList();
        }

    }
}