// <auto-generated />
namespace TravelBot.Common.Repository.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class movedCountryIdToSearchCriteria : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(movedCountryIdToSearchCriteria));
        
        string IMigrationMetadata.Id
        {
            get { return "201808181915551_movedCountryIdToSearchCriteria"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
