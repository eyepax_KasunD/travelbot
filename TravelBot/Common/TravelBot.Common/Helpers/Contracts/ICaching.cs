﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelBot.Common.Helpers.Contracts
{
    public interface ICaching
    {
        object Get(string key);

        T Get<T>(string key) where T : class;

        void Set(string key, object value, int ttl = 900, bool useCamelCaseResolver = true);

        void Invalidate(string key);
    }
}
