﻿using NLog;
using System.Collections.Generic;
using TravelBot.Common;
using TravelBot.Common.Helpers;
using TravelBot.Common.Models;
using TravelbotService.BusinessServices.Contracts;
using TravelbotService.BusinessServices.PriceSources.Contracts;
using TravelBotService.Common;
using TravelBotService.Repository.Contracts;

namespace TravelbotService.BusinessServices.PriceSources
{
    public class TravelBotScraper : IHotelPriceSource
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private readonly IHotelPriceApiRepository _hotelPriceRepository;
        private readonly IHotelSearchRequestMapper _hotelSearchRequestMapper;
        public TravelBotScraper(IHotelPriceApiRepository hotelPriceRepository, IHotelSearchRequestMapper hotelSearchRequestMapper)
        {
            _hotelPriceRepository = hotelPriceRepository;
            _hotelSearchRequestMapper = hotelSearchRequestMapper;
        }
        public HotelPriceSearchRsp GetHotelPriceResponse(HotelPriceSearchReqWrapper reqWrapper)
        {
            List<HotelPriceSearchRsp> ResponseList = new List<HotelPriceSearchRsp>();

            logger.Info("TravelBotScraper ::: getHotelSearchResponseFromScraper START ");

           _hotelSearchRequestMapper.SearchRequestMapper(reqWrapper).ForEach(s => ResponseList.Add(_hotelPriceRepository.CalltoScraper(s))); //Map HotelSearchRequest to Scraper model and give it to Scraper APi
 
            logger.Info("TravelBotScraper ::: getHotelSearchResponseFromScraper END ");

            return Helper.MergeHotelPriceSearchRspList(ResponseList);
        }
        public Enums.DataSourceCode GetDataSourceCode()
        {
            return Enums.DataSourceCode.Booking;
        }
    }
}
