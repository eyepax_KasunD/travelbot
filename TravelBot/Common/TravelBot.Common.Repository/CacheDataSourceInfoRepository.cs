﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;
using TravelBot.Common.Helpers.Contracts;
using TravelBot.Common.Repository.Contracts;

namespace TravelBot.Common.Repository
{
    public class CacheDataSourceInfoRepository : DataSourceInfoRepository, IDataSourceInfoRepository
    {
        private readonly ICaching _cache;
        public CacheDataSourceInfoRepository(ITravelbotDbContext context, ICaching cache) : base(context)
        {
            _cache = cache;
        }
        public HotelSourceMap Find(string id)
        {
            return base.Find(s => s.HotelId.Equals(id)).FirstOrDefault(); //TODO
        }
    }
}
