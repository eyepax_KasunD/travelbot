﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelBotWeb.Common
{
    public class ApiResponseModel<T>
    {
        public T Data { get; set; }

        public List<string> Errors { get; set; }
    }
}