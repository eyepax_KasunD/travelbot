﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static TravelBot.Common.Enums;

namespace TravelBot.Common.Models
{
    public class HotelPriceSearchRsp
    {
        public ResponseStatus Status { get; set; }
        public CurrencyCode Currency { get; set; }
        public string RequestId { get; set; }
        public List<string> Errors { get; set; }
        public List<string> Warnings { get; set; }
        public List<SearchRspCriterion> SearchCriteria { get; set; }
        public List<Hotel> HotelList { get; set; }
        public List<DataSource> DataSources { get; set; }
    }

    public class SearchRspCriterion
    {
        public DateTime CheckinDate { get; set; }
        public DateTime CheckoutDate { get; set; }
        public long? CountryId { get; set; }
        public long? CityId { get; set; }
        public decimal PriceFrom { get; set; }
        public decimal PriceTo { get; set; }
        public List<SearchRspCriteriaCombination> SearchRspCriteriaCombinations { get; set; }
    }
}
