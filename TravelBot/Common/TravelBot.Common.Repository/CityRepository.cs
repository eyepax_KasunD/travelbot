﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;
using TravelBot.Common.Repository.Contracts;

namespace TravelBot.Common.Repository
{
   public class CityRepository : Repository<City>, ICityRepository
    {
        public CityRepository(ITravelbotDbContext context) : base(context)
        {
        }
        public List<long> GetAllCityIds()
        {
         return Find().ToList().Select(p => p.Id).ToList();
        }
        public City Find(long cityid) {
            return Find(s => s.Id==cityid).FirstOrDefault();
        }
    }
}
