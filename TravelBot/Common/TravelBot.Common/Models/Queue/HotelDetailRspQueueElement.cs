﻿using System;
using TravelBot.Common.Models;
using static TravelBot.Common.Enums;

namespace TravelBot.Common.Models.Queue
{
    public class HotelDetailRspQueueElement
    {
        public long QueueRequestId { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        public ScraperHotelDetailRsp SearchResponse { get; set; }
    }
}