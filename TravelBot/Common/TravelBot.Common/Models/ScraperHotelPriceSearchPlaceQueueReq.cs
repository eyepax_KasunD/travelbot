﻿using System;
using System.Collections.Generic;

namespace TravelBot.Common.Models
{
    public class ScraperHotelPriceSearchPlaceQueueReq
    {
        public long RequestId { get; set; }
        public ScraperHotelPriceSearchReq ScraperHotelPriceSearchReq { get; set; }
        public List<ScraperSearchReqCriteriaCombination> SearchCriteriaCombinations { get; set; }
    }

    public class ScraperSearchReqCriteriaCombination
    {
        public DateTime CheckIn { get; set; }
        public int CombinationId { get; set; }
    }
}