﻿using TravelBot.Common.Helpers.Contracts;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;
using TravelBot.Common.Models;
using static TravelBot.Common.Enums;
using Scraper.Common;
using NLog;
using Scraper.Services.Contracts;
using Scraper.Common.Models.Responses;
using Scraper.Common.Models.Requests;
using TravelBot.Common.Helpers;

namespace Scraper.Services
{
    public class MockedScraperService : ScraperService, IScraperService
    {

        public MockedScraperService(ICaching cache, IScrapingSourceFactory IScrapingsourceFactory, IAppSettings appSettings) : base(cache, IScrapingsourceFactory, appSettings)
        {
        }

        public new HotelPriceSearchRsp SearchHotelPrices(ScraperHotelPriceSearchReq request)
        {
            var rspFromFile = GetObjFromXml<HotelPriceSearchRsp>();
            if(rspFromFile == null)
            {
                rspFromFile = base.SearchHotelPrices(request);
                WriteObjToXml(rspFromFile);
            }
            return rspFromFile;
        }

        public new ScraperHotelDetailRsp ScrapeHotelInfo(ScraperHotelDetailReq request)
        {
            var rspFromFile = GetObjFromXml<ScraperHotelDetailRsp>();
            if (rspFromFile == null)
            {
                rspFromFile = base.ScrapeHotelInfo(request);
                WriteObjToXml(rspFromFile);
            }
            return rspFromFile;
        }


        public new List<HotelPriceResponse> GetPriceWithFlexibility(HotelPriceRequest request)
        {
            var rspFromFile = GetObjFromXml<List<HotelPriceResponse>>();
            if (rspFromFile == null)
            {
                base.GetPriceWithFlexibility(request);
                WriteObjToXml(rspFromFile);
            }
            return rspFromFile;
        }

        private void WriteObjToXml<T>(T obj)
        {
            System.Xml.Serialization.XmlSerializer writer =
                new System.Xml.Serialization.XmlSerializer(typeof(T));

            var path = AppDomain.CurrentDomain.BaseDirectory + $"//{typeof(T)}_serialized.xml";
            System.IO.FileStream file = System.IO.File.Create(path);

            writer.Serialize(file, obj);
            file.Close();
        }

        private T GetObjFromXml<T>()
        {
            T obj = default(T);
            try
            {
                System.Xml.Serialization.XmlSerializer reader =
                    new System.Xml.Serialization.XmlSerializer(typeof(T));
                System.IO.StreamReader file = new System.IO.StreamReader(
                    AppDomain.CurrentDomain.BaseDirectory + $"//{typeof(T)}_serialized.xml");
                obj = (T)reader.Deserialize(file);
                file.Close();
            }
            catch (Exception)
            {

            }
            return obj;
        }
    }
}