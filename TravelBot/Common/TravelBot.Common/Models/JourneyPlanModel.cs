﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelBot.Common.Models
{
    public class JourneyPlanModel
    {
        //public int SortOrder { get; set; }
        //public  List<CityInfo> Cities { get; set; }
        //public  List<CountryNameInfo> CountryNames { get; set; }

        public string CountryName { get; set; }
        public List<ImageModel> CountryImages { get; set; }
        public string CountryThumbnailUrl { get; set; }
        public List<CityInfo> Cities { get; set; }
    }
}
