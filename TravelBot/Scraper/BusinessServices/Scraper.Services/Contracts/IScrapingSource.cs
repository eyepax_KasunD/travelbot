﻿using System.Collections.Generic;
using TravelBot.Common.Models;

namespace Scraper.Services.Contracts
{
    public interface IScrapingSource
    {
        HotelPriceSearchRsp GetPriceResponses(ScraperHotelPriceSearchReq request);
        ScraperHotelDetailRsp GetHotelFeatures(ScraperHotelDetailReq requestUrl);
        List<SearchRspCriteriaCombination> GetSearchRspCriteriaCombinations(string url, ScraperHotelPriceSearchReq request);
    }
}
