﻿using System.Collections.Generic;
using System.Linq;
using TravelBot.Common.Domain;
using TravelBot.Common.Repository.Contracts;

namespace TravelBot.Common.Repository
{
    public class CountryContentRepository : Repository<CountryContent>,ICountryContentRepository
    {
        public CountryContentRepository(ITravelbotDbContext context) : base(context)
        {
        }
        public List<long> GetId(string lang)
        {
            return Find(s => s.Language.Equals(lang)).ToList().Select(p => p.Id).ToList();     
        }
        public CountryContent FindCountryContentByLanguage(long countryId,int language)
        {
            return Find(s => s.Country.Id==countryId && s.Language.Id==language).FirstOrDefault();
        }
    }
}
