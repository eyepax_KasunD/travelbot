﻿

using TravelBot.Common.Domain;

namespace TravelBot.Common.Repository.Contracts
{
    public interface ILanguageRepository
    {
        Language Find(string languageTag);
    }
}
