﻿using AgodaHotelImporter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;

namespace AgodaHotelImporter
{
    public class AgodaHotelToHotelMapper
    {
        public Hotel Map(AgodaHotel agodaHotel)
        {
            return new Hotel
            {
                Address = agodaHotel.addressline1,
                GuestReviews = new List<GuestReview>()
                    { new GuestReview
                    {
                        ReviewCount = agodaHotel.number_of_reviews,
                        ReviewScore = agodaHotel.rating_average,
                        DataSourceId = 3
                    } },
                HotelIds = new List<HotelSourceMap>() { new HotelSourceMap { HotelIdFromSource = agodaHotel.hotel_id, DataSourceId = 3 } },
                HotelRating = agodaHotel.star_rating,
                Latitude = agodaHotel.latitude,
                Longitude = agodaHotel.longitude,
                Name = agodaHotel.hotel_name,
                Images = agodaHotel.Images.Select(s => { return new HotelImage { URL = s }; }).ToList(),
                IsApproved = true,
                IsEnabled = true
            };

        }
    }
}
