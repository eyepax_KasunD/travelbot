﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;
using TravelBot.Common.Repository.Contracts;

namespace TravelBot.Common.Repository
{
    public class HotelSourceMapRepository: Repository<HotelSourceMap>, IHotelSourceMapRepository
    {
        public HotelSourceMapRepository(ITravelbotDbContext context) : base(context)
        {
        }
        public long? GetHotelIds(string hotelIdFromSource, long? dataSourceId)
        {
            return base.Find(s => s.HotelIdFromSource.Equals(hotelIdFromSource) && s.DataSourceId == dataSourceId).FirstOrDefault()?.HotelId;
        }
        public HotelSourceMap Find(string id)
        {
            return base.Find(s => s.HotelId.Equals(id)).FirstOrDefault();
        }

        public bool IsExists(long dataSourceId, string hotelIdFromSource)
        {
            var res = base.Find(s => s.DataSourceId == dataSourceId && s.HotelIdFromSource.Equals(hotelIdFromSource));
            return res != null && res.Count() > 0; 
        }
    }
}
