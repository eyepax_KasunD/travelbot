﻿using System.Collections.Generic;
using System.Linq;
using TravelBot.Common.Domain;
using TravelBot.Common.Repository.Contracts;

namespace TravelBot.Common.Repository
{
    public class CountryRepository : Repository<Country>,ICountryRepository
    {
        public CountryRepository(ITravelbotDbContext context) : base(context)
        {
        }
        public override Country Add(Country p)
        {
            return base.Add(p);
        }
        public List<Country> GetAllEnabledCountries()
        {
            return Find(c => c.IsEnabled).ToList();    
        }
        public Country FindbyId(long id)
        {
            return Get(id);
        }
    }
}
