﻿using NLog;
using System;
using System.Messaging;
using TravelBot.Common;
using TravelbotService.BusinessServices.Contracts;

namespace TravelBotService.MVC.QueueListners
{
    public class UserPreferencesQueueListner
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static readonly IQueueProcessor<UserPreferences> _queueProcessor = UnityConfig.Resolve<IQueueProcessor<UserPreferences>>();

        public static void Start()
        {
            var Queue = new MessageQueue(Constants.Queues.UserPreferencesQueue)
            {
                Formatter = new XmlMessageFormatter(new Type[] { typeof(UserPreferences) })
            };
            while (true)
            {
                logger.Info("INSIDE WHILE LOOP USER PREFERENCES");
                var message = Queue.Receive();
                if (message == null)
                    continue;
                logger.Info("USER PREFERENCE FOUND !!!");
                UserPreferences element = new UserPreferences();
                element = (UserPreferences)message.Body;
                _queueProcessor.Process(element);
            }

        }
    }
}
