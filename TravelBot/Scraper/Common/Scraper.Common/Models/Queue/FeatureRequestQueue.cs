﻿using System;
using TravelBot.Common.Models;
using static TravelBot.Common.Enums;

namespace Scraper.Common.Models.Queue
{
    public class FeatureRequestQueue
    {
        public ScraperHotelDetailReq Request { get; set; }
        public long QueueRequestId { get; set; }
    }
}