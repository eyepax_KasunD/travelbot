$(".reasons input").on("change", function() {
    if( $(".radio-container input:checked").val() == "3") {
        $(".comment-of-stop").show();
    }
    else {
        $(".comment-of-stop").hide();
        $(".comment-of-stop").val("");
    }
});

$("#stopSearch").on("click", function(){
    localStorage.setItem("reasonToStop", $(".radio-container input:checked").val());
    localStorage.setItem("reasonToStopComment", $("textarea.comment-of-stop").val()); 
    displayLocalStorageValues();

    var stopSearch = {};
    stopSearch.reasonNo = lsLabelReasonToStop;
    stopSearch.reasonComment = lsLabelReasonToStopComment;

    $.ajax({
        type: "POST",
        url: stopSearchingAPI,
        data: JSON.stringify(stopSearch),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (res) {
            JSON.stringify(res);
            if ((res.Errors == null) && (res.Data !== null && res.Data !== "" && typeof res.Data !== "undefined")) {
                window.location.href = $('a.navbar-brand').attr("href");
            }
        }
    });
    return false;      
});