﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;
using TravelBot.Common.Repository.Contracts;

namespace TravelBot.Common.Repository
{
    public class DataSourceRepository : Repository<DataSource>, IDataSourceRepository
    {
        public DataSourceRepository(ITravelbotDbContext context) : base(context)
        {
        }
        public DataSource FindFromCode(string code)
        {
            return Find(s => s.Code.Equals(code)).FirstOrDefault();
        }

    }
}
