USE [travelbot-dev]
GO
SET IDENTITY_INSERT [dbo].[Sys_Image] ON 

GO
INSERT [dbo].[Sys_Image] ([Id], [URL], [CreateDateTime]) VALUES (1, N'/assets/images/cities/colombo.jpg', CAST(N'2018-05-25T09:24:28.407' AS DateTime))
GO
INSERT [dbo].[Sys_Image] ([Id], [URL], [CreateDateTime]) VALUES (2, N'/assets/images/cities/kandy.jpg', CAST(N'2018-05-25T09:24:28.407' AS DateTime))
GO
INSERT [dbo].[Sys_Image] ([Id], [URL], [CreateDateTime]) VALUES (3, N'/assets/images/cities/galle.jpg', CAST(N'2018-05-25T09:24:28.407' AS DateTime))
GO
INSERT [dbo].[Sys_Image] ([Id], [URL], [CreateDateTime]) VALUES (4, N'/assets/images/cities/frankfurt.jpg', CAST(N'2018-05-25T09:30:58.170' AS DateTime))
GO
INSERT [dbo].[Sys_Image] ([Id], [URL], [CreateDateTime]) VALUES (5, N'/assets/images/cities/munich.jpg', CAST(N'2018-05-25T09:30:58.170' AS DateTime))
GO
INSERT [dbo].[Sys_Image] ([Id], [URL], [CreateDateTime]) VALUES (6, N'/assets/images/cities/nuremberg.jpg', CAST(N'2018-05-25T09:30:58.170' AS DateTime))
GO
INSERT [dbo].[Sys_Image] ([Id], [URL], [CreateDateTime]) VALUES (7, N'/assets/images/cities/Colombo1.png', CAST(N'2018-05-25T09:30:58.170' AS DateTime))
GO
INSERT [dbo].[Sys_Image] ([Id], [URL], [CreateDateTime]) VALUES (8, N'/assets/images/cities/Colombo2.png', CAST(N'2018-05-25T09:30:58.170' AS DateTime))
GO
INSERT [dbo].[Sys_Image] ([Id], [URL], [CreateDateTime]) VALUES (9, N'/assets/images/cities/Colombo3.png', CAST(N'2018-05-25T09:30:58.170' AS DateTime))
GO
INSERT [dbo].[Sys_Image] ([Id], [URL], [CreateDateTime]) VALUES (10, N'/assets/images/cities/Colombo4.png', CAST(N'2018-05-25T09:30:58.170' AS DateTime))
GO
INSERT [dbo].[Sys_Image] ([Id], [URL], [CreateDateTime]) VALUES (11, N'/assets/images/cities/Galle1.png', CAST(N'2018-05-25T09:30:58.170' AS DateTime))
GO
INSERT [dbo].[Sys_Image] ([Id], [URL], [CreateDateTime]) VALUES (12, N'/assets/images/cities/Galle2.png', CAST(N'2018-05-25T09:30:58.170' AS DateTime))
GO
INSERT [dbo].[Sys_Image] ([Id], [URL], [CreateDateTime]) VALUES (13, N'/assets/images/cities/Galle3.png', CAST(N'2018-05-25T09:30:58.170' AS DateTime))
GO
INSERT [dbo].[Sys_Image] ([Id], [URL], [CreateDateTime]) VALUES (14, N'/assets/images/cities/Galle4.png', CAST(N'2018-05-25T09:30:58.170' AS DateTime))
GO
INSERT [dbo].[Sys_Image] ([Id], [URL], [CreateDateTime]) VALUES (15, N'/assets/images/cities/Kandy1.png', CAST(N'2018-05-25T09:30:58.170' AS DateTime))
GO
INSERT [dbo].[Sys_Image] ([Id], [URL], [CreateDateTime]) VALUES (16, N'/assets/images/cities/Kandy2.png', CAST(N'2018-05-25T09:30:58.170' AS DateTime))
GO
INSERT [dbo].[Sys_Image] ([Id], [URL], [CreateDateTime]) VALUES (17, N'/assets/images/cities/Kandy3.png', CAST(N'2018-05-25T09:30:58.170' AS DateTime))
GO
INSERT [dbo].[Sys_Image] ([Id], [URL], [CreateDateTime]) VALUES (18, N'/assets/images/cities/Kandy4.png', CAST(N'2018-05-25T09:30:58.170' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Sys_Image] OFF
GO
SET IDENTITY_INSERT [dbo].[Sys_Country] ON 

GO
INSERT [dbo].[Sys_Country] ([Id], [SortOrder], [IsEnabled], [CreateDateTime], [ThumbnailId]) VALUES (1, 0, 1, CAST(N'2018-05-25T09:24:28.400' AS DateTime), 1)
GO
INSERT [dbo].[Sys_Country] ([Id], [SortOrder], [IsEnabled], [CreateDateTime], [ThumbnailId]) VALUES (2, 1, 0, CAST(N'2018-05-25T09:30:58.170' AS DateTime), 2)
GO
SET IDENTITY_INSERT [dbo].[Sys_Country] OFF
GO
SET IDENTITY_INSERT [dbo].[Sys_City] ON 

GO
INSERT [dbo].[Sys_City] ([Id], [IsEnabled], [CreateDateTime], [Country_Id], [ThumbnailId]) VALUES (1, 1, CAST(N'2018-05-25T09:24:28.407' AS DateTime), 1, 1)
GO
INSERT [dbo].[Sys_City] ([Id], [IsEnabled], [CreateDateTime], [Country_Id], [ThumbnailId]) VALUES (2, 1, CAST(N'2018-05-25T09:24:28.407' AS DateTime), 1, 2)
GO
INSERT [dbo].[Sys_City] ([Id], [IsEnabled], [CreateDateTime], [Country_Id], [ThumbnailId]) VALUES (3, 1, CAST(N'2018-05-25T09:24:28.407' AS DateTime), 1, 3)
GO
INSERT [dbo].[Sys_City] ([Id], [IsEnabled], [CreateDateTime], [Country_Id], [ThumbnailId]) VALUES (4, 1, CAST(N'2018-05-25T09:30:58.170' AS DateTime), 2, 4)
GO
INSERT [dbo].[Sys_City] ([Id], [IsEnabled], [CreateDateTime], [Country_Id], [ThumbnailId]) VALUES (5, 1, CAST(N'2018-05-25T09:30:58.170' AS DateTime), 2, 5)
GO
INSERT [dbo].[Sys_City] ([Id], [IsEnabled], [CreateDateTime], [Country_Id], [ThumbnailId]) VALUES (6, 1, CAST(N'2018-05-25T09:30:58.170' AS DateTime), 2, 6)
GO
SET IDENTITY_INSERT [dbo].[Sys_City] OFF
GO
SET IDENTITY_INSERT [dbo].[Sys_DataSource] ON 

GO
INSERT [dbo].[Sys_DataSource] ([Id], [Code], [Name], [Icon], [CreateDateTime]) VALUES (1, N'Booking', N'Booking.com', N'https://s-ec.bstatic.com/static/img/b26logo/booking_logo_retina/22615963add19ac6b6d715a97c8d477e8b95b7ea.png', CAST(N'2018-05-25T09:24:28.407' AS DateTime))
GO
INSERT [dbo].[Sys_DataSource] ([Id], [Code], [Name], [Icon], [CreateDateTime]) VALUES (2, N'Expedia', N'Expedia.com', N'', CAST(N'2018-05-25T09:24:28.407' AS DateTime))
GO
INSERT [dbo].[Sys_DataSource] ([Id], [Code], [Name], [Icon], [CreateDateTime]) VALUES (3, N'Agoda', N'Agoda.com', N'', CAST(N'2018-05-25T09:24:28.407' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Sys_DataSource] OFF
GO
INSERT [dbo].[Set_SourceCityMap] ([CityId], [DataSourceId], [Value]) VALUES (1, 1, N'colombo, sri lanka')
GO
INSERT [dbo].[Set_SourceCityMap] ([CityId], [DataSourceId], [Value]) VALUES (1, 3, N'7835')
GO
INSERT [dbo].[Set_SourceCityMap] ([CityId], [DataSourceId], [Value]) VALUES (2, 1, N'kandy, sri lanka')
GO
INSERT [dbo].[Set_SourceCityMap] ([CityId], [DataSourceId], [Value]) VALUES (2, 3, N'7835')
GO
INSERT [dbo].[Set_SourceCityMap] ([CityId], [DataSourceId], [Value]) VALUES (3, 1, N'galle, sri lanka')
GO
INSERT [dbo].[Set_SourceCityMap] ([CityId], [DataSourceId], [Value]) VALUES (3, 3, N'7835')
GO
INSERT [dbo].[Set_SourceCityMap] ([CityId], [DataSourceId], [Value]) VALUES (4, 1, N'frankfurt, germany')
GO
INSERT [dbo].[Set_SourceCityMap] ([CityId], [DataSourceId], [Value]) VALUES (5, 1, N'munich, germany')
GO
INSERT [dbo].[Set_SourceCityMap] ([CityId], [DataSourceId], [Value]) VALUES (6, 1, N'nuremberg, germany')
GO
SET IDENTITY_INSERT [dbo].[Sys_Language] ON 

GO
INSERT [dbo].[Sys_Language] ([Id], [Tag], [Varient], [CreateDateTime]) VALUES (1, N'en', N'English', CAST(N'2018-05-25T09:24:28.410' AS DateTime))
GO
INSERT [dbo].[Sys_Language] ([Id], [Tag], [Varient], [CreateDateTime]) VALUES (2, N'de', N'German', CAST(N'2018-05-25T09:24:28.410' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Sys_Language] OFF
GO
SET IDENTITY_INSERT [dbo].[Sys_CityContent] ON 

GO
INSERT [dbo].[Sys_CityContent] ([Id], [Name], [Description], [CreateDateTime], [City_Id], [Language_Id]) VALUES (1, N'colombo', NULL, CAST(N'2018-05-25T09:24:28.410' AS DateTime), 1, 1)
GO
INSERT [dbo].[Sys_CityContent] ([Id], [Name], [Description], [CreateDateTime], [City_Id], [Language_Id]) VALUES (2, N'colombo', NULL, CAST(N'2018-05-25T09:24:28.410' AS DateTime), 1, 2)
GO
INSERT [dbo].[Sys_CityContent] ([Id], [Name], [Description], [CreateDateTime], [City_Id], [Language_Id]) VALUES (3, N'kandy', NULL, CAST(N'2018-05-25T09:24:28.410' AS DateTime), 2, 1)
GO
INSERT [dbo].[Sys_CityContent] ([Id], [Name], [Description], [CreateDateTime], [City_Id], [Language_Id]) VALUES (4, N'kandaee', NULL, CAST(N'2018-05-25T09:24:28.410' AS DateTime), 2, 2)
GO
INSERT [dbo].[Sys_CityContent] ([Id], [Name], [Description], [CreateDateTime], [City_Id], [Language_Id]) VALUES (5, N'galle', NULL, CAST(N'2018-05-25T09:24:28.410' AS DateTime), 3, 1)
GO
INSERT [dbo].[Sys_CityContent] ([Id], [Name], [Description], [CreateDateTime], [City_Id], [Language_Id]) VALUES (6, N'gaele', NULL, CAST(N'2018-05-25T09:24:28.410' AS DateTime), 3, 2)
GO
INSERT [dbo].[Sys_CityContent] ([Id], [Name], [Description], [CreateDateTime], [City_Id], [Language_Id]) VALUES (7, N'frankfurt', NULL, CAST(N'2018-05-25T09:30:58.170' AS DateTime), 4, 1)
GO
INSERT [dbo].[Sys_CityContent] ([Id], [Name], [Description], [CreateDateTime], [City_Id], [Language_Id]) VALUES (8, N'frankfurt', NULL, CAST(N'2018-05-25T09:30:58.173' AS DateTime), 4, 2)
GO
INSERT [dbo].[Sys_CityContent] ([Id], [Name], [Description], [CreateDateTime], [City_Id], [Language_Id]) VALUES (9, N'munich', NULL, CAST(N'2018-05-25T09:30:58.173' AS DateTime), 5, 1)
GO
INSERT [dbo].[Sys_CityContent] ([Id], [Name], [Description], [CreateDateTime], [City_Id], [Language_Id]) VALUES (10, N'München', NULL, CAST(N'2018-05-25T09:30:58.173' AS DateTime), 5, 2)
GO
INSERT [dbo].[Sys_CityContent] ([Id], [Name], [Description], [CreateDateTime], [City_Id], [Language_Id]) VALUES (11, N'nuremberg', NULL, CAST(N'2018-05-25T09:30:58.173' AS DateTime), 6, 1)
GO
INSERT [dbo].[Sys_CityContent] ([Id], [Name], [Description], [CreateDateTime], [City_Id], [Language_Id]) VALUES (12, N'Nürnberg', NULL, CAST(N'2018-05-25T09:30:58.173' AS DateTime), 6, 2)
GO
SET IDENTITY_INSERT [dbo].[Sys_CityContent] OFF
GO
SET IDENTITY_INSERT [dbo].[Sys_CityImage] ON 

GO
INSERT [dbo].[Sys_CityImage] ([Id], [CreateDateTime], [City_Id], [Image_Id]) VALUES (4, CAST(N'2018-05-25T09:30:58.170' AS DateTime), 4, 4)
GO
INSERT [dbo].[Sys_CityImage] ([Id], [CreateDateTime], [City_Id], [Image_Id]) VALUES (5, CAST(N'2018-05-25T09:30:58.170' AS DateTime), 5, 5)
GO
INSERT [dbo].[Sys_CityImage] ([Id], [CreateDateTime], [City_Id], [Image_Id]) VALUES (6, CAST(N'2018-05-25T09:30:58.170' AS DateTime), 6, 6)
GO
INSERT [dbo].[Sys_CityImage] ([Id], [CreateDateTime], [City_Id], [Image_Id]) VALUES (7, CAST(N'2018-05-25T09:30:58.170' AS DateTime), 1, 7)
GO
INSERT [dbo].[Sys_CityImage] ([Id], [CreateDateTime], [City_Id], [Image_Id]) VALUES (8, CAST(N'2018-05-25T09:30:58.170' AS DateTime), 1, 8)
GO
INSERT [dbo].[Sys_CityImage] ([Id], [CreateDateTime], [City_Id], [Image_Id]) VALUES (9, CAST(N'2018-05-25T09:30:58.170' AS DateTime), 1, 9)
GO
INSERT [dbo].[Sys_CityImage] ([Id], [CreateDateTime], [City_Id], [Image_Id]) VALUES (10, CAST(N'2018-05-25T09:30:58.170' AS DateTime), 1, 10)
GO
INSERT [dbo].[Sys_CityImage] ([Id], [CreateDateTime], [City_Id], [Image_Id]) VALUES (11, CAST(N'2018-05-25T09:30:58.170' AS DateTime), 2, 15)
GO
INSERT [dbo].[Sys_CityImage] ([Id], [CreateDateTime], [City_Id], [Image_Id]) VALUES (12, CAST(N'2018-05-25T09:30:58.170' AS DateTime), 2, 16)
GO
INSERT [dbo].[Sys_CityImage] ([Id], [CreateDateTime], [City_Id], [Image_Id]) VALUES (13, CAST(N'2018-05-25T09:30:58.170' AS DateTime), 2, 17)
GO
INSERT [dbo].[Sys_CityImage] ([Id], [CreateDateTime], [City_Id], [Image_Id]) VALUES (14, CAST(N'2018-05-25T09:30:58.170' AS DateTime), 2, 18)
GO
INSERT [dbo].[Sys_CityImage] ([Id], [CreateDateTime], [City_Id], [Image_Id]) VALUES (15, CAST(N'2018-05-25T09:30:58.170' AS DateTime), 3, 11)
GO
INSERT [dbo].[Sys_CityImage] ([Id], [CreateDateTime], [City_Id], [Image_Id]) VALUES (16, CAST(N'2018-05-25T09:30:58.170' AS DateTime), 3, 12)
GO
INSERT [dbo].[Sys_CityImage] ([Id], [CreateDateTime], [City_Id], [Image_Id]) VALUES (17, CAST(N'2018-05-25T09:30:58.170' AS DateTime), 3, 13)
GO
INSERT [dbo].[Sys_CityImage] ([Id], [CreateDateTime], [City_Id], [Image_Id]) VALUES (18, CAST(N'2018-05-25T09:30:58.170' AS DateTime), 3, 14)
GO
SET IDENTITY_INSERT [dbo].[Sys_CityImage] OFF
GO
SET IDENTITY_INSERT [dbo].[Sys_ImageContent] ON 

GO
INSERT [dbo].[Sys_ImageContent] ([Id], [Description], [CreateDateTime], [Image_Id], [Lanuguage_Id]) VALUES (1, N'Colombo is the main city in sri lanka', CAST(N'2018-08-03T09:21:47.367' AS DateTime), 1, 1)
GO
INSERT [dbo].[Sys_ImageContent] ([Id], [Description], [CreateDateTime], [Image_Id], [Lanuguage_Id]) VALUES (2, N'
Colombo ist die Hauptstadt in Sri Lanka', CAST(N'2018-08-03T09:23:34.700' AS DateTime), 1, 2)
GO
INSERT [dbo].[Sys_ImageContent] ([Id], [Description], [CreateDateTime], [Image_Id], [Lanuguage_Id]) VALUES (3, N'Kandy is the main city in sri lanka', CAST(N'2018-08-03T09:25:12.287' AS DateTime), 2, 1)
GO
INSERT [dbo].[Sys_ImageContent] ([Id], [Description], [CreateDateTime], [Image_Id], [Lanuguage_Id]) VALUES (4, N'
Kandy ist die Hauptstadt in Sri Lanka', CAST(N'2018-08-03T09:25:36.390' AS DateTime), 2, 2)
GO
INSERT [dbo].[Sys_ImageContent] ([Id], [Description], [CreateDateTime], [Image_Id], [Lanuguage_Id]) VALUES (5, N'Galle is the main city in sri lanka', CAST(N'2018-08-03T09:25:59.760' AS DateTime), 3, 1)
GO
INSERT [dbo].[Sys_ImageContent] ([Id], [Description], [CreateDateTime], [Image_Id], [Lanuguage_Id]) VALUES (6, N'
Galle ist die Hauptstadt in Sri Lanka', CAST(N'2018-08-03T09:26:22.167' AS DateTime), 3, 2)
GO
INSERT [dbo].[Sys_ImageContent] ([Id], [Description], [CreateDateTime], [Image_Id], [Lanuguage_Id]) VALUES (7, N'Frankfurt is the main city in germany', CAST(N'2018-08-03T09:26:57.370' AS DateTime), 4, 1)
GO
INSERT [dbo].[Sys_ImageContent] ([Id], [Description], [CreateDateTime], [Image_Id], [Lanuguage_Id]) VALUES (8, N'
Frankfurt ist die Hauptstadt in Deutschland', CAST(N'2018-08-03T09:27:04.073' AS DateTime), 4, 2)
GO
INSERT [dbo].[Sys_ImageContent] ([Id], [Description], [CreateDateTime], [Image_Id], [Lanuguage_Id]) VALUES (9, N'Munich is the main city in germany', CAST(N'2018-08-03T09:27:35.723' AS DateTime), 5, 1)
GO
INSERT [dbo].[Sys_ImageContent] ([Id], [Description], [CreateDateTime], [Image_Id], [Lanuguage_Id]) VALUES (10, N'
München ist die Hauptstadt in Deutschland', CAST(N'2018-08-03T09:27:41.113' AS DateTime), 5, 2)
GO
INSERT [dbo].[Sys_ImageContent] ([Id], [Description], [CreateDateTime], [Image_Id], [Lanuguage_Id]) VALUES (11, N'Nuremberg is the main city in germany', CAST(N'2018-08-03T09:28:00.353' AS DateTime), 6, 1)
GO
INSERT [dbo].[Sys_ImageContent] ([Id], [Description], [CreateDateTime], [Image_Id], [Lanuguage_Id]) VALUES (12, N'
Nürnberg ist die Hauptstadt in Deutschland', CAST(N'2018-08-03T09:28:19.883' AS DateTime), 6, 2)
GO
INSERT [dbo].[Sys_ImageContent] ([Id], [Description], [CreateDateTime], [Image_Id], [Lanuguage_Id]) VALUES (13, N'Colombo is the main city in sri lanka', CAST(N'2018-08-03T09:21:47.367' AS DateTime), 7, 1)
GO
INSERT [dbo].[Sys_ImageContent] ([Id], [Description], [CreateDateTime], [Image_Id], [Lanuguage_Id]) VALUES (14, N'Colombo is the main city in sri lanka', CAST(N'2018-08-03T09:21:47.367' AS DateTime), 8, 1)
GO
INSERT [dbo].[Sys_ImageContent] ([Id], [Description], [CreateDateTime], [Image_Id], [Lanuguage_Id]) VALUES (15, N'Colombo is the main city in sri lanka', CAST(N'2018-08-03T09:21:47.367' AS DateTime), 9, 1)
GO
INSERT [dbo].[Sys_ImageContent] ([Id], [Description], [CreateDateTime], [Image_Id], [Lanuguage_Id]) VALUES (16, N'Colombo is the main city in sri lanka', CAST(N'2018-08-03T09:21:47.367' AS DateTime), 10, 1)
GO
INSERT [dbo].[Sys_ImageContent] ([Id], [Description], [CreateDateTime], [Image_Id], [Lanuguage_Id]) VALUES (17, N'Colombo is the main city in sri lanka', CAST(N'2018-08-03T09:21:47.367' AS DateTime), 11, 1)
GO
INSERT [dbo].[Sys_ImageContent] ([Id], [Description], [CreateDateTime], [Image_Id], [Lanuguage_Id]) VALUES (18, N'Colombo is the main city in sri lanka', CAST(N'2018-08-03T09:21:47.367' AS DateTime), 12, 1)
GO
INSERT [dbo].[Sys_ImageContent] ([Id], [Description], [CreateDateTime], [Image_Id], [Lanuguage_Id]) VALUES (19, N'Colombo is the main city in sri lanka', CAST(N'2018-08-03T09:21:47.367' AS DateTime), 13, 1)
GO
INSERT [dbo].[Sys_ImageContent] ([Id], [Description], [CreateDateTime], [Image_Id], [Lanuguage_Id]) VALUES (20, N'Colombo is the main city in sri lanka', CAST(N'2018-08-03T09:21:47.367' AS DateTime), 14, 1)
GO
INSERT [dbo].[Sys_ImageContent] ([Id], [Description], [CreateDateTime], [Image_Id], [Lanuguage_Id]) VALUES (21, N'Colombo is the main city in sri lanka', CAST(N'2018-08-03T09:21:47.367' AS DateTime), 15, 1)
GO
INSERT [dbo].[Sys_ImageContent] ([Id], [Description], [CreateDateTime], [Image_Id], [Lanuguage_Id]) VALUES (22, N'Colombo is the main city in sri lanka', CAST(N'2018-08-03T09:21:47.367' AS DateTime), 16, 1)
GO
INSERT [dbo].[Sys_ImageContent] ([Id], [Description], [CreateDateTime], [Image_Id], [Lanuguage_Id]) VALUES (23, N'Colombo is the main city in sri lanka', CAST(N'2018-08-03T09:21:47.367' AS DateTime), 17, 1)
GO
INSERT [dbo].[Sys_ImageContent] ([Id], [Description], [CreateDateTime], [Image_Id], [Lanuguage_Id]) VALUES (24, N'Colombo is the main city in sri lanka', CAST(N'2018-08-03T09:21:47.367' AS DateTime), 18, 1)
GO
SET IDENTITY_INSERT [dbo].[Sys_ImageContent] OFF
GO
SET IDENTITY_INSERT [dbo].[Sys_CountryContent] ON 

GO
INSERT [dbo].[Sys_CountryContent] ([Id], [Name], [Description], [CreateDateTime], [Country_Id], [Language_Id]) VALUES (1, N'sri lanka', NULL, CAST(N'2018-05-25T09:24:28.413' AS DateTime), 1, 1)
GO
INSERT [dbo].[Sys_CountryContent] ([Id], [Name], [Description], [CreateDateTime], [Country_Id], [Language_Id]) VALUES (2, N'sei lanka', NULL, CAST(N'2018-05-25T09:24:28.413' AS DateTime), 1, 2)
GO
INSERT [dbo].[Sys_CountryContent] ([Id], [Name], [Description], [CreateDateTime], [Country_Id], [Language_Id]) VALUES (3, N'germany', NULL, CAST(N'2018-05-25T09:30:58.173' AS DateTime), 2, 1)
GO
INSERT [dbo].[Sys_CountryContent] ([Id], [Name], [Description], [CreateDateTime], [Country_Id], [Language_Id]) VALUES (4, N'Deutschland', NULL, CAST(N'2018-05-25T09:30:58.173' AS DateTime), 2, 2)
GO
SET IDENTITY_INSERT [dbo].[Sys_CountryContent] OFF
GO
INSERT [dbo].[Set_SourceCountryMap] ([CountryId], [DataSourceId], [Value]) VALUES (1, 1, N'sri lanka')
GO
INSERT [dbo].[Set_SourceCountryMap] ([CountryId], [DataSourceId], [Value]) VALUES (2, 1, N'german')
GO
SET IDENTITY_INSERT [dbo].[Sys_Currency] ON 

GO
INSERT [dbo].[Sys_Currency] ([Id], [Code], [Name], [CreateDateTime]) VALUES (1, N'USD', N'US Dollars', CAST(N'2018-05-25T09:24:28.407' AS DateTime))
GO
INSERT [dbo].[Sys_Currency] ([Id], [Code], [Name], [CreateDateTime]) VALUES (2, N'LKR', N'SL Rupees', CAST(N'2018-05-25T09:24:28.407' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Sys_Currency] OFF
GO
