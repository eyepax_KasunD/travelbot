﻿using System;
using System.Threading.Tasks;
using Quartz;
using NLog;
using static TravelBot.Common.Enums;
using TravelBotWeb.Services.Contracts;
using TravelBotWeb.Repository.Contracts;
using TravelBotWeb.Common.CommonWeb;
using System.Linq;

namespace TravelBotWeb.Scheduling
{
    public class ProcessUserEmails //: IJob
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private readonly IEmailService _emailService;
        private readonly IUserService _userService;
        private readonly IUserRepository _userRepository;


        public ProcessUserEmails(IEmailService emailService, IUserService userService, IUserRepository userRepository)
        {
            _userRepository = userRepository;
            _emailService = emailService;
            _userService = userService;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            logger.Info("start sending emails");
            var _UserSessions = _userRepository.FindAllrequestsUsingCriteria(EmailStatus.Pending, DateTime.Now, false);
            foreach (var userSession in _UserSessions)
            {
                if (userSession != null)
                {
                    var searchingObj = userSession.SearchObjects.Find(x => x.RequestStatus.Equals(TravelBot.Common.Enums.RequestStatus.Running));

                    if (searchingObj != null)
                    {
                        try
                        {
                            logger.Info("Sending Email to Session ID:" + userSession.UserId);
                            userSession.EmailStatus = EmailStatus.Running;
                            _userRepository.Update(userSession);

                            var res = _userService.RetrieveHotelPriceSearchRsp(userSession, searchingObj.RequestId);
                            if (res.Data.Status.Equals(ResponseStatus.Completed) && (res.Data?.SearchCriteria.Any(c => c.SearchRspCriteriaCombinations != null) ?? false))
                            {
                                userSession.EmailStatus = EmailStatus.Pending;
                                userSession.EmailScheduleTime = userSession.EmailScheduleTime.AddHours(Constants.EmailScheduleTimePeriod);
                                searchingObj.RequestStatus = RequestStatus.Closed;

                                if (!string.IsNullOrWhiteSpace(userSession.Email))
                                {
                                    var emailStatus = _emailService.EmailSending(res.Data, userSession, searchingObj.RequestId);

                                    if (emailStatus == EmailStatus.Error)
                                    {
                                        logger.Info("Error occured in email scheduler");
                                        userSession.EmailStatus = EmailStatus.Error;
                                        userSession.LoggedErrorTime = DateTime.Now;
                                    }
                                }
                            }
                            else
                            {
                                userSession.EmailStatus = EmailStatus.Pending;
                            }


                            _userRepository.Update(userSession);
                        }
                        catch (Exception e)
                        {
                            logger.Info("Error occured in email scheduler :  " + e);
                            userSession.EmailStatus = EmailStatus.Error;
                            userSession.LoggedErrorTime = DateTime.Now;
                            _userRepository.Update(userSession);

                        }
                    }
                }
            }
        }
    }
}