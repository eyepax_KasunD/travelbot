var tripStart;
app.controller("tripManagertCtrl", function($scope){
  displayLocalStorageValues();
  if(lsLabelResponseArray.length > 0){
    for(var obj=0; obj<lsLabelResponseArray.length;obj++)
    {
        var x=obj;
        trip =
          '<div class="trips col-lg-12 col-xl-6" id="test-'+obj+'">\n\
            <div class="tb-travel-box">\n\
              <div class="remove-item" onclick="RemoveObj('+obj+')"><span>x</span></div>\n\
              <a class="content" target="_blank">\n\
                  <h2 class="country"></h2>\n\
                  <div class="detail-container">\n\
                      <div class="location-container">\n\
                          <div class="location-thumb">\n\
                            <img src="" class="location-image" alt="picture">\n\
                          </div>\n\
                          <div class="location-description">\n\
                              <div class="hotel-price"><span class="minPrice"  style="margin-right:5px"></span><span class="to"  style="margin-right:5px"></span><span class="maxPrice"></span> </div>\n\
                              <div class="hotel-dates"><span class="checkingdate"  style="margin-right:5px"></span>-<label class="checkoutdate"  style="margin-right:5px"></label></div>\n\
                              <div class="hotel-starts"><span class="trip-start"></span> <span class="numOfDays" style="margin-right:5px"></span><span class="daysOrWeeks"></span></div>\n\
                          </div>\n\
                          <div class="calender">\n\
                              <div class="calnder-week">\n\
                                  <img src="" width="80px" height="90px"/>\n\
                                  <div class="calendar-days">\n\
                                    <span class="numOfDays"></span>\n\
                                    <span class="daysOrWeeks"></span>\n\
                                  </div>\n\
                              </div>\n\
                          </div>\n\
                      </div>\n\
                  </div>\n\
                </a>\n\
              </div>\n\
          </div>'
          
          $(".trip-containers .history").append(trip);
        
          $(".trips#test-" +obj).find(".country").text(lsLabelResponseArray[obj].country);
          $(".trips#test-" +obj).find(".minPrice").text(lsLabelResponseArray[obj].minPrice);
          $(".trips#test-" +obj).find(".maxPrice").text(lsLabelResponseArray[obj].maxPrice);
          $(".trips#test-" +obj).find(".to").text(to);
          $(".trips#test-" +obj).find(".trip-start").text(tripStart);         
          $(".trips#test-" +obj).find(".calnder-week img").attr("src", calendar);

          var checking = new Date(lsLabelResponseArray[obj].checkingdate);
          var checkout = new Date(lsLabelResponseArray[obj].checkoutdate);
          $(".trips#test-" +obj).find(".checkingdate").text(checking.getFullYear() + "." + (checking.getMonth()+1) + "." + checking.getDate());
          $(".trips#test-" +obj).find(".checkoutdate").text(checkout.getFullYear() + "." + (checkout.getMonth()+1) + "." + checkout.getDate());
          $(".trips#test-" +obj).find(".content").attr("href", lsLabelResponseArray[obj].link);

          if(lsCultureCode == "en"){
            $(".trips#test-" +obj).find(".location-image").attr("src", cityId0);
          }

          if(parseInt(lsLabelResponseArray[obj].numOfDays)<7)
          {
              $(".trips#test-" +obj).find(".numOfDays").text(lsLabelResponseArray[obj].numOfDays);
              $(".trips#test-" +obj).find(".daysOrWeeks").text("day(s)");
          }
          else
          {
            var numOfWeeks = Math.round(parseInt(lsLabelResponseArray[obj].numOfDays)/7);
            $(".trips#test-" +obj).find(".numOfDays").text(numOfWeeks);
            $(".trips#test-" +obj).find(".daysOrWeeks").text("week(s)");
          }
    }
  }
  else{
    $(".no-results-messgae").append('No Content').css("display","block");
  }
});


// remove
function RemoveObj(id){
  $('#test-'+id+'').remove();

  var response = JSON.parse(localStorage.getItem("ResponseArray"));

  // here you need to make a loop to find the index of item to delete
  var indexToRemove = id;

  //remove item selected, second parameter is the number of items to delete 
  response.splice(indexToRemove, 1);
  console.log(indexToRemove);
  console.log(response);

  // Put the object into storage
  localStorage.setItem('ResponseArray', JSON.stringify(response));
  location.reload();
}

// sort
(function( $ ) {
	$.sortByDate = function( elements, order ) {
    var arr = [];
		elements.each(function(index) {
			var obj = {},
				$el = $(this),
				date = new Date( $.trim(lsLabelResponseArray[index].currentDate)),
				timestamp = date.getTime();
				
				obj.html = $el[0].outerHTML;
				obj.date = timestamp;
				arr.push( obj );
		});
		
		var sorted = arr.sort(function( a, b ) {
			if( order == "ASC" ) {
				return a.date > b.date;
			} else {
				return b.date > a.date;
			}
		});	
		return sorted;
};
	
$(function() {
    var $newer = $("#newer"),
    $older = $("#older"),
    $content = $(".history"),
    $elements = $(".trips");
    newest(); // sort newest when page load

    $newer.click(function() {
      newest();
		});
			
	$older.click(function() {
		var elements = $.sortByDate($elements, "ASC");
		var html = "";
		for( var i = 0; i < elements.length; ++i ) {
		  html += elements[i].html;
		}
		$content[0].innerHTML = html;
		$( this ).addClass("selected").siblings().removeClass("selected");
		return false;				
    });

    function newest(){
          var elements = $.sortByDate($elements, "DESC");
          var html = "";
          for( var i = 0; i < elements.length; ++i ) {
            html += elements[i].html;
          }
          $content[0].innerHTML = html;
          $("#newer").addClass("selected").siblings().removeClass("selected");
          return false;	
    }
});
	
})( jQuery );  

