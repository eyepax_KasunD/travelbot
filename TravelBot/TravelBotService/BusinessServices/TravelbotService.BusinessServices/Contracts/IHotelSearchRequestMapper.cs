﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Models;
using TravelBotService.Common;

namespace TravelbotService.BusinessServices.Contracts
{
    public interface IHotelSearchRequestMapper
    {
        List<ScraperHotelPriceSearchPlaceQueueReq> SearchRequestMapper(HotelPriceSearchReqWrapper requestWrapper);
    }
}
