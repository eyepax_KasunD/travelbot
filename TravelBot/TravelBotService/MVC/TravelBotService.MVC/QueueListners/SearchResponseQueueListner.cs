﻿using NLog;
using System;
using System.Messaging;
using System.Threading.Tasks;
using TravelBot.Common;
using TravelBot.Common.Models;
using TravelBot.Common.Models.Queue;
using TravelbotService.BusinessServices.Contracts;

namespace TravelBotService.MVC.QueueListners
{
    public class SearchResponseQueueListner
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static readonly IQueueProcessor<HotelPriceSearchRspQueueElement> _queueProcessor = UnityConfig.Resolve<IQueueProcessor<HotelPriceSearchRspQueueElement>>();

        public static void Start()
        {
            var Queue = new MessageQueue(Constants.Queues.ScraperHotelPriceSearchRspQueue);
            Queue.Formatter = new XmlMessageFormatter(new Type[] { typeof(HotelPriceSearchRspQueueElement) });
            while (true)
            {
                logger.Info("INSIDE WHILE LOOP SEARCH RESPONSE");
                var message = Queue.Receive();
                if (message == null)
                    continue;
                logger.Info("SEARCH RESPONSE FOUND !!!");
                HotelPriceSearchRspQueueElement element = new HotelPriceSearchRspQueueElement();
                element = (HotelPriceSearchRspQueueElement)message.Body;
                _queueProcessor.Process(element);
            }         
        }
    }
}
