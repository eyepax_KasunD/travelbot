﻿using System.Collections.Generic;
using System.Web.Http;
using NLog;
using Scraper.Common.Models.Responses;

namespace Scraper.WebAPI.Controllers
{
    public class BaseApiController : ApiController
    {
        protected static Logger logger = LogManager.GetCurrentClassLogger();

        public ApiResponseModel<T> GetApiResponseModel<T>(T data)
        {
            return new ApiResponseModel<T>
            {
                Data = data               
            };
        }
        public ApiResponseModel<T> GetApiResponseModel<T>(T data, List<string> errors)
        {
            return new ApiResponseModel<T>
            {
                Data = data,
                Errors = errors
            };
        }

    }
}
