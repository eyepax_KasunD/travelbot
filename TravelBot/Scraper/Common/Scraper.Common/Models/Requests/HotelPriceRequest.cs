﻿
using static TravelBot.Common.Enums;

namespace Scraper.Common.Models.Requests
{
    public class HotelPriceRequest
    {
        public string RequestId { get; set; }

        public string Href { get; set; }

        public DataSourceCode Source { get; set; }

        public int HowManyDays { get; set; }

        public int FlexibilityPlus { get; set; }
      
        public int FlexibilityMinus { get; set; } 
    }
}