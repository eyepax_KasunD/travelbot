﻿using System;
using TravelBot.Common.Models;
using static TravelBot.Common.Enums;

namespace TravelBot.Common.Models.Queue
{
    public class HotelPriceSearchRspQueueElement
    {
        public long QueueRequestId { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        public ScraperHotelPriceSearchPlaceQueueReq QueueRequest { get; set; }
        public HotelPriceSearchRsp SearchResponse { get; set; }
    }
}