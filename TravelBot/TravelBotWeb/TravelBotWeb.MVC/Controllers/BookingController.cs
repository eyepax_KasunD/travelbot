﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TravelBotWeb.Common;

namespace TravelBotWeb.Controllers
{
    public class BookingController : BaseController
    {
        private readonly IAppSettings _appSettings;
        public BookingController(IAppSettings appSettings) : base(appSettings)
        {
            _appSettings = appSettings;
        }
        // GET: Booking
        [Route("Booking")]
        [OutputCache(Duration = int.MaxValue, VaryByParam = "none")]
        public ActionResult Index()
        {
            ViewBag.AffiliateId = _appSettings.Affiliate_ID;
            return View();
        }
    }

}