﻿using Scraper.Common;
using Scraper.Services.Contracts;
using static TravelBot.Common.Enums;

namespace Scraper.Services
{
    public class ScrapingSourceFactory: IScrapingSourceFactory
    {
        private readonly IAppSettings _appSettings;
        public ScrapingSourceFactory(IAppSettings appSettings)
        {
            _appSettings = appSettings;
        }
        public IScrapingSource Get(DataSourceCode website) {
            if (website == DataSourceCode.Booking)
                return new BookingCom(_appSettings);

            else if (website == DataSourceCode.Expedia)
                return new Expedia();
            else
                return null;

        }
    }
}