﻿using NLog;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using TravelBot.Common;
using TravelBot.Common.Domain;
using TravelBot.Common.Mappers.Contracts;
using TravelBot.Common.Models;
using TravelBot.Common.Repository.Contracts;
using TravelbotService.BusinessServices.Contracts;
using TravelbotService.BusinessServices.Factories.Contracts;
using TravelBotService.Common;

namespace TravelbotService.BusinessServices
{
    public class HotelPriceRetrieveService : IHotelPriceRetrieveService
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private readonly ISearchResponseRepository _searchResponseRepository;
        private readonly IHotelPricesFactory _hotelPriceFactory;
        private readonly IHotelSearchRequestMapper _hotelSearchRequestMapper;
        private readonly IMapper<HotelPriceSearchReq, SearchResponse> _searchResponseMapper;
        private readonly IHotelPriceResponseService _hotelPriceResponseService;
        static ConcurrentDictionary<string, object> LockObjects = new ConcurrentDictionary<string, object>();
        private readonly QueueProvider _queue;
        private readonly string _queueName = Constants.Queues.HotelPriceSearchReqQueue;

        public HotelPriceRetrieveService(IHotelPricesFactory hotelPriceFactory,
            IHotelSearchRequestMapper hotelSearchRequestMapper,
            ISearchResponseRepository searchResponseRepository,
            IMapper<HotelPriceSearchReq, SearchResponse> searchResponseMapper,
            IHotelPriceResponseService hotelPriceResponseService)
        {
            _hotelPriceFactory = hotelPriceFactory;
            _hotelSearchRequestMapper = hotelSearchRequestMapper;
            _searchResponseRepository = searchResponseRepository;
            _searchResponseMapper = searchResponseMapper;
            _hotelPriceResponseService = hotelPriceResponseService;
            _queue = new QueueProvider();
        }

        public HotelPriceSearchRsp GetPrices(HotelPriceSearchReq request)
        {
            HotelPriceSearchRsp priceResponse = null;
            var lockObject = LockObjects.GetOrAdd(request.RequestId, new object());
            lock (lockObject)
            {
                logger.Info("Hotel Price service / Get Prices");
                UpdatePriceSearchRequest(request);
                priceResponse = _hotelPriceResponseService.GetHotelPiceSearchRspFromDb(request.RequestId);
                if (priceResponse == null)
                {
                    var response = _searchResponseMapper.Map(request);
                    _searchResponseRepository.AddSearchResponse(response);
                     _queue.PublishInQueue(request, _queueName);
                    priceResponse = new HotelPriceSearchRsp { Status = Enums.ResponseStatus.Pending };
                }

                LockObjects.TryRemove(request.RequestId, out _);
            }
            return priceResponse;
        }

        private void UpdatePriceSearchRequest(HotelPriceSearchReq request)
        {
            var totalNoOfStays = request.SearchCriteria.Sum(s => (s.CheckoutDate - s.CheckinDate).Days);
            request.SearchCriteria.ForEach(s =>
            {
                var noOfDays = (s.CheckoutDate - s.CheckinDate).Days;                
                s.PriceFrom = request.PriceFrom * noOfDays / totalNoOfStays;
                s.PriceTo = request.PriceTo;
            });
        }
    }
}
