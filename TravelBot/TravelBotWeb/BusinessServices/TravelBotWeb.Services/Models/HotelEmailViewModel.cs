﻿using System;
using System.Collections.Generic;
using TravelBot.Common.Models;

namespace TravelBotWeb.Services.Models
{
    public class HotelSearchResponseEmail //starting...........
    {
        public List<string> To { get; set; }
        public List<string> Cc { get; set; }
        public List<string> Bcc { get; set; }
        public string Subject { get; set; }
        public EmailBodyContentModel EmailBodyContent { get; set; }
        //public List<HotelEmailViewModel> HotelList1 { get; set; }
        //public List<HotelEmailViewModel> HotelList2 { get; set; }
        //public string SessionId { get; set; }
        //public decimal PriceFrom { get; set; }
        //public decimal PriceTo { get; set; }
        //public string CheckinDate { get; set; }
        //public List<SearchReqCriterion> Place { get; set; }
    }

    public class EmailBodyContentModel
    {
        public string Country { get; set; }
        public string SessionId { get; set; }
        public string App_Domain { get; set; }
        public string TeamIcon { get; set; }
        public string Icon { get; set; }
        public decimal PriceFrom { get; set; }
        public decimal PriceTo { get; set; }
        public string ViewInWebUrl { get; set; }
        public string StopSearchUrl { get; set; }
                                           // public List<SearchReqCriterion> Place { get; set; } //?????
        public DateTime CheckinDate { get; set; }
        public List<EmailHotelCities> HotelCities { get; set; }        
    }
    public class EmailHotelCities
    {
        public string WeatherIcon { get; set; }
        public string CityName { get; set; }
        public DateTime CheckingDate { get; set; }
        public DateTime CheckoutDate { get; set; }
        public string ImgUrl { get; set; }
        public string CityOrder { get; set; }
        public int NoOfStays { get; set; }
        public List<HotelEmailViewModel> HotelList { get; set; }
    }

    public class HotelEmailViewModel
    {
        public string HotelImage { get; set; }
        public string HotelName { get; set; }
        public string BookingUrl { get; set; }
        public string HotelLocation { get; set; }
        public Review Review { get; set; }
        public string HotelRatingImgUrl { get; set; }
        public decimal HotelPrice { get; set; }
        public Review TravelbotReview { get; set; }
        public string SourceIcon { get; set; }
    }
}
