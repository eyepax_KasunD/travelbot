﻿using System.Linq;
using TravelBot.Common.Domain;
using TravelBot.Common.Repository.Contracts;

namespace TravelBot.Common.Repository
{
    public class LanguageRepository : Repository<Language>, ILanguageRepository
    {
        public LanguageRepository(ITravelbotDbContext context) : base(context)
        {
        }

        public Language Find(string languageTag)
        {
            return base.Find(l => l.Tag.Equals(languageTag)).FirstOrDefault();
        }
    }
}
