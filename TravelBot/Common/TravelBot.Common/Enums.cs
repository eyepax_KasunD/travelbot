﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelBot.Common
{
    public class Enums
    {
        //public static object ScrapingSources { get; set; }

        public enum RequestStatus
        {          
            Pending,
            Running,
            Ready,      
            Closed,
            Error
        }
        public enum ResponseStatus
        {
            Pending,
            Partial,
            Completed,
            Error
        }
        public enum DataSourceCode
        {
            Booking, //booking.com
            Expedia, //expedia.com
            Agoda, //agoda.com
        }
        public enum EmailStatus
        {
            Pending,
            Running,
            Success,
            Error
        }
        public enum StopSearchingReasons
        {
            Succeed = 1,
            Canceled,
            OtherPackage,
            Comment
        }

        public enum CurrencyCode
        {
            LKR,
            USD            
        }
    }
}
