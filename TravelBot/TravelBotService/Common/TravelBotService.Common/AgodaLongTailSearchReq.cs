﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelBotService.Common
{
    public class AgodaLongTailSearchReq
    {
        public Criteria criteria { get; set; } 
        
    }

    public class Criteria
    {
        public Additional additional { get; set; }
        public string checkInDate { get; set; }
        public string checkOutDate { get; set; }
        public int cityId { get; set; }
    }

    public class Additional
    {
        public string currency { get; set; }
        public DailyRate dailyRate { get; set; }
        public bool discountOnly { get; set; }
        public string language { get; set; }
        public int maxResult { get; set; }
        public int minimumReviewScore { get; set; }
        public int minimumStarRating { get; set; }
        public Occupancy occupancy { get; set; }
        public string sortBy { get; set; }
    }
    public class DailyRate
    {
        public decimal maximum { get; set; }
        public decimal minimum { get; set; }
    }
    public class Occupancy
    {
        public int numberOfAdult { get; set; }
        public int numberOfChildren { get; set; }
    }
}
