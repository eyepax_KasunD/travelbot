using Quartz.Unity;
using System;
using System.Collections.Generic;
using TravelBot.Common.Domain;
using TravelBot.Common.Mappers.Contracts;
using TravelBot.Common.Models;
using TravelBot.Common.Repository;
using TravelBot.Common.Repository.Contracts;
using TravelBotWeb.Common;
using TravelBotWeb.Repository;
using TravelBotWeb.Repository.Contracts;
using TravelBotWeb.Services;
using TravelBotWeb.Services.Contracts;
using TravelBotWeb.Services.Mappers;
using TravelBotWeb.Services.Models;
using Unity;
using Unity.Lifetime;

namespace TravelBotWeb
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => container.Value;
        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below.
            // Make sure to add a Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your type's mappings here.
            // container.RegisterType<IProductRepository, ProductRepository>();
            container.RegisterType<IAppSettings, AppSettings>();

            container.RegisterType<IEmailService, EmailService>();
            container.RegisterType<IUserService, UserService>();
            container.RegisterType<IEmailRepository, EmailRepository>(new ContainerControlledLifetimeManager());
            container.RegisterType<TravelBotWeb.Repository.Contracts.IUserRepository, TravelBotWeb.Repository.UserRepository>(new ContainerControlledLifetimeManager());
            container.AddNewExtension<QuartzUnityExtension>();

            //In TravelBotWeb.Repository in "3 Repository" in solution
            //var context = new TravelbotWebDbContext();

            container.RegisterType<ITravelbotDbContext, TravelbotDbContext>(new ContainerControlledLifetimeManager());
            container.RegisterType<ICountryRepository, CountryRepository>();

            container.RegisterType<ICountryContentRepository, CountryContentRepository>();

            container.RegisterType<IHotelRepository, HotelRepository>(); 
            container.RegisterType<IHotelPriceRepository, HotelPriceRepository>();
            container.RegisterType<TravelBot.Common.Repository.Contracts.IUserRepository, TravelBot.Common.Repository.UserRepository>();

            //In TravelBotWeb.Services in "2 Business Services" in solution
            container.RegisterType<IUserServiceSQL, UserServiceSQL>();
            container.RegisterType<ICountryService, CountryService>(); 
            container.RegisterType<IMapper<Tuple<HotelPriceSearchRsp, int, string, string>, List<CityHotelData>>, HotelPriceSearchRspToCityHotelDataList>();
            container.RegisterType<IHotelViewModelWebMapper, HotelViewModelWebMapper>(); 
            container.RegisterType<IJourneyPlanModelMapper, JourneyPlanModelMapper>();
            container.RegisterType<ILanguageRepository, LanguageRepository>();
            container.RegisterType<IMapper<UserRequestModel, UserSessionModel>, UserReqToUserSessionModel>();
            container.RegisterType<IMapper<UserSessionModel, UserSession>, UserSessionModelToUserSession>();

            container.RegisterType <ICurrencyRepository, CurrencyRepository>(); 
            container.RegisterType <IMapper<Tuple<HotelPriceSearchRsp, string, string>, EmailBodyContentModel>, HotelPriceSearchRspToEmailBodyMapper>(); 
            container.RegisterType <ICityContentRepository, CityContentRepository>(); 
            container.RegisterType <IImageRepository, ImageRepository>();
            container.RegisterType <ICityImageRepository, CityImageRepository>();
            container.RegisterType <ICityRepository, CityRepository>();
        }

        public static T Resolve<T>()
        {
            return container.Value.Resolve<T>();
        }
       
    }
}