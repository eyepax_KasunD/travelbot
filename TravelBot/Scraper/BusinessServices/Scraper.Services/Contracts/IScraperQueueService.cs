﻿using Scraper.Common.Models.Requests;
using Scraper.Common.Models.Responses;
using System.Collections.Generic;
using TravelBot.Common.Models;

namespace Scraper.Services.Contracts
{
    public interface IScraperQueueService
    {
        HotelPriceSearchRsp PlaceHotelPriceSearchReq(ScraperHotelPriceSearchPlaceQueueReq request);
        bool PlaceHotelDetailReq(ScraperHotelDetailPlaceQueueReq request);
    }
}
