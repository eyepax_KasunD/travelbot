﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;
using TravelBot.Common.Repository.Contracts;

namespace TravelBot.Common.Repository
{
    public class UnfilteredHotelPriceRepository : Repository<UnfilteredHotelPrice>, IUnfilteredHotelPriceRepository
    {
        public UnfilteredHotelPriceRepository(ITravelbotDbContext context) : base(context)
        {
        }
        public UnfilteredHotelPrice Add(UnfilteredHotelPrice hotelPrice)
        {
            return base.Add(hotelPrice);
        }

        public List<UnfilteredHotelPrice> Find(Expression<Func<UnfilteredHotelPrice, bool>> func)
        {
            return base.Find(func).ToList();
        }
    }
}
