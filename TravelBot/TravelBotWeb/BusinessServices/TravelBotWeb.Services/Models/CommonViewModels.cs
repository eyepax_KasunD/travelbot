﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelBotWeb.Services
{
    public class Review
    {
        public double Score { get; set; }
        public int NoofReviews { get; set; }
        public string ReviewText { get; set; }
        public string IconUrl { get; set; }
    }
    public class HotelPricingViewModel
    {
        public string CheckInDate { get; set; }
        public string Price { get; set; }
        public string HotelUrl { get; set; }

    }
}