﻿using NLog;
using System;
using System.Linq;
using TravelBot.Common;
using TravelBotWeb.Services.Contracts;
using TravelBotWeb.Common;
using TravelBot.Common.Repository.Contracts;
using TravelBot.Common.Models;
using TravelbotService.BusinessServices.Mappers;
using TravelBot.Common.Domain;
using TravelBot.Common.Mappers.Contracts;
using TravelBotWeb.Services.Mappers;

namespace TravelBotWeb.Services
{
    public class UserServiceSQL : IUserServiceSQL
    {
        private readonly TravelBot.Common.Repository.Contracts.IUserRepository _userRepository;
        private readonly IHotelRepository _hotelRepository;
        private readonly HotelMapper _hotelmapper;
        private readonly UserSessionMapper _usersession;
        private readonly IHotelPriceRepository _hotelPricerepo;
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private readonly IMapper<UserSessionModel, UserSession> _userSessionModelToUserSessionMapper;

        public UserServiceSQL(TravelBot.Common.Repository.Contracts.IUserRepository userRepository, IHotelRepository hotelRepository,IHotelPriceRepository hotelPriceRepository, IMapper<UserSessionModel, UserSession> userSessionModelToUserSessionMapper)
        {
            _userRepository = userRepository;
            _hotelRepository = hotelRepository;
            _hotelPricerepo = hotelPriceRepository;
            _hotelmapper = new HotelMapper();
            _usersession = new UserSessionMapper();
            _userSessionModelToUserSessionMapper = userSessionModelToUserSessionMapper;
        }

        public string AddUser(UserSessionModel userSessionModel)
        {
          try
           {
                var session= _userRepository.AddSession(_userSessionModelToUserSessionMapper.Map(userSessionModel));
                logger.Info("Adding UserSession to Sql server ID ");

                //        //First Add Hotels Then Add Session
                //        var mappedHotels = usersessionmodel.SearchObjects.Where(s => s.HotelSearchResponse != null).SelectMany(s => s.HotelSearchResponse.HotelPriceWrappers.Select(p => _hotelmapper.GetHotel(p.Hotel))).ToList();
                //        int a = 1;
                //        foreach (var hotel in mappedHotels)
                //        {
                //            a++;
                //            if (a > 20)
                //            {

                //            }
                //            _hotelRepository.AddHotel(hotel);

                //        }
                //        //Adding Session
                //  _userRepository.AddSession(_usersession.MapUserSessionModel(usersessionmodel));
                //        logger.Info("Adding UserSession to Sql server ID: " + usersessionmodel.UserId);
                return session.Id;
            }
            catch (Exception ex)
            {
                logger.Info("Adding UserSession failed: " + ex);
                return "";
            }
        }


        public bool UpdateUser(string userSessionId, string requestId, int travelbotIndex)
        {
            try
            {
               // var userSession = _userRepository.FindSession(userSessionId);
               // var searchRequest = userSession?.SearchRequests.Where(r => true).FirstOrDefault();
               // var hotelPrice = searchRequest?.HotelPrices.Where(e => e.TravelbotIndex.Equals(travelbotIndex)).FirstOrDefault();
               // hotelPrice.IsClicked = true;
               //// _hotelPricerepo.UpdateSession(hotelPrice);
               // logger.Info("Updating UserSession to Sql server ID: " + userSessionId);
                return true;
            }
            catch (Exception)
            {
                logger.Info("Updating UserSession failed: " + userSessionId);
                return false;
            }
        }

        public bool GetUser(string userSessionId)
        {
            var result = _userRepository.GetUser(userSessionId);
            logger.Info("Checking UserSession: " + result);
            return result;
        }
        public bool StopSearch(string userSessionId, StopSearchingModel model)
        {
            try
            {
                var userSession = _userRepository.FindSession(userSessionId);
                if(userSession != null)
                {
                    userSession.StopSearch = model.ReasonNo != Enums.StopSearchingReasons.Comment ? model.ReasonNo.ToString() : model.ReasonComment;
                    _userRepository.UpdateSession(userSession);
                    logger.Info("Updated UserSession to Sql server with user session Id: " + userSessionId);
                    return true;
                }
                else
                {
                    return false;
                }  

            }
            catch (Exception e)
            {
                logger.Info("User session updating failed to Sql server with exception : " + e);
                return false;
            }
        }

        
       
    }
}
