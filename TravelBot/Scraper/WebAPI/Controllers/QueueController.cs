﻿using TravelBot.Common.Models;
using Scraper.WebAPI.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Unity.Attributes;
using Newtonsoft.Json;
using System.Messaging;
using System;
using Scraper.Services.Contracts;
using Scraper.Common.Models.Responses;
using Scraper.Common.Models.Requests;
using Scraper.Common.Models.Queue;

namespace Scraper.WebAPI.Controllers
{
    public class QueueController : BaseApiController
    {
        private readonly IScraperQueueService _scraperQueueService;

        public QueueController(IScraperQueueService scraperQueueService) //give ScraperQueueService object
        {
            _scraperQueueService = scraperQueueService;    
        }
        [HttpPost]
        public ApiResponseModel<HotelPriceSearchRsp> PlaceHotelPriceSearchReq([FromBody]ScraperHotelPriceSearchPlaceQueueReq requestModel)
        {
            logger.Info("ScraperHotelPriceSearchPlaceQueueReq recieved");
            var response = _scraperQueueService.PlaceHotelPriceSearchReq(requestModel);
            return GetApiResponseModel(response);
        }

        [HttpPost]
        public ApiResponseModel<bool> PlaceHotelDetailReq([FromBody]ScraperHotelDetailPlaceQueueReq requestModel)
        {
            logger.Info("ScraperHotelDetailPlaceQueueReq recieved");
            var responses = _scraperQueueService.PlaceHotelDetailReq(requestModel);
            return GetApiResponseModel(responses);
        }
    }
}
