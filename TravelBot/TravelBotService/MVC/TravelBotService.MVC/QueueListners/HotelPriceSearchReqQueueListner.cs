﻿using NLog;
using System;
using System.Messaging;
using System.Threading.Tasks;
using TravelBot.Common;
using TravelBot.Common.Domain;
using TravelBot.Common.Mappers.Contracts;
using TravelBot.Common.Models;
using TravelBot.Common.Repository.Contracts;
using TravelbotService.BusinessServices.Contracts;
using TravelbotService.BusinessServices.Factories.Contracts;
using TravelBotService.Common;

namespace TravelBotService.MVC.QueueListners
{
    public class HotelPriceSearchReqQueueListner
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static readonly IQueueProcessor<HotelPriceSearchReq> _queueProcessor = UnityConfig.Resolve<IQueueProcessor<HotelPriceSearchReq>>();
       public static void Start()
        {
            var Queue = new MessageQueue(Constants.Queues.HotelPriceSearchReqQueue)
            {
                Formatter = new XmlMessageFormatter(new Type[] { typeof(HotelPriceSearchReq) })
            };
            while (true)
            {
                logger.Info("INSIDE WHILE LOOP hotel price search requests");
                var message = Queue.Receive();
                if (message == null)
                    continue;
                logger.Info("Hotel Price Search Request FOUND !!!");
                HotelPriceSearchReq request = (HotelPriceSearchReq)message.Body;
                _queueProcessor.Process(request);
            }

        }
    }
}
