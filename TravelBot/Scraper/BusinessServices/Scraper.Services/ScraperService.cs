﻿using TravelBot.Common.Helpers.Contracts;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;
using TravelBot.Common.Models;
using static TravelBot.Common.Enums;
using Scraper.Common;
using NLog;
using Scraper.Services.Contracts;
using Scraper.Common.Models.Responses;
using Scraper.Common.Models.Requests;
using TravelBot.Common.Helpers;

namespace Scraper.Services
{
    public class ScraperService : IScraperService
    {
        //private readonly IHotelRepository _hotelRepository;
        private readonly ICaching _cache;
        private readonly IScrapingSourceFactory _scrapingsourceFactory;
        private const int _cacheTtl = 60 * 60 * 24; // cahe for 1 day
        private const string _cacheKeyPrefix = "Scraper.Services_";
        private readonly IAppSettings _appconfig;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public ScraperService(ICaching cache, IScrapingSourceFactory IScrapingsourceFactory, IAppSettings appSettings)
        {
            _cache = cache;
            _scrapingsourceFactory = IScrapingsourceFactory;
            _appconfig = appSettings;
        }

        public HotelPriceSearchRsp SearchHotelPrices(ScraperHotelPriceSearchReq request)
        {
            return _scrapingsourceFactory.Get(request.ScrapingSource).GetPriceResponses(request);
        }

        public ScraperHotelDetailRsp ScrapeHotelInfo(ScraperHotelDetailReq request)
        {
            ScraperHotelDetailRsp hotelResponse = _scrapingsourceFactory.Get(request.Website).GetHotelFeatures(request);
            //HotelResponse hotelResponse = _cache.Get<HotelResponse>(request.HotelId);
            //if (hotelResponse == null)
            //{
            //    hotelResponse = new HotelResponse
            //    {
            //        Hotel = _hotelRepository.FindHotelFromRequest(request)
            //    };
            //    var expired = CheckDateExpiration(hotelResponse.Hotel?.LastUpdatedDate);
            //    if (hotelResponse.Hotel == null || expired)
            //    {
            //        hotelResponse = _scrapingsourceFactory.Get(request.Website).GetHotelFeatures(request);

            //       // hotelResponse.Hotel.HotelIds = new List<string>() { request.HotelId }; //TODO 
            //        hotelResponse.Hotel.LastUpdatedDate = DateTime.Now;
            //        if (hotelResponse.Hotel != null && hotelResponse.Errors.Count == 0)
            //        {
            //            if (!expired)
            //            {
            //                _hotelRepository.AddHotel(hotelResponse.Hotel);
            //            }
            //            else
            //            {
            //                _hotelRepository.Update(request, hotelResponse.Hotel);
            //            }
            //            _cache.Set(request.HotelId, hotelResponse, _cacheTtl);
            //        }
            //    }
            //    else
            //    {
            //        hotelResponse.CachedResponse = true;
            //    }
            //}
            //else
            //{
            //    hotelResponse.CachedResponse = true;
            //}

            return hotelResponse;
        }

        private bool CheckDateExpiration(DateTime? lastUpdatedDate)
        {           
            var limit = _appconfig.HotelDataExpirationTime;
            var days = (DateTime.Now - lastUpdatedDate)?.TotalDays;
            return days > limit;
        }

        public List<HotelPriceResponse> GetPriceWithFlexibility(HotelPriceRequest request)
        {
            var cacheKey = _cacheKeyPrefix + request.Source + request.RequestId;
            List<HotelPriceResponse> cachedObjectData = _cache.Get<List<HotelPriceResponse>>(cacheKey);
            if (cachedObjectData == null)
            {
                if (request.Source == DataSourceCode.Expedia)
                {
                    var exp = new Expedia();
                    cachedObjectData = exp.GetMinPrice(request);
                }
                else if (request.Source == DataSourceCode.Booking)
                {
                    var bookingCom = new BookingCom(_appconfig);
                    cachedObjectData = bookingCom.GetMinPrice(request);
                }

                if (cachedObjectData != null)
                    _cache.Set(cacheKey, cachedObjectData, _cacheTtl);
            }
            return cachedObjectData;
        }
    }
}