﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace TravelBotWeb
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                name: "Default-language",
                url: "{lang}",
                defaults: new { controller = "Home", action = "Index", lang = "en" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}",
                defaults: new { controller = "Home", action = "Index", lang = "en" }
            );

            routes.MapRoute(
                name: "DefaultLocalized",
                url: "{lang}/{controller}/{action}",
                defaults: new
                {
                    controller = "Home",
                    action = "Index",
                    lang = "en"
                });

        }
    }
}
