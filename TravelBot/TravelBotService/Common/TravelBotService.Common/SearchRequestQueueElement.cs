﻿using System;
using TravelBot.Common.Models;

namespace TravelBotService.Common
{
    public class SearchRequestQueueElement
    {
        //[BsonId]
        //[BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public HotelPriceSearchReq SearchRequest { get; set; }
        public HotelPriceSearchRsp SearchResponse { get; set; }
        public DateTime LastUpdatedDateTime { get; set; }
        public int ErrorRepetitionCount { get; set; }
    }
}
