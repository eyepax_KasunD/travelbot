﻿using System.Data.Entity;
using TravelBot.Common.Domain;

namespace TravelBot.Common.Repository.Contracts
{
    public interface ITravelbotDbContext : IDbContextCommon
    {
        DbSet<UserSession> UserSessions { get; set; }
        DbSet<UnfilteredHotelPrice> HotelPrices { get; set; }
        DbSet<Hotel> Hotels { get; set; }
        DbSet<GuestReview> GuestReviews { get; set; }
        DbSet<Country> Countries { get; set; }
        DbSet<City> Cities { get; set; }
        DbSet<CityContent> CityNames { get; set; }
        DbSet<CountryContent> CountryNames { get; set; }
        DbSet<UserClick> UserClick { get; set; }
        DbSet<HotelPriceSearchQueue> HotelPriceSearchQueue { get; set; }
        DbSet<HotelDetailQueue> HotelDetailQueue { get; set; }
        DbSet<SourceCityMap> SourceCityMap { get; set; }
        DbSet<SearchStatus> SeachStatus { get; set; }


        /// <summary>
        ///     Gets the get database context.
        /// </summary>
        DbContext GetDatabaseContext { get; }
    }
}
