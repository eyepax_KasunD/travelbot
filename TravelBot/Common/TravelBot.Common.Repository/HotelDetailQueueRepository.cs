﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;
using TravelBot.Common.Repository.Contracts;

namespace TravelBot.Common.Repository
{
    public class HotelDetailQueueRepository : Repository<HotelDetailQueue>, IHotelDetailQueueRepository
    {
        public HotelDetailQueueRepository(ITravelbotDbContext context) : base(context)
        {
        }

        public IEnumerable<HotelDetailQueue> Find(Expression<Func<HotelDetailQueue, bool>> filter)
        {
            return base.Find(filter);
        }

        public List<HotelDetailQueue> GetElementsBySource(long dataSourceId, string hotelIdFromSource)
        {
            return base.Find(s => s.DataSourceId == dataSourceId && s.HotelIdFromSource.Equals(hotelIdFromSource)).ToList();
        }

        public bool IsExistInQueue(long dataSourceId, string hotelIdFromSource)
        {
            var res = base.Find(s => s.DataSourceId == dataSourceId && s.HotelIdFromSource.Equals(hotelIdFromSource));
            return res != null && res.Count() > 0;
        }

        public bool IsRemainingResponsesExists(long responseId, long dataSourceId)
        {
            var res = base.Find(q => q.SearchResponseId == responseId && q.DataSourceId == dataSourceId && q.ResponseRecieved == false && q.ErrorOccurred == false);
            return res != null && res.Count() > 0;
        }
    }
}
