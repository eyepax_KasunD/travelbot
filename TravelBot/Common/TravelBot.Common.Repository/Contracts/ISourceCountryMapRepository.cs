﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;

namespace TravelBot.Common.Repository.Contracts
{
    public interface ISourceCountryMapRepository
    {
        SourceCountryMap GetCountry(long countryId, long datasourceId);
    }
}
