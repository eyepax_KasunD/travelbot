﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;
using TravelBot.Common.Repository.Contracts;

namespace TravelBot.Common.Repository
{
    public class CurrencyRepository : Repository<Currency>,ICurrencyRepository
    {
        public CurrencyRepository(ITravelbotDbContext context) : base(context)
        {
        }
        public List<Currency> GetCurrency(Enums.CurrencyCode code)
        {
            return Find(e => e.Code.Equals(code.ToString())).ToList();
        }
        public Currency GetCurrencyByCode(Enums.CurrencyCode code)
        {
            return Find(e => e.Code.Equals(code.ToString())).FirstOrDefault();
        }
    }
}
