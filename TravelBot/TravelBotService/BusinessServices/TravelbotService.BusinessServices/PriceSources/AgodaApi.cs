﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TravelBot.Common;
using TravelBot.Common.Domain;
using TravelBot.Common.Mappers.Contracts;
using TravelBot.Common.Models;
using TravelBot.Common.Repository.Contracts;
using TravelbotService.BusinessServices.Mappers;
using TravelbotService.BusinessServices.PriceSources.Contracts;
using TravelBotService.Common;
using TravelBotService.Repository.Contracts;

namespace TravelbotService.BusinessServices.PriceSources
{
    public class AgodaApi : IHotelPriceSource
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private readonly IHotelPriceApiRepository _hotelPriceApiRepository;
        private readonly IUnfilteredHotelPriceRepository _unfilteredHotelPriceRepository;
        private readonly IMapper<Tuple<AgodaLongTailSearchRsp, HotelPriceSearchReq, SearchRspCriteriaCombination>, HotelPriceSearchRsp> _agodaLongTailSearchRspToHotelSearchRsp;
        private readonly IMapper<HotelPriceSearchRsp, SearchResponse> _hotelPriceSearchRspToSearchRspMapper;
        private readonly ISearchStatusRepository _searchStatusRepository;
        private readonly IDataSourceRepository _dataSourceRepository;
        private readonly IMapper<Tuple<HotelPriceSearchReq, SearchRspCriteriaCombination, SearchReqCriterion>, AgodaLongTailSearchReq> _hotelSearchReqToAgodaLongTailSearchReq;

        public AgodaApi(IHotelPriceApiRepository hotelPriceApiRepository,
            IDataSourceRepository dataSourceRepository,
            IMapper<Tuple<AgodaLongTailSearchRsp, HotelPriceSearchReq, SearchRspCriteriaCombination>, HotelPriceSearchRsp> agodaLongTailSearchRspToHotelSearchRsp,
            IMapper<Tuple<HotelPriceSearchReq, SearchRspCriteriaCombination, SearchReqCriterion>, AgodaLongTailSearchReq> hotelSearchReqToAgodaLongTailSearchReq,
            IMapper<HotelPriceSearchRsp, SearchResponse> hotelPriceSearchRspToSearchRspMapper, IUnfilteredHotelPriceRepository unfilteredHotelPriceRepository,
            ISearchStatusRepository searchStatusRepository)
        {
            _hotelPriceApiRepository = hotelPriceApiRepository;
            _agodaLongTailSearchRspToHotelSearchRsp = agodaLongTailSearchRspToHotelSearchRsp;
            _hotelSearchReqToAgodaLongTailSearchReq = hotelSearchReqToAgodaLongTailSearchReq;
            _hotelPriceSearchRspToSearchRspMapper = hotelPriceSearchRspToSearchRspMapper;
            _unfilteredHotelPriceRepository = unfilteredHotelPriceRepository;
            _searchStatusRepository = searchStatusRepository;
            _dataSourceRepository = dataSourceRepository;
        }

        public Enums.DataSourceCode GetDataSourceCode()
        {
            return Enums.DataSourceCode.Agoda;
        }

        public HotelPriceSearchRsp GetHotelPriceResponse(HotelPriceSearchReqWrapper reqWrapper)
        {
            //Call to Agoda Api and get the results
            logger.Info("AgodaApi ::: getHotelSearchResponseFromScraper started");

            var partialRes = new HotelPriceSearchRsp
            {
                SearchCriteria = reqWrapper.HotelPriceSearchReq.SearchCriteria.Select(s =>
                {
                    List<DateTime> checkingdates = new List<DateTime>();
                    for (var i = -Math.Abs(reqWrapper.HotelPriceSearchReq.FlexDatesMinus); i <= reqWrapper.HotelPriceSearchReq.FlexDatesPlus; i++)
                    {
                        checkingdates.Add(s.CheckinDate.AddDays(i));
                    }

                    return new SearchRspCriterion
                    {
                        CheckinDate = s.CheckinDate,
                        CheckoutDate = s.CheckoutDate,
                        CityId = s.CityId,
                        CountryId = s.CountryId,
                        PriceFrom = s.PriceFrom,
                        PriceTo = s.PriceTo,
                        SearchRspCriteriaCombinations = checkingdates.Select(c =>
                        {
                            var cmb = new SearchRspCriteriaCombination
                            {
                                CheckIn = c,
                                CheckOut = c.AddDays(s.NoOfStays)
                            };
                            var SearchReqCriteria = reqWrapper.HotelPriceSearchReq.SearchCriteria.Where(w => w.CityId.Equals(s.CityId)).FirstOrDefault();
                            var agodaLongTailSearchReq = _hotelSearchReqToAgodaLongTailSearchReq.Map(new Tuple<HotelPriceSearchReq, SearchRspCriteriaCombination, SearchReqCriterion>(reqWrapper.HotelPriceSearchReq, cmb, SearchReqCriteria));
                            var agodaLongTailSearchRsp = _hotelPriceApiRepository.CallAgodaAPi(agodaLongTailSearchReq);
                            var res = _agodaLongTailSearchRspToHotelSearchRsp.Map(new Tuple<AgodaLongTailSearchRsp, HotelPriceSearchReq, SearchRspCriteriaCombination>(agodaLongTailSearchRsp, reqWrapper.HotelPriceSearchReq, cmb));
                            cmb.HotelPrices = res.SearchCriteria.SelectMany(sc => sc.SearchRspCriteriaCombinations.SelectMany(scmb => scmb.HotelPrices)).ToList();
                            return cmb;
                        }).ToList(),
                    };
                }).ToList()
            };

            //save to DB
            var searchResponse = _hotelPriceSearchRspToSearchRspMapper.Map(partialRes); //with Ids

            MapRSPCriteriaCombinationIds(searchResponse, reqWrapper.SearchResponse);

            var unfilterePrices = searchResponse.SearchCriteria.SelectMany(s => s.searchCriteriaCombinations.SelectMany(p => p.UnfilteredHotelPrices)).ToList(); //Save to DB

            _unfilteredHotelPriceRepository.AddAll(unfilterePrices);

            var dataSource = _dataSourceRepository.FindFromCode(Enums.DataSourceCode.Agoda.ToString());
            var searchStatus = _searchStatusRepository.GetSearchStatus(reqWrapper.SearchResponse.Id, dataSource.Id);
            searchStatus.Status = (int)Enums.ResponseStatus.Completed;
            _searchStatusRepository.Update(searchStatus);

            logger.Info("AgodaApi ::: getHotelSearchResponseFromScraper finished");
            return partialRes;

        }

        private void MapRSPCriteriaCombinationIds(SearchResponse searchRspFromAgoda, SearchResponse initialSearchRsp)
        {
            foreach (var initialSearchCriteria in initialSearchRsp.SearchCriteria) //reqWrapper.SearchResponse has no Ids// unfiltered hotel prices null
            {
                foreach (var agodaSearchCriteria in searchRspFromAgoda.SearchCriteria)
                {
                    if (agodaSearchCriteria.CityId == initialSearchCriteria.CityId)
                    {
                        foreach (var initialCmb in initialSearchCriteria.searchCriteriaCombinations)
                        {
                            foreach (var agodaCmb in agodaSearchCriteria.searchCriteriaCombinations)
                            {
                                if (agodaCmb.CheckIn == initialCmb.CheckIn)
                                {
                                    foreach (var unfilteredPrice in agodaCmb.UnfilteredHotelPrices)
                                    {
                                        unfilteredPrice.SearchCriteriaCombinationId = initialCmb.Id;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
