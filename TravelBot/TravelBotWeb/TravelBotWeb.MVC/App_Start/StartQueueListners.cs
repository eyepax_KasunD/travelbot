﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using TravelBotWeb.QueueServices;
using TravelBotWeb.Scheduling;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(TravelBotWeb.App_Start.StartQueueListners), nameof(TravelBotWeb.App_Start.StartQueueListners.Start))]
namespace TravelBotWeb.App_Start
{
    public static class StartQueueListners
    {
        public static void Start()
        {
            new Thread(HotelSearchResponseQueueListener.Start) { IsBackground = true }.Start();
        }
    }
}