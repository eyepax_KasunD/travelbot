﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;
using TravelBot.Common.Repository.Contracts;

namespace TravelBot.Common.Repository
{
    public class SourceCityMapRepository : Repository<SourceCityMap>, ISourceCityMapRepository
    {
        private readonly ITravelbotDbContext _context;
        public SourceCityMapRepository(ITravelbotDbContext context) : base(context)
        {
            _context = context;
        }
        public SourceCityMap GetCity(long cityId, long datasourceId)
        {
            return _context.SourceCityMap.FirstOrDefault(s => s.CityId == cityId && s.DataSourceId == datasourceId);
        }
    }
}
