﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using Quartz;
using Quartz.Impl;

namespace Scraper.WebAPI.Scheduling
{
    public class JobScheduler
    {
        public async static void Start()
        {
            NameValueCollection props = new NameValueCollection
            {
                     { "quartz.serializer.type", "binary" }
            };
            ISchedulerFactory factory = UnityConfig.Resolve<ISchedulerFactory>();

            // get a scheduler
            IScheduler sched = await factory.GetScheduler();
            await sched.Start();

            // define the job and tie it to our HelloJob class
            IJobDetail job1 = JobBuilder.Create<ProcessSearchRequests>()
                .WithIdentity("SearchRequests")
                .Build();
            IJobDetail job2 = JobBuilder.Create<ProcessHotelRequests>()
                .WithIdentity("HotelRequests")
                .Build();
            // Trigger the job to run now      .WithCronSchedule()
            ITrigger trigger = TriggerBuilder.Create()
                 .WithIdentity("SearchRequests")
              .StartNow()
              .WithSimpleSchedule(x => x
                  .WithIntervalInMinutes(5)
                  .RepeatForever())
              .Build();
            
            ITrigger trigger2 = TriggerBuilder.Create()
                 .WithIdentity("HotelRequests")
              .StartNow()
              .WithSimpleSchedule(x => x
                  .WithIntervalInMinutes(3)
                  .RepeatForever())
              .Build();

            IJobDetail job3 = JobBuilder.Create<ManageHotelRequests>()
                .WithIdentity("ManageHotelRequests")
                .Build();

            ITrigger trigger3 = TriggerBuilder.Create()
                 .WithIdentity("ManageHotelRequests")
              .StartNow()
              .WithSimpleSchedule(x => x
                  .WithIntervalInMinutes(10)
                  .RepeatForever())
              .Build();

            IJobDetail job4 = JobBuilder.Create<CleanSearchRequests>()
               .WithIdentity("CleanSearchRequests")
               .Build();

            ITrigger trigger4 = TriggerBuilder.Create()
                 .WithIdentity("CleanSearchRequests")
              .StartNow()
              .WithSimpleSchedule(x => x
                  .WithIntervalInHours(1)
                  .RepeatForever())
              .Build();

            IJobDetail job5 = JobBuilder.Create<ManageSearchRequests>()
               .WithIdentity("ManageSearchRequests")
               .Build();

            ITrigger trigger5 = TriggerBuilder.Create()
                 .WithIdentity("ManageSearchRequests")
              .StartNow()
              .WithSimpleSchedule(x => x
                  .WithIntervalInMinutes(10)
                  .RepeatForever())
              .Build();

            //await sched.ScheduleJob(job1, trigger);
            //await sched.ScheduleJob(job2, trigger2);
            await sched.ScheduleJob(job3, trigger3);
            await sched.ScheduleJob(job4, trigger4);
            await sched.ScheduleJob(job5, trigger5);
        }
    }
}