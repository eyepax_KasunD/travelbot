﻿using System;
using System.Web.Http;
using TravelBotWeb.Models;
using Newtonsoft.Json;
using System.Collections;
using TravelBotWeb.Common;
using TravelBotWeb.Services.Contracts;
using TravelBotWeb.Services.Models;
using TravelBotWeb.Services;
using TravelBot.Common.Domain;

namespace TravelBotWeb.Controllers
{
    public class NotificationController : BaseApiController
    {
        private readonly IEmailService _emailService;
        private readonly Services.Contracts.ICountryService _countryService;
        private readonly Services.Contracts.IUserServiceSQL _userServices;
        public NotificationController(IEmailService emailService, Services.Contracts.ICountryService countryService, Services.Contracts.IUserServiceSQL userService)
        {
            _emailService = emailService;
            _countryService = countryService;
            _userServices = userService;
        }

        [HttpPost]
        public ApiResponseModel<EmailResponseModel> SendEmailDirectly([FromBody]EmailRequestModel model) //send email without generating cshtml body,, just send the email with given parameters (body will be coming from "EmailRequestModel", no need to generate the body... this approach is used to send error reports to Admin, like that. )
        {
            logger.Info("Send Email List request recieved: " + JsonConvert.SerializeObject(model));
            var responses = _emailService.SendEmailDirectly(model);
            return GetApiResponseModel(responses);
        }

        [HttpPost]
        public ApiResponseModel<EmailResponseModel> SendEmailtoCustomer([FromBody]HotelSearchResponseEmail model) //send email to customer dispalying info of list of hotels
        {
            logger.Info("Send Email Error Report request recieved: " + JsonConvert.SerializeObject(model));
            var responses = _emailService.SendEmailtoCustomer(model);
            return GetApiResponseModel(responses);
        }

        [HttpPost]
        public ApiResponseModel<ArrayList> ReadCsvFile() //checking the hotel features
        {
            logger.Info("Read Csv file request recieved");
            CsvService _csv = new CsvService();
            var responses = _csv.Read();
            return GetApiResponseModel(responses);
        }
        [HttpPost]
        public ApiResponseModel<bool> StopSearch(string id, [FromBody]StopSearchingModel model) //Unsubscribe for emails
        {
            logger.Info("User Unsubscribe Emails SessionId: "+ id+" Request: "+ JsonConvert.SerializeObject(model));
            IUserService _userservice = UnityConfig.Resolve<IUserService>();
            var response = _userservice.StopSearch(model.ReasonNo.ToString(), id);
            var ResponseFromSql = _userServices.StopSearch(id, model);
            return GetApiResponseModel(response);
        }
        [HttpGet]
        public ApiResponseModel<string> ResendEmail(string sessionId) //Re-send email to customer
        {
            logger.Info("Resending Emails, SessionId: " + sessionId);     
            var response = _emailService.ResendEmail(sessionId); 
            return GetApiResponseModel(response.ToString());
        }

        [HttpPost]
        public ApiResponseModel<string> CreateCityListInSql([FromBody] Country model)
        {
            try
            {
                _countryService.AddCountry(model);
                return GetApiResponseModel("Success");
            }
            catch (Exception)
            {
                return GetApiResponseModel("Failed");
            }
        }
    }
}
