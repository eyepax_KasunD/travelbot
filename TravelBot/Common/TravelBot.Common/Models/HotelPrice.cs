﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static TravelBot.Common.Enums;

namespace TravelBot.Common.Models
{
    public class SearchRspCriteriaCombination
    {
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
        public List<HotelPrice> HotelPrices { get; set; }
    }


    public class HotelPrice
    {
        public long HotelPriceId { get; set; }
        public int SortOrder { get; set; }

        public bool IsPriceMatched { get; set; }

        public string URL { get; set; }

        public decimal Price { get; set; }

        public string HotelRef { get; set; }

        public DataSourceCode Source { get; set; }
    }
}
