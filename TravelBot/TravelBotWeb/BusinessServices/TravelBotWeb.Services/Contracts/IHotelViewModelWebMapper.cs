﻿using TravelBot.Common.Models;
using TravelBotWeb.Common;
using TravelBotWeb.Services.Models;

namespace TravelBotWeb.Services.Contracts
{
    public interface IHotelViewModelWebMapper
    {
        HotelViewModel MapHotelObjectModelToWeb(SearchRspCriteriaCombination ob, UserSessionModel userSession, string requestId);
    }
}
