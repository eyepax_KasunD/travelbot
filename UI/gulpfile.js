var gulp = require('gulp'),
	sass = require('gulp-sass'),
	minifyCss = require('gulp-minify-css'),
    concat = require('gulp-concat'),
	rename = require('gulp-rename'),
	uglify = require('gulp-uglify'),
	gulpIf = require('gulp-if'),
	runSequence = require('run-sequence'),
	browserSync = require('browser-sync').create();

var target = '../TravelBot/TravelBotWeb/TravelBotWeb.MVC/cdn/';
var config = {
    env: 'dev'
};

gulp.task('set-prod-env', function() {
   config.env = 'prod';
});

//minify all custom css files and sass compilation
gulp.task('appCSS', function () {
    gulp.src('src/scss/app-bundle.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(concat('app-bundle.css')) //create css file
        .pipe(minifyCss()) // minify content
        .pipe(rename({ //rename as min file
            basename: 'app-bundle',
            extname: '-min.css'
        }))

		.pipe(gulp.dest(target+'css/'))
		.pipe(browserSync.reload({stream:true}));
});

//minify all vendor css files
gulp.task('vendorCSS', function () {
    return gulp.src([  //don't change the order
		'src/css/vendor/bootstrap.css',
		'src/css/vendor/bootstrap-datepicker.css',
		'src/css/vendor/fontawesome-all.css',
		'src/css/vendor/nouislider.css',
		'src/css/vendor/slick.css',
		'src/css/vendor/slick-theme.css'
	])
	.pipe(concat('vendor-bundle.css'))
	.pipe(minifyCss())
	.pipe(rename({
		basename: 'vendor-bundle',
		extname: '-min.css'
	}))
	.pipe(gulp.dest(target+'css/'));
});


var uglifyAndCopyToDest = function (srcFiles, targetName, destination) {
	return gulp.src(srcFiles)
		.pipe(rename(targetName))
        .pipe(gulpIf(config.env === 'prod' , uglify()))
        .pipe(gulp.dest(destination));
}
var CopyToDest = function (srcFiles, targetName, destination) {
	return gulp.src(srcFiles)
		.pipe(rename(targetName))
        .pipe(gulp.dest(destination));
}
//minify app-bundle(common scripts) js file
gulp.task('appJS', function () {
	return CopyToDest("src/js/app/app-bundle.js","app-bundle-min.js",(target+"js/"));
});

//index page
gulp.task('indexJS', function () {
	return CopyToDest("src/js/app/index.js","index-min.js",(target+"js/"));
});

//plan page
gulp.task('planJS', function(){
	return CopyToDest("src/js/app/plan.js","plan-min.js",(target+"js/"));
});
//stop search page
gulp.task('stopSearchJS', function () {
	return CopyToDest("src/js/app/stop-search.js","stop-search-min.js",(target+"js/"));
});
//trip manager page
gulp.task('tripManagerJS', function () {
	return CopyToDest("src/js/app/trip-manager.js","trip-manager-min.js",(target+"js/"));
});
//hotel list page
gulp.task('hotelListJS', function () {
	return CopyToDest("src/js/app/hotel-list.js","hotel-list-min.js",(target+"js/"));
});

//minify all vendor js files
gulp.task('vendorJS', function () {
    return gulp.src([ //don't change the order
		'src/js/vendor/jquery-3.3.1.js',
		'src/js/vendor/angular.js',
		'src/js/vendor/bootstrap.bundle.js',
		'src/js/vendor/bootstrap-datepicker.js',
		'src/js/vendor/jquery.json.js',
		'src/js/vendor/touch-dnd.js',
		'src/js/vendor/nouislider.min.js',
		'src/js/vendor/wNumb.js',
		'src/js/vendor/moment.js',
		'src/js/vendor/slick.js'
	])
	.pipe(concat('vendor-bundle.js'))
    .pipe(uglify())
	.pipe(rename({
		basename: 'vendor-bundle',
		extname: '-min.js'
	}))
	.pipe(gulp.dest(target+'js/'));
});

//Copy assets
gulp.task('copyAssets', function () {
    return gulp.src('assets/**/*.*')
	.pipe(gulp.dest(target+'assets/'));
});

// auto browser reload
gulp.task('run', function() {
	browserSync.init({
		server: {
			baseDir:'build' //this should be parent folder which build
		}
	});
	//detect any save actions from these files and refresh the browser
	gulp.watch('src/scss/**/*.scss',['appCSS']);
	gulp.watch('src/js/app/app-bundle.js',['appJS']);
	gulp.watch('src/js/app/index.js',['indexJS']);
	gulp.watch('src/js/app/plan.js',['planJS']);
	gulp.watch('src/js/app/stop-search.js',['stopSearchJS']);
	gulp.watch('src/js/app/trip-manager.js',['tripManagerJS']);
	gulp.watch('src/js/app/hotel-list.js',['hotelListJS']);
	gulp.watch('assets/**/*.*',['copyAssets']);
});

//run all tasks - suitable for development environment (command - gulp)
gulp.task('default', ['appCSS', 'vendorCSS', 'appJS', 'indexJS', 'planJS', 'stopSearchJS', 'tripManagerJS', 'hotelListJS', 'vendorJS', 'copyAssets', 'run']);

//run tasks - suitable for production environment (command - gulp production)
gulp.task('production', function(){
	runSequence('set-prod-env', ['appCSS', 'vendorCSS', 'appJS', 'indexJS', 'planJS', 'stopSearchJS', 'tripManagerJS', 'hotelListJS', 'vendorJS', 'copyAssets']);
});
