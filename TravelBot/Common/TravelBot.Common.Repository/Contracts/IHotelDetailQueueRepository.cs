﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;

namespace TravelBot.Common.Repository.Contracts
{
    public interface IHotelDetailQueueRepository
    {
        HotelDetailQueue Add(HotelDetailQueue queueReq);
        HotelDetailQueue Get(long id);
        IEnumerable<HotelDetailQueue> Find(Expression<Func<HotelDetailQueue, bool>> filter);
        bool IsExistInQueue(long dataSourceId, string hotelIdFromSource);
        bool IsRemainingResponsesExists(long responseId, long dataSourceId);
        List<HotelDetailQueue> GetElementsBySource(long dataSourceId, string hotelIdFromSource);
        HotelDetailQueue Update(HotelDetailQueue queueReq);
    }
}
