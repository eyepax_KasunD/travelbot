﻿using System;
using TravelBot.Common;
using TravelBot.Common.Mappers.Contracts;
using TravelBot.Common.Models;
using TravelBot.Common.Repository.Contracts;
using TravelBotService.Common;

namespace TravelbotService.BusinessServices.Mappers
{
    public class HotelPriceSearchReqToAgodaLongTailSearchReq : IMapper<Tuple<HotelPriceSearchReq, SearchRspCriteriaCombination, SearchReqCriterion>, AgodaLongTailSearchReq>
    {
        private readonly ISourceCityMapRepository _sourceCityMapRepository;
        private readonly IDataSourceRepository _dataSourceRepository;
        private readonly ICurrencyRepository _currencyRepository;
        public HotelPriceSearchReqToAgodaLongTailSearchReq(ISourceCityMapRepository sourceCityMapRepository, IDataSourceRepository dataSourceRepository, ICurrencyRepository currencyRepository)
        {
            _sourceCityMapRepository = sourceCityMapRepository;
            _dataSourceRepository = dataSourceRepository;
            _currencyRepository = currencyRepository;
        }

        public AgodaLongTailSearchReq Map(Tuple<HotelPriceSearchReq, SearchRspCriteriaCombination, SearchReqCriterion> sourceObject)
        {
            //List<AgodaLongTailSearchReq> AgodaLongTailSearchReqList = new List<AgodaLongTailSearchReq>();
            var priceSearchReq = sourceObject.Item1;
            var searchCriteriaCombination = sourceObject.Item2;
            var searchCriteria = sourceObject.Item3;

            var dailyRate = new DailyRate
            {
                maximum = searchCriteria.PricePerNightTo,
                minimum = searchCriteria.PricePerNightFrom
            };
            var occupancy = new Occupancy
            {
                numberOfAdult = priceSearchReq.AdultCount,
                numberOfChildren = priceSearchReq.ChildCount
            };
            var additionals = new Additional
            {
                currency = _currencyRepository.GetCurrencyByCode(priceSearchReq.Currency).Code,
                dailyRate = dailyRate,
                occupancy = occupancy,
                discountOnly = false,
                language = "en-us",
                maxResult = 30,
                minimumReviewScore = 1,
                minimumStarRating = 4,
                sortBy = "PriceAsc"
            };

            var codeId = _dataSourceRepository.FindFromCode(Enums.DataSourceCode.Agoda.ToString()).Id;
            var PricesourceCode = searchCriteria.CityId.HasValue?_sourceCityMapRepository.GetCity((int)searchCriteria.CityId , codeId).Value : "7835"; //set colombo as the default city
            var criteria = new Criteria
            {
                additional = additionals,
                checkInDate = searchCriteriaCombination.CheckIn.ToString("yyyy-MM-dd"),
                checkOutDate = searchCriteriaCombination.CheckOut.ToString("yyyy-MM-dd"),
                cityId = Int32.Parse(PricesourceCode)
            };

            var agodatailres = new AgodaLongTailSearchReq()
            {
                criteria = criteria
            };
            return agodatailres;
        }


    }
}
