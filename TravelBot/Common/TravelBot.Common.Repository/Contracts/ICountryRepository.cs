﻿using System.Collections.Generic;
using TravelBot.Common.Domain;

namespace TravelBot.Common.Repository.Contracts
{
    public interface ICountryRepository
    {
        List<Country> GetAllEnabledCountries();
        Country Add(Country p);
        Country FindbyId(long id);
    }
}
