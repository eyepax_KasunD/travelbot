﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;
using TravelBot.Common.Repository.Contracts;

namespace TravelBot.Common.Repository
{
    public class HotelPriceSearchQueueRepository : Repository<HotelPriceSearchQueue>, IHotelPriceSearchQueueRepository
    {
        public HotelPriceSearchQueueRepository(ITravelbotDbContext context) : base(context)
        {
        }

        public HotelPriceSearchQueue Add(HotelPriceSearchQueue queueReq)
        {
            return base.Add(queueReq);
        }
        public HotelPriceSearchQueue Find(Expression<Func<HotelPriceSearchQueue, bool>> func)
        {
            return base.Find(func).FirstOrDefault();
        }

        public bool IsRemainingResponsesExists(long responseId, long dataSourceId)
        {
            var res = base.Find(q => q.SearchResponseId == responseId && q.DataSourceId == dataSourceId && q.ResponseRecieved == false);
            return res != null && res.Count() > 0;
        }
    }
}
