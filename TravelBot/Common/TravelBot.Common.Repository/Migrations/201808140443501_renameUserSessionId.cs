namespace TravelBot.Common.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class renameUserSessionId : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Trn_SearchResponse", new[] { "UserSession_Id" });
            DropColumn("dbo.Trn_SearchResponse", "UserSessionId");
            RenameColumn(table: "dbo.Trn_SearchResponse", name: "UserSession_Id", newName: "UserSessionId");
            AlterColumn("dbo.Trn_SearchResponse", "UserSessionId", c => c.String(maxLength: 40));
            CreateIndex("dbo.Trn_SearchResponse", "UserSessionId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Trn_SearchResponse", new[] { "UserSessionId" });
            AlterColumn("dbo.Trn_SearchResponse", "UserSessionId", c => c.Long());
            RenameColumn(table: "dbo.Trn_SearchResponse", name: "UserSessionId", newName: "UserSession_Id");
            AddColumn("dbo.Trn_SearchResponse", "UserSessionId", c => c.Long());
            CreateIndex("dbo.Trn_SearchResponse", "UserSession_Id");
        }
    }
}
