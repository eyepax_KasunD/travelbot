﻿using Scraper.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using NLog;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Collections.ObjectModel;
using TravelBot.Common.Models;
using TravelBot.Common;
using Scraper.Services.Contracts;
using Scraper.Common.Models.Responses;
using Scraper.Common.Models.Requests;
using TravelBot.Common.Helpers;

namespace Scraper.Services
{
    public class Expedia : IScrapingSource
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private const int reviewScoreMultiplier = 2;
        private readonly List<int> _priceRanges = new List<int> { 0, 75, 125, 200, 299 };
        public Hotel hotelFeatures;

        public HotelPriceSearchRsp GetPriceResponses(ScraperHotelPriceSearchReq request)
        {
            logger.Info("GetPriceResponses start for the request id : " + request.RequestId);
            List<SearchRspCriteriaCombination> criteriaCombinations = new List<SearchRspCriteriaCombination>();
            var warnings = new List<string>();
            var errors = new List<string>();

            try
            {
                var chromeOptions = new ChromeOptions();
                //chromeOptions.AddArguments(new List<string>() { "headless" });
                using (var driver = new ChromeDriver(chromeOptions))
                {
                    driver.Navigate().GoToUrl("https://www.expedia.com/hotels");
                    driver.FindElementById("hotel-destination-hlp").SendKeys(request.SearchString);
                    driver.FindElementById("hotel-checkout-hlp").SendKeys(request.CheckOutDate.ToString("MM/dd/yyyy"));
                    driver.FindElementById("hotel-checkin-hlp").SendKeys(request.CheckInDate.ToString("MM/dd/yyyy"));
                   
                    new Actions(driver).SendKeys(Keys.Enter).Perform();

                    var uriBuilder = new UriBuilder(driver.Url);
                    var qs = HttpUtility.ParseQueryString(uriBuilder.Query);
                    qs.Set("adults",request.AdultCount.ToString());
                    uriBuilder.Query = qs.ToString();

                    driver.Navigate().GoToUrl(uriBuilder.Uri + getPriceFilterText(request, driver));
                    logger.Info("expedia.com search url :" + driver.Url);
                    new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.ElementExists((By.XPath("//*[@id=\"paginationContainer\"]/div/nav/fieldset/p"))));
                    criteriaCombinations = GetSearchRspCriteriaCombinations(driver.Url, request);
                    //hotelPrices = _hotelsMerging.GetUnion(responseOfAllDays);
                }
            }
            catch (Exception ex)
            {
                errors.Add(ex.ToString());
            }
            if (criteriaCombinations.Count == 0)
            {
                warnings.Add("Hotel count = 0, source : booking.com ");
            }

            return new HotelPriceSearchRsp
            {
                //HotelPriceWrappers = hotelPrices,
                Errors = errors,
                Warnings = warnings
            };
        }

        public List<SearchRspCriteriaCombination> GetSearchRspCriteriaCombinations(string url, ScraperHotelPriceSearchReq request)
        {
            var uriBuilder = new UriBuilder(url);
            List<SearchRspCriteriaCombination> CriteriaCombinations = new List<SearchRspCriteriaCombination>();
            try
            {
                List<DateTime> checkingdates = new List<DateTime>();
                List<DateTime> checkoutdates = new List<DateTime>();
                DateTime chekin = request.CheckInDate;
                var chromeOptions = new ChromeOptions();

                for (var i = -Math.Abs(request.FlexDatesMinus); i <= request.FlexDatesPlus; i++)
                {
                    checkingdates.Add(chekin.AddDays(i));
                }
                var checkingCheckoutDates = checkingdates.Select(checkin => new Tuple<DateTime, DateTime>(checkin, checkin.AddDays(request.NoOfStays)));

                using (var driver = new ChromeDriver(chromeOptions))
                {
                    logger.Info("Get list of hotel price list of flexible days");

                    foreach (var checkinCheckoutdate in checkingCheckoutDates)
                    {
                        var checkin = checkinCheckoutdate.Item1;
                        var checkout = checkinCheckoutdate.Item2;
                        var qs = HttpUtility.ParseQueryString(uriBuilder.Query);
                        qs.Set("chkin", checkin.Date.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture));
                        qs.Set("chkout", checkout.Date.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture));
                        uriBuilder.Query = qs.ToString();

                        driver.Navigate().GoToUrl(uriBuilder.Uri);
                        logger.Info("Url with replaced chkin and chkout dates in felxible days" + driver.Url);
                        var hotelCards = driver.FindElementsByXPath("//*[@id=\"resultsContainer\"]//article").Select((element, j) => new { element, j }).ToList();
                        var hotelPrices = new ConcurrentBag<HotelPrice>();
                        Parallel.ForEach(hotelCards, (hotel) =>
                        {
                            var hotelPrice = new HotelPrice()
                            {
                                //Source = DataSourceCode.Expedia,
                                //CheckInDate = checkingdates[i],
                                IsPriceMatched = true
                            };
                            try
                            {
                                hotelPrice.Price = getPrice(hotel.element.FindElement(By.ClassName("actualPrice")).Text) * request.NoOfStays;
                                if (hotelPrice.Price > request.PriceTo || hotelPrice.Price < request.PriceFrom)
                                    hotelPrice.IsPriceMatched = false;

                                hotelPrice.URL = hotel.element.FindElement(By.XPath(".//a[contains(@class,\"flex-link\")]")).GetAttribute("href");
                                hotelPrice.HotelRef = HotelIdentifier.GetIdentifierFromUrl(hotelPrice.URL);
                            }
                            catch (NoSuchElementException)
                            {
                                return;
                            }
                            hotelPrices.Add(hotelPrice);
                        });
                        logger.Info("hotel count : " + hotelPrices.Count);
                        CriteriaCombinations.Add(new SearchRspCriteriaCombination
                        {
                            HotelPrices = hotelPrices.ToList(),
                            CheckIn = checkin,
                            CheckOut = checkout
                        });
                    }
                }
                return CriteriaCombinations;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return CriteriaCombinations;
        }

        private decimal getPrice(string priceText)
        {
            decimal price = -1;
            decimal.TryParse(priceText.Replace(",", string.Empty).Replace("$", string.Empty), out price);
            return price;
        }

        private Review GetReview(IWebElement hotel)
        {
            var rw = new Review() { Count = -1, Score = -1 };
            try
            {
                var reviewString = hotel.FindElement(By.XPath(".//li[contains(@class,\"reviewOverall\")]//span[contains(@class,\"maxRatingText\")]/..")).Text; //Ex:- value of 4.4/5 like that
                rw.Score = getReviewScore(reviewString);
            }
            catch (Exception) { }

            try
            {
                var reviewCount = hotel.FindElement(By.XPath(".//li[contains(@class,\"reviewCount\")]/span")).Text; //no of user comments about hotel
                rw.Count = getReviewCount(reviewCount);
            }
            catch (Exception) { }

            return rw;
        }

        private decimal getReviewScore(string text)
        {
            decimal numerator = -1;
            decimal denominator = 1;
            decimal.TryParse(text.Split('/')[0], out numerator);
            decimal.TryParse(text.Split('/')[1], out denominator);
            return numerator / denominator * 10;
        }
        private int getReviewCount(string text)
        {
            MatchCollection matchList = Regex.Matches(text.Replace(",", string.Empty), @"\d+");
            return matchList.Count > 0 ?
                matchList.Cast<Match>().Select(match => match.Value).Select(s => int.Parse(s)).FirstOrDefault() : -1;
        }

        private string getPriceFilterText(ScraperHotelPriceSearchReq request, ChromeDriver driver)
        {
            //TODO : Apply per room, per night price

            var returnString = string.Empty;
            for (int i = 1; i < _priceRanges.Count; i++)
            {
                if (request.PricePerNightFrom < _priceRanges[i] && request.PricePerNightTo > _priceRanges[i - 1])
                    returnString += $"&price={i - 1}";
            }
            return returnString;
        }


        public List<HotelPriceResponse> GetMinPrice(HotelPriceRequest request)
        {
            Uri myUri = new Uri(request.Href);
            DateTime chekin = DateTime.Parse(HttpUtility.ParseQueryString(myUri.Query).Get("chkin"));


            var uriBuilder = new UriBuilder(request.Href);

            List<DateTime> checkingdates = new List<DateTime>(), checkoutdates = new List<DateTime>();

            for (var i = -Math.Abs(request.FlexibilityMinus); i <= request.FlexibilityPlus; i++)
            {
                checkingdates.Add(chekin.AddDays(i));
            }
            checkingdates.ForEach(checkingdate => checkoutdates.Add(checkingdate.AddDays(request.HowManyDays)));

            var chromeOptions = new ChromeOptions();
            using (var driver = new ChromeDriver(chromeOptions))
            {
                List<HotelPriceResponse> str = new List<HotelPriceResponse>();
                logger.Info("GetMinPrice get price url :" + request.Href);
                driver.Navigate().GoToUrl(request.Href);

                for (var i = 0; i < checkingdates.Count; i++)
                { //loop for dates
                    HotelPriceResponse res = new HotelPriceResponse
                    {
                        Checkindate = checkingdates[i].Date,
                        Checkoutdate = checkoutdates[i].Date,
                        Source = request.Source.ToString()
                    };

                    var qs = HttpUtility.ParseQueryString(uriBuilder.Query);
                    qs.Set("chkin", checkingdates[i].Date.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture));
                    qs.Set("chkout", checkoutdates[i].Date.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture));
                    uriBuilder.Query = qs.ToString();

                    driver.Navigate().GoToUrl(uriBuilder.Uri);
                    logger.Info("Url with replaced chkin and chkout dates" + driver.Url);

                    new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.ElementExists((By.ClassName("room-price-info-wrapper"))));

                    try
                    {
                        var elements = driver.FindElementsByClassName("room-price-info-wrapper");
                        //var elements = driver.FindElementsByXPath("//td[contains(@class,\"avg-rate\")]");  //get set of cards to get prices, if there is no cards like this,, catch will fire

                        var num2 = double.MaxValue;

                        foreach (var ele in elements) //if card set is ok
                        {
                            try
                            {
                                var pricepernightperroom = ele.FindElement(By.ClassName("room-price-value")).Text; // get room price value, card by card. if there is no room price val for that specific card, catch will fire
                                String pricePNPR = pricepernightperroom.Substring(1);
                                String formattedpricePNPR = pricePNPR.Replace(",", String.Empty);
                                if (double.Parse(formattedpricePNPR) < num2)
                                {
                                    num2 = double.Parse(formattedpricePNPR);
                                }
                                res.MinPricePernightPerRoom = num2;

                            }
                            catch (Exception)
                            {    //if there is no room price val for specific card ,, loop will continue to check next card
                                continue;

                            }

                        }// finally get min room price value from card set
                        if (num2 == double.MaxValue)
                        {   //If num2 is not changed, that means there is no class like room-price-value or there is no min room price value
                            res.MinPricePernightPerRoom = -1;
                            res.MinTotalPrice = -1;
                            str.Add(res);
                            continue;   //continue to check next date set
                        }
                        else
                        {    //get the total price for specific min room price value
                            foreach (var ele in elements)
                            {
                                var pricepernightperroom = ele.FindElement(By.ClassName("room-price-value")).Text;  // no need of try catch because previously handled try catch
                                String pricePNPR = pricepernightperroom.Substring(1);
                                String formattedpricePNPR = pricePNPR.Replace(",", String.Empty);
                                if (double.Parse(formattedpricePNPR) == num2)
                                { // true means , found the place of min room price value
                                    try
                                    {
                                        var totalprice = ele.FindElement(By.ClassName("exposeTotalPriceContainer")).Text;  // then check if there exist total price value, if not catch will fire
                                        String tot = totalprice.Substring(1); //remove dollar mark
                                        String formattedtot = tot.Replace(",", String.Empty);
                                        res.MinTotalPrice = double.Parse(formattedtot);
                                        break;
                                    }
                                    catch (Exception)
                                    {
                                        res.MinTotalPrice = -1;
                                    }
                                }
                            }
                        }

                        //num = elements.Where(ele => Int32.Parse(ele.Text.Substring(1)) < num).Select(ele => Int32.Parse(ele.Text.Substring(1))).Min();

                        str.Add(res);
                    }
                    catch (Exception)
                    {
                        res.MinPricePernightPerRoom = -1;
                        res.MinTotalPrice = -1;
                        str.Add(res);
                        continue;
                    }
                }
                return str;
            }
        }

        public ScraperHotelDetailRsp GetHotelFeatures(ScraperHotelDetailReq requestModel)
        {
            logger.Info("Get Hotel Features from the website  : " + requestModel.Website);
            var chromeOptions = new ChromeOptions();
            ScraperHotelDetailRsp response = new ScraperHotelDetailRsp();

            Hotel hotel = new Hotel()
            {
                GuestReview =
                {
                    ReviewScore=-1,ReviewCount=-1, RoomCleanliness = -1, ServiceStaff = -1, RoomComfort = -1, HotelCondition = -1
                }
            };

            using (var driver = new ChromeDriver(chromeOptions))
            {
                driver.Navigate().GoToUrl(requestModel.Hotelurl);
                new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.ElementExists((By.Id("tab-reviews"))));
                var gButton = driver.FindElementById("tab-reviews");
                gButton.Click();
                try
                {
                    new WebDriverWait(driver, TimeSpan.FromSeconds(30)).Until(ExpectedConditions.ElementExists((By.XPath("//*[@class=\"dimensions\"]/div"))));
                    var hotelName = driver.FindElementByXPath("//*[@id=\"hotel-name\"]").Text;
                    hotel.Name = hotelName;
                }
                catch (Exception e)
                {
                    response.Errors.Add("hotel name is empty, exception : " + e);
                }
                try
                {
                   
                    var reviewSection = driver.FindElementsByXPath("//*[@class=\"dimensions\"]/div");
                    int reviewcount = int.Parse(driver.FindElement(By.XPath("//*[@id=\"link-to-reviews\"]/span")).Text);
                    string scoreString = driver.FindElement(By.ClassName("rating-scale")).Text;
                    hotel.GuestReview = GetGuestReviews(reviewSection, reviewcount, scoreString);                 
                }
                catch (Exception)
                {
                    response.Warnings.Add("errors of guest reviews");
                }

                try
                {
                    decimal[] NumList = new decimal[] { };
                    hotel.Location.Street = driver.FindElementByClassName("street-address").Text;
                    hotel.Location.City = driver.FindElementByClassName("city").Text;
                  //  hotelFeatures.Location.LocationString = driver.FindElementByXPath("//*[@id=\"license-plate\"]/div[@class=\"address\"]/a").Text;
                    var latitudeAndLongitude = driver.FindElementByXPath("//*[@class= \"map uitk-col\"]/a/figure/img").GetAttribute("src");
                    NumList = GetlatitudeAndLongitude(latitudeAndLongitude);
                    hotel.Location.Latitude = NumList[0];
                    hotel.Location.Longitude = NumList[1];
                }
                catch (Exception)
                {
                    response.Warnings.Add("errors of hotel location");
                }
     
                try
                {
                    var gallerySection = driver.FindElementsByXPath("//*[@id=\"jumbo-wrapper\"]/figure/img");
                    List<string> imgList = new List<string>();
                    imgList = GetImages(gallerySection);
                    hotel.ImgUrl = imgList;
                }
                catch (Exception)
                {
                    response.Warnings.Add("errors of image urls");
                }
                try
                {  
                    string sr = driver.FindElement(By.XPath("//*[@id=\"license-plate\"]/div[1]/strong/span[1]")).Text;
                    var arr = Regex.Split(sr, @"[^0-9\.]+").Where(c => c != "." && c.Trim() != "");
                    hotel.HotelRating = double.Parse(arr.FirstOrDefault());
                }
                catch (Exception)
                {
                    response.Warnings.Add("Number of stars are not mensioned");

                }
            }
            response.Hotel = hotel;
            return response;
        }
        private decimal[] GetlatitudeAndLongitude(string latitudeAndLongitude)
        {     
                int NumOfValues = 0;
                decimal[] NumList= new decimal[2];
            MatchCollection Numbers = Regex.Matches(latitudeAndLongitude, @"[-]?[0-9]+[\.][0-9]+");
                //  Convert Match type to string array
                var array = Numbers.Cast<Match>().Select(m => m.Value).ToArray();
                foreach (string val in array)
                {
                    if (NumOfValues < 2)
                    {
                        decimal num = decimal.Parse(val);

                        if (NumOfValues == 0)
                        {
                            NumList[0] = num;
                        }
                        else
                        {
                            NumList[1] = num;
                        }
                        NumOfValues++;
                    }
                };
            return NumList;
            
        }
        private List<string> GetImages(ReadOnlyCollection<IWebElement> gallerySection)
        {
            List<string> imgList = new List<string>();
            for (int i = 0; i < 10 && i < gallerySection.Count; i++)
            {
                if (gallerySection[i].GetAttribute("data-src") != null)
                {
                    var img = gallerySection[i].GetAttribute("data-src");
                    imgList.Add(img);
                }
            };
            return imgList;
        }

        private GuestReview GetGuestReviews(ReadOnlyCollection<IWebElement> reviewSection, int reviewcount, string scoreString)
        {
            Hotel hotelFeatures = new Hotel()
            {
                GuestReview =
                {
                    ReviewScore=-1,ReviewCount=-1,RoomCleanliness = -1, ServiceStaff = -1, RoomComfort = -1, HotelCondition = -1
                }
            };
            foreach (var review in reviewSection)
            {
                var reviewName = review.Text;

                var reviewRate = review.FindElement(By.XPath(".//span")).Text;

                if (reviewName.Contains(Constants.ExpediaHotelGuestReviews.clean))
                {
                    hotelFeatures.GuestReview.RoomCleanliness = double.Parse(reviewRate) * reviewScoreMultiplier;
                }
                else if (reviewName.Contains(Constants.ExpediaHotelGuestReviews.staff))
                {
                    hotelFeatures.GuestReview.ServiceStaff = double.Parse(reviewRate) * reviewScoreMultiplier;
                }
                else if (reviewName.Contains(Constants.ExpediaHotelGuestReviews.comfort))
                {
                    hotelFeatures.GuestReview.RoomComfort = double.Parse(reviewRate) * reviewScoreMultiplier;
                }
                else if (reviewName.Contains(Constants.ExpediaHotelGuestReviews.service))
                {
                    hotelFeatures.GuestReview.HotelCondition = double.Parse(reviewRate) * reviewScoreMultiplier;
                }

            };
            hotelFeatures.GuestReview.ReviewCount = reviewcount;
            var doubleArray = Regex.Split(scoreString, @"[^0-9\.]+").Where(c => c != "." && c.Trim() != "");
            var array = doubleArray.ToArray();
            hotelFeatures.GuestReview.ReviewScore = (double.Parse(array[0]) / double.Parse(array[1])) * 10;
            return hotelFeatures.GuestReview;
        }

    }
}