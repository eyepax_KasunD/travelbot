﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TravelBot.Common.Domain
{
    [Table("Trn_UserSession")]
    public class UserSession
    {
        [Key]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(40)]
        public string Id { get; set; }
        [Required, DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreateDateTime { get; set; }
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
        public int Adults { get; set; }
        public int Children { get; set; }
        public int FlexiDateCount { get; set; }
        public decimal PriceFrom { get; set; }
        public decimal PriceTo { get; set; }
        public string Email { get; set; }
        public string DeviceId { get; set; }
        public virtual ICollection<SearchResponse> SearchResponses { get; set; }
        public string StopSearch { get; set; }
    }

    [Table("Trn_SearchResponse")]
    public class SearchResponse : BaseEntity
    {
        public string RequestId { get; set; }
        public string ResponseStatus { get; set; }
        public long? CurrencyId { get; set; }
        public virtual Currency Currency { get; set; }
        public string UserSessionId { get; set; }
        public virtual UserSession UserSession { get; set; }
        public virtual ICollection<SearchCriteria> SearchCriteria { get; set; }
    }

    [Table("Trn_SearchCriteria")]
    public class SearchCriteria : BaseEntity
    {
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
        public long? CityId { get; set; }
        public City City { get; set; }
        public long? CountryId { get; set; }
        public virtual Country Country { get; set; }
        public decimal PriceFrom { get; set; }
        public decimal PriceTo { get; set; }
        public long? SearchResponseId { get; set; }
        public virtual SearchResponse SearchResponse { get; set; }
        public virtual ICollection<SearchCriteriaCombination> searchCriteriaCombinations { get; set; }
    }

    [Table("Trn_SearchCriteriaCombination")]
    public class SearchCriteriaCombination : BaseEntity
    {
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
        public virtual SearchCriteria SearchCriteria { get; set; }
        public virtual ICollection<UnfilteredHotelPrice> UnfilteredHotelPrices { get; set; }
        public virtual ICollection<HotelPrice> HotelPrices { get; set; }
    }

    [Table("Trn_UnfilteredHotelPrice")]
    public class UnfilteredHotelPrice : BaseEntity
    {
        public string Url { get; set; }
        public long? DataSourceId { get; set; }
        public virtual DataSource DataSource { get; set; }
        public string HotelIdFromSource { get; set; }
        public decimal Price { get; set; }
        public virtual SearchCriteriaCombination SearchCriteriaCombination { get; set; }
        public long? SearchCriteriaCombinationId { get; set; }
    }


    [Table("Trn_HotelPrice")]
    public class HotelPrice : BaseEntity
    {
        public int SortOrder { get; set; }
        public string Url { get; set; }
        public long? DataSourceId { get; set; }
        public virtual DataSource DataSource { get; set; }
        public string HotelIdFromSource { get; set; }
        public decimal Price { get; set; }
        public virtual SearchCriteriaCombination SearchCriteriaCombination { get; set; }
        public long? SearchCriteriaCombinationId { get; set; }
    }

    [Table("Trn_HotelPriceSearchQueue")]
    public class HotelPriceSearchQueue : ModifiableBaseEntity
    {
        public long? SearchResponseId { get; set; }
        public virtual SearchResponse SearchResponse { get; set; }
        public long? DataSourceId { get; set; }
        public virtual DataSource DataSource { get; set; }
        public bool ResponseRecieved { get; set; }
        public bool ErrorOccurred { get; set; }

    }

    [Table("Trn_HotelDetailQueue")]
    public class HotelDetailQueue : ModifiableBaseEntity
    {
        public long? SearchResponseId { get; set; }
        public virtual SearchResponse SearchResponse { get; set; }
        public long? DataSourceId { get; set; }
        public virtual DataSource DataSource { get; set; }
        public string HotelIdFromSource { get; set; }
        public bool ResponseRecieved { get; set; }
        public bool ErrorOccurred { get; set; }

    }

    [Table("Trn_UserClick")]
    public class UserClick : BaseEntity
    {
        public string UserSessionId { get; set; }
        public virtual UserSession UserSession { get; set; }
        public long? HotelPriceId { get; set; }
        public virtual HotelPrice HotelPrice { get; set; }
    }

    [Table("Trn_SearchStatus")]
    public class SearchStatus
    {
        [Key, Column(Order = 0)]
        public long? SearchResponseId { get; set; }
        public virtual SearchResponse SearchResponse { get; set; }
        [Key, Column(Order = 1)]
        public long? DataSourceId { get; set; }
        public virtual DataSource DataSource { get; set; }
        public int Status { get; set; }
        [Required, DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreateDateTime { get; set; }
        [Required, DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime LastModifiedDateTime { get; set; }
    }
}
