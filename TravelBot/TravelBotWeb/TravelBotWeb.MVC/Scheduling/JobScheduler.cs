﻿using System.Collections.Specialized;
using Quartz;

namespace TravelBotWeb.Scheduling
{
    public class JobScheduler
    {
        public async static void Start()
        {
            NameValueCollection props = new NameValueCollection
            {
                     { "quartz.serializer.type", "binary" }
            };
            ISchedulerFactory factory = UnityConfig.Resolve<ISchedulerFactory>(); 
            

            // get a scheduler
            IScheduler sched = await factory.GetScheduler();
            await sched.Start();

            // define the job and tie it to our HelloJob class
            IJobDetail job = JobBuilder.Create<ProcessUserRequests>()
                .WithIdentity("UserRequests")
                .Build();

            // Trigger the job to run now .WithCronSchedule()
            ITrigger trigger = TriggerBuilder.Create()
                 .WithIdentity("UserRequests")
              .StartNow()
              .WithSimpleSchedule(x => x
                  .WithIntervalInMinutes(1)
                  .RepeatForever())
              .Build();

            //IJobDetail job2 = JobBuilder.Create<ProcessUserEmails>()
            //    .WithIdentity("UserEmails")
            //    .Build();

            //// Trigger the job to run now .WithCronSchedule()
            //ITrigger trigger2 = TriggerBuilder.Create()
            //     .WithIdentity("UserEmails")
            //  .StartNow()
            //  .WithSimpleSchedule(x => x
            //      .WithIntervalInMinutes(2)
            //      .RepeatForever())
            //  .Build();

            IJobDetail job3 = JobBuilder.Create<ManageUserEmails>()
                .WithIdentity("ErrorsOfUserEmails")
                .Build();
            
            ITrigger trigger3 = TriggerBuilder.Create()
                 .WithIdentity("ErrorsOfUserEmails")
              .StartNow()
              .WithSimpleSchedule(x => x
                  .WithIntervalInMinutes(5)
                  .RepeatForever())
              .Build();

            //await sched.ScheduleJob(job, trigger);
            ////await sched.ScheduleJob(job2, trigger2);
            //await sched.ScheduleJob(job3, trigger3);
        }
    }
}