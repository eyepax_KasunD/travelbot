﻿using System.Collections.Generic;
using System.Linq;
using TravelBot.Common.Domain;
using TravelBot.Common.Repository.Contracts;

namespace TravelBot.Common.Repository
{
    public class CityContentRepository : Repository<CityContent>,ICityContentRepository
    {
        public CityContentRepository(ITravelbotDbContext context) : base(context)
        {
        }
     
        public CityContent FindCityContent(long cityId, int languageId)
        {
            return Find(s => s.City.Id == cityId && s.Language.Id == languageId).FirstOrDefault();
        }
    }
}
