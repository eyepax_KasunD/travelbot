﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;

namespace TravelBot.Common.Repository.Contracts
{
    public interface ISearchResponseRepository
    {
        SearchResponse AddSearchResponse(SearchResponse searchResponse);
        SearchResponse UpdateSearchResponse(SearchResponse searchResponse);
        SearchResponse GetSearchResponseFromId(long id);
        SearchResponse GetSearchResponseFromRequestId(string id);
    }
}
