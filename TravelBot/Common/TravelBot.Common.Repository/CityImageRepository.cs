﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;
using TravelBot.Common.Repository.Contracts;

namespace TravelBot.Common.Repository
{
   public class CityImageRepository : Repository<CityImage>, ICityImageRepository
    {
        public CityImageRepository(ITravelbotDbContext context) : base(context)
        {
        }
       public CityImage Find(int cityid)
        {
            return Find(s => s.City.Id == cityid).FirstOrDefault();
        }
    }
}
