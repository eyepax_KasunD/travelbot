﻿using TravelBot.Common.Domain;
using TravelBot.Common.Repository.Contracts;

namespace TravelBot.Common.Repository
{
    public class HotelRepository : Repository<Hotel>, IHotelRepository
    {
        public HotelRepository(ITravelbotDbContext context) : base(context)
        {
        }
        public Hotel AddHotel(Hotel p)
        {
            return  base.Add(p);
        }

        public Hotel GetHotelFromId(long id)
        {
            return base.Get(id);
        }
    }
}
