﻿using NLog;
using System;
using System.Linq;
using System.Net.Mail;
using static TravelBot.Common.Enums;
using TravelBotWeb.Repository.Contracts;
using TravelBotWeb.Common;

namespace TravelBotWeb.Repository
{
    public class EmailRepository: IEmailRepository
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public EmailResponseModel SendEmail(EmailRequestModel model)
        {           
            try
            {
                var email = new MailMessage()
                {
                    Body = model.Body,
                    IsBodyHtml = true,
                    Subject = model.Subject
                };
                foreach (var mail in model.To)
                {
                    email.To.Add(mail);
                }
                foreach (var mail in model.Cc)
                {
                    email.CC.Add(mail);
                }
                foreach (var mail in model.Bcc)
                {
                    email.Bcc.Add(mail);
                }
                var smtpClient = new SmtpClient();
                smtpClient.Send(email);
                logger.Info(string.Format("Email sent to {0}",model.To.FirstOrDefault()));
                return new EmailResponseModel() { result = "Successfully sent !!!", emailStatus = EmailStatus.Success };
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Exception thrown EmailRepository, email : {0}, error : {1}", model.To.FirstOrDefault(), e));
                return new EmailResponseModel() { result = "Error", emailStatus = EmailStatus.Error };
            }
        }
    }
}