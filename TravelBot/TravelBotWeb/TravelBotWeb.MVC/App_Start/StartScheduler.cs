using TravelBotWeb.Scheduling;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(TravelBotWeb.StartScheduler), nameof(TravelBotWeb.StartScheduler.Start))]

namespace TravelBotWeb
{

    public static class StartScheduler
    {
        /// <summary>
        /// Stars the scheduler.
        /// </summary>
        public static void Start() 
        {
         //   JobScheduler.Start();
        }
    }
}