﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common;
using TravelBot.Common.Domain;
using TravelBot.Common.Mappers.Contracts;
using TravelBot.Common.Models;
using TravelBot.Common.Repository.Contracts;

namespace TravelbotService.BusinessServices.Mappers
{
    public class HotelrPriceSearchRspToSearchResponse : IMapper<HotelPriceSearchRsp, SearchResponse>
    {
        private readonly IDataSourceRepository _dataSourceRepository;
        private readonly IHotelSourceMapRepository _hotelSourceMapRepository;
        public HotelrPriceSearchRspToSearchResponse(IDataSourceRepository dataSourceRepository, IHotelSourceMapRepository hotelSourceMapRepository)
        {
            _dataSourceRepository = dataSourceRepository;
            _hotelSourceMapRepository = hotelSourceMapRepository;

        }

        public SearchResponse Map(HotelPriceSearchRsp source_object)
        {
            return new SearchResponse()
            {
                ResponseStatus = source_object.Status.ToString(),
                SearchCriteria = source_object.SearchCriteria.Select(s => MapTosearchCriteria(s)).ToList(),
            };
        }
        private SearchCriteria MapTosearchCriteria(SearchRspCriterion searchRspCriterion)
        {
            return new SearchCriteria()
            {
                CheckIn = searchRspCriterion.CheckinDate,
                CheckOut = searchRspCriterion.CheckoutDate,
                CityId = searchRspCriterion.CityId,
                //City =,
                PriceFrom = searchRspCriterion.PriceFrom,
                PriceTo = searchRspCriterion.PriceTo,
                searchCriteriaCombinations = MapSearchCriteriaCombinations(searchRspCriterion.SearchRspCriteriaCombinations)

            };
        }

        private List<SearchCriteriaCombination> MapSearchCriteriaCombinations(List<SearchRspCriteriaCombination> hotelPriceWrappers)
        {
            List<SearchCriteriaCombination> list = new List<SearchCriteriaCombination>();
            hotelPriceWrappers.ForEach(s => list.Add(new SearchCriteriaCombination()
            {
                UnfilteredHotelPrices = MapHotelPrices(s.HotelPrices),
                CheckIn = s.CheckIn,
                CheckOut = s.CheckOut
            }));
            return list;
        }

        private List<UnfilteredHotelPrice> MapHotelPrices(List<TravelBot.Common.Models.HotelPrice> hotelPrices)
        {
            List<UnfilteredHotelPrice> list = new List<UnfilteredHotelPrice>();
            hotelPrices.ForEach(s => list.Add(new UnfilteredHotelPrice()
            {
                Url = s.URL,
                DataSourceId = _dataSourceRepository.FindFromCode(s.Source.ToString()).Id,
                HotelIdFromSource = s.HotelRef,
                Price = s.Price
            }));
            return list;
        }
    }
}
