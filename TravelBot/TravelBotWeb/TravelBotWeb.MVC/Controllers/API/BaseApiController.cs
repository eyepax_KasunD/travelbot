﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TravelBotWeb.Common;
using TravelBotWeb.Models;

namespace TravelBotWeb.Controllers
{
    public class BaseApiController : ApiController
    {

        protected static Logger logger = LogManager.GetCurrentClassLogger();

        public ApiResponseModel<T> GetApiResponseModel<T>(T data)
        {
            return new ApiResponseModel<T>
            {
                Data = data
            };
        }
        public ApiResponseModel<T> GetApiResponseModel<T>(T data, List<string> errors)
        {
            return new ApiResponseModel<T>
            {
                Data = data,
                Errors = errors
            };
        }
    }
}

