﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace TravelBot.Common.Models
{
    public class UserRequestModel
    {
        private DateTime checkingDate;
        public int CountryId { get; set; }
        public string CheckInDate { get; set; }

        public DateTime CheckInDateTime { get
            {
                DateTime dt = DateTime.Now;
                DateTime.TryParseExact(CheckInDate,
                                       "dd/MM/yyyy",
                                       CultureInfo.InvariantCulture,
                                       DateTimeStyles.None,
                                       out dt);
                dt = DateTime.SpecifyKind(dt, DateTimeKind.Utc);
                return dt;
            } }
        public int AdultCount { get; set; }
        public int ChildCount { get; set; }
        public decimal PriceFrom { get; set; }
        public decimal PriceTo { get; set; }
        public string Email { get; set; }
        public int FlexDates { get; set; }
        public string DeviceId { get; set; }
        public List<CityDetails> Destinations { get; set; }
    }
    public class CityDetails
    {
        public int? CityId { get; set; }
        public int Days { get; set; }
    }
    
}