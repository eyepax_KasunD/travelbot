﻿using TravelBot.Common.Repositories;
using System.Linq;
using TravelBot.Common.Models;
using Scraper.Repository.Contracts;

namespace Scraper.Repository
{
    public class HotelRepository:IHotelRepository
    {
        private readonly MongoDBRepository<Hotel> _hotelRepository;
        public HotelRepository()
        {
            _hotelRepository = new MongoDBRepository<Hotel>("HotelInfo", "Hotels");
        }

        public Hotel FindHotelFromRequest(ScraperHotelDetailReq request)
        {
            return _hotelRepository.Find(x => x.HotelIds.Select(h => h.value).Contains(request.HotelId)).FirstOrDefault();
        }
        
        public void AddHotel(Hotel hotel)
        {
            _hotelRepository.Insert(hotel);
        }

        public void Update(ScraperHotelDetailReq request, Hotel hotel)
        {
            _hotelRepository.Update(x => x.HotelIds.Select(h => h.value).Contains(request.HotelId), hotel);
        }

        public Hotel FindHotelFromId(string HotelId)
        {
            return _hotelRepository.Find(x => x.HotelIds.Select(h => h.value).Contains(HotelId)).FirstOrDefault();
        }      
    }
}