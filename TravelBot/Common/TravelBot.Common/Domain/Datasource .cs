﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TravelBot.Common.Domain
{

    [Table("Sys_DataSource")]
    public class DataSource : BaseEntity
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }
    }
}
