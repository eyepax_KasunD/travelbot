﻿using TravelBot.Common.Models;
using TravelBotWeb.Common;

namespace TravelBotWeb.Services.Contracts
{
    public interface IUserServiceSQL
    {
        string AddUser(UserSessionModel userSessionModel);
        bool UpdateUser(string userSessionId, string requestId, int travelbotIndex);
        bool GetUser(string userSessionId);
        bool StopSearch(string userSessionId, StopSearchingModel model);
    }
}
