﻿using TravelBot.Common.Helpers;
using static TravelBot.Common.Enums;

namespace TravelBot.Common.Models
{
    public class ScraperHotelDetailReq
    {
        public string Hotelurl { get; set; }
        public DataSourceCode Website { get; set; }
        public string HotelId { get { return HotelIdentifier.GetIdentifierFromUrl(Hotelurl); } }
        public string RequestId { get; set; }
    }
}