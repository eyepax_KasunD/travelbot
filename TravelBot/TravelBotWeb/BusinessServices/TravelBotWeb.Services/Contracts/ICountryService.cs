﻿using System.Collections.Generic;
using TravelBot.Common.Domain;
using TravelBot.Common.Models;
using TravelBotWeb.Common;

namespace TravelBotWeb.Services.Contracts
{
    public interface ICountryService
    {
        bool AddCountry(Country model);
        List<JourneyPlanModel> GetCountryList(string lang);
    }
}
