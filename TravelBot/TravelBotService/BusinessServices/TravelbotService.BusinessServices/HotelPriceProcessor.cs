﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using TravelBot.Common.Domain;
using TravelBot.Common.Repository.Contracts;
using TravelbotService.BusinessServices.Contracts;
using TravelBotService.Repository.Contracts;

namespace TravelbotService.BusinessServices
{


    public class HotelPriceProcessor : IHotelPriceProcessor
    {
        private readonly ISearchResponseRepository _searchResponseRepository;
        private readonly IHotelPriceRepository _HotelPriceRepository;
        private readonly IHotelPriceSearchQueueRepository _hotelPriceSearchQueueRepository;
        private readonly IHotelDetailQueueRepository _hotelDetailQueueRepository;
        private readonly IHotelSourceMapRepository _hotelSourceMapRepository;
        private readonly IDataSourceRepository _dataSourceRepository;
        private readonly IHotelDetailRepository _hotelDetailRepository;
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public HotelPriceProcessor(
            ISearchResponseRepository searchResponseRepository,
            IHotelPriceRepository HotelPriceRepository,
            IHotelPriceSearchQueueRepository hotelPriceSearchQueueRepository,
            IHotelDetailQueueRepository hotelDetailQueueRepository,
            IHotelSourceMapRepository hotelSourceMapRepository,
            IDataSourceRepository dataSourceRepository,
            IHotelDetailRepository hotelDetailRepository)
        {
            _searchResponseRepository = searchResponseRepository;
            _HotelPriceRepository = HotelPriceRepository;
            _hotelPriceSearchQueueRepository = hotelPriceSearchQueueRepository;
            _hotelSourceMapRepository = hotelSourceMapRepository;
            _dataSourceRepository = dataSourceRepository;
            _hotelDetailQueueRepository = hotelDetailQueueRepository;
            _hotelDetailRepository = hotelDetailRepository;
        }

        public void ProcessHotelPrices(long responseId)
        {
            var searchRsp = _searchResponseRepository.GetSearchResponseFromId(responseId);
            var priceFrom = searchRsp.UserSession.PriceFrom;
            var priceTo = searchRsp.UserSession.PriceTo;
            var priceResults = searchRsp.SearchCriteria.SelectMany(s
                                => s.searchCriteriaCombinations.SelectMany(c =>
                                {
                                    var maxPriceFromOtherCriteria = searchRsp.SearchCriteria.Where(o => o.Id != s.Id)
                                                                        .Sum(a => Math.Min(a.searchCriteriaCombinations.Max(b => b.UnfilteredHotelPrices.Max(d => d.Price)), a.PriceTo));
                                    var minPriceFrom = priceFrom - maxPriceFromOtherCriteria;
                                    minPriceFrom = Math.Max(minPriceFrom, s.PriceFrom);

                                    var minPriceFromOtherCriteria = searchRsp.SearchCriteria.Where(o => o.Id != s.Id)
                                                                        .Sum(a => Math.Max(a.searchCriteriaCombinations.Min(b => b.UnfilteredHotelPrices.Min(d => d.Price)), a.PriceFrom));
                                    var maxPriceTo = priceTo - minPriceFromOtherCriteria;
                                    maxPriceTo = Math.Min(maxPriceTo, s.PriceTo);

                                    return c.UnfilteredHotelPrices.Where(u
                                         => u.Price < maxPriceTo && u.Price  > minPriceFrom);
                                })).ToList();

            var unfilteredHotelPrices = new List<UnfilteredHotelPrice>();
            foreach (var groupedPrices in priceResults.GroupBy(h => h.SearchCriteriaCombinationId))
            {
                unfilteredHotelPrices.AddRange(groupedPrices.OrderBy(p => p.Price).Take(15));
            }

            var hotelPrices = unfilteredHotelPrices.Select((obj, i) => new { i, obj }).Select(u =>
            {
                return
                new TravelBot.Common.Domain.HotelPrice
                {
                    DataSourceId = u.obj.DataSourceId,
                    HotelIdFromSource = u.obj.HotelIdFromSource,
                    SearchCriteriaCombinationId = u.obj.SearchCriteriaCombinationId,
                    Url = u.obj.Url,
                    Price = u.obj.Price,
                    SortOrder = u.i

                };
            }).ToList();

            _HotelPriceRepository.AddAll(hotelPrices);
        }
    }
}
