﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelBotService.Common
{
    public interface IAppSettings
    {
        String Scraper_Domain { get; }
    }
}
