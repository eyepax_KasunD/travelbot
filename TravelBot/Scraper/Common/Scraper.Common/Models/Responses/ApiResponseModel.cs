﻿using System.Collections.Generic;

namespace Scraper.Common.Models.Responses
{
    public class ApiResponseModel<T>
    {

        public T Data { get; set; }
        
        public List<string> Errors { get; set; }
    }
}