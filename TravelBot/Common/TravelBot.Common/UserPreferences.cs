﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelBot.Common
{
    public class UserPreferences
    {
        public string UserSessionId { get; set; }
        public int HotelPriceId { get; set; }
        public string RequestId { get; set; }
    }
}
