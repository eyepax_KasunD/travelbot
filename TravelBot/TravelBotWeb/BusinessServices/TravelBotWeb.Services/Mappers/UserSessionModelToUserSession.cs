﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common;
using TravelBot.Common.Domain;
using TravelBot.Common.Mappers.Contracts;
using TravelBot.Common.Repository.Contracts;
using TravelBotWeb.Common;

namespace TravelBotWeb.Services.Mappers
{
   public class UserSessionModelToUserSession : IMapper<UserSessionModel, UserSession>
    {
        public UserSession Map(UserSessionModel sourceObject)
        {
            return new UserSession()
            {
                Id = sourceObject.UserId,
                CheckIn = sourceObject.CheckInDate,
                FlexiDateCount = Math.Abs(sourceObject.FlexDatesPlus),
                CheckOut = sourceObject.CheckInDate.AddDays(TotalNumOfStayingDays(sourceObject)),
                Adults = sourceObject.AdultCount,
                Children = sourceObject.ChildCount,
                PriceFrom = sourceObject.PriceFrom,
                PriceTo = sourceObject.PriceTo,
                Email = sourceObject.Email,
                DeviceId = sourceObject.DeviceId
            };
        }
        private int TotalNumOfStayingDays(UserSessionModel sourceObject)
        {
            var totalNoOfDays = 0;
            foreach (var e in sourceObject.SearchSegmentList)
            {
                totalNoOfDays = totalNoOfDays + e.NoOfStays;
            };
            return totalNoOfDays;
        }
        
    }
}
