﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using TravelBotService.MVC.Configs;

namespace TravelBotService.MVC
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();
            config.MessageHandlers.Add(new LogRequestAndResponseHandler());
            config.Filters.Add(new HandleAndLogApiErrorAttribute());
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
