﻿using System;
using TravelBot.Common.Models;

namespace Scraper.Common.Models.Queue
{
    public class SearchRequestQueueElement
    {
        public string Id { get; set; }
        public ScraperHotelPriceSearchPlaceQueueReq SearchRequest { get; set; }
        public int ErrorRepetitionCount { get; set; }
    }
}