﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace TravelBotService.Common
{
    public class AppSettings: IAppSettings
    {
        public String Scraper_Domain { get { return GetString("Scraper_Domain"); } }

        private String GetString(string key)
        {
            return WebConfigurationManager.AppSettings[key];
        }
    }
}
