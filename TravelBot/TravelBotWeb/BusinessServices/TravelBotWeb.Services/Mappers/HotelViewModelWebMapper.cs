﻿using TravelBot.Common.Models;
using System;
using System.Linq;
using TravelBotWeb.Common;
using TravelBotWeb.Services.Contracts;
using TravelBotWeb.Services.Models;
using TravelBotWeb.Services.Helpers;

namespace TravelBotWeb.Services.Mappers
{
    public class HotelViewModelWebMapper : IHotelViewModelWebMapper
    {
        private readonly IAppSettings _appSettings;
        public HotelViewModelWebMapper(IAppSettings appSettings)
        {
            _appSettings = appSettings;
        }

        public HotelViewModel MapHotelObjectModelToWeb(SearchRspCriteriaCombination ob, UserSessionModel userSession, string requestId)
        {
            //var reviewtext = Helper.GetReviewText(ob.Hotel.GuestReview.ReviewScore);
            //var review = new Review()
            //{
            //    //NoofReviews = ob.Hotel.GuestReview.ReviewCount,
            //    //Score = Math.Round(ob.Hotel.GuestReview.ReviewScore, 2),
            //    ReviewText = reviewtext
            //};
            var hotelPricing = ob.HotelPrices.OrderBy(h => h.Price).Take(1).Select(pricing =>
               new HotelPricingViewModel
               {
                   //CheckInDate = pricing.CheckInDate.ToLongDateString(),
                   //HotelUrl = Helper.GetEncodedHotelLink(pricing, userSession.UserId, requestId, _appSettings.APP_Domain, userSession.DeviceId),
                   Price = pricing.Price.ToString()
               }).ToList();
            string _hotleRatingImgUrl;
            //if (ob.Hotel.HotelRating == 0)
            //{
            //    _hotleRatingImgUrl = null;
            //}
            //else
            //{
            //    _hotleRatingImgUrl = _appSettings.CDN_Domain + "/assets/images/" + ob.Hotel.HotelRating + "-star.png";
            //}
            HotelViewModel viewmodel = new HotelViewModel()
            {
                //HotelImgUrl = ob.Hotel.ImgUrl.FirstOrDefault(),
                //HotelName = ob.Hotel.Name,
                //HotelRatingImgUrl = _hotleRatingImgUrl,
                //Address = ob.Hotel.Location.LocationString,
                //Reviews = review,
                //HotelPricing = hotelPricing
            }; //only one hotel
            return viewmodel;
        }
    }
}