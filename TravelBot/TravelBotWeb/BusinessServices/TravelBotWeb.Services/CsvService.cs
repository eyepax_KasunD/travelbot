﻿using System.Collections;
using System.IO;
using TravelBotWeb.Services.Contracts;

namespace TravelBotWeb.Services
{
    public class CsvService: ICsvService
    {
        public ArrayList Read() //send email to customer dispalying info of list of hotels
        {

            ArrayList arr = new ArrayList();
            StreamReader reader = new StreamReader(File.OpenRead(@"E:\TravelBot\travelbot\Scraper\TravelBotWeb\Csv\feature_map.csv"));
            var i = 0;
            string contents = File.ReadAllText("E:\\TravelBot\\travelbot\\Scraper\\TravelBotWeb\\Csv\\HotelbookingInfo.txt");
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                if (i == 0)
                {
                    i++;
                    continue;
                }


                string[] cols = line.Split(',');

                string bookingcom = cols[0]; //read only first column :: (bookingcom)
                string[] amenities = bookingcom.Split('|');
                foreach (string s in amenities)
                {
                    if (contents.Contains(s))
                    {
                        string[] p = new string[2];
                        p[0] = cols[2];
                        p[1] = cols[3];
                        arr.Add(p);
                        break;
                    }

                }
                //if (!String.IsNullOrWhiteSpace(line))
                //{
                //    lst.Add(line);        
                //}
            }
            return arr;
        }
    }
}