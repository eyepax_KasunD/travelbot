﻿using Scraper.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Text.RegularExpressions;
using NLog;
using System.Globalization;
using OpenQA.Selenium.Support.UI;
using System.Collections.ObjectModel;
using TravelBot.Common.Models;
using Scraper.Common;
using TravelBot.Common;
using Scraper.Services.Contracts;
using System.Web;
using Scraper.Common.Models.Responses;
using Scraper.Common.Models.Requests;
using static TravelBot.Common.Enums;
using TravelBot.Common.Helpers;

namespace Scraper.Services
{
    public class BookingCom : IScrapingSource
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private readonly IAppSettings _appSettings;
        public BookingCom(IAppSettings appSettings)
        {
            _appSettings = appSettings;
        }

        #region HotelPriceSearch
        public HotelPriceSearchRsp GetPriceResponses(ScraperHotelPriceSearchReq request)
        {
            logger.Info("GetPriceResponses start for the request id : " + request.RequestId);
            List<SearchRspCriteriaCombination> criteriaCombinations = new List<SearchRspCriteriaCombination>();
            var warnings = new List<string>();
            var errors = new List<string>();

            try
            {
                var chromeOptions = new ChromeOptions();
                //chromeOptions.AddArguments(new List<string>() { "headless" });
                using (var driver = new ChromeDriver(chromeOptions))
                {
                    driver.Navigate().GoToUrl(_appSettings.BookingUrl);
                    logger.Info("Booking url  : " + _appSettings.BookingUrl);

                    IWebElement iframeSwitch = driver.FindElement(By.XPath("//*[contains(@id,'booking_widget')]"));
                    driver.SwitchTo().Frame(iframeSwitch);
                    driver.FindElementById("b_destination").SendKeys(request.SearchString);
                    new Actions(driver).SendKeys(Keys.Tab).Perform();

                    driver.FindElementByClassName("searchbtn__btn").Click();
                    string newTabHandle = driver.WindowHandles.Last();
                    var newTab = driver.SwitchTo().Window(newTabHandle);
                    var uriBuilder = new UriBuilder(driver.Url);
                    var qs = HttpUtility.ParseQueryString(uriBuilder.Query);
                    qs.Set("group_adults", request.AdultCount.ToString());
                    qs.Set("group_children", request.ChildCount.ToString());
                    qs.Set("selected_currency", request.CurrencyCode.ToString());
                    qs.Set("no_dorms", "1");
                    qs.Set("nflt", getPriceFilterText(request, driver));

                    uriBuilder.Query = qs.ToString();
                    criteriaCombinations = GetSearchRspCriteriaCombinations(uriBuilder.Uri.ToString(), request);              
                }
            }
            catch (Exception ex)
            {
                errors.Add(ex.ToString());
            }

            if (criteriaCombinations.All(c => c.HotelPrices.Count == 0))
            {
                warnings.Add("Hotel count = 0, source : booking.com ");
                logger.Info("** warnings  :  Hotel count = 0, source : booking.com");
            }

            return new HotelPriceSearchRsp
            {
                SearchCriteria = new List<SearchRspCriterion>()
                {
                    new SearchRspCriterion
                    {
                        CityId = request.CityId,
                        CountryId = request.CountryId,
                        PriceFrom = request.PriceFrom,
                        PriceTo =request.PriceTo,
                        SearchRspCriteriaCombinations = criteriaCombinations
                    }
                },
                RequestId = request.RequestId,
                Status = ResponseStatus.Completed,
                Currency = request.CurrencyCode,
                Errors = errors,
                Warnings = warnings
            };
        }

        public List<SearchRspCriteriaCombination> GetSearchRspCriteriaCombinations(string url, ScraperHotelPriceSearchReq request)
        {
            List<SearchRspCriteriaCombination> CriteriaCombinations = new List<SearchRspCriteriaCombination>();
            try
            {
                var uriBuilder = new UriBuilder(url);
                List<DateTime> checkingdates = new List<DateTime>();
                List<DateTime> checkoutdates = new List<DateTime>();
                DateTime chekin = request.CheckInDate;
                var chromeOptions = new ChromeOptions();

                for (var i = -Math.Abs(request.FlexDatesMinus); i <= request.FlexDatesPlus; i++)
                {
                    checkingdates.Add(chekin.AddDays(i));
                }
                var checkingCheckoutDates = checkingdates.Select(checkin => new Tuple<DateTime, DateTime>(checkin, checkin.AddDays(request.NoOfStays)));
                using (var driver = new ChromeDriver(chromeOptions))
                {
                    foreach (var checkinCheckoutdate in checkingCheckoutDates)
                    {
                        var checkin = checkinCheckoutdate.Item1;
                        var checkout = checkinCheckoutdate.Item2;

                        var qs = HttpUtility.ParseQueryString(uriBuilder.Query);
                        qs.Set("checkin_monthday", checkin.Day.ToString());
                        qs.Set("checkin_year_month", $"{checkin.Year}-{checkin.Month}");
                        qs.Set("checkout_monthday", checkout.Day.ToString());
                        qs.Set("checkout_year_month", $"{checkout.Year}-{checkout.Month}");
                        uriBuilder.Query = qs.ToString();

                        logger.Info("Url of flexible days " + uriBuilder.Uri);
                        driver.Navigate().GoToUrl(uriBuilder.Uri);
                        var hotelCards = driver.FindElementsByXPath("//*[@id=\"hotellist_inner\"]/div[contains(@class, \"sr_property_block\")]").Select((element, j) => new { element, j }).ToList();
                        var hotelPrices = new ConcurrentBag<HotelPrice>();
                        Parallel.ForEach(hotelCards, (hotel) =>
                        // foreach(var hotel in hotelCards)
                        {
                            var hotelPrice = new HotelPrice()
                            {
                                Source = DataSourceCode.Booking,
                                IsPriceMatched = true
                            };
                            try
                            {
                                hotelPrice.Price = getPriceFromText(hotel.element.FindElement(By.XPath(".//div[contains(@class, \"sr_rooms_table_block\")]")).Text);
                                if (hotelPrice.Price > request.PriceTo || hotelPrice.Price < request.PriceFrom)
                                    hotelPrice.IsPriceMatched = false;

                                hotelPrice.URL = hotel.element.FindElement(By.XPath(".//a[contains(@class,'hotel_name_link')]")).GetAttribute("href");
                                hotelPrice.HotelRef = HotelIdentifier.GetIdentifierFromUrl(hotelPrice.URL);

                            }
                            catch (NoSuchElementException)
                            {
                                return;
                            }

                            hotelPrices.Add(hotelPrice);
                        });
                        logger.Info("hotel count : " + hotelPrices.Count);
                        CriteriaCombinations.Add(new SearchRspCriteriaCombination
                        {
                            HotelPrices = hotelPrices.ToList(),
                            CheckIn = checkin,
                            CheckOut = checkout
                        });
                    }
                }
                return CriteriaCombinations;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return CriteriaCombinations;
        }

        private int getNumberFromText(string text)
        {
            MatchCollection matchList = Regex.Matches(text.Replace(",", string.Empty), @"\d+");
            return matchList.Count > 0 ?
                matchList.Cast<Match>().Select(match => match.Value).Select(s => int.Parse(s)).FirstOrDefault() : -1;
        }

        private string getPriceFilterText(ScraperHotelPriceSearchReq request, ChromeDriver driver)
        {
            var priceString = "$0";
            try
            {
                priceString += String.Join(string.Empty,
                    driver.FindElements(By.XPath("//a[contains(@data-name,\"pri\")]//span[contains(@class,\"filter_label\")]")).Select(e => e.Text).ToList());
            }
            catch (Exception) { }
            MatchCollection matchList = Regex.Matches(priceString, @"[$]\d+");
            var priceRanges = matchList.Cast<Match>().Select(match => match.Value.Replace("$", string.Empty)).Distinct().Select(s => int.Parse(s)).OrderBy(p => p).ToList();
            priceRanges.Add(int.MaxValue);
            var returnString = "pri;";
            for (int i = 1; i < priceRanges.Count; i++)
            {
                if (request.PricePerNightFrom < priceRanges[i] && request.PricePerNightTo > priceRanges[i - 1])
                    returnString += $"pri={i};";
            }
            returnString += "oos=1;"; // show available properties only
            return HttpUtility.UrlEncode(returnString);
        }

        private decimal getPriceFromText(string priceText)
        {
            MatchCollection matchList = Regex.Matches(priceText.Replace(",", string.Empty), @"[$]\d+(\.\d+)?|\.\d+");
            if (matchList.Count == 0)
            {
                throw new NoSuchElementException("Price not found");
            }
            return matchList.Cast<Match>().Select(match => match.Value.Replace("$", string.Empty)).Select(s => decimal.Parse(s)).Min();
        }

        #endregion HotelPriceSearch

        #region HotelDetails
        public ScraperHotelDetailRsp GetHotelFeatures(ScraperHotelDetailReq requestModel)
        {
            logger.Info("Get Hotel Features from the website  : " + requestModel.Website);
            var chromeOptions = new ChromeOptions();
            ScraperHotelDetailRsp response = new ScraperHotelDetailRsp();

            Hotel hotel = new Hotel()
            {
                GuestReview =
                {
                    RoomCleanliness = -1,
                    ServiceStaff = -1,
                    RoomComfort = -1,
                    HotelCondition = -1,
                    ReviewCount = -1,
                    ReviewScore = -1,
                    Source = DataSourceCode.Booking
                },
                HotelIds = new List<DataSourceInfo>() { new DataSourceInfo { pricingSource = DataSourceCode.Booking, value = requestModel.HotelId} }
            };

            using (var driver = new ChromeDriver(chromeOptions))
            {
                driver.Navigate().GoToUrl(requestModel.Hotelurl);
                try
                {
                    var searchResults = driver.FindElements(By.XPath(".//a[contains(@class,'hotel_name_link')]"));
                    if(searchResults.Count > 0)
                    {                        
                       driver.Navigate().GoToUrl(searchResults.First().GetAttribute("href"));
                    }
                }
                catch (Exception)
                {
                    //ignore
                }
                try
                {
                    new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.ElementExists((By.XPath("//*[@id=\"hp_hotel_name\"]"))));
                    var hotelName = driver.FindElementByXPath("//*[@id=\"hp_hotel_name\"]").Text;
                    hotel.Name = hotelName;
                }
                catch (Exception e)
                {
                    response.Errors.Add("hotel name is empty, exception : " + e);
                }
                try
                {
                    var reviewString = driver.FindElementByXPath("//*[@class=\"review-score-badge\"]").Text;
                    var reviewCount = driver.FindElementByXPath("//*[@class=\"review-score-widget__subtext\"]").Text;
                    var reviewSection = driver.FindElementsByXPath("//*[@class=\"review-breakdown-tooltip\"]/div/span/ul/li");
                    hotel.GuestReview = GetGuestReviews(reviewSection, reviewString, reviewCount);
                }
                catch (Exception)
                {
                    response.Warnings.Add("errors of guest reviews");
                }

                try
                {
                    string latlong = driver.FindElementByClassName("hp_address_subtitle").GetAttribute("data-bbox");
                    var latLong = GetlatitudeAndLongitude(latlong);
                    hotel.Location.Latitude = latLong.Item1;
                    hotel.Location.Longitude = latLong.Item2;
                }
                catch (Exception)
                {
                    hotel.Location.Latitude = -1;
                    hotel.Location.Longitude = -1;

                }
                try
                {
                    var gallerySection = driver.FindElementsByXPath("//*[@class=\"bh-photo-modal-thumbs-container bh-no-user-select\"]/a");
                    List<string> imgList = new List<string>();
                    imgList = GetImages(gallerySection);
                    hotel.ImgUrl = imgList;
                }
                catch (Exception)
                {
                    response.Warnings.Add("errors of image urls");
                }

                try
                {
                    var entireTitle = driver.FindElementByXPath("//*[@id=\"showMap2\"]").Text;
                    string[] location = entireTitle.Split('–');
                    hotel.Location.LocationString = location[0];
                }
                catch
                {
                    response.Errors.Add("errors of hotel location");
                }
                try
                {
                    string stars = driver.FindElementByXPath("//*[@id=\"wrap-hotelpage-top\"]/div[@class=\"hp__hotel-title\"]/span[@class=\"hp__hotel_ratings\"]/span[@class=\"hp__hotel_ratings__stars nowrap\"]/i").GetAttribute("title");
                    var doubleArray = Regex.Split(stars, @"[^0-9\.]+").Where(c => c != "." && c.Trim() != "");
                    var num = doubleArray.FirstOrDefault();
                    hotel.HotelRating = double.Parse(num);
                }
                catch
                {
                    response.Warnings.Add("Hotel Rating is not mensioned");
                }
                if (response.Warnings.Count > 0)
                {
                    response.Warnings.Add("Hotel url : " + driver.Url);
                }
            }
            response.Hotel = hotel;
            return response;
        }


        private List<string> GetImages(ReadOnlyCollection<IWebElement> gallerySection)
        {
            List<string> imgList = new List<string>();
            for (int i = 0; i < 10 && i < gallerySection.Count; i++)
            {
                if (gallerySection[i].GetAttribute("href") != null)
                {
                    var img = gallerySection[i].GetAttribute("href");
                    imgList.Add(img);
                }
            };
            return imgList;
        }

        private GuestReview GetGuestReviews(ReadOnlyCollection<IWebElement> reviewSection, string reviewString, string reviewCount)
        {
            Hotel hotelFeatures = new Hotel()
            {
                GuestReview =
                {
                    RoomCleanliness = -1,
                    ServiceStaff = -1,
                    RoomComfort = -1,
                    HotelCondition = -1,
                    ReviewCount = -1,
                    ReviewScore = -1,
                    Source = DataSourceCode.Booking
                }
            };
            foreach (var review in reviewSection)
            {
                var reviewName = review.GetAttribute("data-question");
                var reviewRate = review.FindElement(By.XPath(".//div/div")).GetAttribute("data-score");
                if (reviewName.Contains(Constants.BookingHotelGuestReviews.clean))
                {
                    hotelFeatures.GuestReview.RoomCleanliness = double.Parse(reviewRate) / 10;
                }
                else if (reviewName.Contains(Constants.BookingHotelGuestReviews.staff))
                {
                    hotelFeatures.GuestReview.ServiceStaff = double.Parse(reviewRate) / 10;
                }
                else if (reviewName.Contains(Constants.BookingHotelGuestReviews.comfort))
                {
                    hotelFeatures.GuestReview.RoomComfort = double.Parse(reviewRate) / 10;
                }
                else if (reviewName.Contains(Constants.BookingHotelGuestReviews.service))
                {
                    hotelFeatures.GuestReview.HotelCondition = double.Parse(reviewRate) / 10;
                }
            };
            hotelFeatures.GuestReview.ReviewScore = double.Parse(reviewString);
            hotelFeatures.GuestReview.ReviewCount = getNumberFromText(reviewCount);

            return hotelFeatures.GuestReview;
        }

        private Tuple<decimal, decimal> GetlatitudeAndLongitude(string latitudeAndLongitude)
        {
            var ar = new decimal[] { -1, -1 };
            try
            {
                string[] str = latitudeAndLongitude.Split(',');
                decimal long1 = decimal.Parse(str[0]);
                decimal lat1 = decimal.Parse(str[1]);
                decimal long2 = decimal.Parse(str[2]);
                decimal lat2 = decimal.Parse(str[3]);
                decimal longitude = (long1 + long2) / 2;
                decimal latitude = (lat1 + lat2) / 2;
                ar[0] = latitude;
                ar[1] = longitude;
            }
            catch (Exception)
            {
            }
            return new Tuple<decimal, decimal>(ar[0], ar[1]);
        }

        #endregion HotelDetails


        private Review GetReviewHotelPriceSearchResults(IWebElement hotel)
        {
            var rw = new Review() { Count = -1, Score = -1 };
            try
            {
                decimal reviewScore = -1;
                var reviewString = hotel.FindElement(By.XPath(".//span[contains(@class, \"review-score-widget\")]//span[contains(@class,\"review-score-badge\")]")).Text;
                decimal.TryParse(reviewString.Trim(), out reviewScore);
                rw.Score = reviewScore;
            }
            catch (Exception) { }
            try
            {
                var reviewCount = hotel.FindElement(By.XPath(".//span[contains(@class, \"review-score-widget\")]//span[contains(@class,\"review-score-widget__subtext\")]")).Text;
                rw.Count = getNumberFromText(reviewCount);
            }
            catch (Exception) { }
            return rw;
        }


        public List<HotelPriceResponse> GetMinPrice(HotelPriceRequest request)
        {
            List<HotelPriceResponse> str = new List<HotelPriceResponse>();
            try
            {
                var uriBuilder = new UriBuilder(request.Href);
                var qs = HttpUtility.ParseQueryString(uriBuilder.Query);
                string check=qs.Get("checkin"); //have to check
                DateTime chekin = DateTime.Parse(check);
                List<DateTime> checkingdates= new List<DateTime>(), checkoutdates = new List<DateTime>();

                for (var i = -Math.Abs(request.FlexibilityMinus); i <= request.FlexibilityPlus; i++)
                {
                    checkingdates.Add(chekin.AddDays(i));
                }
                checkingdates.ForEach(checkingdate => checkoutdates.Add(checkingdate.AddDays(request.HowManyDays)));

                var chromeOptions = new ChromeOptions();
                using (var driver = new ChromeDriver(chromeOptions))
                {
                    logger.Info("GetMinPrice get price url :" + request.Href);          
                    for (var i = 0; i < checkingdates.Count; i++)
                    {
                        HotelPriceResponse res = new HotelPriceResponse
                        {
                            Checkindate = checkingdates[i].Date,
                            Checkoutdate = checkoutdates[i].Date,
                            Source = request.Source.ToString()
                        };
                        qs.Set("checkin", checkingdates[i].Date.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture));
                        qs.Set("checkout", checkoutdates[i].Date.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture));
                        logger.Info("Url with replaced chkin and chkout dates" + driver.Url);
                        uriBuilder.Query = qs.ToString();
                        driver.Navigate().GoToUrl(uriBuilder.Uri);                    
                        new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.ElementExists((By.XPath("//span[contains(@class,\"hprt-price-price\")]"))));

                        var elements = driver.FindElementsByXPath("//span[contains(@class,\"hprt-price-price\") and not (contains(@class,\"rackrate\"))]");

                        var num = double.MaxValue;                      
                        foreach (var ele in elements)
                        {
                            var val = Regex.Replace(ele.Text, "[^0-9.]", "");
                            if (double.Parse(val) < num)
                            {
                                num = double.Parse(val);
                            }
                            res.MinTotalPrice = num;
                        }
                        res.MinPricePernightPerRoom = -1;
                        str.Add(res);
                    }
                    return str;
                }
            }
            catch (Exception)
            {
                List<HotelPriceResponse> list = new List<HotelPriceResponse>();
                HotelPriceResponse res = new HotelPriceResponse
                {
                    Checkindate = new DateTime(),
                    Checkoutdate = new DateTime(),
                    Source = Constants.ScrapingSources.booking,
                    MinPricePernightPerRoom = -1,
                    MinTotalPrice = -1
                };
                list.Add(res);
                return list;
            }
        }
    }
}