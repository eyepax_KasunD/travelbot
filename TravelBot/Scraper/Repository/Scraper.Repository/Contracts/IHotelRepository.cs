﻿using TravelBot.Common.Models;

namespace Scraper.Repository.Contracts
{
    public interface IHotelRepository
    {
        Hotel FindHotelFromRequest(ScraperHotelDetailReq request);
        void AddHotel(Hotel hotel);
        void Update(ScraperHotelDetailReq request, Hotel hotel);
        Hotel FindHotelFromId(string HotelId);
    }
}
