﻿using System;
using System.Collections.Generic;
using System.Linq;
using TravelBot.Common;
using TravelBot.Common.Domain;
using TravelBot.Common.Models;
using TravelBot.Common.Repository.Contracts;
using TravelbotService.BusinessServices.Contracts;
using TravelBotService.Common;

namespace TravelbotService.BusinessServices.Mappers
{
    public class HotelPriceSearchReqToScraperHotelPriceSearchReqMapper : IHotelSearchRequestMapper
    {
        private readonly ICountryRepository _countryRepository;
        private readonly ISourceCityMapRepository _sourceCityMapRepository;
        private readonly ISourceCountryMapRepository _sourceCountryMapRepository;
        private readonly IDataSourceRepository _dataSourceRepository;
        private readonly IHotelPriceSearchQueueRepository _hotelPriceSearchQueueRepository;

        public HotelPriceSearchReqToScraperHotelPriceSearchReqMapper(ICountryRepository countryRepository,
            ISourceCityMapRepository sourceCityMapRepository,
            ISourceCountryMapRepository sourceCountryMapRepository,
            IDataSourceRepository dataSourceRepository,
            IHotelPriceSearchQueueRepository hotelPriceSearchQueueRepository)
        {
            _countryRepository = countryRepository;
            _sourceCityMapRepository = sourceCityMapRepository;
            _sourceCountryMapRepository = sourceCountryMapRepository;
            _dataSourceRepository = dataSourceRepository;
            _hotelPriceSearchQueueRepository = hotelPriceSearchQueueRepository;

        }
        public List<ScraperHotelPriceSearchPlaceQueueReq> SearchRequestMapper(HotelPriceSearchReqWrapper requestWrapper)
        {
            var request = requestWrapper.HotelPriceSearchReq;
            var queueReqTemplate = new HotelPriceSearchQueue
            {
                DataSourceId = _dataSourceRepository.FindFromCode(Enums.DataSourceCode.Booking.ToString()).Id,
                SearchResponseId = requestWrapper.SearchResponse.Id
            };

            var reqList = request.SearchCriteria.Select(s => 
            {
                return new ScraperHotelPriceSearchPlaceQueueReq
                {
                    ScraperHotelPriceSearchReq = new ScraperHotelPriceSearchReq
                    {
                        CityId = s.CityId,
                        CountryId = s.CountryId,
                        RequestId = requestWrapper.SearchResponse.Id.ToString(),
                        SearchString = GetSearchString(s.CityId, s.CountryId),
                        CheckInDate = s.CheckinDate,
                        CheckOutDate = s.CheckoutDate,
                        AdultCount = request.AdultCount,
                        ChildCount = request.ChildCount,
                        PriceFrom = s.PriceFrom,
                        PriceTo = s.PriceTo,
                        CurrencyCode = request.Currency,
                        ScrapingSource = Enums.DataSourceCode.Booking,
                        FlexDatesPlus = request.FlexDatesPlus,
                        FlexDatesMinus = request.FlexDatesMinus,
                        NoOfStays = Convert.ToInt32((s.CheckoutDate - s.CheckinDate).TotalDays)
                    },
                    SearchCriteriaCombinations = requestWrapper.SearchResponse.SearchCriteria.FirstOrDefault(c => c.CityId == s.CityId).searchCriteriaCombinations.Select(c => new ScraperSearchReqCriteriaCombination
                    {
                        CheckIn = c.CheckIn,
                        CombinationId = (int)c.Id
                    }).ToList()
                };
            }).ToList();

            foreach(var req in reqList)
            {
                var queueReq = _hotelPriceSearchQueueRepository.Add(queueReqTemplate);
                req.RequestId = queueReq.Id;
            }
            return reqList;
        }

        private string GetSearchString(long? cityId, long countryId)
        {
            var DataSourceId = _dataSourceRepository.FindFromCode(Enums.DataSourceCode.Booking.ToString()).Id;
            if (!cityId.HasValue)
            {
                return _sourceCountryMapRepository.GetCountry(countryId, DataSourceId).Value; //for bookingcom pass 1 as last parameter. for agoda pass 3
            }
            else
            {
                return _sourceCityMapRepository.GetCity((long)cityId, DataSourceId).Value; //for bookingcom pass 1 as last parameter. for agoda pass 3
            }
        }
    }
}
