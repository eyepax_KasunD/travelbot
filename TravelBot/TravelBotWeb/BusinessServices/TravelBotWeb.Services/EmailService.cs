﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using TravelBot.Common.Models;
using static TravelBot.Common.Enums;
using TravelBotWeb.Common;
using TravelBotWeb.Services.Contracts;
using NLog;
using TravelBotWeb.Repository.Contracts;
using TravelBotWeb.Services.Models;
using TravelBotWeb.Common.CommonWeb;
using RazorEngine;
using RazorEngine.Templating;
using TravelBot.Common.Mappers.Contracts;

namespace TravelBotWeb.Services
{
    public class EmailService : IEmailService
    {
        private readonly IEmailRepository _emailRepository;
        private readonly IAppSettings _appSettings;
        private readonly IUserRepository _userRepository;
        private readonly Contracts.IUserService _userService;
        private readonly TravelBot.Common.Repository.Contracts.ICountryRepository _countryRepository;
        private readonly TravelBot.Common.Repository.Contracts.ICountryContentRepository _countryContentRepository;
        private readonly IMapper<Tuple<HotelPriceSearchRsp, string, string>, EmailBodyContentModel> _hotelPriceSearchRspToEmailBodyMapper;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public EmailService(IEmailRepository emailRepository, IUserRepository userRepository,
            Contracts.IUserService userService, IAppSettings appsettings,
            TravelBot.Common.Repository.Contracts.ICountryRepository countryRepository,
            TravelBot.Common.Repository.Contracts.ICountryContentRepository countryContentRepository, IMapper<Tuple<HotelPriceSearchRsp, string, string>, EmailBodyContentModel> hotelPriceSearchRspToEmailBodyMapper)
        {
            _emailRepository = emailRepository;
            _appSettings = appsettings;
            _userRepository = userRepository;
            _userService = userService;
            _countryRepository = countryRepository;
            _countryContentRepository = countryContentRepository;
            _hotelPriceSearchRspToEmailBodyMapper = hotelPriceSearchRspToEmailBodyMapper;
        }
        public void SendUserEmail(HotelPriceSearchRsp response, UserSessionModel userSession)
        {
            //find the search request from the search request + reponse queue. no nooed to find user session here. only for checking
            //var userSession = _userRepository.FindRequestFromSearchingObjectId(response.RequestId);

            if (userSession != null)
            {
                var searchingObj = userSession.SearchObjects.Find(x => x.RequestId.Equals(response.RequestId));

                if (searchingObj != null)
                {
                    try
                    {
                        //    var res = _userService.GetHotelSearchResponseFromScraper(userSession, requestId);
                        if (response.SearchCriteria.First().SearchRspCriteriaCombinations != null)
                        {
                            userSession.EmailScheduleTime = userSession.EmailScheduleTime.AddHours(Constants.EmailScheduleTimePeriod);

                            if (!string.IsNullOrWhiteSpace(userSession.Email))
                            {
                                var emailStatus = EmailSending(response, userSession, response.RequestId);

                                if (emailStatus == EmailStatus.Error)
                                {
                                    logger.Info("Error occured in email scheduler email state should be error");
                                    userSession.EmailStatus = EmailStatus.Error;
                                    userSession.LoggedErrorTime = DateTime.Now;
                                }
                            }
                        }
                        else
                        {
                            userSession.EmailStatus = EmailStatus.Pending;
                            logger.Info("Email state should be pending");
                        }
                    }
                    catch (Exception e)
                    {
                        logger.Info("Error occured in email scheduler email state should be error" + e);
                        userSession.LoggedErrorTime = DateTime.Now;
                        _userRepository.Update(userSession);
                    }
                }
            }
        }
        public EmailStatus EmailSending(HotelPriceSearchRsp response, UserSessionModel userSession, string requestId)
        {
            var countryId = userSession.SearchSegmentList.FirstOrDefault().CountryId;
            var countryname = _countryContentRepository.FindCountryContentByLanguage(countryId, 1).Name; //countryId=1 for english
            var bodyContent = _hotelPriceSearchRspToEmailBodyMapper.Map(new Tuple<HotelPriceSearchRsp, string, string>(response,userSession.UserId, requestId));
            bodyContent.Country = countryname;
            bodyContent.SessionId = userSession.UserId;
            bodyContent.CheckinDate = userSession.CheckInDate;
            bodyContent.PriceFrom = userSession.PriceFrom;
            bodyContent.PriceTo = userSession.PriceTo;
            bodyContent.ViewInWebUrl = $@"{_appSettings.APP_Domain}/journey/hotellist?id={userSession.UserId}";
            bodyContent.StopSearchUrl = $@"{_appSettings.APP_Domain}/journey/StopSearch?id={userSession.UserId}";
            HotelSearchResponseEmail emailmodel = new HotelSearchResponseEmail()
            {
                To = new List<string>
                {
                    userSession.Email
                },
                Cc = new List<string>(),
                Bcc = _appSettings.Admin_Emails,
                Subject = $"Your journey to {countryname}",
                EmailBodyContent = bodyContent
            };
            var EmailResponse = SendEmailtoCustomer(emailmodel);

            return EmailResponse.emailStatus;
        }
     

        public EmailResponseModel SendEmailtoCustomer(HotelSearchResponseEmail model)
        {
            EmailRequestModel _emailRequestModel = new EmailRequestModel()
            {
                Body = GenerateCshtml(model.EmailBodyContent),
                To = model.To,
                Cc = model.Cc,
                Bcc = model.Bcc,
                Subject = model.Subject
            };

            return _emailRepository.SendEmail(_emailRequestModel);
        }
        private string GenerateCshtml(EmailBodyContentModel model)
        {
            var result = Engine.Razor.RunCompile(File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "\\EmailTemplates\\hotel-results.cshtml"), "EmailTemplate", null, model);
            return result;
        }

        public EmailResponseModel SendEmailDirectly(EmailRequestModel model) //send without generating cshtml body //send error reports to Admin
        {
            return _emailRepository.SendEmail(model);
        }

        public EmailStatus ResendEmail(string id)
        {
            return ReSendEmailFromUserSession(_userRepository.FindRequestFromId(id));
        }

        private EmailStatus ReSendEmailFromUserSession(UserSessionModel userSession)
        {
            EmailStatus success = EmailStatus.Pending;
            var res1 = userSession.SearchObjects.First();
            if (res1 != null)
            {
                logger.Info("Sending emails in email service");
                success = EmailSending(res1.HotelSearchResponse, userSession, res1.RequestId);
            }
            else
            {
                //TODO : send first email for the moment but need to send the  correct email
                var res = _userService.RetrieveHotelPriceSearchRsp(userSession, userSession.SearchObjects.First().RequestId);
                if (res.Data.Status.Equals(ResponseStatus.Completed) && res.Data.SearchCriteria.First().SearchRspCriteriaCombinations != null)
                {
                    logger.Info("Sending emails in email service else");
                    success = EmailSending(res.Data, userSession, res1.RequestId);
                }
            }
            return success;
        }
        public EmailStatus SendEmailFromUserSession(UserSessionModel userSession, string requestId)
        {
            EmailStatus success = EmailStatus.Pending;
            var res = _userService.RetrieveHotelPriceSearchRsp(userSession, requestId);
            if (res.Data.Status.Equals(ResponseStatus.Completed) && res.Data.SearchCriteria.First().SearchRspCriteriaCombinations != null)
            {
                success = EmailSending(res.Data, userSession, requestId);
            }
            return success;
        }
      
    }
}