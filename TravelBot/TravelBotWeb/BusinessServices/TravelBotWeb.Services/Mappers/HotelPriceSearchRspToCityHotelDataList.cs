﻿using System;
using System.Collections.Generic;
using System.Linq;
using TravelBot.Common.Mappers.Contracts;
using TravelBot.Common.Models;
using TravelBot.Common.Repository.Contracts;
using TravelBotWeb.Common;
using TravelBotWeb.Services.Contracts;
using TravelBotWeb.Services.Helpers;
using TravelBotWeb.Services.Models;

namespace TravelBotWeb.Services.Mappers
{
    public class HotelPriceSearchRspToCityHotelDataList : IMapper<Tuple<HotelPriceSearchRsp, int, string, string>, List<CityHotelData>>
    {
        private readonly IAppSettings _appSettings;
        private readonly ICityContentRepository _cityContentRepository;
        private readonly ICountryContentRepository _countryContentRepository;
        private readonly IImageRepository _imageRepository;
        public HotelPriceSearchRspToCityHotelDataList(IAppSettings appSettings, 
            ICityContentRepository cityContentRepository,
            ICountryContentRepository countryContentRepository,
            IImageRepository imageRepository)
        {
            _appSettings = appSettings;
            _cityContentRepository = cityContentRepository;
            _imageRepository = imageRepository;
            _countryContentRepository = countryContentRepository;
        }

        public List<CityHotelData> Map(Tuple<HotelPriceSearchRsp, int, string, string> input)
        {
            var rsp = input.Item1;
            var flexDate = input.Item2;
            var userSessionId = input.Item3;
            var requestId = input.Item4;
            var cityHotelDataList = rsp.SearchCriteria.Select(searchCriteria =>
            {
                var cmb = searchCriteria.SearchRspCriteriaCombinations.FirstOrDefault(cm => cm.CheckIn.Equals(searchCriteria.CheckinDate.AddDays(flexDate)));
                return new CityHotelData
                {
                    City = searchCriteria.CityId.HasValue ? _cityContentRepository.FindCityContent((long)searchCriteria.CityId, 1).Name
                    : _countryContentRepository.FindCountryContentByLanguage((long)searchCriteria.CountryId, 1).Name, //1 for english TODO: must include in rsp
                    CurrencyPrefix = "$",
                    NoOfStays = (cmb.CheckOut - cmb.CheckIn).Days,
                    Hotels = cmb.HotelPrices.Where(h => !string.IsNullOrEmpty(h.HotelRef)).Select(h => MapToHotelEmailViewModel(h, rsp, userSessionId, requestId)).ToList()
                };
            }).ToList();
            return cityHotelDataList;
        }

        private HotelViewModel MapToHotelEmailViewModel(HotelPrice hotelPrice, HotelPriceSearchRsp rsp, string userSessionId, string requestID)
        {
            var hotel = rsp.HotelList.Where(h => h.Id.Equals(hotelPrice.HotelRef)).FirstOrDefault();
            var datasource = rsp.DataSources.Where(h => h.Code == hotelPrice.Source).FirstOrDefault();
            return new HotelViewModel
            {
                Address = hotel.Location.LocationString,
                HotelName = hotel.Name,
                Price = hotelPrice.Price,
                HotelRatingImgUrl = _appSettings.CDN_Domain + "/assets/images/" + hotel.HotelRating + "-star.png",
                BookingUrl = Helper.GetEncodedHotelLink(hotelPrice.HotelPriceId.ToString(), userSessionId, requestID, _appSettings.APP_Domain),
                HotelImgUrl = hotel.ImgUrl.FirstOrDefault(),
                SourceReview = MapReviews(hotel.GuestReview, datasource),
                TravelbotReview = MapReviews(hotel.GuestReview, datasource)
            };
        }

        private Review MapReviews(GuestReview guestReview, DataSource dataSource)
        {
            return new Review()
            {
                IconUrl = dataSource.Icon,
                NoofReviews = guestReview.ReviewCount,
                // ReviewText=guestReview.,
                Score = guestReview.ReviewScore
            };
        }
    }
}
