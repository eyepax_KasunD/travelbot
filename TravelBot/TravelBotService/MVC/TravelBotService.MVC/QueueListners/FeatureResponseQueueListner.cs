﻿using NLog;
using System;
using System.Messaging;
using TravelBot.Common;
using TravelBot.Common.Models.Queue;
using TravelbotService.BusinessServices.Contracts;

namespace TravelBotService.MVC.QueueListners
{
    public class FeatureResponseQueueListner
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static readonly IQueueProcessor<HotelDetailRspQueueElement> _queueProcessor = UnityConfig.Resolve<IQueueProcessor<HotelDetailRspQueueElement>>();

        public static void Start()
        {
            var Queue = new MessageQueue(Constants.Queues.ScraperHotelDetailRspQueue);
            Queue.Formatter = new XmlMessageFormatter(new Type[] { typeof(HotelDetailRspQueueElement) });
            while (true)
            {
                logger.Info("INSIDE WHILE LOOP FEATURE RESPONSE");
                var message = Queue.Receive();
                if (message == null)
                    continue;
                logger.Info("FEATURE RESPONSE FOUND !!!");
                HotelDetailRspQueueElement element = new HotelDetailRspQueueElement();
                element = (HotelDetailRspQueueElement)message.Body;
                _queueProcessor.Process(element);
                //var x = mapper.Map(element);
            }
        } 
    }
}
