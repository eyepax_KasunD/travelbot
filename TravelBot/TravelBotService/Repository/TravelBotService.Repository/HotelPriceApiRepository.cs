﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Web.Script.Serialization;
using TravelBot.Common.Models;
using TravelBotService.Common;
using TravelBotService.Repository.Contracts;

namespace TravelBotService.Repository
{
    public class HotelPriceApiRepository: IHotelPriceApiRepository
    {
        private readonly IAppSettings _appSettings;
        public HotelPriceApiRepository(IAppSettings appSettings)
        {
            _appSettings = appSettings;
        }
        public HotelPriceSearchRsp CalltoScraper(ScraperHotelPriceSearchPlaceQueueReq req)
        {
            ApiResponseModel<HotelPriceSearchRsp> responseContent = null;
            var client = new RestClient(_appSettings.Scraper_Domain);
            var request = new RestRequest("api/Queue/PlaceHotelPriceSearchReq", Method.POST);
            var json = JsonConvert.SerializeObject(req);
            request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            var content = response.Content;
            responseContent = JsonConvert.DeserializeObject<ApiResponseModel<HotelPriceSearchRsp>>(content);
            return responseContent.Data;
        }
        public AgodaLongTailSearchRsp CallAgodaAPi(AgodaLongTailSearchReq agodaApiRequest)
        {
            AgodaLongTailSearchRsp responseContent = null;
            var client = new RestClient("http://affiliateapi7643.agoda.com/");
            var request = new RestRequest("affiliateservice/lt_v1", Method.POST);
            request.AddHeader("Accept-Encoding", "gzip");
            request.AddHeader("Authorization", "1807119:72c39a71-f905-49dc-bcbc-8c2c84cebe4b");
            request.AddHeader("Accept", "application/json");
            var json = JsonConvert.SerializeObject(agodaApiRequest);
            request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            var content = response.Content;
#if DEBUG
            var path = $@"{AppDomain.CurrentDomain.BaseDirectory}logs\agoda";
            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }
            System.IO.File.WriteAllText($@"{path}\{DateTime.Now.Ticks}_req.json", json);
            System.IO.File.WriteAllText($@"{path}\{DateTime.Now.Ticks}_rsp.json", content);
#endif
            responseContent = JsonConvert.DeserializeObject<AgodaLongTailSearchRsp>(content);
            return responseContent;
        }
    }
}
