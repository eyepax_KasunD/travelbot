﻿using static TravelBot.Common.Enums;

namespace Scraper.Services.Contracts
{
    public interface IScrapingSourceFactory
    {
        IScrapingSource Get(DataSourceCode website);
    }
}
