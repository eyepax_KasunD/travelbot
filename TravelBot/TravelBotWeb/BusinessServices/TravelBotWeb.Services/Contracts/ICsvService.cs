﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelBotWeb.Services.Contracts
{
    public interface ICsvService
    {
        ArrayList Read();
    }
}
