﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;
using TravelBot.Common.Mappers.Contracts;
using TravelBot.Common.Models;
using TravelBot.Common.Repository.Contracts;
using static TravelBot.Common.Enums;

namespace TravelbotService.BusinessServices.Mappers
{
    public class SearchResponseToHotelrPriceSearchRsp : IMapper<SearchResponse, HotelPriceSearchRsp>
    {
        private readonly IDataSourceRepository _dataSourceInfoRepository;
        private readonly IHotelSourceMapRepository _hotelSourceMapRepository;
        private readonly IHotelRepository _hotelRepository;
        private readonly ICurrencyRepository _currencyRepository;
        private readonly IMapper<TravelBot.Common.Domain.Hotel, TravelBot.Common.Models.Hotel> _searchRspToHotelMapper;

        public SearchResponseToHotelrPriceSearchRsp(IDataSourceRepository dataSourceInfoRepository,
            IHotelSourceMapRepository hotelSourceMapRepository,
            IMapper<TravelBot.Common.Domain.Hotel,TravelBot.Common.Models.Hotel> searchRspToHotelMapper,
            ICurrencyRepository currencyRepository,
            IHotelRepository hotelRepository)
        {
            _dataSourceInfoRepository = dataSourceInfoRepository;
            _hotelSourceMapRepository = hotelSourceMapRepository;
            _searchRspToHotelMapper = searchRspToHotelMapper;
            _hotelRepository = hotelRepository;
            _currencyRepository = currencyRepository;

        }

        public HotelPriceSearchRsp Map(SearchResponse searchResponse)
        {
            var searchCriteria = searchResponse.SearchCriteria.Select(s => MapTosearchRSPCriteria(s)).ToList();

            var distinctHotelIds = searchCriteria.SelectMany(c => 
                c.SearchRspCriteriaCombinations.SelectMany(cmb => cmb.HotelPrices).Where(p => p.HotelRef != null).Select(p => long.Parse(p.HotelRef))).Distinct().ToList();
            var hotelList = distinctHotelIds.Select(id => _hotelRepository.GetHotelFromId(id)).Select(h => _searchRspToHotelMapper.Map(h)).ToList();

            var dataSourceCodes = searchCriteria.SelectMany(c =>
                c.SearchRspCriteriaCombinations.SelectMany(cmb => cmb.HotelPrices).Select(p => p.Source)).Distinct().ToList();
            var dataSources = dataSourceCodes.Select(d => _dataSourceInfoRepository.FindFromCode(d.ToString())).Select(s => MapDataSource(s)).ToList();
            var currency = _currencyRepository.Get((long)searchResponse.CurrencyId);

            return new HotelPriceSearchRsp()
            {
                Status = (ResponseStatus)Enum.Parse(typeof(ResponseStatus), searchResponse.ResponseStatus),
                RequestId = searchResponse.RequestId,
                SearchCriteria = searchCriteria,
                HotelList = hotelList,
                Currency = (CurrencyCode)Enum.Parse(typeof(CurrencyCode), currency.Code),
                DataSources = dataSources                
            };
        }

      
        private SearchRspCriterion MapTosearchRSPCriteria(SearchCriteria searchCriterion)
        {
            return new SearchRspCriterion()
            {
                CheckinDate = searchCriterion.CheckIn,
                CheckoutDate = searchCriterion.CheckOut,
                CityId = searchCriterion.CityId,
                CountryId = searchCriterion.CountryId,
                PriceFrom = searchCriterion.PriceFrom,
                PriceTo = searchCriterion.PriceTo,
                SearchRspCriteriaCombinations = searchCriterion.searchCriteriaCombinations.Select(s => MapsearchCriteriaCombinationsToRSPCombinations(s)).ToList()

            };
        }

        private SearchRspCriteriaCombination MapsearchCriteriaCombinationsToRSPCombinations(SearchCriteriaCombination searchCriteriaCombinations)
        {
            return new SearchRspCriteriaCombination
            {
                CheckIn = searchCriteriaCombinations.CheckIn,
                CheckOut = searchCriteriaCombinations.CheckOut,
                HotelPrices = searchCriteriaCombinations.HotelPrices.Select(s => MapHotelPrices(s)).ToList()
            };
        }

        private TravelBot.Common.Models.HotelPrice MapHotelPrices(TravelBot.Common.Domain.HotelPrice hotelPrice)
        {
            return new TravelBot.Common.Models.HotelPrice
            {
                HotelRef = GetIdFromHotelSourceMapTable(hotelPrice.DataSourceId, hotelPrice.HotelIdFromSource),
                //IsPriceMatched= ,
                Price = hotelPrice.Price,
                HotelPriceId = hotelPrice.Id,
                SortOrder=hotelPrice.SortOrder,
                Source = (DataSourceCode)Enum.Parse(typeof(DataSourceCode), hotelPrice.DataSource.Code),
                URL = hotelPrice.Url
            };
        }

        private string GetIdFromHotelSourceMapTable(long? dataSourceId, string hotelIdFromSource)
        {
            return _hotelSourceMapRepository.GetHotelIds(hotelIdFromSource, dataSourceId)?.ToString();
        }

        private TravelBot.Common.Models.DataSource MapDataSource(TravelBot.Common.Domain.DataSource dataSource)
        {
            return new TravelBot.Common.Models.DataSource
            {
                Code = (DataSourceCode)Enum.Parse(typeof(DataSourceCode), dataSource.Code),
                Icon = dataSource.Icon,
                Name = dataSource.Name                
            };
        }
    }
}
