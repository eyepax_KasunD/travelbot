﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelBot.Common.Models
{
    public class CityInfo //one city
    {
        public List<ImageModel> CityImages { get; set; }
        public string CityName { get; set; }
        public string Description { get; set; }
        public string Province { get; set; }
        public string CityThumbnailUrl { get; set; }
    }
}
