﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Configuration;

namespace TravelBotWeb.Common
{
    public class AppSettings : IAppSettings
    {
        public String CDN_Domain { get { return GetString("CDN_Domain"); } }
        public String APP_Domain { get { return GetString("APP_Domain"); } }
        public String Scraper_Domain { get { return GetString("Scraper_Domain"); } }
        public String Service_Domain { get { return GetString("Service_Domain"); } }
        public List<String> Admin_Emails { get { return GetString("Admin_Emails").Equals("")? new List<string>(): GetString("Admin_Emails").Split(',').ToList(); } }
        public String Affiliate_ID { get { return GetString("Affiliate_ID"); } }

        private String GetString(string key)
        {      
            return WebConfigurationManager.AppSettings[key];
        }
    }
}