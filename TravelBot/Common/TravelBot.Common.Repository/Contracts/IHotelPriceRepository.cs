﻿using System.Collections.Generic;
using TravelBot.Common.Domain;

namespace TravelBot.Common.Repository.Contracts
{
    public interface IHotelPriceRepository
    {
        HotelPrice Add(HotelPrice p);
        List<HotelPrice> AddAll(List<HotelPrice> p);
    }
}
