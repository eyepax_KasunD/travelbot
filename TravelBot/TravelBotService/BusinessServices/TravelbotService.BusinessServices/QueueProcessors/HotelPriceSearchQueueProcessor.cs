﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using TravelBot.Common;
using TravelBot.Common.Domain;
using TravelBot.Common.Helpers;
using TravelBot.Common.Mappers.Contracts;
using TravelBot.Common.Models;
using TravelBot.Common.Models.Queue;
using TravelBot.Common.Repository.Contracts;
using TravelbotService.BusinessServices.Contracts;
using TravelBotService.Repository.Contracts;

namespace TravelbotService.BusinessServices.QueueProcessors
{
    public class HotelPriceSearchQueueProcessor: IQueueProcessor<HotelPriceSearchRspQueueElement>
    {
        private readonly IMapper<HotelPriceSearchRsp, SearchResponse> _mapper;
        private readonly IUnfilteredHotelPriceRepository _unfilteredHotelPriceRepository;
        private readonly IHotelPriceSearchQueueRepository _hotelPriceSearchQueueRepository;
        private readonly IHotelPriceResponseService _hotelPriceResponseService;
        private readonly ISearchResponseRepository _searchResponseRepository;
        private readonly IHotelPriceRepository _HotelPriceRepository;
        private readonly IHotelDetailQueueRepository _hotelDetailQueueRepository;
        private readonly IHotelSourceMapRepository _hotelSourceMapRepository;
        private readonly IDataSourceRepository _dataSourceRepository;
        private readonly IHotelDetailRepository _hotelDetailRepository;
        private readonly ISearchStatusRepository _searchStatusRepository;
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public HotelPriceSearchQueueProcessor(IUnfilteredHotelPriceRepository unfilteredHotelPriceRepository,
            IMapper<HotelPriceSearchRsp, SearchResponse> mapper,
            IHotelPriceSearchQueueRepository hotelPriceSearchQueueRepository,
            IHotelPriceResponseService hotelPriceResponseService,
            ISearchResponseRepository searchResponseRepository,
            IHotelPriceRepository HotelPriceRepository,
            IHotelDetailQueueRepository hotelDetailQueueRepository,
            IHotelSourceMapRepository hotelSourceMapRepository,
            IDataSourceRepository dataSourceRepository,
            IHotelDetailRepository hotelDetailRepository,
            ISearchStatusRepository searchStatusRepository) {
            _unfilteredHotelPriceRepository = unfilteredHotelPriceRepository;
            _mapper = mapper;
            _hotelPriceSearchQueueRepository = hotelPriceSearchQueueRepository;
            _hotelPriceResponseService = hotelPriceResponseService;
            _searchResponseRepository = searchResponseRepository;
            _HotelPriceRepository = HotelPriceRepository;
            _hotelSourceMapRepository = hotelSourceMapRepository;
            _dataSourceRepository = dataSourceRepository;
            _hotelDetailQueueRepository = hotelDetailQueueRepository;
            _hotelDetailRepository = hotelDetailRepository;
            _searchStatusRepository = searchStatusRepository;
        }

        public void Process(HotelPriceSearchRspQueueElement element)
        {
            logger.Info("Process price search reponse queue element, queue req id = "+ element.QueueRequestId);
            var queueElement = _hotelPriceSearchQueueRepository.Find(q => q.Id == element.QueueRequestId);

            if (element.ResponseStatus == TravelBot.Common.Enums.ResponseStatus.Completed)
            {
                var res = _mapper.Map(element.SearchResponse);
                var searchCriteriaCombinations = element.QueueRequest.SearchCriteriaCombinations;
                var combinations = res.SearchCriteria.FirstOrDefault(s => s.CityId == element.QueueRequest.ScraperHotelPriceSearchReq.CityId).searchCriteriaCombinations.ToList();

                combinations.ForEach(c =>
                {
                    var id = searchCriteriaCombinations.First(s => s.CheckIn == c.CheckIn).CombinationId;
                    var prices = c.UnfilteredHotelPrices.ToList();
                    prices.ForEach(p =>
                    {
                        p.SearchCriteriaCombinationId = id;
                    });
                    _unfilteredHotelPriceRepository.AddAll(prices);
                });

            }
            else if(element.ResponseStatus == TravelBot.Common.Enums.ResponseStatus.Error)
            {
                queueElement.ErrorOccurred = true;
            }

            queueElement.ResponseRecieved = true;
            queueElement.LastModifiedDateTime = DateTime.UtcNow;
            _hotelPriceSearchQueueRepository.Update(queueElement);


            if(HaveAllResponseseRecieved(queueElement))
            {
                if (!QueueHotelDetailRequestIfNeeded(queueElement))
                {
                    var searchStatus = _searchStatusRepository.GetSearchStatus((long)queueElement.SearchResponseId, (long)queueElement.DataSourceId);
                    searchStatus.Status = (int)Enums.ResponseStatus.Completed;
                    _searchStatusRepository.Update(searchStatus);

                    _hotelPriceResponseService.NotifyResponseCompletion((long)queueElement.SearchResponseId);
                }
            }
        }

        private bool HaveAllResponseseRecieved(HotelPriceSearchQueue queueElement)
        {
            return !_hotelPriceSearchQueueRepository.IsRemainingResponsesExists((long)queueElement.SearchResponseId, (long)queueElement.DataSourceId);
        }

        private bool QueueHotelDetailRequestIfNeeded(HotelPriceSearchQueue queueElement)
        {
            var responseId = (long)queueElement.SearchResponseId;
            var searchRsp = _searchResponseRepository.GetSearchResponseFromId(responseId);
            var priceResults = searchRsp.SearchCriteria.SelectMany(s => s.searchCriteriaCombinations
                                .SelectMany(c => c.UnfilteredHotelPrices
                                    .Where(p => p.DataSourceId == (long)queueElement.DataSourceId).ToList())).ToList();


            var dataSourceId = _dataSourceRepository.FindFromCode(Enums.DataSourceCode.Booking.ToString()).Id;

            var nonExistingHotels = priceResults.Select(h => new { h.Url, HotelIdFromSource = HotelIdentifier.GetIdentifierFromUrl(h.Url) })
                .GroupBy(d => d.HotelIdFromSource).Select(g => g.First()).Where(h =>
                {
                    return !(_hotelSourceMapRepository.IsExists(dataSourceId, h.HotelIdFromSource));
                }).ToList();


            var hotelDetailRequests = nonExistingHotels.Select(p =>
            {
                var existsInQueue = _hotelDetailQueueRepository.IsExistInQueue(dataSourceId, p.HotelIdFromSource);
                var queueId = _hotelDetailQueueRepository.Add(
                    new HotelDetailQueue
                    {
                        DataSourceId = dataSourceId,
                        SearchResponseId = responseId,
                        HotelIdFromSource = p.HotelIdFromSource,
                    }).Id;
                return new { queueId, p.HotelIdFromSource, p.Url, existsInQueue };
            }).Where(h => !h.existsInQueue).Select(p =>
            {
                return new ScraperHotelDetailPlaceQueueReq
                {
                    QueueRequestId = p.queueId,
                    ScraperHotelDetailReq = new ScraperHotelDetailReq
                    {
                        Hotelurl = p.Url,
                        Website = Enums.DataSourceCode.Booking,
                        RequestId = responseId.ToString()
                    }
                };
            });

            hotelDetailRequests.ToList().ForEach(req =>
            {
                _hotelDetailRepository.GetHotelDetailFromScraper(req);
            });
            return nonExistingHotels.Count() > 0;
        }
    }
}
