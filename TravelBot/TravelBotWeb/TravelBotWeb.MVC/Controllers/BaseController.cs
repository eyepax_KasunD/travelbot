﻿using System.Web.Mvc;
using NLog;
using TravelBotWeb.Common;

namespace TravelBotWeb.Controllers
{
    public class BaseController : Controller
    {
        protected readonly IAppSettings _appsettings;
        protected static Logger logger = LogManager.GetCurrentClassLogger();
    
        public BaseController(IAppSettings appsettings)
        {
            _appsettings = appsettings;
            ViewBag.Title = "DestinationLanka";
            ViewBag.Domain = _appsettings.CDN_Domain;
            ViewBag.APP_Domain = _appsettings.APP_Domain;
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            logger.Error(filterContext.Exception);
            filterContext.ExceptionHandled = true;
            filterContext.Result = RedirectToAction("InternalError", "Error");
        }
    }
}

