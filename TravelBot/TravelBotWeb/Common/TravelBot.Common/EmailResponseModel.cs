﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static TravelBot.Common.Enums;

namespace TravelBotWeb.Common
{
    public class EmailResponseModel
    {
        public string result { get; set; }
        public EmailStatus emailStatus { get; set; }
    }
}