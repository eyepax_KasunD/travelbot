﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Models;
using TravelBotService.Common;

namespace TravelBotService.Repository.Contracts
{
    public interface IHotelPriceApiRepository
    {
         HotelPriceSearchRsp CalltoScraper(ScraperHotelPriceSearchPlaceQueueReq req);
         AgodaLongTailSearchRsp CallAgodaAPi(AgodaLongTailSearchReq agodaApiRequest);
    }
}
