﻿using Scraper.Common.Models.Requests;
using Scraper.Common.Models.Responses;
using System.Collections.Generic;
using TravelBot.Common.Models;

namespace Scraper.Services.Contracts
{
    public interface IScraperService
    {
        HotelPriceSearchRsp SearchHotelPrices(ScraperHotelPriceSearchReq request);
        ScraperHotelDetailRsp ScrapeHotelInfo(ScraperHotelDetailReq request);
        List<HotelPriceResponse> GetPriceWithFlexibility(HotelPriceRequest href);
    }
}
