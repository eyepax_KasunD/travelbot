﻿using TravelBot.Common.Domain;
using TravelBot.Common.Models;
using TravelBotWeb.Common;

namespace TravelBotWeb.Services.Contracts
{
    public interface IJourneyPlanModelMapper
    {
        JourneyPlanModel ModelMapper(Country country, string lang);
    }
}
