﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;

namespace TravelBot.Common.Repository.Contracts
{
    public interface IUnfilteredHotelPriceRepository
    {
        UnfilteredHotelPrice Add(UnfilteredHotelPrice hotelPrice);
        List<UnfilteredHotelPrice> AddAll(List<UnfilteredHotelPrice> hotelPrice);
        List<UnfilteredHotelPrice> Find(Expression<Func<UnfilteredHotelPrice,bool>> func);
    }
}
