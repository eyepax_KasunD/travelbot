﻿using NLog;
using System.Messaging;
using System.Threading;

namespace TravelBot.Common
{
    public class QueueProvider
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public void PublishInQueue<T>(T element, string queueName)
        {
            using (var Queue = new MessageQueue(queueName))
            {
                    SendMessage(Queue, element, queueName);
                    Thread.Sleep(10);              
            }
        }

        private static void SendMessage<T>(MessageQueue queue, T content, string name)
        {
            queue.Send(content);
            logger.Info($"** An elemenet was published in {name} queue");
        }

    }
}