﻿using AgodaHotelImporter.Models;
using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgodaHotelImporter
{
    public class AgodaHotelImportMapper : ClassMap<AgodaHotel>
    {
        public AgodaHotelImportMapper()
        {
            AutoMap();
            Map(m => m.Images).ConvertUsing(row =>
            {
                return new List<string> { row.GetField("photo1"), row.GetField("photo2"), row.GetField("photo3"), row.GetField("photo4"), row.GetField("photo5") };
            });
        }
    }
}
