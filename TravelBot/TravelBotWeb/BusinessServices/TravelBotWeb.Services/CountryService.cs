﻿using System;
using System.Collections.Generic;
using System.Linq;
using TravelBot.Common.Domain;
using TravelBot.Common.Models;
using TravelBot.Common.Repository.Contracts;
using TravelBotWeb.Common;
using TravelBotWeb.Repository;
using TravelBotWeb.Repository.Contracts;
using TravelBotWeb.Services.Contracts;

namespace TravelBotWeb.Services
{
    public class CountryService:ICountryService
    {
        private readonly ICountryRepository _countryRepository;
        private readonly ICountryContentRepository _countryNameRepository;
        private readonly IJourneyPlanModelMapper _journeyPlanModelMapper;
        public CountryService(ICountryRepository countryRepository, ICountryContentRepository countryNameRepository, IJourneyPlanModelMapper journeyPlanModelMapper)
        {
            _countryRepository = countryRepository;
            _countryNameRepository = countryNameRepository;
            _journeyPlanModelMapper = journeyPlanModelMapper;
        }
        public bool AddCountry(Country model)
        {
            try
            {
                _countryRepository.Add(model);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public List<JourneyPlanModel> GetCountryList(string lang)
        {
            return _countryRepository.GetAllEnabledCountries().Select(s => _journeyPlanModelMapper.ModelMapper(s, lang)).ToList();
          
        }
    }
}
