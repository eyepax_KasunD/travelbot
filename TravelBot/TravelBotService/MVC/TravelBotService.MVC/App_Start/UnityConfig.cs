using System;
using System.Collections.Generic;
using TravelBot.Common;
using TravelBot.Common.Domain;
using TravelBot.Common.Mappers.Contracts;
using TravelBot.Common.Models;
using TravelBot.Common.Models.Queue;
using TravelBot.Common.Repository;
using TravelBot.Common.Repository.Contracts;
using TravelbotService.BusinessServices;
using TravelbotService.BusinessServices.Contracts;
using TravelbotService.BusinessServices.Factories;
using TravelbotService.BusinessServices.Factories.Contracts;
using TravelbotService.BusinessServices.Mappers;
using TravelbotService.BusinessServices.PriceSources;
using TravelbotService.BusinessServices.PriceSources.Contracts;
using TravelbotService.BusinessServices.QueueProcessors;
using TravelBotService.Common;
using TravelBotService.Repository;
using TravelBotService.Repository.Contracts;
using Unity;
using Unity.Lifetime;

namespace TravelBotService.MVC
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => container.Value;
        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below.
            // Make sure to add a Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your type's mappings here.
            // container.RegisterType<IProductRepository, ProductRepository>();

            container.RegisterType<IAppSettings, AppSettings>();
            container.RegisterType<IHotelPriceApiRepository, HotelPriceApiRepository>();
            container.RegisterType<IHotelPriceRetrieveService, HotelPriceRetrieveService>(); 
            container.RegisterType<IHotelPricesFactory, HotelPricesFactory>(); 
            container.RegisterType<IHotelPriceSource, TravelBotScraper>("TravelBotScraper"); 
            container.RegisterType<IHotelPriceSource, AgodaApi>("Agoda");
            container.RegisterType<ICountryRepository, CountryRepository>(); 
            container.RegisterType<IMapper<Tuple<AgodaLongTailSearchRsp, HotelPriceSearchReq, SearchRspCriteriaCombination>, HotelPriceSearchRsp>, AgodaLongTailSearchRspToHotelSearchRsp>();
            container.RegisterType<IMapper<Tuple<HotelPriceSearchReq, SearchRspCriteriaCombination, SearchReqCriterion>, AgodaLongTailSearchReq>, HotelPriceSearchReqToAgodaLongTailSearchReq>();
            container.RegisterType<IHotelSearchRequestMapper, HotelPriceSearchReqToScraperHotelPriceSearchReqMapper>(); 
            container.RegisterType<IDataSourceRepository, DataSourceRepository>(); 
            container.RegisterType<IHotelPriceSearchQueueRepository, HotelPriceSearchQueueRepository>();
            container.RegisterType<IUnfilteredHotelPriceRepository, UnfilteredHotelPriceRepository>();            
            container.RegisterType<IHotelPriceRepository, HotelPriceRepository>();
            container.RegisterType<IHotelSourceMapRepository, HotelSourceMapRepository>();
            container.RegisterType<ISourceCityMapRepository, SourceCityMapRepository>();
            container.RegisterType<ISourceCountryMapRepository, SourceCountryMapRepository>();
            container.RegisterType<IHotelDetailQueueRepository, HotelDetailQueueRepository>();
            container.RegisterType<IUserClickRepository, UserClickRepository>();
            container.RegisterType<IHotelPriceProcessor, HotelPriceProcessor>();
            container.RegisterType<IHotelDetailRepository, HotelDetailRepository>();
            container.RegisterType<IHotelRepository, HotelRepository>();
            container.RegisterType<IHotelPriceResponseService, HotelPriceResponseService>();

            container.RegisterType<IQueueProcessor<HotelDetailRspQueueElement>, HotelDetailQueueProcessor>();
            container.RegisterType<IQueueProcessor<HotelPriceSearchRspQueueElement>, HotelPriceSearchQueueProcessor>();
            container.RegisterType<IQueueProcessor<HotelPriceSearchReq>, HotelPriceSearchReqQueueProcessor>();
            container.RegisterType<IQueueProcessor<UserPreferences>, UserPreferencesQueueProcessor>();

            container.RegisterType<IMapper<HotelPriceSearchRsp, SearchResponse>, HotelrPriceSearchRspToSearchResponse>();
            container.RegisterType<IMapper<SearchResponse, HotelPriceSearchRsp>, SearchResponseToHotelrPriceSearchRsp>();
            container.RegisterType<IMapper<ScraperHotelDetailRsp, TravelBot.Common.Domain.Hotel>, MapHotelResponseToHotelSQL>();
            container.RegisterType<IMapper<HotelPriceSearchReq, SearchResponse>, HotelPriceSearchReqToSearchResponseMapper>();

            container.RegisterType<IMapper<TravelBot.Common.Domain.Hotel, TravelBot.Common.Models.Hotel>, SearchRSPToHotelMapper>(); 
            container.RegisterType<ISearchResponseRepository, SearchResponseRepository>(); 
            container.RegisterType<ICurrencyRepository, CurrencyRepository>();
            container.RegisterType<ISearchStatusRepository, SearchStatusRepository>();            
            container.RegisterType<ITravelbotDbContext, TravelbotDbContext>();            
        }

        public static T Resolve<T>()
        {
            return container.Value.Resolve<T>();
        }

    }
}