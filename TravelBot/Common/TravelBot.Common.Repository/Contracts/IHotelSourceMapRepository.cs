﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;

namespace TravelBot.Common.Repository.Contracts
{
    public interface IHotelSourceMapRepository
    {
        long? GetHotelIds(string hotelIdFromSource, long? dataSourceId);
        HotelSourceMap Find(string id);
        bool IsExists(long dataSourceId, string hotelIdFromSource);
    }
}
