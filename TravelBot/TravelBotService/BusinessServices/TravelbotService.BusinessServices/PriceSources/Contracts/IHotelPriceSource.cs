﻿using TravelBot.Common.Models;
using TravelBotService.Common;
using static TravelBot.Common.Enums;

namespace TravelbotService.BusinessServices.PriceSources.Contracts
{
    public interface IHotelPriceSource
    {
        DataSourceCode GetDataSourceCode();
        HotelPriceSearchRsp GetHotelPriceResponse(HotelPriceSearchReqWrapper reqWrapper);
    }
}
