﻿using System.Web.Http;
using TravelBot.Common.Models;
using TravelbotService.BusinessServices.Contracts;
using TravelBotService.Common;

namespace TravelBotService.MVC.Controllers.API
{
    public class HotelPriceController : BaseApiController
    {
        private readonly IHotelPriceRetrieveService _hotelPriceService= UnityConfig.Resolve<IHotelPriceRetrieveService>();

        public HotelPriceController()
        {
           
        }
        [HttpPost]
        public ApiResponseModel<HotelPriceSearchRsp> GetHotelPrices(HotelPriceSearchReq request)
        {
            logger.Info("Get Hotel Prices From Request Id : " + request.RequestId);
            var result = _hotelPriceService.GetPrices(request);
            return GetApiResponseModel(result);
        }

    }
}
