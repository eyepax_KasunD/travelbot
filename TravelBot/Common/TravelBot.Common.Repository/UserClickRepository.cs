﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;
using TravelBot.Common.Repository.Contracts;

namespace TravelBot.Common.Repository
{
    public class UserClickRepository : Repository<UserClick>, IUserClickRepository
    {
        private readonly ITravelbotDbContext _context;
        public UserClickRepository(ITravelbotDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
