﻿using System.Collections.Generic;
using System.Linq;
using TravelBot.Common.Domain;
using TravelBot.Common.Helpers.Contracts;
using TravelBot.Common.Repository.Contracts;

namespace TravelBot.Common.Repository
{
    public class CachedCountryContentRepository : CountryContentRepository, ICountryContentRepository
    {
        private readonly ICaching _cache;
        const string prefix = "TravelBot.Common.Repository.CountryIdListForLanguage";
        const int ttl = 60 * 60 * 24;
        public CachedCountryContentRepository(ITravelbotDbContext context, ICaching cache) : base(context)
        {
            _cache = cache;
        }
        public List<long> GetId(string lang)
        {
            List<long> idList = _cache.Get<List<long>>(prefix);
            if (idList == null)
            {
                idList = base.GetId(lang);
                _cache.Set(prefix, idList, ttl);              
            }
            return idList;
        }
    }
}
