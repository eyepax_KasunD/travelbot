﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Repository;

namespace AgodaHotelImporter
{
    class Program
    {
        static void Main(string[] args)
        {
            var agodaHotelImporter = new AgodaHotelImporter();
            var mapper = new AgodaHotelToHotelMapper();
            var hotelList = agodaHotelImporter.Import(@"D:\Development\SL_EN.csv");
            var hotels = hotelList.Select(h => mapper.Map(h)).ToList();

            var hotelRepo = new HotelRepository(new TravelbotDbContext());
            hotelRepo.AddAll(hotels);           

        }
    }
}
