namespace TravelBot.Common.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sys_City",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        IsEnabled = c.Boolean(nullable: false),
                        ThumbnailId = c.Long(),
                        CreateDateTime = c.DateTime(nullable: false),
                        Country_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sys_Country", t => t.Country_Id)
                .ForeignKey("dbo.Sys_Image", t => t.ThumbnailId)
                .Index(t => t.ThumbnailId)
                .Index(t => t.Country_Id);
            
            CreateTable(
                "dbo.Sys_CityContent",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        CreateDateTime = c.DateTime(nullable: false),
                        City_Id = c.Long(),
                        Language_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sys_City", t => t.City_Id)
                .ForeignKey("dbo.Sys_Language", t => t.Language_Id)
                .Index(t => t.City_Id)
                .Index(t => t.Language_Id);
            
            CreateTable(
                "dbo.Sys_Language",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Tag = c.String(),
                        Varient = c.String(),
                        CreateDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Sys_CityImage",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CreateDateTime = c.DateTime(nullable: false),
                        City_Id = c.Long(),
                        Image_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sys_City", t => t.City_Id)
                .ForeignKey("dbo.Sys_Image", t => t.Image_Id)
                .Index(t => t.City_Id)
                .Index(t => t.Image_Id);
            
            CreateTable(
                "dbo.Sys_Image",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        URL = c.String(),
                        CreateDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Sys_ImageContent",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Description = c.String(),
                        CreateDateTime = c.DateTime(nullable: false),
                        Image_Id = c.Long(),
                        Lanuguage_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sys_Image", t => t.Image_Id)
                .ForeignKey("dbo.Sys_Language", t => t.Lanuguage_Id)
                .Index(t => t.Image_Id)
                .Index(t => t.Lanuguage_Id);
            
            CreateTable(
                "dbo.Sys_Country",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        SortOrder = c.Int(nullable: false),
                        IsEnabled = c.Boolean(nullable: false),
                        ThumbnailId = c.Long(),
                        CreateDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sys_Image", t => t.ThumbnailId)
                .Index(t => t.ThumbnailId);
            
            CreateTable(
                "dbo.Sys_CountryContent",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        CreateDateTime = c.DateTime(nullable: false),
                        Country_Id = c.Long(),
                        Language_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sys_Country", t => t.Country_Id)
                .ForeignKey("dbo.Sys_Language", t => t.Language_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.Language_Id);
            
            CreateTable(
                "dbo.Sys_CountryImage",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CreateDateTime = c.DateTime(nullable: false),
                        Country_Id = c.Long(),
                        Image_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sys_Country", t => t.Country_Id)
                .ForeignKey("dbo.Sys_Image", t => t.Image_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.Image_Id);
            
            CreateTable(
                "dbo.Set_SourceCountryMap",
                c => new
                    {
                        CountryId = c.Long(nullable: false),
                        DataSourceId = c.Long(nullable: false),
                        Value = c.String(),
                    })
                .PrimaryKey(t => new { t.CountryId, t.DataSourceId })
                .ForeignKey("dbo.Sys_Country", t => t.CountryId, cascadeDelete: true)
                .ForeignKey("dbo.Sys_DataSource", t => t.DataSourceId, cascadeDelete: true)
                .Index(t => t.CountryId)
                .Index(t => t.DataSourceId);
            
            CreateTable(
                "dbo.Sys_DataSource",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Code = c.String(),
                        Name = c.String(),
                        Icon = c.String(),
                        CreateDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Set_SourceCityMap",
                c => new
                    {
                        CityId = c.Long(nullable: false),
                        DataSourceId = c.Long(nullable: false),
                        Value = c.String(),
                    })
                .PrimaryKey(t => new { t.CityId, t.DataSourceId })
                .ForeignKey("dbo.Sys_City", t => t.CityId, cascadeDelete: true)
                .ForeignKey("dbo.Sys_DataSource", t => t.DataSourceId, cascadeDelete: true)
                .Index(t => t.CityId)
                .Index(t => t.DataSourceId);
            
            CreateTable(
                "dbo.Sys_Currency",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Code = c.String(),
                        Name = c.String(),
                        CreateDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Trn_GuestReview",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ReviewCount = c.Int(nullable: false),
                        ReviewScore = c.Double(nullable: false),
                        RoomCleanliness = c.Double(nullable: false),
                        ServiceStaff = c.Double(nullable: false),
                        RoomComfort = c.Double(nullable: false),
                        HotelCondition = c.Double(nullable: false),
                        DataSourceId = c.Long(),
                        LastModifiedDateTime = c.DateTime(nullable: false),
                        CreateDateTime = c.DateTime(nullable: false),
                        Hotel_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sys_DataSource", t => t.DataSourceId)
                .ForeignKey("dbo.Set_Hotel", t => t.Hotel_Id)
                .Index(t => t.DataSourceId)
                .Index(t => t.Hotel_Id);
            
            CreateTable(
                "dbo.Set_Hotel",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        HotelRating = c.Double(),
                        IsApproved = c.Boolean(nullable: false),
                        IsEnabled = c.Boolean(nullable: false),
                        Address = c.String(),
                        Latitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Longitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LastModifiedDateTime = c.DateTime(nullable: false),
                        CreateDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Set_HotelSourceMap",
                c => new
                    {
                        HotelId = c.Long(nullable: false),
                        DataSourceId = c.Long(nullable: false),
                        HotelIdFromSource = c.String(),
                    })
                .PrimaryKey(t => new { t.HotelId, t.DataSourceId })
                .ForeignKey("dbo.Sys_DataSource", t => t.DataSourceId, cascadeDelete: true)
                .ForeignKey("dbo.Set_Hotel", t => t.HotelId, cascadeDelete: true)
                .Index(t => t.HotelId)
                .Index(t => t.DataSourceId);
            
            CreateTable(
                "dbo.Set_HotelImage",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        URL = c.String(),
                        CreateDateTime = c.DateTime(nullable: false),
                        Hotel_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Set_Hotel", t => t.Hotel_Id)
                .Index(t => t.Hotel_Id);
            
            CreateTable(
                "dbo.Trn_HotelDetailQueue",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        SearchResponseId = c.Long(),
                        DataSourceId = c.Long(),
                        HotelIdFromSource = c.String(),
                        ResponseRecieved = c.Boolean(nullable: false),
                        ErrorOccurred = c.Boolean(nullable: false),
                        LastModifiedDateTime = c.DateTime(nullable: false),
                        CreateDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sys_DataSource", t => t.DataSourceId)
                .ForeignKey("dbo.Trn_SearchResponse", t => t.SearchResponseId)
                .Index(t => t.SearchResponseId)
                .Index(t => t.DataSourceId);
            
            CreateTable(
                "dbo.Trn_SearchResponse",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        RequestId = c.String(),
                        ResponseStatus = c.String(),
                        UserSessionId = c.Long(),
                        CreateDateTime = c.DateTime(nullable: false),
                        UserSession_Id = c.String(maxLength: 40),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Trn_UserSession", t => t.UserSession_Id)
                .Index(t => t.UserSession_Id);
            
            CreateTable(
                "dbo.Trn_SearchCriteria",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CheckIn = c.DateTime(nullable: false),
                        CheckOut = c.DateTime(nullable: false),
                        CityId = c.Long(),
                        PriceFrom = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PriceTo = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SearchResponseId = c.Long(),
                        CreateDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sys_City", t => t.CityId)
                .ForeignKey("dbo.Trn_SearchResponse", t => t.SearchResponseId)
                .Index(t => t.CityId)
                .Index(t => t.SearchResponseId);
            
            CreateTable(
                "dbo.Trn_SearchCriteriaCombination",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CheckIn = c.DateTime(nullable: false),
                        CheckOut = c.DateTime(nullable: false),
                        CreateDateTime = c.DateTime(nullable: false),
                        SearchCriteria_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Trn_SearchCriteria", t => t.SearchCriteria_Id)
                .Index(t => t.SearchCriteria_Id);
            
            CreateTable(
                "dbo.Trn_HotelPrice",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        SortOrder = c.Int(nullable: false),
                        Url = c.String(),
                        DataSourceId = c.Long(),
                        HotelIdFromSource = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SearchCriteriaCombinationId = c.Long(),
                        CreateDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sys_DataSource", t => t.DataSourceId)
                .ForeignKey("dbo.Trn_SearchCriteriaCombination", t => t.SearchCriteriaCombinationId)
                .Index(t => t.DataSourceId)
                .Index(t => t.SearchCriteriaCombinationId);
            
            CreateTable(
                "dbo.Trn_UnfilteredHotelPrice",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Url = c.String(),
                        DataSourceId = c.Long(),
                        HotelIdFromSource = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SearchCriteriaCombinationId = c.Long(),
                        CreateDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sys_DataSource", t => t.DataSourceId)
                .ForeignKey("dbo.Trn_SearchCriteriaCombination", t => t.SearchCriteriaCombinationId)
                .Index(t => t.DataSourceId)
                .Index(t => t.SearchCriteriaCombinationId);
            
            CreateTable(
                "dbo.Trn_UserSession",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 40),
                        CreateDateTime = c.DateTime(nullable: false),
                        CheckIn = c.DateTime(nullable: false),
                        CheckOut = c.DateTime(nullable: false),
                        Adults = c.Int(nullable: false),
                        Children = c.Int(nullable: false),
                        FlexiDateCount = c.Int(nullable: false),
                        PriceFrom = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PriceTo = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Email = c.String(),
                        DeviceId = c.String(),
                        StopSearch = c.String(),
                        CurrencyId = c.Long(),
                        CountryId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sys_Country", t => t.CountryId)
                .ForeignKey("dbo.Sys_Currency", t => t.CurrencyId)
                .Index(t => t.CurrencyId)
                .Index(t => t.CountryId);
            
            CreateTable(
                "dbo.Trn_HotelPriceSearchQueue",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        SearchResponseId = c.Long(),
                        DataSourceId = c.Long(),
                        ResponseRecieved = c.Boolean(nullable: false),
                        ErrorOccurred = c.Boolean(nullable: false),
                        LastModifiedDateTime = c.DateTime(nullable: false),
                        CreateDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sys_DataSource", t => t.DataSourceId)
                .ForeignKey("dbo.Trn_SearchResponse", t => t.SearchResponseId)
                .Index(t => t.SearchResponseId)
                .Index(t => t.DataSourceId);
            
            CreateTable(
                "dbo.Trn_UserClick",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserSessionId = c.Long(),
                        HotelPriceId = c.Long(),
                        CreateDateTime = c.DateTime(nullable: false),
                        UserSession_Id = c.String(maxLength: 40),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Trn_HotelPrice", t => t.HotelPriceId)
                .ForeignKey("dbo.Trn_UserSession", t => t.UserSession_Id)
                .Index(t => t.HotelPriceId)
                .Index(t => t.UserSession_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Trn_UserClick", "UserSession_Id", "dbo.Trn_UserSession");
            DropForeignKey("dbo.Trn_UserClick", "HotelPriceId", "dbo.Trn_HotelPrice");
            DropForeignKey("dbo.Trn_HotelPriceSearchQueue", "SearchResponseId", "dbo.Trn_SearchResponse");
            DropForeignKey("dbo.Trn_HotelPriceSearchQueue", "DataSourceId", "dbo.Sys_DataSource");
            DropForeignKey("dbo.Trn_HotelDetailQueue", "SearchResponseId", "dbo.Trn_SearchResponse");
            DropForeignKey("dbo.Trn_SearchResponse", "UserSession_Id", "dbo.Trn_UserSession");
            DropForeignKey("dbo.Trn_UserSession", "CurrencyId", "dbo.Sys_Currency");
            DropForeignKey("dbo.Trn_UserSession", "CountryId", "dbo.Sys_Country");
            DropForeignKey("dbo.Trn_SearchCriteria", "SearchResponseId", "dbo.Trn_SearchResponse");
            DropForeignKey("dbo.Trn_UnfilteredHotelPrice", "SearchCriteriaCombinationId", "dbo.Trn_SearchCriteriaCombination");
            DropForeignKey("dbo.Trn_UnfilteredHotelPrice", "DataSourceId", "dbo.Sys_DataSource");
            DropForeignKey("dbo.Trn_SearchCriteriaCombination", "SearchCriteria_Id", "dbo.Trn_SearchCriteria");
            DropForeignKey("dbo.Trn_HotelPrice", "SearchCriteriaCombinationId", "dbo.Trn_SearchCriteriaCombination");
            DropForeignKey("dbo.Trn_HotelPrice", "DataSourceId", "dbo.Sys_DataSource");
            DropForeignKey("dbo.Trn_SearchCriteria", "CityId", "dbo.Sys_City");
            DropForeignKey("dbo.Trn_HotelDetailQueue", "DataSourceId", "dbo.Sys_DataSource");
            DropForeignKey("dbo.Set_HotelImage", "Hotel_Id", "dbo.Set_Hotel");
            DropForeignKey("dbo.Set_HotelSourceMap", "HotelId", "dbo.Set_Hotel");
            DropForeignKey("dbo.Set_HotelSourceMap", "DataSourceId", "dbo.Sys_DataSource");
            DropForeignKey("dbo.Trn_GuestReview", "Hotel_Id", "dbo.Set_Hotel");
            DropForeignKey("dbo.Trn_GuestReview", "DataSourceId", "dbo.Sys_DataSource");
            DropForeignKey("dbo.Sys_City", "ThumbnailId", "dbo.Sys_Image");
            DropForeignKey("dbo.Set_SourceCityMap", "DataSourceId", "dbo.Sys_DataSource");
            DropForeignKey("dbo.Set_SourceCityMap", "CityId", "dbo.Sys_City");
            DropForeignKey("dbo.Sys_Country", "ThumbnailId", "dbo.Sys_Image");
            DropForeignKey("dbo.Set_SourceCountryMap", "DataSourceId", "dbo.Sys_DataSource");
            DropForeignKey("dbo.Set_SourceCountryMap", "CountryId", "dbo.Sys_Country");
            DropForeignKey("dbo.Sys_CountryImage", "Image_Id", "dbo.Sys_Image");
            DropForeignKey("dbo.Sys_CountryImage", "Country_Id", "dbo.Sys_Country");
            DropForeignKey("dbo.Sys_CountryContent", "Language_Id", "dbo.Sys_Language");
            DropForeignKey("dbo.Sys_CountryContent", "Country_Id", "dbo.Sys_Country");
            DropForeignKey("dbo.Sys_City", "Country_Id", "dbo.Sys_Country");
            DropForeignKey("dbo.Sys_CityImage", "Image_Id", "dbo.Sys_Image");
            DropForeignKey("dbo.Sys_ImageContent", "Lanuguage_Id", "dbo.Sys_Language");
            DropForeignKey("dbo.Sys_ImageContent", "Image_Id", "dbo.Sys_Image");
            DropForeignKey("dbo.Sys_CityImage", "City_Id", "dbo.Sys_City");
            DropForeignKey("dbo.Sys_CityContent", "Language_Id", "dbo.Sys_Language");
            DropForeignKey("dbo.Sys_CityContent", "City_Id", "dbo.Sys_City");
            DropIndex("dbo.Trn_UserClick", new[] { "UserSession_Id" });
            DropIndex("dbo.Trn_UserClick", new[] { "HotelPriceId" });
            DropIndex("dbo.Trn_HotelPriceSearchQueue", new[] { "DataSourceId" });
            DropIndex("dbo.Trn_HotelPriceSearchQueue", new[] { "SearchResponseId" });
            DropIndex("dbo.Trn_UserSession", new[] { "CountryId" });
            DropIndex("dbo.Trn_UserSession", new[] { "CurrencyId" });
            DropIndex("dbo.Trn_UnfilteredHotelPrice", new[] { "SearchCriteriaCombinationId" });
            DropIndex("dbo.Trn_UnfilteredHotelPrice", new[] { "DataSourceId" });
            DropIndex("dbo.Trn_HotelPrice", new[] { "SearchCriteriaCombinationId" });
            DropIndex("dbo.Trn_HotelPrice", new[] { "DataSourceId" });
            DropIndex("dbo.Trn_SearchCriteriaCombination", new[] { "SearchCriteria_Id" });
            DropIndex("dbo.Trn_SearchCriteria", new[] { "SearchResponseId" });
            DropIndex("dbo.Trn_SearchCriteria", new[] { "CityId" });
            DropIndex("dbo.Trn_SearchResponse", new[] { "UserSession_Id" });
            DropIndex("dbo.Trn_HotelDetailQueue", new[] { "DataSourceId" });
            DropIndex("dbo.Trn_HotelDetailQueue", new[] { "SearchResponseId" });
            DropIndex("dbo.Set_HotelImage", new[] { "Hotel_Id" });
            DropIndex("dbo.Set_HotelSourceMap", new[] { "DataSourceId" });
            DropIndex("dbo.Set_HotelSourceMap", new[] { "HotelId" });
            DropIndex("dbo.Trn_GuestReview", new[] { "Hotel_Id" });
            DropIndex("dbo.Trn_GuestReview", new[] { "DataSourceId" });
            DropIndex("dbo.Set_SourceCityMap", new[] { "DataSourceId" });
            DropIndex("dbo.Set_SourceCityMap", new[] { "CityId" });
            DropIndex("dbo.Set_SourceCountryMap", new[] { "DataSourceId" });
            DropIndex("dbo.Set_SourceCountryMap", new[] { "CountryId" });
            DropIndex("dbo.Sys_CountryImage", new[] { "Image_Id" });
            DropIndex("dbo.Sys_CountryImage", new[] { "Country_Id" });
            DropIndex("dbo.Sys_CountryContent", new[] { "Language_Id" });
            DropIndex("dbo.Sys_CountryContent", new[] { "Country_Id" });
            DropIndex("dbo.Sys_Country", new[] { "ThumbnailId" });
            DropIndex("dbo.Sys_ImageContent", new[] { "Lanuguage_Id" });
            DropIndex("dbo.Sys_ImageContent", new[] { "Image_Id" });
            DropIndex("dbo.Sys_CityImage", new[] { "Image_Id" });
            DropIndex("dbo.Sys_CityImage", new[] { "City_Id" });
            DropIndex("dbo.Sys_CityContent", new[] { "Language_Id" });
            DropIndex("dbo.Sys_CityContent", new[] { "City_Id" });
            DropIndex("dbo.Sys_City", new[] { "Country_Id" });
            DropIndex("dbo.Sys_City", new[] { "ThumbnailId" });
            DropTable("dbo.Trn_UserClick");
            DropTable("dbo.Trn_HotelPriceSearchQueue");
            DropTable("dbo.Trn_UserSession");
            DropTable("dbo.Trn_UnfilteredHotelPrice");
            DropTable("dbo.Trn_HotelPrice");
            DropTable("dbo.Trn_SearchCriteriaCombination");
            DropTable("dbo.Trn_SearchCriteria");
            DropTable("dbo.Trn_SearchResponse");
            DropTable("dbo.Trn_HotelDetailQueue");
            DropTable("dbo.Set_HotelImage");
            DropTable("dbo.Set_HotelSourceMap");
            DropTable("dbo.Set_Hotel");
            DropTable("dbo.Trn_GuestReview");
            DropTable("dbo.Sys_Currency");
            DropTable("dbo.Set_SourceCityMap");
            DropTable("dbo.Sys_DataSource");
            DropTable("dbo.Set_SourceCountryMap");
            DropTable("dbo.Sys_CountryImage");
            DropTable("dbo.Sys_CountryContent");
            DropTable("dbo.Sys_Country");
            DropTable("dbo.Sys_ImageContent");
            DropTable("dbo.Sys_Image");
            DropTable("dbo.Sys_CityImage");
            DropTable("dbo.Sys_Language");
            DropTable("dbo.Sys_CityContent");
            DropTable("dbo.Sys_City");
        }
    }
}
