namespace TravelBot.Common.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSearchStatus : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Trn_SearchStatus",
                c => new
                    {
                        SearchResponseId = c.Long(nullable: false),
                        DataSourceId = c.Long(nullable: false),
                        Status = c.Int(nullable: false),
                        CreateDateTime = c.DateTime(nullable: false),
                        LastModifiedDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.SearchResponseId, t.DataSourceId })
                .ForeignKey("dbo.Sys_DataSource", t => t.DataSourceId, cascadeDelete: true)
                .ForeignKey("dbo.Trn_SearchResponse", t => t.SearchResponseId, cascadeDelete: true)
                .Index(t => t.SearchResponseId)
                .Index(t => t.DataSourceId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Trn_SearchStatus", "SearchResponseId", "dbo.Trn_SearchResponse");
            DropForeignKey("dbo.Trn_SearchStatus", "DataSourceId", "dbo.Sys_DataSource");
            DropIndex("dbo.Trn_SearchStatus", new[] { "DataSourceId" });
            DropIndex("dbo.Trn_SearchStatus", new[] { "SearchResponseId" });
            DropTable("dbo.Trn_SearchStatus");
        }
    }
}
