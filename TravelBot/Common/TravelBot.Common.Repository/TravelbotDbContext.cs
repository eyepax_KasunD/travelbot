﻿using System.Data.Entity;
using TravelBot.Common.Domain;
using TravelBot.Common.Repository.Contracts;

namespace TravelBot.Common.Repository
{
    public class TravelbotDbContext : DbContext, ITravelbotDbContext
    {
        public TravelbotDbContext(): base("name=usersqldb")
        {
            //this.Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<UserSession> UserSessions { get; set; }
        public DbSet<UnfilteredHotelPrice> HotelPrices { get; set; }
        public DbSet<Hotel> Hotels { get; set; }
        public DbSet<GuestReview> GuestReviews { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<CityContent> CityNames { get; set; }
        public DbSet<CountryContent> CountryNames { get; set; }
        public DbSet<UserClick> UserClick { get; set; }
        public DbSet<Currency> CurrencyTypes { get; set; }
        public DbSet<HotelPriceSearchQueue> HotelPriceSearchQueue { get; set; }
        public DbSet<HotelDetailQueue> HotelDetailQueue { get; set; }

        public DbSet<SourceCityMap> SourceCityMap { get; set; }

        public DbSet<SearchStatus> SeachStatus { get; set; }
        public DbContext GetDatabaseContext
        {
            get { return this; }
        }
    }
}
