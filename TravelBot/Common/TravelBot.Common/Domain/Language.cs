﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TravelBot.Common.Domain
{

    [Table("Sys_Language")]
    public class Language: BaseEntity
    {
        public string Tag { get; set; }
        public string Varient { get; set; }
    }
}
