﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelBotWeb.Models
{
    public class UserReturnedValuesModel
    {
        public string Link { get; set; }
        public string DeviceId { get; set; }
    }
}