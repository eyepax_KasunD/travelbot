﻿using NLog;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TravelBot.Common.Domain;
using TravelBot.Common.Helpers;
using TravelBot.Common.Models;
using TravelBot.Common.Repository.Contracts;
using TravelbotService.BusinessServices.Factories.Contracts;
using TravelbotService.BusinessServices.PriceSources.Contracts;
using TravelBotService.Common;
using Unity;
using static TravelBot.Common.Enums;

namespace TravelbotService.BusinessServices.Factories
{
    public class HotelPricesFactory: IHotelPricesFactory
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private readonly IUnityContainer _container ;
        private readonly ISearchStatusRepository _searchStatusRepository;
        private readonly IDataSourceRepository _dataSourceRepository;
        public HotelPricesFactory(IUnityContainer container,
            IDataSourceRepository dataSourceRepository,
            ISearchStatusRepository searchStatusRepository)
        {
            _container = container;
            _dataSourceRepository = dataSourceRepository;
            _searchStatusRepository = searchStatusRepository;
        }

        public  HotelPriceSearchRsp GetHotelPrices(HotelPriceSearchReqWrapper reqWrapper) 
        {
            var request = reqWrapper.HotelPriceSearchReq;
            List<HotelPriceSearchRsp> ResponseList = new List<HotelPriceSearchRsp>(); //HotelSearch Responses From Agoda, Scraper, Expedia etc.
            List<Task> tasks = new List<Task>();
            var SourceList = new List<IHotelPriceSource>() {
                _container.Resolve<IHotelPriceSource>("TravelBotScraper")
                ,
                _container.Resolve<IHotelPriceSource>("Agoda")
            };

            var searchStatusList = SourceList.Select(s =>
            {
                var sourceCode = _dataSourceRepository.FindFromCode(s.GetDataSourceCode().ToString()).Id;
                return new SearchStatus { SearchResponseId = reqWrapper.SearchResponse.Id, DataSourceId = sourceCode, Status = (int)ResponseStatus.Pending };
            }).ToList();

            _searchStatusRepository.AddAll(searchStatusList);

            foreach (var source in SourceList) // _container.ResolveAll<IHotelPriceSource>()
            {
                tasks.Add(Task.Run(() => { ResponseList.Add(source.GetHotelPriceResponse(reqWrapper)); })); 
            }
            logger.Info("Waiting For tasks get completed");
            Task.WaitAll(tasks.ToArray()) ;
            logger.Info("Tasks completed");
            return Helper.MergeHotelPriceSearchRspList(ResponseList); //Merge Hotel Search Responses
        }
    }
}
