﻿using TravelBot.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using static TravelBot.Common.Enums;
using TravelBotWeb.Common;
using TravelBotWeb.Services.Contracts;
using TravelBot.Common;
using TravelBotWeb.Repository.Contracts;
using TravelBotWeb.Services.Models;
using RestSharp;
using System.Web.Script.Serialization;
using TravelBotWeb.Services.Helpers;
using Newtonsoft.Json;
using TravelBot.Common.Repository.Contracts;
using TravelBot.Common.Mappers.Contracts;
using System.Dynamic;
using System.Threading.Tasks;

namespace TravelBotWeb.Services
{
    public class UserService : IUserService
    {
        private readonly Repository.Contracts.IUserRepository _userRepository;
        private readonly TravelBotWeb.Services.Contracts.IUserServiceSQL _userService ;
        private readonly ICountryRepository _countryRepository;
        private readonly IAppSettings _appsettings;
        private readonly IMapper<Tuple<HotelPriceSearchRsp, int, string, string>, List<CityHotelData>> _cityHotelDataMapper;
        private readonly QueueProvider _queue;
        private readonly IMapper<UserRequestModel, UserSessionModel> _userRequestModelToUserSessionModelMapper;

        public UserService(Repository.Contracts.IUserRepository userRepository,
            TravelBotWeb.Services.Contracts.IUserServiceSQL userService,
            ICountryRepository countryRepository,
            IAppSettings appsettings,
            IMapper<Tuple<HotelPriceSearchRsp, int, string, string>, List<CityHotelData>> cityHotelDataMapper,
            IMapper<UserRequestModel, UserSessionModel> userRequestModelToUserSessionModelMapper)
        {
            _userRepository = userRepository;
            _userService = userService;
            _countryRepository = countryRepository;
            _appsettings = appsettings;
            _cityHotelDataMapper = cityHotelDataMapper;
            _queue = new QueueProvider();
            _userRequestModelToUserSessionModelMapper = userRequestModelToUserSessionModelMapper;

        }

        public UserSessionModel StoreUserSession(UserRequestModel UserRequest)
        {
            UserSessionModel userSessionModel = _userRequestModelToUserSessionModelMapper.Map(UserRequest);
            _userRepository.AddUserRequest(userSessionModel);
            return userSessionModel;
        }


        public ApiResponseModel<HotelPriceSearchRsp> RetrieveHotelPriceSearchRsp(UserSessionModel sessionModel, string requestId)
            {
            
            ApiResponseModel<HotelPriceSearchRsp> responseContent = null;
            var client = new RestClient(_appsettings.Service_Domain);
            var request = new RestRequest("api/hotelPrice/GetHotelPrices", Method.POST);
            HotelPriceSearchReq scraperRequest = new HotelPriceSearchReq
            {
                RequestId = requestId,
                UserSessionId = sessionModel.UserId,
                CheckInDate = sessionModel.CheckInDate,
                SearchCriteria = sessionModel.SearchSegmentList,
                AdultCount = sessionModel.AdultCount,
                ChildCount = sessionModel.ChildCount,
                PriceFrom = sessionModel.PriceFrom,
                PriceTo = sessionModel.PriceTo,
                FlexDatesPlus = sessionModel.FlexDatesPlus,
                FlexDatesMinus = sessionModel.FlexDatesMinus,
                Currency = CurrencyCode.USD

            };
            var json = new JavaScriptSerializer().Serialize(scraperRequest);

            request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
            // execute the request
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string   
            responseContent = JsonConvert.DeserializeObject<ApiResponseModel<HotelPriceSearchRsp>>(content);
            if (responseContent.Data != null)
            {
                if (responseContent.Data.Status == Enums.ResponseStatus.Completed)
                {
                    var travelbotIndex = 0;
                    responseContent.Data.SearchCriteria.First().SearchRspCriteriaCombinations.ForEach(p => p.HotelPrices.ForEach(d => d.SortOrder = travelbotIndex++));
                    var searchObj = sessionModel.SearchObjects.Find(s => s.RequestId.Equals(requestId));
                    searchObj.HotelSearchResponse = responseContent.Data;
                    //searchObj.HotelSearchResponse.SearchCriteria.First().SearchRspCriteriaCombinations = responseContent.Data.SearchCriteria.First().SearchRspCriteriaCombinations.Where(x => x.Hotel != null).ToList();
                    _userRepository.Update(sessionModel);
                }
            }

            return responseContent;
        }

        public HotelPriceSearchRsp ScrappingCallerForSearchingObjects(UserSessionModel sessionModel, string requestId)
        {
            HotelPriceSearchRsp rsp = null;
            var responseContent = RetrieveHotelPriceSearchRsp(sessionModel, requestId);
            try
            {
                if (responseContent.Errors == null)
                {
                    rsp = responseContent.Data;
                    sessionModel.SearchObjects.Find(s => s.RequestId.Equals(requestId)).RequestStatus = TravelBot.Common.Enums.RequestStatus.Running;
                    //    UserSession.SearchObjects.First().RequestStatus = TravelBot.Common.Enums.RequestStatus.Running;
                    _userRepository.Update(sessionModel);
                }
            }
            catch (Exception)
            {
            }
            return rsp;
        }
        public ApiResponseModel<HotelPriceSearchRsp> ScrappingCallerFromId(string UserId)
        {
            UserSessionModel UserSession = new UserSessionModel();
            UserSession = _userRepository.FindRequestFromId(UserId);
            ApiResponseModel<HotelPriceSearchRsp> response = null;
            if (UserSession.SearchObjects.First().RequestStatus.Equals(TravelBot.Common.Enums.RequestStatus.Running))
            {
                var responseContent = RetrieveHotelPriceSearchRsp(UserSession, UserSession.SearchObjects.First().RequestId);
                return responseContent;
            }
            return response;
        }

        public List<CityHotelData> GetCityHotelData(UserSessionModel userSession, ContinuousSearch searchObj, int flexDates)
        {
            return _cityHotelDataMapper.Map(new Tuple<HotelPriceSearchRsp, int, string, string>(searchObj.HotelSearchResponse, flexDates, userSession.UserId, searchObj.RequestId));
        }

        public List<FlextDateViewModel> GenerateFlexDateOptions(HotelPriceSearchRsp rsp)
        {
            var searchCriterion = rsp.SearchCriteria.First();
            var noOfStays = rsp.SearchCriteria.Sum(s => (s.CheckoutDate - s.CheckinDate).TotalDays);
            return searchCriterion.SearchRspCriteriaCombinations.Select(cmb =>
            {
                var checking = cmb.CheckIn.ToLocalTime();
                var ret = new FlextDateViewModel
                {
                    DateText = checking.ToString("MMM dd") + " - " + checking.AddDays(noOfStays).ToString("MMM dd"),
                    Flex = (int)(cmb.CheckIn - searchCriterion.CheckinDate).TotalDays
                };
                return ret;
            }).ToList();
        }
        public bool  StopSearch(string reason, string id)
        {
            UserSessionModel model = _userRepository.FindRequestFromId(id);
            model.StopEmails = true;
            model.ReasonforStoppingEmails = reason;

            try
            {
                _userRepository.Update(model);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public Tuple<string, string> GetHotelUrlAndSource(string userSessionId, int hotelPriceId, string requestId)
        {
            var userSession = _userRepository.FindRequestFromId(userSessionId);
            var hotelPrice = userSession.SearchObjects.Find(s => s.RequestId.Equals(requestId)).HotelSearchResponse
                .SearchCriteria.SelectMany(s => s.SearchRspCriteriaCombinations.SelectMany(c => c.HotelPrices)).FirstOrDefault(h => h.HotelPriceId == hotelPriceId);
            return new Tuple<string, string>(Helper.GetUserFreindlySourceName(hotelPrice.Source), hotelPrice.URL);
        }
        public void SaveUserClickedHotelIndex(string userSessionId, int hotelPriceId, string requestId)
        {
           _queue.PublishInQueue(new UserPreferences() {UserSessionId=userSessionId, HotelPriceId=hotelPriceId, RequestId=requestId }, Constants.Queues.UserPreferencesQueue); //publish user preferences into queue
            _userService.UpdateUser(userSessionId, requestId, hotelPriceId);
        }
        public UserSessionModel GetUserSession(string userSessionId)
        {
            return _userRepository.FindRequestFromId(userSessionId);
        }

        public ContinuousSearch GetSearchObjectAndRequestPriceIfNeeded(UserSessionModel userSession, bool requestPricesIfNeeded = false)
        {
            var searchObjects = userSession.SearchObjects.Where(s => s.RequestDate < DateTime.Now).OrderByDescending(s => s.RequestDate);
            var returnObj = searchObjects.FirstOrDefault();
            if (returnObj.HotelSearchResponse == null)
            {
                if (requestPricesIfNeeded)
                {
                    Task.Run(() => RetrieveHotelPriceSearchRsp(userSession, returnObj.RequestId));
                }
                returnObj = searchObjects.FirstOrDefault(s => s.HotelSearchResponse != null) ?? searchObjects.FirstOrDefault();
            }
            return returnObj;
        }
        public UserSessionModel FindAndUpdateUserSession(HotelPriceSearchRsp response) 
        {
            UserSessionModel userSession = null;
            if (!string.IsNullOrEmpty(response.RequestId))
            {
                try
                {
                    userSession = _userRepository.FindRequestFromSearchingObjectId(response.RequestId);
                    var searchingObj = userSession.SearchObjects.Find(x => x.RequestId.Equals(response.RequestId));
                    searchingObj.HotelSearchResponse = response;
                    _userRepository.Update(userSession);
                }
                catch(Exception e)
                {
                    //log the error
                }
            }
            return userSession;
        }
    }
}
