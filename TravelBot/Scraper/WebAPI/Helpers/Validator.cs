using Scraper.Common.Models.Requests;
using System;
using System.Collections.Generic;
using TravelBot.Common;
using TravelBot.Common.Models;

namespace Scraper.WebAPI.Helpers
{

    public class Validator
    {
        public static List<string> ValidateRequest(HotelPriceRequest requestModel)
        {
            List<string> str = new List<string>();
            string ValidateDays = ValidateHowmanyDays(requestModel.HowManyDays);
            string ValidateFlex = Validateflexibility(requestModel.FlexibilityPlus, requestModel.FlexibilityMinus);
            string ValidateHref = ValidateUrl(requestModel.Href);

            if (!ValidateDays.Equals(string.Empty)) {
                str.Add(ValidateDays);
            }
            if (!ValidateFlex.Equals(string.Empty))
            {
                str.Add(ValidateFlex);
            }
            if (!ValidateHref.Equals(string.Empty))
            {
                str.Add(ValidateHref);
            }

            return str;

        }

        public static string ValidateHowmanyDays(int days)
        {
            if (days <= 0)
            {
                return Constants.ValidationStatements.DaysNotOk;
            }
            else
            {
                return string.Empty;
            }
        }

        public static string Validateflexibility(int flexplus, int flexminus)
        {
            if (flexminus < 0 || flexplus < 0)
            {
                return Constants.ValidationStatements.flexNotok ;
            }
            else
            {
                return string.Empty;
            }
        }

        public static string ValidateUrl(string url)
        {
            Uri uriResult;
            bool result = Uri.TryCreate(url, UriKind.Absolute, out uriResult)
                && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
            if (result)
            {
                return string.Empty ;
            }
            else
            {
                return Constants.ValidationStatements.urlNotok ;
            }

        }

        public static List<string> ValidateFeatureRequest(ScraperHotelDetailReq requestModel)
        {
            List<string> str = new List<string>();
            string ValidateHref = ValidateUrl(requestModel.Hotelurl);

            if (!ValidateHref.Equals(string.Empty))
            {
                str.Add(ValidateHref);
            }

            return str;

        }
    }
}