﻿using System;
using System.Linq;
using Quartz;
using System.Threading.Tasks;
using NLog;
using enums = TravelBot.Common.Enums;
using TravelBot.Common.Helpers;
using Scraper.Repository.Contracts;
using Scraper.Services.Contracts;

namespace Scraper.WebAPI.Scheduling
{
    public class ProcessSearchRequests : IJob
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private readonly IQueueRepository _queueRepository;
        private readonly IScraperService _scraperService;

        public ProcessSearchRequests(IQueueRepository queueRepository, IScraperService scraperService)
        {
            _queueRepository = queueRepository;
            _scraperService = scraperService;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            //logger.Info("start processing search requests");
            //var element = _queueRepository.GetSearchElementFromQueue(e => e.SearchResponse.Status == enums.ResponseStatus.Pending);
            //if(element != null)
            //{
            //    element.SearchResponse.Status = enums.ResponseStatus.Partial;
            //    _queueRepository.Update(element);
            //    try
            //    {
            //        element.SearchResponse = _scraperService.SearchHotelPrices(element.SearchRequest);

            //        if(element.SearchResponse.Errors.Count == 0)
            //        {
            //            element.LastUpdatedDateTime = DateTime.Now;
            //            _queueRepository.Update(element);

            //            _queueRepository.QueueHotelRequest(element.SearchResponse.SearchCriteria.First().SearchRspCriteriaCombinations.Select(p =>
            //            new FeatureRequestQueue()
            //            {
            //                Request = new HotelInfoRequest()
            //                {
            //                    Hotelurl = p.HotelPrices.First().URL,
            //                    Website = p.HotelPrices.First().Source
            //                },
            //                HotelId = HotelIdentifier.GetIdentifierFromUrl(p.HotelPrices.First().URL),
            //                RequestId = element.SearchRequest.RequestId,
            //                Status = enums.RequestStatus.Pending
            //            }).ToList());
            //        }
            //        else
            //        {
            //            throw new Exception(string.Join(Environment.NewLine, element.SearchResponse.Errors));
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        logger.Error("Ex : " + ex);
            //        element.SearchResponse.Status = enums.ResponseStatus.Error;
            //        element.LastUpdatedDateTime = DateTime.Now;
            //        _queueRepository.Update(element);
            //    }

            //}

            //logger.Info("end processing search requests");
        }

    }
}