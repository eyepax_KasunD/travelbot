﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TravelBot.Common.Domain
{
    public abstract class BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required, DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreateDateTime { get; set; }
    }

    public abstract class ModifiableBaseEntity : BaseEntity
    {
        [Required, DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime LastModifiedDateTime { get; set; }
    }
}
