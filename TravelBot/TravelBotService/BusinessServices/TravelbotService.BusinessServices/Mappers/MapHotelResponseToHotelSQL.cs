﻿using System.Collections.Generic;
using TravelBot.Common.Domain;
using TravelBot.Common.Mappers.Contracts;
using TravelBot.Common.Models;
using TravelBot.Common.Repository.Contracts;
using TravelBotService.Common;

namespace TravelbotService.BusinessServices.Mappers
{
    public class MapHotelResponseToHotelSQL:IMapper<ScraperHotelDetailRsp, TravelBot.Common.Domain.Hotel>
    {
        private readonly IDataSourceRepository _dataSourceInfoRepository;
        private readonly IHotelSourceMapRepository _hotelSourceMapRepository;
        public MapHotelResponseToHotelSQL(IDataSourceRepository dataSourceInfoRepository, IHotelSourceMapRepository hotelSourceMapRepository)
        {
            _dataSourceInfoRepository = dataSourceInfoRepository;
            _hotelSourceMapRepository = hotelSourceMapRepository;

        }

        public TravelBot.Common.Domain.Hotel Map(ScraperHotelDetailRsp source_object)
        {
            List<TravelBot.Common.Domain.GuestReview> list = new List<TravelBot.Common.Domain.GuestReview>();
            list.Add(MapGuestReviews(source_object.Hotel.GuestReview));

            return new TravelBot.Common.Domain.Hotel()
            {
                Name = source_object.Hotel.Name,
                HotelIds = HotelIdMapper(source_object.Hotel.HotelIds),
                HotelRating = source_object.Hotel.HotelRating,
                GuestReviews = list,
                Images = MapImageUrls(source_object.Hotel.ImgUrl),
                //IsApproved =,
                //IsEnabled =,
                Address = source_object.Hotel.Location.LocationString,
                Latitude = source_object.Hotel.Location.Latitude,
                Longitude = source_object.Hotel.Location.Longitude
            };
        }

        private List<HotelSourceMap> HotelIdMapper(List<DataSourceInfo> hotelIds)
        {
            List<HotelSourceMap> list = new List<HotelSourceMap>();
            hotelIds.ForEach(s => list.Add(new HotelSourceMap()
            {
                HotelIdFromSource = s.value,
                DataSourceId = _dataSourceInfoRepository.FindFromCode(s.pricingSource.ToString()).Id
            }));
            return list;
        }

        private List<HotelImage> MapImageUrls(List<string> imgUrl)
        {
            List<HotelImage> hotelImages = new List<HotelImage>();
            imgUrl.ForEach(s => hotelImages.Add(new HotelImage() { URL = s }));
            return hotelImages;
        }
        private TravelBot.Common.Domain.GuestReview MapGuestReviews(TravelBot.Common.Models.GuestReview guestReview)
        {
            return new TravelBot.Common.Domain.GuestReview()
            {
                ReviewCount = guestReview.ReviewCount,
                ReviewScore = guestReview.ReviewScore,
                RoomCleanliness = guestReview.RoomCleanliness,
                ServiceStaff = guestReview.ServiceStaff,
                RoomComfort = guestReview.RoomComfort,
                HotelCondition = guestReview.HotelCondition,
                DataSourceId = _dataSourceInfoRepository.FindFromCode(guestReview.Source.ToString()).Id
                // DataSource 
                // Hotel Hotel

            };
        }

    }
}
