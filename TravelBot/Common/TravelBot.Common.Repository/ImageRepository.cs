﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;
using TravelBot.Common.Repository.Contracts;

namespace TravelBot.Common.Repository
{
    public class ImageRepository : Repository<Image>, IImageRepository
    {
        public ImageRepository(ITravelbotDbContext context) : base(context)
        {
        }
        public Image Find(long imageid)
        {
            return Find(s => s.Id == imageid).FirstOrDefault();
        }
    }
}
