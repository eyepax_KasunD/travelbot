﻿using System.Collections.Generic;
using NLog;
using enums = TravelBot.Common.Enums;
using TravelBot.Common.Models;
using System;
using TravelBot.Common;
using Scraper.Services.Contracts;
using Scraper.Common.Models.Queue;
using Scraper.Common.Models.Responses;
using Scraper.Common.Models.Requests;

namespace Scraper.Services
{
    public class ScraperQueueService : IScraperQueueService
    {
        //private readonly IQueueRepository _queueRepository;
        private readonly IScraperService _scraperService;
        //private readonly IHotelRepository _hotelRepository;
        private readonly QueueProvider _queue;
        private readonly string _queueName = Constants.Queues.ScraperHotelPriceSearchReqQueue;
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static readonly string _featureRequestQueueName = Constants.Queues.ScraperHotelDetailReqQueue;

        public ScraperQueueService(IScraperService scraperService)
        {
            _scraperService = scraperService;
            _queue = new QueueProvider();
        }

        public HotelPriceSearchRsp PlaceHotelPriceSearchReq(ScraperHotelPriceSearchPlaceQueueReq request)
        {
            logger.Info("Inside ScraperQueueService");
            var result = new SearchRequestQueueElement()
            {
                SearchRequest = request
            };

            _queue.PublishInQueue(result, _queueName);
            return new HotelPriceSearchRsp() { Status = enums.ResponseStatus.Pending };
        }



        public bool PlaceHotelDetailReq(ScraperHotelDetailPlaceQueueReq request)
        {
            _queue.PublishInQueue(new FeatureRequestQueue()
            {
                Request = request.ScraperHotelDetailReq,
                QueueRequestId = request.QueueRequestId,
            }, _featureRequestQueueName); //publish to feature request queue
            
            return true;
        }
    }
}