﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;
using TravelBot.Common.Helpers.Contracts;
using TravelBot.Common.Repository.Contracts;

namespace TravelBot.Common.Repository
{
   public class CachedCityRepository : CityRepository, ICityRepository
    {
        private readonly ICaching _cache;
        const string prefix = "TravelBot.Common.Repository.CityIds";
        const int ttl = 60 * 60 * 24;
        public CachedCityRepository(ITravelbotDbContext context, ICaching cache) : base(context)
        {
            _cache = cache;
        }
        public List<long> GetAllCityIds()
        {
            List<long> idList = _cache.Get<List<long>>(prefix);
            if (idList == null)
            {
                idList = base.GetAllCityIds();
                _cache.Set(prefix, idList, ttl);
            }
            return idList;      
        }
    }
}
