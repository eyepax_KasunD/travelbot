﻿using System.Threading;
using TravelBotService.MVC.QueueListners;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(TravelBotService.MVC.App_Start.StartQueue), nameof(TravelBotService.MVC.App_Start.StartQueue.Start))]

namespace TravelBotService.MVC.App_Start
{
    public static class StartQueue
    {
        public static void Start()
        {
            //Task.Run(() => ProcessSearchResponseQueue.Start());

            new Thread(HotelPriceSearchReqQueueListner.Start) { IsBackground = true }.Start();
            new Thread(SearchResponseQueueListner.Start) { IsBackground = true }.Start();
            new Thread(FeatureResponseQueueListner.Start) { IsBackground = true }.Start();
            new Thread(UserPreferencesQueueListner.Start) { IsBackground = true }.Start();
        }
    }
}