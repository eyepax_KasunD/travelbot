﻿using NLog;
using System;
using System.Linq;
using TravelBot.Common;
using TravelBot.Common.Mappers.Contracts;
using TravelBot.Common.Models;
using TravelBot.Common.Models.Queue;
using TravelBot.Common.Repository.Contracts;
using TravelbotService.BusinessServices.Contracts;

namespace TravelbotService.BusinessServices.QueueProcessors
{
    public class HotelDetailQueueProcessor : IQueueProcessor<HotelDetailRspQueueElement>
    {
        private readonly IMapper<ScraperHotelDetailRsp, TravelBot.Common.Domain.Hotel> _mapper;
        private readonly IHotelRepository _hotelRepository;
        private readonly IHotelPriceSearchQueueRepository _hotelPriceSearchQueueRepository;
        private readonly IHotelDetailQueueRepository _hotelDetailQueueRepository;
        private readonly IDataSourceRepository _dataSourceRepository;
        private readonly IHotelPriceResponseService _hotelPriceResponseService;
        private readonly ISearchStatusRepository _searchStatusRepository;
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public HotelDetailQueueProcessor(IUnfilteredHotelPriceRepository unfilteredHotelPriceRepository,
            IMapper<ScraperHotelDetailRsp, TravelBot.Common.Domain.Hotel> mapper,
            IHotelPriceSearchQueueRepository hotelPriceSearchQueueRepository,
            IHotelDetailQueueRepository hotelDetailQueueRepository,
            IDataSourceRepository dataSourceRepository,
            IHotelRepository hotelRepository,
            IHotelPriceResponseService hotelPriceResponseService,
            ISearchStatusRepository searchStatusRepository)
        {
            _hotelRepository = hotelRepository;
            _mapper = mapper;
            _hotelPriceSearchQueueRepository = hotelPriceSearchQueueRepository;
            _hotelDetailQueueRepository = hotelDetailQueueRepository;
            _dataSourceRepository = dataSourceRepository;
            _hotelPriceResponseService = hotelPriceResponseService;
            _searchStatusRepository = searchStatusRepository;
        }

        public void Process(HotelDetailRspQueueElement element)
        {
            logger.Info("Process hotel detail reponse queue element, req id = " + element.QueueRequestId);

            var queueElement = _hotelDetailQueueRepository.Get(element.QueueRequestId);
            var elementsWithErrors = _hotelDetailQueueRepository.Find(q =>  q.SearchResponseId == queueElement.SearchResponseId
                                                        && q.DataSourceId == queueElement.DataSourceId
                                                        && !q.ResponseRecieved
                                                        && q.Id < queueElement.Id);
            foreach(var el in elementsWithErrors)
            {
                el.ErrorOccurred = true;
                _hotelDetailQueueRepository.Update(el);
            }

            if (element.ResponseStatus == TravelBot.Common.Enums.ResponseStatus.Completed)
            {
                var hotel = _mapper.Map(element.SearchResponse);
                _hotelRepository.AddHotel(hotel);
                var hotelId = element.SearchResponse.Hotel.HotelIds.First();
                var dataSourceId = _dataSourceRepository.FindFromCode(Enums.DataSourceCode.Booking.ToString()).Id;
                var elements = _hotelDetailQueueRepository.GetElementsBySource(dataSourceId, hotelId.value);
                elements.ForEach(el =>
                {
                    el.ResponseRecieved = true;
                    el.LastModifiedDateTime = DateTime.UtcNow;
                    _hotelDetailQueueRepository.Update(el);
                    if(!_hotelDetailQueueRepository.IsRemainingResponsesExists((long)el.SearchResponseId, (long)el.DataSourceId))
                    {
                        var searchStatus = _searchStatusRepository.GetSearchStatus((long)queueElement.SearchResponseId, (long)queueElement.DataSourceId);
                        searchStatus.Status = (int)Enums.ResponseStatus.Completed;
                        _searchStatusRepository.Update(searchStatus);

                        _hotelPriceResponseService.NotifyResponseCompletion((long)el.SearchResponseId);
                    }
                });

            }
            else if (element.ResponseStatus == TravelBot.Common.Enums.ResponseStatus.Error)
            {
                queueElement.ErrorOccurred = true;
                queueElement.ResponseRecieved = true;
                queueElement.LastModifiedDateTime = DateTime.UtcNow;
                _hotelDetailQueueRepository.Update(queueElement);
            }
        }
    }
}
