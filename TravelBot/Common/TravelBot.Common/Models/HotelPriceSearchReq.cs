﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static TravelBot.Common.Enums;

namespace TravelBot.Common.Models
{
    public class HotelPriceSearchReq
    {
        public string RequestId { get; set; }
        public List<SearchReqCriterion> SearchCriteria { get; set; }
        public DateTime CheckInDate { get; set; }
        public int AdultCount { get; set; }
        public int ChildCount { get; set; }
        public decimal PriceFrom { get; set; }
        public decimal PriceTo { get; set; }
        public int FlexDatesPlus { get; set; }
        public int FlexDatesMinus { get; set; }
        public string UserSessionId { get; set; }
        public string DeviceId { get; set; }
        public DateTime RequestDateTime { get; set; }
        public CurrencyCode Currency { get; set; }
    }

    public class SearchReqCriterion
    {
        public DateTime CheckinDate { get; set; }
        public DateTime CheckoutDate { get; set; }
        public long CountryId { get; set; }
        public long? CityId { get; set; }
        public decimal PriceFrom { get; set; }
        public decimal PriceTo { get; set; }
        public int NoOfStays { get { return (CheckoutDate - CheckinDate).Days; } }
        public decimal PricePerNightFrom { get { return PriceFrom / NoOfStays; } }
        public decimal PricePerNightTo { get { return PriceTo / NoOfStays; } }
    }
}