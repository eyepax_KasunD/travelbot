﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scraper.WebAPI.Models
{
    public class Review
    {
        public decimal Score { get; set; }
        public int Count { get; set; }
    }
}