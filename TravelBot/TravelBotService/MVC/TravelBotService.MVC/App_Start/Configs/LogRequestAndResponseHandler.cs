﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace TravelBotService.MVC.Configs
{
    public class LogRequestAndResponseHandler : DelegatingHandler
    {
        NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        protected override async Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request, CancellationToken cancellationToken)
        {
            // log request body
            string requestBody = await request.Content.ReadAsStringAsync();
            logger.Info(request.RequestUri);
            logger.Info(requestBody);

            // let other handlers process the request
            var result = await base.SendAsync(request, cancellationToken);

            if (result.Content != null)
            {
                // once response body is ready, log it
                var responseBody = await result.Content.ReadAsStringAsync();
#if DEBUG
                //logger.Info(responseBody);
#endif
            }

            return result;
        }
    }
 }