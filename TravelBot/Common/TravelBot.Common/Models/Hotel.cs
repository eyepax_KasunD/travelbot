﻿using System;
using System.Collections.Generic;
using static TravelBot.Common.Enums;

namespace TravelBot.Common.Models
{
    public class Hotel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public List<DataSourceInfo> HotelIds { get; set; }
        public Location Location { get; set; }
        public List<string> ImgUrl { get; set; }
        public GuestReview GuestReview { get; set; }
        public double HotelRating { get; set; }
        public DateTime LastUpdatedDate { get; set; }

        public Hotel()
        {
            GuestReview = new GuestReview();
            Location = new Location();
        }
    }

    public class DataSourceInfo
    {
        public DataSourceCode pricingSource { get; set; }
        public string value { get; set; }
    }

    public class GuestReview
    {
        public int ReviewCount { get; set; }
        public double ReviewScore { get; set; }
        public double RoomCleanliness { get; set; }
        public double ServiceStaff { get; set; }
        public double RoomComfort { get; set; }
        public double HotelCondition { get; set; }
        public DataSourceCode Source { get; set; }
    }

    public class Location
    {
        public string City { get; set; }
        public string Street { get; set; }
        public string LocationString { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
    }
}
