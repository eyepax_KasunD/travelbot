﻿using TravelBot.Common.Models;
using static TravelBot.Common.Enums;
using TravelBotWeb.Common;
using TravelBotWeb.Services.Models;

namespace TravelBotWeb.Services.Contracts
{
    public interface IEmailService
    {
        EmailResponseModel SendEmailDirectly(EmailRequestModel model);
        EmailResponseModel SendEmailtoCustomer(HotelSearchResponseEmail model);
        EmailStatus EmailSending(HotelPriceSearchRsp response, UserSessionModel userSession, string requestId);
        EmailStatus ResendEmail(string id);
        EmailStatus SendEmailFromUserSession(UserSessionModel userSession, string requestId);
        void SendUserEmail(HotelPriceSearchRsp response, UserSessionModel userSession);
    }
}
