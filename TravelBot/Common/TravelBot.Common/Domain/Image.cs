﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TravelBot.Common.Domain
{

    [Table("Sys_Image")]
    public class Image: BaseEntity
    {
        public string URL { get; set; }
        public virtual ICollection<ImageContent> ImageContents { get; set; }
    }
    [Table("Sys_ImageContent")]
    public class ImageContent : BaseEntity
    {
        public string Description { get; set; }
        public virtual Image Image { get; set; }
        public virtual Language Lanuguage { get; set; }
    }
}
