﻿using System;
using System.Collections.Generic;
using System.Linq;
using TravelBot.Common.Models;

namespace TravelBotWeb.Services.Helpers
{
    public class Validator
    {
        private const int MaxDaysBefore = 3;
        private const int MaxDaysOfStay = 30;

        public static List<string> ValidateUserRequest(UserRequestModel userRequest)
        {
            List<string> ErrorMesages = new List<string>();
            string Email = userRequest.Email;
            DateTime Checkin = userRequest.CheckInDateTime;
            var total = userRequest.Destinations.Sum(d => d.Days);

            //var EmailResults = ValidateEmail(Email);
            var CheckinResults = ValidateCheckin(Checkin);
            var StayingDaysResults = ValidateNoOfStays(total);

            //if (EmailResults != "")
            //{
            //    ErrorMesages.Add(EmailResults);
            //}
            if (CheckinResults != "")
            {
                ErrorMesages.Add(CheckinResults);
            }
            if(StayingDaysResults != "")
            {
                ErrorMesages.Add(StayingDaysResults);
            }
            return ErrorMesages;
        }


        public static string ValidateCheckin(DateTime Checkin)
        {
            var DateDifference = (Checkin - DateTime.Now).TotalDays;
            if(MaxDaysBefore <= DateDifference)
            {
                return "";
            }
            else
            {
                return "chekin date is not valid";
            }
        }

        public static string ValidateNoOfStays(int NoOfStays)
        {
            
            if (NoOfStays <= MaxDaysOfStay)
            {
                return "";
            }
            else
            {
                return "Number of staying days is not valid";
            }
        }

    }
}