﻿using System;
using System.Linq;
using Quartz;
using System.Threading.Tasks;
using NLog;
using enums = TravelBot.Common.Enums;
using Scraper.Repository.Contracts;
using Scraper.Services.Contracts;

namespace Scraper.WebAPI.Scheduling
{
    public class ProcessHotelRequests : IJob
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private readonly IQueueRepository _queueRepository;
        private readonly IScraperService _scraperService;
        private readonly int MaxRecordsToScrap = 3;

        public ProcessHotelRequests(IQueueRepository queueRepository, IScraperService scraperService)
        {
            _queueRepository = queueRepository;
            _scraperService = scraperService;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            //logger.Info("start processing hotel requests");
            //var scrapedHotels = 0;
            //do
            //{
            //    var elements = _queueRepository.GetHotelElementsFromQueue(e => e.Status == enums.RequestStatus.Pending, MaxRecordsToScrap);

            //    foreach (var element in elements)
            //    {

            //        element.Status = enums.RequestStatus.Running;
            //        try
            //        {
            //            logger.Info($"Scrape hotel, request id  :{ element.RequestId},  hotel id : { element.Request.HotelId}");
            //            _queueRepository.Update(element);
            //           var response =  _scraperService.ScrapeHotelInfo(element.Request);
            //            if(!response.CachedResponse)
            //                scrapedHotels++;
            //            if (response.Errors.Count == 0)
            //            {
            //                element.Status = enums.RequestStatus.Ready;
            //                element.LastUpdatedDateTime = DateTime.Now;
            //                _queueRepository.Update(element);
            //            }
            //            else
            //            {
            //                throw new Exception(string.Join(Environment.NewLine, response.Errors));
            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            logger.Error("Url : " + element.Request.Hotelurl);
            //            logger.Error("Ex : " + ex);
            //            element.Status = enums.RequestStatus.Error;
            //            element.LastUpdatedDateTime = DateTime.Now;
            //            _queueRepository.Update(element);
            //        }
            //    }
            //    if (elements.Count < MaxRecordsToScrap)
            //        break;

            //} while (scrapedHotels < MaxRecordsToScrap);
            
            //logger.Info("end processing hotel requests");
        }

    }
}