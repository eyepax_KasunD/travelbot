﻿using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using TravelBotWeb.Common;
using TravelBotWeb.Services.Contracts;
using TravelBotWeb.Services.Helpers;

namespace TravelBotWeb.Controllers
{
    public class RedirectController : BaseController
    {
        private readonly IUserService _service;
        public RedirectController(IUserService userService, IAppSettings appSettings) : base(appSettings)

        {
            _service = userService;
        }

        [Route("redirect")]
        public ActionResult Index(string id, string lang = "en")
        {
            logger.Info("User Redirecting to page with language: " + lang+" and Id: "+id);
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(lang);
            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(lang);

            var decryptString = Crypto.Base64Decode(id);
            var queryParams = HttpUtility.ParseQueryString(decryptString);
            var userSessionId = queryParams.Get("uId");
            var hotelPriceId = int.Parse(queryParams.Get("prId"));
            var requestId = queryParams.Get("rId"); 

            ViewBag.Lang = lang;
            var hotelData = _service.GetHotelUrlAndSource(userSessionId, hotelPriceId, requestId);
            _service.SaveUserClickedHotelIndex(userSessionId, hotelPriceId, requestId);
            ViewBag.Source = hotelData.Item1;
            ViewBag.HotelUrl = hotelData.Item2;
            return View();
        }
    }
}