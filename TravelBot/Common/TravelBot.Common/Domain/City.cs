﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TravelBot.Common.Domain
{
    [Table("Sys_City")]
    public class City: BaseEntity
    {
        public virtual ICollection<CityImage> CityImages { get; set; }
        public virtual Country Country { get; set; }
        public virtual ICollection<CityContent> CityContents { get; set; }
        public virtual ICollection<SourceCityMap> SourceMaps { get; set; }
        public bool IsEnabled { get; set; }
        public virtual Image Thumbnail { get; set; } //thumbnail
        public long? ThumbnailId { get; set; }
    }

    [Table("Sys_CityContent")]
    public class CityContent : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual Language Language { get; set; }
        public virtual City City { get; set; }
    }
    [Table("Sys_CityImage")]
    public class CityImage : BaseEntity
    {
        public virtual City City { get; set; }
        public virtual Image Image { get; set; }
    }

    [Table("Set_SourceCityMap")]
    public class SourceCityMap
    {
        public virtual City City { get; set; }
        [Key, Column(Order = 0)]
        public long? CityId { get; set; }
        public virtual DataSource Source { get; set; }
        [Key, Column(Order = 1)]
        public long? DataSourceId { get; set; }
        public string Value { get; set; }
    }
}
