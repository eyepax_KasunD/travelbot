﻿using System;
using System.Collections.Generic;
using TravelBot.Common.Models;
using TravelBotWeb.Common;
using TravelBotWeb.Services.Models;

namespace TravelBotWeb.Services.Contracts
{
    public interface IUserService
    {
        UserSessionModel StoreUserSession(UserRequestModel UserRequest);
        ApiResponseModel<HotelPriceSearchRsp> ScrappingCallerFromId(string UserId);
        HotelPriceSearchRsp ScrappingCallerForSearchingObjects(UserSessionModel sessionModel, string requestId);
        ApiResponseModel<HotelPriceSearchRsp> RetrieveHotelPriceSearchRsp(UserSessionModel sessionModel, string requestId);
        ContinuousSearch GetSearchObjectAndRequestPriceIfNeeded(UserSessionModel userSession, bool requestPricesIfNeeded = false);
        List<CityHotelData>  GetCityHotelData(UserSessionModel userSession, ContinuousSearch searchObj, int flexDates);
        List<FlextDateViewModel> GenerateFlexDateOptions(HotelPriceSearchRsp rsp);
        bool StopSearch(string reason, string id);
        Tuple<string, string> GetHotelUrlAndSource(string userSessionId, int travelbotIndex, string requestId);
        void SaveUserClickedHotelIndex(string userSessionId, int HotelPriceId, string requestId);
        UserSessionModel GetUserSession(string userSessionId);
        UserSessionModel FindAndUpdateUserSession(HotelPriceSearchRsp response);
    }
}

