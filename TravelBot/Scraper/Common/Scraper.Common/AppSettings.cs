﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace Scraper.Common
{
    public class AppSettings : IAppSettings
    {
        public int HotelDataExpirationTime { get { return GetIntegerValue("UpdatingTimePeriod"); } }
        public string BookingUrl { get { return GetValue("Booking_Url"); } }

        private int GetIntegerValue(string key)
        {
            string value = WebConfigurationManager.AppSettings[key];
            int.TryParse(value, out int parsedValue);
            return parsedValue;
        }

        private String GetValue(string key)
        {
            return WebConfigurationManager.AppSettings[key];
        }
    }
}