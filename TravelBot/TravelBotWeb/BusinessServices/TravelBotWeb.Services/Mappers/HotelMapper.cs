﻿using System.Collections.Generic;
using TravelBot.Common.Domain;
using TravelBotWeb.Common;

namespace TravelBotWeb.Services.Mappers
{
    public class HotelMapper
    {
        public Hotel GetHotel(TravelBot.Common.Models.Hotel hotel)
        {
            return new Hotel()
            {
               // Name = hotel.Name,
               //// HotelIds =  string.Join("|", hotel.HotelIds),
               // ImgUrls = string.Join("|", hotel.ImgUrl),
               // HotelRating = hotel.HotelRating,
               // GuestReviews = new List<GuestReview> { GetGuestReview(hotel.GuestReview) },
               // Location = new Location()
               // {
               //     City = hotel.Location.City,
               //     Street = hotel.Location.Street,
               //     LocationString = hotel.Location.LocationString,
               //     Lat = hotel.Location.Latitude,
               //     Lang = hotel.Location.Longitude
               // },

            };
        }
        private GuestReview GetGuestReview(TravelBot.Common.Models.GuestReview guestReview)
        {
            return new GuestReview()
            {
                ReviewCount = guestReview.ReviewCount,
                ReviewScore = guestReview.ReviewScore,
                RoomCleanliness = guestReview.RoomCleanliness,
                ServiceStaff = guestReview.ServiceStaff,
                RoomComfort = guestReview.RoomComfort,
                HotelCondition = guestReview.HotelCondition,
                //Source = guestReview.Source
            };
        }

    }
}
