﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scraper.Common
{
    public  interface IAppSettings
    {
        int HotelDataExpirationTime { get; }
        string BookingUrl { get; }
        
    }
}
