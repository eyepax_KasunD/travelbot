﻿using NLog;
using System;
using System.Linq;
using TravelBot.Common;
using TravelBot.Common.Domain;
using TravelBot.Common.Mappers.Contracts;
using TravelBot.Common.Models;
using TravelBot.Common.Models.Queue;
using TravelBot.Common.Repository.Contracts;
using TravelbotService.BusinessServices.Contracts;

namespace TravelbotService.BusinessServices.QueueProcessors
{
    public class UserPreferencesQueueProcessor : IQueueProcessor<UserPreferences>
    {
        private readonly IUserClickRepository _userClickRepository;
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public UserPreferencesQueueProcessor(IUserClickRepository userClickRepository)
        {
            _userClickRepository = userClickRepository;
        }

        public void Process(UserPreferences element)
        {
            var userClick = new UserClick
            {
                UserSessionId = element.UserSessionId,
                HotelPriceId = element.HotelPriceId
            };
            _userClickRepository.Add(userClick);
        }
    }
}
