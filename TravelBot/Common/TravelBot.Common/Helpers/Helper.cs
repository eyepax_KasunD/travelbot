﻿using System;
using System.Collections.Generic;
using System.Linq;
using TravelBot.Common.Models;
using static TravelBot.Common.Enums;

namespace TravelBot.Common.Helpers
{
    public static class Helper
    {
        public static HotelPriceSearchRsp MergeHotelPriceSearchRspList(List<HotelPriceSearchRsp> rspList)
        {
            var requestIdList = rspList.Select(r => r.RequestId).Distinct().ToList();
            if(requestIdList.Count != 1)
            {
                throw new InvalidOperationException("Multiple request IDs found when merging HotelPriceSearchRsp list");
            }
            var searchCritaria = rspList.Where(r => r.SearchCriteria != null).SelectMany(r => r.SearchCriteria).ToList();
            var hotels = rspList.Where(r => r.HotelList != null).SelectMany(r => r.HotelList).GroupBy(h => h.Id).Select(g => g.First()).ToList();
            var dataSources = rspList.Where(r => r.DataSources != null).SelectMany(r => r.DataSources).GroupBy(d => d.Code).Select(g => g.First()).ToList();
            var errors = rspList.Where(r => r.Errors != null).SelectMany(r => r.Errors).ToList();
            var warnings = rspList.Where(r => r.Warnings != null).SelectMany(r => r.Warnings).ToList();

            var statusList = rspList.Select(r => r.Status).Distinct();
            var status = statusList.Count() == 1 ? statusList.First() : ResponseStatus.Partial; 

            return new HotelPriceSearchRsp
            {
                SearchCriteria = searchCritaria,
                HotelList = hotels,
                DataSources = dataSources,
                Status = status,
                RequestId = requestIdList.First(),
                Errors = errors,
                Warnings = warnings
            };
        }

    }
}
