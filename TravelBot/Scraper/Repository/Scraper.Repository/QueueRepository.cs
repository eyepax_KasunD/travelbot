﻿using TravelBot.Common.Repositories;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using enums = TravelBot.Common.Enums;
using TravelBot.Common.Models;
using Scraper.Common.Models.Queue;
using Scraper.Repository.Contracts;

namespace Scraper.Repository
{
    public class QueueRepository:IQueueRepository
    {
        //private static Logger logger = LogManager.GetCurrentClassLogger();
        //private readonly MongoDBRepository<SearchRequestQueueElement> _searchRequestQueueRepository;
        //private readonly MongoDBRepository<FeatureRequestQueue> _featureRequestQueueRepository;
        //public QueueRepository()
        //{
        //    _searchRequestQueueRepository = new MongoDBRepository<SearchRequestQueueElement>("Queue", "SearchRequests");
        //    _featureRequestQueueRepository = new MongoDBRepository<FeatureRequestQueue>("Queue", "featureRequests");
        //}

        //public bool QueueSearchRequest(SearchRequestQueueElement request)
        //{
        //    var result = false;
        //    try
        //    {
        //        _searchRequestQueueRepository.Insert(request);
        //        result = true;

        //    }
        //    catch (Exception ex)
        //    { logger.Error(ex); }
        //    return result;
        //}

        //public SearchRequestQueueElement GetSearchElementFromQueue(Expression<Func<SearchRequestQueueElement, bool>> predicate)
        //{
        //    return _searchRequestQueueRepository.Find(predicate).FirstOrDefault();
        //}

        //public void Update(SearchRequestQueueElement element)
        //{
        //   // _searchRequestQueueRepository.Update(e => e.Id == element.Id, element);
        //}

        //public SearchRequestQueueElement GetSearchRequestFromQueue(ScraperHotelPriceReq request)
        //{
        //    return _searchRequestQueueRepository.Find(r => r.SearchRequest.RequestId == request.RequestId).FirstOrDefault();
        //}

        //public bool QueueHotelRequest(List<FeatureRequestQueue> requests)
        //{
        //    var result = false;
        //    try
        //    {
        //        _featureRequestQueueRepository.Insert(requests);
        //        result = true;

        //    }
        //    catch (Exception ex)
        //    { logger.Error(ex); }
        //    return result;
        //}

        //public List<FeatureRequestQueue> GetHotelElementsFromQueue(Expression<Func<FeatureRequestQueue, bool>> predicate, int count)
        //{
        //    return _featureRequestQueueRepository.Find(predicate).Take(count).ToList();
        //}

        //public void Update(FeatureRequestQueue element)
        //{
        //    element.LastUpdatedDateTime = DateTime.Now;
        //    _featureRequestQueueRepository.Update(e => e.Id== element.Id, element);
        //}

        //public bool FindReadyHotelFeatures(string RequestId)
        //{
        //    var result = _featureRequestQueueRepository.Find(h => h.RequestId.Equals(RequestId)).All(h => h.Status == enums.RequestStatus.Ready);
        //    return result;
        //}
        //public List<FeatureRequestQueue> FindAllHotelRequestsfromStatus(TravelBot.Common.Enums.RequestStatus requestStatus, int maxRepetitions, int errorCheckinghours)
        //{
        //    return _featureRequestQueueRepository.Find(x => x.Status.Equals(requestStatus) && DateTime.Now.AddHours(-errorCheckinghours) > x.LastUpdatedDateTime && x.RepetitionCount <= maxRepetitions).ToList();
        //}

        //public List<SearchRequestQueueElement> FindAllSearchRequestsfromStatus(TravelBot.Common.Enums.ResponseStatus responseStatus, int maxRepetitions, int errorCheckinghours)
        //{
        //    return _searchRequestQueueRepository.Find(x => x.SearchResponse.Status.Equals(responseStatus) && DateTime.Now.AddHours(-errorCheckinghours) > x.LastUpdatedDateTime && x.ErrorRepetitionCount <= maxRepetitions).ToList();
        //}

        //public void DeleteSearchRequests(DateTime date)
        //{
        //    _searchRequestQueueRepository.DeleteMany(x => x.SearchRequest.CheckInDate < date);
        //}
        //public void DeleteFeatureRequests(string id)
        //{
        //    _featureRequestQueueRepository.DeleteMany(x => x.RequestId.Equals(id));
        //}
        //public List<SearchRequestQueueElement> FindSearchRequestsUsingCriteria(DateTime date)
        //{
        //    return _searchRequestQueueRepository.Find(x => x.SearchRequest.CheckInDate < date).ToList();
        //}
        //public void DeleteFeatureRequest(FeatureRequestQueue element)
        //{
        //    //TODO : use object id
        //    _featureRequestQueueRepository.Delete(e => e.Id == element.Id);
        //}
    }
}