﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;
using TravelBot.Common.Repository.Contracts;

namespace TravelBot.Common.Repository
{
    public class SourceCountryMapRepository : Repository<SourceCountryMap>, ISourceCountryMapRepository
    {
        public SourceCountryMapRepository(ITravelbotDbContext context) : base(context)
        {
        }
        public SourceCountryMap GetCountry(long countryId, long datasourceId)
        {
            return Find(s => s.CountryId == countryId && s.DataSourceId == datasourceId).FirstOrDefault();
        }

    }
}
