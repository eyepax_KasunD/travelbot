﻿using System;
using System.Collections.Generic;
using static TravelBot.Common.Enums;

namespace TravelBot.Common.Models
{
    public class ScraperHotelPriceSearchReq
    {
        public long? CityId { get; set; }
        public long? CountryId { get; set; }
        public string RequestId { get; set; }
        public string SearchString { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
        public int AdultCount { get; set; }
        public int ChildCount { get; set; }
        public CurrencyCode CurrencyCode { get; set; }
        public DataSourceCode ScrapingSource { get; set; }
        public decimal PriceFrom { get; set; }
        public decimal PriceTo { get; set; }
        public int FlexDatesPlus { get; set; }
        public int FlexDatesMinus { get; set; }
        public int NoOfStays { get; set; }
        public decimal PricePerNightFrom { get { return PriceFrom / NoOfStays; } }
        public decimal PricePerNightTo { get { return PriceTo / NoOfStays; } }
    }
}