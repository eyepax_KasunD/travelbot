﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;
using TravelBot.Common.Mappers.Contracts;
using TravelBot.Common.Models;
using TravelBot.Common.Repository.Contracts;
using TravelbotService.BusinessServices.Contracts;
using TravelbotService.BusinessServices.Factories.Contracts;
using TravelBotService.Common;

namespace TravelbotService.BusinessServices.QueueProcessors
{
    public class HotelPriceSearchReqQueueProcessor : IQueueProcessor<HotelPriceSearchReq>
    {
        private readonly IHotelPricesFactory _hotelPriceFactory;
        private readonly IMapper<HotelPriceSearchReq, SearchResponse> _searchResponseMapper;
        private readonly ISearchResponseRepository _searchResponseRepository;

        public HotelPriceSearchReqQueueProcessor(IHotelPricesFactory hotelPriceFactory,
            IMapper<HotelPriceSearchReq, SearchResponse> searchResponseMapper,
            ISearchResponseRepository searchResponseRepository)
        {
            _hotelPriceFactory = hotelPriceFactory;
            _searchResponseMapper = searchResponseMapper;
            _searchResponseRepository = searchResponseRepository;
        }

        public void Process(HotelPriceSearchReq req)
        {
            var response = _searchResponseRepository.GetSearchResponseFromRequestId(req.RequestId);
            var requestWrapper = new HotelPriceSearchReqWrapper { HotelPriceSearchReq = req, SearchResponse = response };
            _hotelPriceFactory.GetHotelPrices(requestWrapper);
        }
    }
}
