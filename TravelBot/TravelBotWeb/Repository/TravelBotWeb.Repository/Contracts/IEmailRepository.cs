﻿using TravelBotWeb.Common;

namespace TravelBotWeb.Repository.Contracts
{
    public interface IEmailRepository
    {
        EmailResponseModel SendEmail(EmailRequestModel model);
    }
}
