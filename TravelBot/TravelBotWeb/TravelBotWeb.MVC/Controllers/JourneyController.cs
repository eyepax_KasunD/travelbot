﻿using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Web.Mvc;
using System.Threading.Tasks;
using TravelBot.Common.Models;
using TravelBotWeb.Common;
using TravelBotWeb.Services.Contracts;
using TravelBotWeb.Services.Models;

namespace TravelBotWeb.Controllers
{
    public class JourneyController : BaseController
    {
        private readonly IUserService _service;
        private readonly IEmailService _emailService;
        protected readonly ICountryService _countryService;
        public JourneyController(IEmailService emailService,IUserService userService, IAppSettings appSettings, ICountryService countryService) : base(appSettings)
        {
            _service = userService;
            _emailService = emailService;
             _countryService = countryService; 
        }

        [ActionName("plan")]
        [OutputCache(Duration = int.MaxValue, VaryByParam = "lang")]
        public ActionResult JournyPlan(string lang)
        {
            logger.Info("Going to journey plan page with language: " + lang);
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(lang);
            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(lang);

            ViewBag.Lang = lang;
            
            ViewBag.CityList = _countryService.GetCountryList(lang);
            return View();
        }

        [OutputCache(Duration = int.MaxValue, VaryByParam = "lang")]
        public ActionResult TripManager(string lang)
        {
            logger.Info("Going to trip manager page with language: " + lang);
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(lang);
            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(lang);

            ViewBag.Lang = lang;
            return View();
        }
        
        public ActionResult HotelList(string lang, string id)
        {
            logger.Info("User requesting to browse history with language: "+lang+" and Id: "+id);

            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(lang);
            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(lang);

            ViewBag.Lang = lang;
            ViewBag.reqId = id;
            var userSession = _service.GetUserSession(id);
            if(userSession == null)
            {
                return View("Error");
            }
            var searchObj = _service.GetSearchObjectAndRequestPriceIfNeeded(userSession, true);

            if (searchObj.HotelSearchResponse != null)
            {
                ViewBag.sufficient = true;
                ViewBag.SessionId = id;
                ViewBag.FlexDates = _service.GenerateFlexDateOptions(searchObj.HotelSearchResponse);
            }
            else
            {
                ViewBag.sufficient = false;
                //TODO : handle emails
                //Task.Run(() => _emailService.SendEmailFromUserSession(userSession, searchObj.RequestId));
            }
            ViewBag.maxBudget = "$"+userSession.PriceTo;
            return View();
        }

        public ActionResult StopSearch(string lang, string id)
        {
            logger.Info("User requesting to stop search with language: " + lang+" and Id: "+id);
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(lang);
            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(lang);

            ViewBag.Lang = lang;
            ViewBag.SessionId = id;
            return View();
        }
    }
}