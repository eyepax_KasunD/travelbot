﻿using NLog;
using System;
using System.Messaging;
using TravelBot.Common;
using TravelBot.Common.Models;
using TravelBotWeb.Services.Contracts;

namespace TravelBotWeb.QueueServices
{
    public class HotelSearchResponseQueueListener
    {
        public static void Start()
        {
            IEmailService _emailService = UnityConfig.Resolve<IEmailService>();
            IUserService _userService = UnityConfig.Resolve<IUserService>();
            Logger logger = LogManager.GetCurrentClassLogger();
            logger.Info("EmailQueueListener starting");
            var path = Constants.Queues.HotelPriceSearchRspQueue;

            using (var queue = new MessageQueue(path))
            {
                while (true)
                {
                    queue.Formatter = new XmlMessageFormatter(new Type[] { typeof(HotelPriceSearchRsp) });
                    var message = queue.Receive();
                    if (message == null)
                        return;

                    HotelPriceSearchRsp queueElement = new HotelPriceSearchRsp();
                    queueElement = (HotelPriceSearchRsp)message.Body;
                    logger.Info("Hotel Price Search Rsp Request id: " + queueElement.RequestId);
                    var userSession = _userService.FindAndUpdateUserSession(queueElement);
                    if(userSession != null)
                    {
                        _emailService.SendUserEmail(queueElement, userSession);
                    }
                }
            }
        }
    }
}