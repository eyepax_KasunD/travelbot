﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common.Domain;
using TravelBot.Common.Repository.Contracts;

namespace TravelBot.Common.Repository
{
    public class SearchResponseRepository : Repository<SearchResponse>, ISearchResponseRepository
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public SearchResponseRepository(ITravelbotDbContext context) : base(context)
        {
        }

        public SearchResponse AddSearchResponse(SearchResponse searchResponse)
        {
            return base.Add(searchResponse);
        }

        public SearchResponse GetSearchResponseFromId(long id)
        {
            return base.Get(id);
        }

        public SearchResponse GetSearchResponseFromRequestId(string id)
        {
            return base.Find(r => r.RequestId.Equals(id)).FirstOrDefault();
        }

        public SearchResponse UpdateSearchResponse(SearchResponse searchResponse)
        {
            return base.Update(searchResponse);
        }

    }
}
