﻿using System;
using System.Linq;
using TravelBot.Common.Domain;
using TravelBot.Common.Models;
using TravelBot.Common.Repository.Contracts;
using TravelBotWeb.Common;
using TravelBotWeb.Services.Contracts;

namespace TravelBotWeb.Services.Mappers
{
    public class JourneyPlanModelMapper : IJourneyPlanModelMapper
    {
        private readonly IAppSettings _appsettings;
        private readonly ILanguageRepository _languageRepository;
        public JourneyPlanModelMapper(IAppSettings appsettings, ILanguageRepository languageRepository)
        {
            _appsettings = appsettings;
            _languageRepository = languageRepository;
        }
        public JourneyPlanModel ModelMapper(Country country, string lang)
        {
            return new JourneyPlanModel()
            {
                CountryName = country.CountryContents.Where(s => s.Language.Tag == lang).FirstOrDefault().Name,
                Cities = country.Cities.Select(s => GetCityList(s, lang)).ToList(),
                CountryImages = country.CountryImages.Select(e => GetImageModels(e.Image, lang)).ToList(),
                CountryThumbnailUrl = _appsettings.CDN_Domain + country.Thumbnail.URL

            };
        }
        private CityInfo GetCityList(City s, string lang)
        {
            return new CityInfo()
            {
                CityName = s.CityContents.Where(r => r.Language.Tag == lang).FirstOrDefault().Name,
                Description = s.CityContents.Where(r => r.Language.Tag == lang).FirstOrDefault().Description,
                CityImages = s.CityImages.Select(e => GetImageModels(e.Image, lang)).ToList(),
                CityThumbnailUrl = _appsettings.CDN_Domain + s.Thumbnail.URL,
                Province = ""
            };
        }

        private ImageModel GetImageModels(Image image, string lang)
        {
            return new ImageModel()
            {
                Description = image.ImageContents.Where(w => w.Lanuguage.Tag == lang).FirstOrDefault().Description,
                URL = _appsettings.CDN_Domain + image.URL
            };
        }

    }
}
