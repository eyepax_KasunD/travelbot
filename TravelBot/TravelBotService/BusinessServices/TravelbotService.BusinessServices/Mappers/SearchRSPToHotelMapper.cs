﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelBot.Common;
using TravelBot.Common.Domain;
using TravelBot.Common.Mappers.Contracts;
using TravelBot.Common.Models;
using TravelBot.Common.Repository.Contracts;
using static TravelBot.Common.Enums;

namespace TravelbotService.BusinessServices.Mappers
{
    public class SearchRSPToHotelMapper: IMapper<TravelBot.Common.Domain.Hotel, TravelBot.Common.Models.Hotel>
    {

        public TravelBot.Common.Models.Hotel Map(TravelBot.Common.Domain.Hotel hotel)
        {
            return new TravelBot.Common.Models.Hotel()
            {
                GuestReview = hotel.GuestReviews.Select(s => MapGuestReviews(s)).FirstOrDefault(), //TODO,,, make GuestReview List in TravelBot.Common.Models.Hotel
                HotelIds = hotel.HotelIds.Select(s => MapHotelIds(s)).ToList(),
                HotelRating = (double)hotel.HotelRating,
                Id = hotel.Id.ToString(),
                ImgUrl = hotel.Images.Select(s => MapImages(s)).ToList(),
                Location = MapLocation(hotel),
                Name = hotel.Name,
                LastUpdatedDate = hotel.LastModifiedDateTime
            };
        }

        private Location MapLocation(TravelBot.Common.Domain.Hotel hotel)
        {
            return new Location()
            {
                LocationString = hotel.Address,
                Latitude = hotel.Latitude,
                Longitude = hotel.Longitude
            };
        }

        private string MapImages(HotelImage images)
        {
           return images.URL;
        }

        private DataSourceInfo MapHotelIds(HotelSourceMap hotelIds)
        {
           return new DataSourceInfo() {
               pricingSource=(DataSourceCode)Enum.Parse(typeof(DataSourceCode), hotelIds.DataSource.Code),
               value = hotelIds.DataSourceId.ToString()
           };
        }

        private TravelBot.Common.Models.GuestReview MapGuestReviews(TravelBot.Common.Domain.GuestReview guestReviews)
        {
            return new TravelBot.Common.Models.GuestReview() {
                HotelCondition=guestReviews.HotelCondition,
                ReviewCount=guestReviews.ReviewCount,
                ReviewScore=guestReviews.ReviewScore,
                RoomCleanliness=guestReviews.RoomCleanliness,
                RoomComfort=guestReviews.RoomComfort,
                ServiceStaff=guestReviews.ServiceStaff,
                Source = (DataSourceCode)Enum.Parse(typeof(DataSourceCode), guestReviews.DataSource.Code) //TODO check with sys_DataSource Table and return id, give guestReviews.DataSourceId as parameter
            };
        }

    }
}
