﻿using System.Linq;
using TravelBot.Common.Domain;
using TravelBot.Common.Helpers.Contracts;
using TravelBot.Common.Repository.Contracts;

namespace TravelBot.Common.Repository
{
    public class CachedLanguageRepository : LanguageRepository, ILanguageRepository
    {
        private readonly ICaching _cache;
        const string prefix = "TravelBot.Common.Repository.Language";
        const int ttl = 60 * 60 * 24;
        public CachedLanguageRepository(ITravelbotDbContext context, ICaching cache) : base(context)
        {
            _cache = cache;
        }

        public Language Find(string languageTag)
        {
            Language language = _cache.Get<Language>(prefix);
            if (language == null)
            {
                language = base.Find(languageTag);
                _cache.Set(prefix, language, ttl);
            }
            return language;            
        }
    }
}
