﻿using NLog;
using System;
using System.IO;
using System.Linq;
using TravelBot.Common;
using TravelBot.Common.Domain;
using TravelBot.Common.Helpers;
using TravelBot.Common.Mappers.Contracts;
using TravelBot.Common.Models;
using TravelBot.Common.Repository;
using TravelBot.Common.Repository.Contracts;
using TravelbotService.BusinessServices.Contracts;
using TravelbotService.BusinessServices.Factories.Contracts;
using TravelBotService.Common;
using TravelBotService.Repository;
using static TravelBot.Common.Enums;
using enums = TravelBot.Common.Enums;

namespace TravelbotService.BusinessServices
{
    public class HotelPriceResponseService : IHotelPriceResponseService
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private readonly ISearchResponseRepository _searchResponseRepository;
        private readonly IHotelPricesFactory _hotelPriceFactory;
        private readonly IHotelSearchRequestMapper _hotelSearchRequestMapper;
        private readonly IMapper<SearchResponse, HotelPriceSearchRsp> _searchResponseMapper;
        private readonly IHotelDetailQueueRepository _hotelDetailQueueRepository;
        private readonly IHotelPriceSearchQueueRepository _hotelPriceSearchQueueRepository;
        private readonly ISearchStatusRepository _searchStatusRepository;
        private readonly IHotelPriceProcessor _hotelPriceprocessor;
        private readonly QueueProvider _queue;
        private readonly string _queueName = Constants.Queues.HotelPriceSearchRspQueue;

        public HotelPriceResponseService(IHotelPricesFactory hotelPriceFactory, 
            IHotelSearchRequestMapper hotelSearchRequestMapper, 
            ISearchResponseRepository searchResponseRepository,
            IMapper<SearchResponse, HotelPriceSearchRsp> searchResponseMapper,
            IHotelDetailQueueRepository hotelDetailQueueRepository,
            IHotelPriceProcessor hotelPriceprocessor,
            ISearchStatusRepository searchStatusRepository,
            IHotelPriceSearchQueueRepository hotelPriceSearchQueueRepository)
        {
            _hotelPriceFactory = hotelPriceFactory;
            _hotelSearchRequestMapper = hotelSearchRequestMapper;
            _searchResponseRepository = searchResponseRepository;
            _searchResponseMapper = searchResponseMapper;
            _hotelDetailQueueRepository = hotelDetailQueueRepository;
            _hotelPriceSearchQueueRepository = hotelPriceSearchQueueRepository;
            _hotelPriceprocessor = hotelPriceprocessor;
            _searchStatusRepository = searchStatusRepository;
            _queue = new QueueProvider();
        }

        public HotelPriceSearchRsp GetHotelPiceSearchRspFromDb(string requestId)
        {
            HotelPriceSearchRsp priceResponse = null;
            var responseFromDb = _searchResponseRepository.GetSearchResponseFromRequestId(requestId);
            if(responseFromDb!= null)
            {
                priceResponse = _searchResponseMapper.Map(responseFromDb);
            }
            return priceResponse;
        }

        public void NotifyResponseCompletion(long responseId)
        {
            if (_searchStatusRepository.IsSearchCompleted(responseId))
            {
                _hotelPriceprocessor.ProcessHotelPrices(responseId);
                var response = _searchResponseRepository.GetSearchResponseFromId(responseId);
                response.ResponseStatus = ResponseStatus.Completed.ToString();
                var priceResponse = _searchResponseMapper.Map(response);
                logger.Info($"## added  response id({responseId}) to the queue");
                WritePriceResponseToFile(priceResponse);
                _queue.PublishInQueue(priceResponse, _queueName);
            }
            else
            {
                logger.Info($"response id({responseId}) isn't a completed rsp");
            }
        }

        private void WritePriceResponseToFile(HotelPriceSearchRsp priceResponse)
        {
#if DEBUG
            var path = $@"{AppDomain.CurrentDomain.BaseDirectory}logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            WriteObjToXml(priceResponse, path, priceResponse.RequestId);
#endif
        }

        private void WriteObjToXml<T>(T obj, string path, string requestId)
        {
            System.Xml.Serialization.XmlSerializer writer =
                new System.Xml.Serialization.XmlSerializer(typeof(T));

            var filePath = $@"{path}\SearchRsp_{requestId}.xml";
            System.IO.FileStream file = System.IO.File.Create(filePath);

            writer.Serialize(file, obj);
            file.Close();
        }
    }
}
