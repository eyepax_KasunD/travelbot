﻿using TravelBot.Common.Helpers;
using System.Linq;
using TravelBotWeb.Common;
using TravelBotWeb.Repository;
using System;
using System.Collections.Generic;
using TravelBot.Common.Models;
using TravelBot.Common.Repository;
using TravelBot.Common.Domain;

namespace TravelBotWeb.Services.Mappers
{
    public class UserSessionMapper
    {
        public readonly HotelRepository _hotelRepository;
        public UserSessionMapper()
        {
            _hotelRepository = new HotelRepository(new TravelbotDbContext());
        }
        public UserSession MapUserSessionModel(UserSessionModel userSession)
        {
            return new UserSession()
            {
                //UserSessionId = userSession.UserId,
                //MappedSearchSegmentList = GetMappedSearchSegmentList(userSession.SearchSegmentList),
                //CheckinDate = userSession.CheckInDate,
                //Adults = userSession.AdultCount,
                //Children = userSession.ChildCount,
                //PriceFrom = userSession.PriceFrom,
                //PriceTo = userSession.PriceTo,
                //Email = userSession.Email,
                //DeviceId = userSession.DeviceId,
                //SearchRequests = userSession.SearchObjects.Select(s => GetSearchRequest(s)).ToList(),
            };

        }

        private List<string> GetMappedSearchSegmentList(List<SearchReqCriterion> searchSegmentList)
        {
            //List<MappedSearchSegment> list = new List<MappedSearchSegment>();
            //searchSegmentList.ForEach(s => list.Add(new MappedSearchSegment() {
            //    CheckingDate=s.CheckingDate,
            //    CheckoutDate=s.CheckoutDate,
            //    CityId=s.CityId,
            //    CountryId=s.CountryId,
            //    HotelId=s.HotelId
            //})) ;
            return null;
        }

        //private SearchResponse GetSearchRequest(ContinuousSearch s)
        //{
        //    return new SearchResponse()
        //    {
        //        //RequestDate = s.RequestDate,
        //        //RequestId = s.RequestId,
        //        //HotelPrices = s.HotelSearchResponse == null ? null : s.HotelSearchResponse.HotelPriceWrappers.SelectMany(w => w.HotelPrices.Select(p => GetPriceList(p))).ToList()
        //    };
        //}
        private TravelBot.Common.Domain.UnfilteredHotelPrice GetPriceList(TravelBot.Common.Models.HotelPrice hotelPrice)
        {
            return new TravelBot.Common.Domain.UnfilteredHotelPrice()
            {
                //Order = hotelPrice.Index,
                //TravelbotIndex = hotelPrice.TravelBotIndex,
                //IsPriceMatched = hotelPrice.IsPriceMatched,
                //Url = hotelPrice.Link,
                //Source = hotelPrice.Source.ToString(),
                //Price = hotelPrice.Price,
                //CheckinDate = hotelPrice.CheckInDate,
               // HotelId = _hotelRepository.GetId(HotelIdentifier.GetIdentifierFromUrl(hotelPrice.Link)) //find the relevant hotel id from table 'Hotel' . get the url (From HotelPrices Table) and check it with "HotelIds" in 'Hotel' table, if matches return Hotel_Id
                 
            };
        }
    }
}
