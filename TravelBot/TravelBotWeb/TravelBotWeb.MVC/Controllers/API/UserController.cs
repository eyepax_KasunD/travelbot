﻿using Newtonsoft.Json;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using TravelBotWeb.Models;
using TravelBot.Common.Models;
using System.Messaging;
using TravelBotWeb.Services.Contracts;
using TravelBotWeb.Services.Models;
using TravelBotWeb.Services.Helpers;
using TravelBotWeb.Common;
using TravelBotWeb.Services;
using System.Collections.Generic;

namespace TravelBotWeb.Controllers
{
    public class UserController : BaseApiController
    {
        private readonly IUserService _userService;
        private readonly IUserServiceSQL _userServiceSql;
        private UserReturnedValuesModel model;
        public UserController(IUserService userService, IUserServiceSQL userServiceSql)
        {
            _userService = userService;
            model = new UserReturnedValuesModel();
            _userServiceSql = userServiceSql;
        }

        [HttpPost]
        public ApiResponseModel<UserReturnedValuesModel> UserRequest([FromBody]UserRequestModel requestModel)
        {
            logger.Info("User Request recieved");
            var ErrorMessages = Validator.ValidateUserRequest(requestModel);
            if (ErrorMessages.Any())
            {
                return GetApiResponseModel<UserReturnedValuesModel>(null, ErrorMessages);
            }
            else
            {
                var userSessionModel = _userService.StoreUserSession(requestModel);
                var userId = _userServiceSql.AddUser(userSessionModel);
                Task.Run(() => _userService.ScrappingCallerForSearchingObjects(userSessionModel, userSessionModel.SearchObjects.First().RequestId));
                model.Link = "/journey/hotellist?id=" + userSessionModel.UserId;
                model.DeviceId = userSessionModel.DeviceId;
                var res = GetApiResponseModel(model);
                return res;
            }
        }
        [HttpGet]
        public ApiResponseModel<HotelPriceSearchRsp> GetHotelDetailsFromId(string id)
        {
            logger.Info("Get Hotel Details From Id : " + id);
            var result = _userService.ScrappingCallerFromId(id);
            // var DeserializedResponse = JsonConvert.DeserializeObject<ApiResponseModel<HotelSearchResponse>>(result);
            return GetApiResponseModel(result.Data);
        }

        public ApiResponseModel<List<CityHotelData>> GetCityHotels(string reqId, int flex)
        {
            var userSession = _userService.GetUserSession(reqId);
            var searchObj = _userService.GetSearchObjectAndRequestPriceIfNeeded(userSession);
            var cityHotels = _userService.GetCityHotelData(userSession, searchObj, flex);
            
            return GetApiResponseModel(cityHotels);
        }

    }
}


