﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Configuration;

namespace TravelBotWeb.Common
{
    public interface IAppSettings
    {
        String CDN_Domain { get; }
        String APP_Domain { get; }
        String Scraper_Domain { get; }
        List<String> Admin_Emails { get; }
        String Affiliate_ID { get; }
        String Service_Domain { get; }
    }
}