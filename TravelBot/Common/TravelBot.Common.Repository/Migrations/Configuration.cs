namespace TravelBot.Common.Repository.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Model;
    using System.Data.Entity.SqlServer;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<TravelBot.Common.Repository.TravelbotDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ////IF THIS FALSE WHEN YOU NEED TO DO MIGRATE WITH OUT EFFECT TO EXIT DATA
            AutomaticMigrationDataLossAllowed = false;
            SetSqlGenerator("System.Data.SqlClient", new CustomSqlServerMigrationSqlGenerator());
        }

        protected override void Seed(TravelBot.Common.Repository.TravelbotDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
        internal class CustomSqlServerMigrationSqlGenerator : SqlServerMigrationSqlGenerator
        {
            protected override void Generate(AddColumnOperation addColumnOperation)
            {
                SetCreatedUtcColumn(addColumnOperation.Column);

                base.Generate(addColumnOperation);
            }

            protected override void Generate(CreateTableOperation createTableOperation)
            {
                SetCreatedUtcColumn(createTableOperation.Columns);

                base.Generate(createTableOperation);
            }

            private static void SetCreatedUtcColumn(IEnumerable<ColumnModel> columns)
            {
                foreach (var columnModel in columns)
                {
                    SetCreatedUtcColumn(columnModel);
                }
            }

            private static void SetCreatedUtcColumn(PropertyModel column)
            {
                if (column.Name == "CreateDateTime" || column.Name == "LastModifiedDateTime")
                {
                    column.DefaultValueSql = "GETUTCDATE()";
                }

                // All Bit Fileds should have defualt 0
                if (column.Type == System.Data.Entity.Core.Metadata.Edm.PrimitiveTypeKind.Boolean)
                {
                    column.DefaultValueSql = "(0)";
                }
            }
        }

    }

}
